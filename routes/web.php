<?php

use App\Http\Controllers\Auth\WelcomeController;
use App\Http\Controllers\Home\HomeController;
use App\Http\Controllers\Home\PanelHomeController;
use App\Http\Controllers\Home\StudentHomeController;
use App\Http\Controllers\Home\TutorialController;
use App\Http\Controllers\Panel\AnnouncementController;
use App\Http\Controllers\Panel\BadgeController;
use App\Http\Controllers\Panel\ChallengeController;
use App\Http\Controllers\Panel\GalleryController as PanelGalleryController;
use App\Http\Controllers\Panel\LiveLessonController;
use App\Http\Controllers\Panel\PenalizationController;
use App\Http\Controllers\Panel\QuestionController;
use App\Http\Controllers\Panel\QuestionOptionController;
use App\Http\Controllers\Panel\RewardController;
use App\Http\Controllers\Panel\RewardRedemptionStatusController;
use App\Http\Controllers\Panel\UserController;
use App\Http\Controllers\Portal\CommentController;
use App\Http\Controllers\Portal\GalleryController as PortalGalleryController;
use App\Http\Controllers\Portal\ProfileInformationController;
use App\Http\Controllers\Portal\RankingController;
use App\Http\Controllers\Portal\RewardRedemptionController;
use App\Http\Controllers\Portal\UserBadgesController;
use App\Http\Controllers\Portal\UserChallengeController;
use App\Http\Controllers\Portal\UserLiveLessonController;
use Illuminate\Support\Facades\Route;
use Spatie\WelcomeNotification\WelcomesNewUsers;
use App\Http\Controllers\Portal\NotificationEnrouteController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['web', WelcomesNewUsers::class]], function () {
    Route::get('welcome/{user}', [WelcomeController::class, 'showWelcomeForm'])->name('welcome');
    Route::post('welcome/{user}', [WelcomeController::class, 'savePassword']);
});

Route::middleware(['auth'])->group(function () {


    Route::get('', HomeController::class)->name('index');
    Route::view('/notification', 'modules.landing.notification.index');

    Route::get('challenges/{userChallenge}/{userChallengeAttempt}/score', [UserChallengeController::class, 'finishChallenge']);

    //RUTAS NOTIFICACIONES
    Route::get('/notification/read/{id}', [NotificationEnrouteController::class, 'read_notification'])->name('read_notification');

    Route::prefix('panel')->name('panel.')->middleware('can:is-administrator')->group(function () {
        Route::get('home_panel', PanelHomeController::class)->name('index');

        Route::middleware('verified')->group(function () {
            Route::controller(LiveLessonController::class)->prefix('live-lessons')->name('live-lessons.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::middleware('request.only.ajax')->group(function () {
                    Route::get('list', 'list')->name('list');
                    Route::post('', 'store')->name('store');
                    Route::get('{liveLesson}', 'show')->name('show');
                    Route::put('{liveLesson}', 'update')->name('update');
                    Route::delete('{liveLesson}', 'destroy')->name('destroy');
                });
            });

            Route::controller(ChallengeController::class)->prefix('challenges')->name('challenges.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::get('{challenge}', 'show')->name('show');

                Route::middleware('request.only.ajax')->group(function () {
                    Route::post('', 'store')->name('store');
                    Route::put('{challenge}', 'update')->name('update');
                    Route::delete('{challenge}', 'destroy')->name('destroy');
                    Route::put('{challenge}/publish', 'publish')->name('publish');
                    Route::put('{challenge}/move-to-draft', 'moveToDraft')->name('move_to_draft');

                    Route::controller(QuestionController::class)->prefix('{challenge}/questions')->name('questions.')->scopeBindings()->group(function () {
                        Route::get('', 'index')->name('index');
                        Route::post('', 'store')->name('store');
                        Route::get('{question}', 'show')->name('show');
                        Route::put('{question}', 'update')->name('update');
                        Route::delete('{question}', 'destroy')->name('destroy');

                        Route::controller(QuestionOptionController::class)->prefix('{question}/options')->name('options.')->scopeBindings()->group(function () {
                            Route::get('', 'index')->name('index');
                            Route::post('', 'store')->name('store');
                            Route::get('{option}', 'show')->name('show');
                            Route::put('{option}', 'update')->name('update');
                            Route::delete('{option}', 'destroy')->name('destroy');
                        });
                    });
                });
            });

            //RUTAS USER
            Route::controller(UserController::class)->prefix('users')->name('users.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::middleware('request.only.ajax')->group(function () {
                    Route::get('list', [UserController::class, 'list'])->name('list');
                    Route::post('', 'store')->name('store');
                    Route::get('{user}', 'show')->name('show');
                    Route::put('{user}', 'update')->name('update');
                });
            });

            //RUTAS GIFT
            Route::controller(RewardController::class)->prefix('rewards')->name('rewards.')->group(function () {
                Route::get('', 'index')->name('all');
                Route::post('', 'store')->name('store');

                Route::controller(RewardRedemptionStatusController::class)->prefix('redemption-status')->name('redemption-status.')->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('list', 'list')->name('list');
                    Route::get('{rewardRedemption}', 'show')->name('show');
                    Route::patch('{rewardRedemption}', 'update')->name('patch');
                });
                Route::view('create', 'panel.rewards.create')->name('create');
                Route::get('{reward}/edit', 'edit')->name('edit');
                Route::put('{reward}', 'update')->name('update');
                Route::delete('{reward}', 'destroy')->name('destroy');
            });

            Route::controller(PanelGalleryController::class)->prefix('gallery')->name('gallery.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::middleware('request.only.ajax')->group(function () {
                    Route::post('', 'store')->name('store');
                    Route::get('list', 'list')->name('list');
                    Route::get('{gallery}', 'show')->name('show');
                    Route::put('{gallery}', 'update')->name('update');
                });
            });

            Route::controller(PenalizationController::class)->prefix('penalizations')->name('penalizations.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::post('{user}', 'store')->name('store')->middleware('request.only.ajax');
            });

            Route::controller(AnnouncementController::class)->prefix('announcements')->name('announcements.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::middleware('request.only.ajax')->group(function () {
                    Route::post('', 'store')->name('store');
                    Route::delete('', 'destroy')->name('destroy');
                });
            });

            Route::controller(BadgeController::class)->prefix('badges')->name('badges.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::middleware('request.only.ajax')->group(function () {
                    Route::get('list', 'list')->name('list');
                    Route::post('', 'store')->name('store');
                    Route::get('{badge}', 'show')->name('show');
                    Route::put('{badge}', 'update')->name('update');
                });
            });

            //RUTAS OTHER
            Route::view('other/tutorial', 'panel.other.tutorial')->name('other.tutorial');

            //RUTAS PROFILE
            Route::view('profile', 'panel.profile.index')->name('profile.index');

            //RUTAS CONFIG
            Route::view('config', 'panel.config.index')->name('config.index');
        });
    });

    Route::middleware('can:is-student')->group(function () {
        Route::prefix('tutorial')->name('tutorial.')->controller(TutorialController::class)->group(function () {
            Route::get('', 'index')->name('index');
            Route::post('', 'finishTutorial')->name('finish');
        });

        Route::middleware('tutorial')->group(function () {
            Route::get('home', StudentHomeController::class)->name('portal.index');

            //RUTAS PROFILE
            Route::controller(ProfileInformationController::class)->prefix('profile')->name('profile.')->group(function () {
                Route::get('', 'index')->name('index');
                Route::post('avatar', 'updateProfileImage')->name('image.update');
            });

            Route::middleware('verified')->group(function () {
                //RUTAS LIVE
                Route::controller(UserLiveLessonController::class)->prefix('live-lessons')->name('live-lessons.')->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('live', 'liveLesson')->name('live');
                    Route::get('saved-lessons', 'savedLessons')->name('saved_lessons');

                    Route::middleware('request.only.ajax')->group(function () {
                        Route::post('calendar', 'calendar')->name('calendar');
                        Route::get('load-lesson/{liveLesson}', 'loadLesson')->name('load_lesson');
                        Route::patch('{userLiveLesson}/tracking', 'trackLiveLesson')->name('track_live_lesson');
                    });
                });

                //RUTAS DESAFIOS
                Route::controller(UserChallengeController::class)->prefix('challenges')->name('challenges.')->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('my-challenges', 'viewChallengesWithDiploma')->name('my_challenges');
                    Route::post('my-challenges/{userChallenge}/download', 'downloadDiploma')->name('download_diploma')->middleware('request.only.ajax');
                    Route::get('{userChallenge}', 'challengeIndex')->name('challenge_index');

                    Route::middleware('request.only.ajax')->group(function () {
                        Route::get('{userChallenge}/status', 'getUserChallengeData')->name('check');
                        Route::put('{userChallenge}/{userChallengeAttempt}/score', 'finishChallenge')->name('score_challenge');
                        Route::post('{userChallenge}/redeem-attempt', 'redeemAttempt')->name('redeem_attempt');
                    });
                });

                //RUTAS GALERÍA
                Route::prefix('gallery')->name('gallery.')->group(function () {
                    Route::get('', [PortalGalleryController::class, 'index'])->name('index');
                    Route::get('video/{video}', [PortalGalleryController::class, 'viewVideo'])->name('view_video');
                    Route::get('video/{video}/download', [PortalGalleryController::class, 'downloadVideo'])->name('download_video');

                    Route::controller(CommentController::class)->middleware('request.only.ajax')->prefix('video/{video}/comments')->name('comments.')->group(function () {
                        Route::get('', 'index')->name('index');
                        Route::post('', 'store')->name('store');
                    });
                });

                //RUTAS RANKING
                Route::get('ranking', RankingController::class)->name('ranking.index');

                //Redención de puntos
                Route::controller(RewardRedemptionController::class)->prefix('rewards')->name('rewards.')->group(function () {
                    Route::get('', 'index')->name('index');
                    Route::get('list', 'list')->name('list')->middleware('request.only.ajax');
                    Route::get('{reward}/check-redemption-conditions', 'checkRedemptionConditions')->name('check_redemption_conditions')->middleware('request.only.ajax');
                    Route::get('{reward}/redeem', 'viewReedemptionForm')->name('redeem');
                    Route::post('{reward}/redeem', 'redeem')->middleware(['request.only.ajax', 'throttle:redemptions']);
                });

                Route::get('badges', UserBadgesController::class)->name('badges');
                Route::get('badges/my_medal', [UserBadgesController::class, 'my_medal'])->name('badges.my_medal');
            });
        });
    });
});
