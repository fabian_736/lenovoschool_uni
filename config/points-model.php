<?php

return [
    // Points to award config
    'award' => [
        // Points for registration
        'completing_registration' => env('COMPLETING_REGISTRATION_AWARD', 10),

        // Points for attend a live lesson on time
        'attend_a_live_lesson_on_time' => env('ATTEND_A_LIVE_LESSON_ON_TIME_AWARD', 1),
    ],

    // Redemptions config
    'redemptions' => [
        // Required points for redeem a challenge attempt
        'challenge_attempt' => env('CHALLENGE_ATTEMPT_REDEMPTIONS', 1),
    ],

    // Live lessons config
    'live_lessons' => [
        'required_viewed_time' => env('REQUIRED_VIEWED_TIME_IN_SECONDS_FOR_LIVE_LESSONS', 1),
    ],
];
