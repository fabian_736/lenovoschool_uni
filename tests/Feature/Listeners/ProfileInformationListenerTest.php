<?php

namespace Tests\Feature\Listeners;

use App\Events\ProfileInformation;
use App\Listeners\ProfileInformationListener;
use App\Models\User;
use App\Services\PointTransactionService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class ProfileInformationListenerTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle_no_give_points()
    {
        config()->set('points-model.award.completing_registration', 10);

        $user = User::factory()->withoutPoints()->create();
        $event = new ProfileInformation($user);
        $listener = new ProfileInformationListener(new PointTransactionService());
        $listener->handle($event);

        $user->refresh();

        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);
    }

    public function test_handle_give_points()
    {
        config()->set('points-model.award.completing_registration', 10);

        $user = User::factory()->withoutPoints()->create();

        $user->addMedia(UploadedFile::fake()->image('avatar.png'))
            ->toMediaCollection('avatar');

        $event = new ProfileInformation($user);
        $listener = new ProfileInformationListener(new PointTransactionService());
        $listener->handle($event);

        $user->refresh();

        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(ProfileInformation::class, ProfileInformationListener::class);
    }
}
