<?php

namespace Tests\Feature\Listeners;

use App\Events\UpdatedProgress;
use App\Listeners\UpdatedProgressListener;
use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use App\Models\UserLiveLesson;
use App\Models\UserLiveLessonTrackingHistory;
use App\Services\UserService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class UpdatedProgressListenerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_updates_the_user_total_progress()
    {
        $viewedTime = 10;
        config()->set('points-model.live_lessons.required_viewed_time', $viewedTime);
        config()->set('points-model.award.attend_a_live_lesson_on_time', 10);

        Carbon::setTestNow('2022-01-01 00:00:00');
        Event::fake();

        $user = User::factory()->create();

        // Creating the live lesson as viewed
        $liveLesson = LiveLesson::factory()->set('scheduled_date', '2022-01-01')->create();
        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->watched()
            ->create();

        UserLiveLessonTrackingHistory::factory()
            ->for($userLiveLesson)
            ->set('watched_time', $viewedTime)
            ->set('created_at', '2022-01-01 00:00:00')
            ->create();

        // Creating the challenge as finished
        $challenge = Challenge::factory()->for($liveLesson)->published()->create();
        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->for($challenge)
            ->finished()
            ->set('max_earned_points', 10)
            ->create();

        UserChallengeAttempt::factory()->for($userChallenge)->finished()->create();

        // Testing the event
        $event = new UpdatedProgress($user);
        $listener = new UpdatedProgressListener(new UserService());
        $listener->handle($event);

        $user->refresh();

        $this->assertEquals(100, $user->total_progress);
        $this->assertEquals(10, $user->challenge_ranking_points);
        $this->assertEquals(10, $user->live_lesson_ranking_points);
        $this->assertEquals(20, $user->general_ranking_points);
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(UpdatedProgress::class, UpdatedProgressListener::class);
    }
}
