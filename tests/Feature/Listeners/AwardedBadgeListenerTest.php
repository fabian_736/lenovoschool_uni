<?php

namespace Tests\Feature\Listeners;

use App\Events\AwardedBadge;
use App\Listeners\AwardedBadgeListener;
use App\Models\Badge;
use App\Models\User;
use App\Notifications\Badges\AwardedBadgeNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class AwardedBadgeListenerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_event_sends_the_notification()
    {
        $badge = Badge::factory()->create();

        $user = User::factory()
            ->hasAttached(factory: $badge, relationship: 'badges')
            ->create();

        $badges = Badge::all();

        $event = new AwardedBadge($user, $badges);
        $listener = new AwardedBadgeListener();

        Notification::fake();

        $listener->handle($event);
        Notification::assertSentTo(
            $user,
            function (AwardedBadgeNotification $notification, $channels) use ($badge) {
                $this->assertEqualsCanonicalizing(['mail', 'database'], $channels);
                $this->assertEquals($notification->badge->id, $badge->id);

                return true;
            }
        );
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(AwardedBadge::class, AwardedBadgeListener::class);
    }
}
