<?php

namespace Tests\Feature\Listeners;

use App\Events\SyncBadges;
use App\Listeners\LogSuccessfulLogin;
use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class LogSuccessfulLoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle()
    {
        Event::fake();

        $user = User::factory()->withoutPoints()->create();
        $event = new Login('web', $user, false);
        $listener = new LogSuccessfulLogin();
        $listener->handle($event);

        $user->refresh();

        $this->assertEquals(1, $user->authenticationLogs->count());

        Event::assertDispatched(SyncBadges::class);
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(Login::class, LogSuccessfulLogin::class);
    }
}
