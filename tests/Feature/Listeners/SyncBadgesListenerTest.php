<?php

namespace Tests\Feature\Listeners;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Events\AwardedBadge;
use App\Events\SyncBadges;
use App\Listeners\SyncBadgesListener;
use App\Models\AuthenticationLog;
use App\Models\Badge;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\RewardRedemption;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserLiveLesson;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class SyncBadgesListenerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_receive_an_acumulated_points_badge()
    {
        $user = User::factory()->create();
        $badge = Badge::factory()->set('type', BadgeTypes::TOTAL_ACCUMULATED_POINTS)->create();

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_an_accumulated_points_badge_per_badge_type()
    {
        $user = User::factory()->create();
        $badgeType = BadgeTypes::TOTAL_ACCUMULATED_POINTS;
        $badge = Badge::factory()->set('type', $badgeType)->create();

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_an_acumulated_points_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->set('type', BadgeTypes::TOTAL_ACCUMULATED_POINTS)->set('total', 1000)->create();

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_an_accumulated_points_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_ACCUMULATED_POINTS;
        $badge = Badge::factory()->set('type', $badgeType)->set('total', 1000)->create();

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_finished_challenges_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_FINISHED_CHALLENGES,
            'total' => 1,
        ]);
        $challenge = UserChallenge::factory()->finished()->create();
        $user = $challenge->user;

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_finished_challenges_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_FINISHED_CHALLENGES;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $challenge = UserChallenge::factory()->finished()->create();
        $user = $challenge->user;

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_finished_challenges_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_FINISHED_CHALLENGES,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            UserChallenge::factory()->for($user)->finished()->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_finished_challenges_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_FINISHED_CHALLENGES;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            UserChallenge::factory()->for($user)->finished()->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_gallery_comments_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_GALLERY_COMMENTS,
            'total' => 1,
        ]);
        $comment = Comment::factory()->create();
        $user = $comment->author;

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_gallery_comments_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_GALLERY_COMMENTS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $comment = Comment::factory()->create();
        $user = $comment->author;

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_gallery_comments_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_GALLERY_COMMENTS,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            Comment::factory()->for($user, 'author')->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_gallery_comments_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_GALLERY_COMMENTS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            Comment::factory()->for($user, 'author')->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_gallery_downloads_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_GALLERY_DOWNLOADS,
            'total' => 1,
        ]);

        $user = User::factory()->create();
        $video = Gallery::factory()->create();

        $video->pointTransaction()->create([
            'type' => PointTransactionTypes::earnedPoints,
            'user_id' => $user->id,
            'points' => 1,
        ]);

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_gallery_downloads_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_GALLERY_DOWNLOADS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $user = User::factory()->create();
        $video = Gallery::factory()->create();

        $video->pointTransaction()->create([
            'type' => PointTransactionTypes::earnedPoints,
            'user_id' => $user->id,
            'points' => 1,
        ]);

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_gallery_downloads_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_GALLERY_DOWNLOADS,
            'total' => 1,
        ]);

        $video = Gallery::factory()->create();

        $awardedUsers->each(function (User $user) use ($video) {
            $video->pointTransaction()->create([
                'type' => PointTransactionTypes::earnedPoints,
                'user_id' => $user->id,
                'points' => 1,
            ]);
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_gallery_downloads_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_GALLERY_DOWNLOADS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $video = Gallery::factory()->create();

        $awardedUsers->each(function (User $user) use ($video) {
            $video->pointTransaction()->create([
                'type' => PointTransactionTypes::earnedPoints,
                'user_id' => $user->id,
                'points' => 1,
            ]);
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_viewed_lessons_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS,
            'total' => 1,
        ]);
        $liveLesson = UserLiveLesson::factory()->watched()->create();
        $user = $liveLesson->user;

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_viewed_lessons_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $liveLesson = UserLiveLesson::factory()->watched()->create();
        $user = $liveLesson->user;

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_viewed_lessons_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            UserLiveLesson::factory()->for($user)->watched()->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_viewed_lessons_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            UserLiveLesson::factory()->for($user)->watched()->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_total_redeemed_points_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_REDEEMED_POINTS,
            'total' => 1,
        ]);
        $rewardRedemption = RewardRedemption::factory()->create();
        $user = $rewardRedemption->user;

        $rewardRedemption->pointTransaction()->create([
            'type' => PointTransactionTypes::redemption,
            'user_id' => $user->id,
            'points' => 1,
        ]);

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_total_redeemed_points_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_REDEEMED_POINTS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $rewardRedemption = RewardRedemption::factory()->create();
        $user = $rewardRedemption->user;

        $rewardRedemption->pointTransaction()->create([
            'type' => PointTransactionTypes::redemption,
            'user_id' => $user->id,
            'points' => 1,
        ]);

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_total_redeemed_points_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_REDEEMED_POINTS,
            'total' => 1,
        ]);

        $rewardRedemption = RewardRedemption::factory()->create();

        $awardedUsers->each(function (User $user) use ($rewardRedemption) {
            $rewardRedemption->pointTransaction()->create([
                'type' => PointTransactionTypes::redemption,
                'user_id' => $user->id,
                'points' => 1,
            ]);
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_total_redeemed_points_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_REDEEMED_POINTS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $rewardRedemption = RewardRedemption::factory()->create();

        $awardedUsers->each(function (User $user) use ($rewardRedemption) {
            $rewardRedemption->pointTransaction()->create([
                'type' => PointTransactionTypes::redemption,
                'user_id' => $user->id,
                'points' => 1,
            ]);
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_total_reward_redemptions_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_REWARD_REDEMPTIONS,
            'total' => 1,
        ]);
        $rewardRedemption = RewardRedemption::factory()->create();
        $user = $rewardRedemption->user;

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_total_reward_redemptions_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_REWARD_REDEMPTIONS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $rewardRedemption = RewardRedemption::factory()->create();
        $user = $rewardRedemption->user;

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_total_reward_redemptions_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_REWARD_REDEMPTIONS,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            RewardRedemption::factory()->for($user)->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_total_reward_redemptions_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_REWARD_REDEMPTIONS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);

        $awardedUsers->each(function (User $user) {
            RewardRedemption::factory()->for($user)->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_can_receive_a_total_logins_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_LOGINS,
            'total' => 1,
        ]);
        $user = User::factory()->create();

        $user->authenticationLogs()->create(['ip' => '127.0.0.1']);

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_total_logins_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_LOGINS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $user = User::factory()->create();

        $user->authenticationLogs()->create(['ip' => '127.0.0.1']);

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_total_logins_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_LOGINS,
            'total' => 2,
        ]);

        $awardedUsers->each(function (User $user) {
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-01 00:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-01 01:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-02 00:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-02 01:00:00')->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_total_logins_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_LOGINS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 2,
        ]);

        $awardedUsers->each(function (User $user) {
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-01 00:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-01 01:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-02 00:00:00')->create();
            AuthenticationLog::factory()->for($user)->set('created_at', '2022-01-02 01:00:00')->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_user_cannot_receive_a_badge_twice()
    {
        $badge = Badge::factory()->create();
        $user = User::factory()->hasAttached(factory: $badge, relationship: 'badges')->create();

        $event = new SyncBadges(badge: $badge, user: $user);
        $listener = new SyncBadgesListener($event);

        Event::fake();

        $listener->handle($event);

        Event::assertNotDispatched(AwardedBadge::class);
    }

    public function test_user_can_receive_a_total_commented_galery_videos_badge()
    {
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS,
            'total' => 1,
        ]);
        $user = User::factory()->create();
        $video = Gallery::factory()->create();
        Comment::factory()->count(3)->for($user, 'author')->for($video, 'gallery')->create();

        $this->assertSingleUserBadgeAssignmentWith($badge, $user);
    }

    public function test_user_can_receive_a_total_commented_gallery_videos_badge_per_badge_type()
    {
        $badgeType = BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $user = User::factory()->create();
        $video = Gallery::factory()->create();
        Comment::factory()->count(3)->for($user, 'author')->for($video, 'gallery')->create();

        $this->assertSingleUserBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $user);
    }

    public function test_users_can_receive_a_total_total_commented_gallery_videos_badge()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badge = Badge::factory()->create([
            'type' => BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS,
            'total' => 1,
        ]);
        $video = Gallery::factory()->create();

        $awardedUsers->each(function (User $user) use ($video) {
            Comment::factory()->count(3)->for($user, 'author')->for($video, 'gallery')->create();
        });

        $this->assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers);
    }

    public function test_users_can_receive_a_total_commented_gallery_videos_badge_per_badge_type()
    {
        $awardedUsers = User::factory()->count(10)->create();
        $nonAwardedUsers = User::factory()->count(10)->set('total_redemption_points', 0)->create();
        $badgeType = BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS;
        $badge = Badge::factory()->create([
            'type' => $badgeType,
            'total' => 1,
        ]);
        $video = Gallery::factory()->create();

        $awardedUsers->each(function (User $user) use ($video) {
            Comment::factory()->count(3)->for($user, 'author')->for($video, 'gallery')->create();
        });

        $this->assertMultipleBadgeAssignmentByBadgeTypeWith($badge, $badgeType, $awardedUsers, $nonAwardedUsers);
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(SyncBadges::class, SyncBadgesListener::class);
    }

    /**
     * Test that the given user has attached the given badge.
     *
     * @param  Badge  $badge
     * @param  User  $user
     * @return void
     */
    private function assertSingleUserBadgeAssignmentWith(Badge $badge, User $user)
    {
        $event = new SyncBadges(badge: $badge, user: $user);
        $listener = new SyncBadgesListener($event);

        Event::fake();

        $listener->handle($event);

        $user->refresh();

        $this->assertTrue($user->badges->contains($badge));

        Event::assertDispatched(AwardedBadge::class);
    }

    /**
     * Test that the given user has attached the given badge by badge type.
     *
     * @param  Badge  $badge
     * @param  BadgeTypes  $badgeType
     * @param  User  $user
     * @return void
     */
    private function assertSingleUserBadgeAssignmentByBadgeTypeWith(Badge $badge, BadgeTypes $badgeType, User $user)
    {
        $event = new SyncBadges(badgeTypes: [$badgeType], user: $user);
        $listener = new SyncBadgesListener($event);

        Event::fake();

        $listener->handle($event);

        $user->refresh();

        $this->assertTrue($user->badges->contains($badge));

        Event::assertDispatched(AwardedBadge::class);
    }

    /**
     * Test that the users that will receive the badge has attached the given badge and
     * the users that will not receive the badge does not have attached the given badge.
     *
     * @param  Badge  $badge
     * @param  \Illuminate\Database\Eloquent\Collection  $awardedUsers
     * @param  \Illuminate\Database\Eloquent\Collection  $nonAwardedUsers
     * @return void
     */
    private function assertMultipleUserBadgeAssignmentWith($badge, $awardedUsers, $nonAwardedUsers)
    {
        $event = new SyncBadges(badge: $badge);
        $listener = new SyncBadgesListener($event);

        Event::fake();

        $listener->handle($event);

        $awardedUsers->each(function (User $user) use ($badge) {
            $this->assertTrue($user->badges->contains($badge));
        });

        $nonAwardedUsers->each(function (User $user) use ($badge) {
            $this->assertFalse($user->badges->contains($badge));
        });

        Event::assertDispatchedTimes(AwardedBadge::class, $awardedUsers->count());
    }

    /**
     * Test that the users that will receive the badge has attached the given badge and
     * the users that will not receive the badge does not have attached the given badge.
     *
     * @param  Badge  $badge
     * @param  BadgeTypes  $badgeType
     * @param  \Illuminate\Database\Eloquent\Collection  $awardedUsers
     * @param  \Illuminate\Database\Eloquent\Collection  $nonAwardedUsers
     * @return void
     */
    private function assertMultipleBadgeAssignmentByBadgeTypeWith(Badge $badge, BadgeTypes $badgeType, $awardedUsers, $nonAwardedUsers)
    {
        $event = new SyncBadges(badgeTypes: [$badgeType]);
        $listener = new SyncBadgesListener($event);

        Event::fake();

        $listener->handle($event);

        $awardedUsers->each(function (User $user) use ($badge) {
            $this->assertTrue($user->badges->contains($badge));
        });

        $nonAwardedUsers->each(function (User $user) use ($badge) {
            $this->assertFalse($user->badges->contains($badge));
        });

        Event::assertDispatchedTimes(AwardedBadge::class, $awardedUsers->count());
    }
}
