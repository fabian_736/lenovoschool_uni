<?php

namespace Tests\Feature\Listeners;

use App\Listeners\RegisteredListener;
use App\Models\User;
use App\Notifications\Auth\WelcomeNotification;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class RegisteredListenerTest extends TestCase
{
    use RefreshDatabase;

    public function test_handle()
    {
        Notification::fake();

        $user = User::factory()->create();
        $event = new Registered($user);
        $listener = new RegisteredListener();
        $listener->handle($event);

        Notification::assertSentTo($user, WelcomeNotification::class);
    }

    public function test_is_attached_to_event()
    {
        Event::fake();
        Event::assertListening(Registered::class, RegisteredListener::class);
    }
}
