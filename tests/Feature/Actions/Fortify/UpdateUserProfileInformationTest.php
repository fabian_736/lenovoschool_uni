<?php

namespace Tests\Feature\Actions\Fortify;

use App\Models\DocumentType;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UpdateUserProfileInformationTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_view_the_user_profile_information_view()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('profile.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.profile.index');
    }

    public function test_el_usuario_puede_actualizar_su_perfil()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        /** @var DocumentType */
        $documentType = DocumentType::factory()->create();

        $data = [
            'name' => 'Test',
            'lastname' => 'Test',
            'document_type_id' => $documentType->id,
            'document_number' => '12345678',
            'cellphone_number' => '300000000',
            'email' => 'test@gmail.com',
            'position' => 'Test',
            'company' => 'Test',
        ];

        $this
            ->actingAs($user)
            ->putJson(route('user-profile-information.update'), $data, self::$HEADERS)
            ->assertOk();

        $this->assertSame($data['name'], $user->name);
        $this->assertSame($data['lastname'], $user->lastname);
        $this->assertSame($data['document_type_id'], $user->document_type_id);
        $this->assertSame($data['document_number'], $user->document_number);
        $this->assertSame($data['cellphone_number'], $user->cellphone_number);
        $this->assertSame($data['email'], $user->email);
        $this->assertSame($data['position'], $user->position);
        $this->assertSame($data['company'], $user->company);
    }
}
