<?php

namespace Tests\Feature\Auth;

use App\Models\DocumentType;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_user_can_view_the_register_form()
    {
        $documentTypes = DocumentType::factory()->count(2)->create();

        $this
            ->get('register')
            ->assertOk()
            ->assertViewIs('modules.auth.register')
            ->assertViewHas('documentTypes', $documentTypes);
    }

    public function test_the_user_can_register()
    {
        Storage::fake('public');
        Event::fake();

        /** @var DocumentType */
        $documentType = DocumentType::factory()->create();

        $data = [
            'name' => 'test',
            'lastname' => 'test',
            'document_type_id' => $documentType->id,
            'document_number' => '12345678',
            'cellphone_number' => '123456789',
            'email' => 'test@test.com',
            'position' => 'test',
            'company' => 'test',
        ];
        $file = UploadedFile::fake()->image('avatar.jpg', 300, 300);
        $postData = array_merge($data, ['file' => $file]);

        $this->followingRedirects()
            ->post('register', $postData)
            ->assertOk()
            ->assertSessionDoesntHaveErrors()
            ->assertViewIs('modules.auth.login')
            ->assertSee('¡Registro exitoso! Se le ha enviado un correo electrónico para establecer su contraseña.');

        $this->assertDatabaseHas('users', $data);

        $user = User::whereEmail('test@test.com')->first();
        $this->assertTrue($user->active);

        $avatar = $user->getFirstMedia('avatar');
        $this->assertFileExists($avatar->first()->getPath());

        Event::assertDispatched(Registered::class);
    }

    public function test_the_user_can_view_the_welcome_form()
    {
        $user = User::factory()->create([
            'welcome_valid_until' => now()->addDay(),
        ]);

        $url = URL::temporarySignedRoute(
            'welcome',
            $user->validUntil,
            ['user' => $user->welcomeNotificationKeyValue()]
        );

        $this
            ->get($url)
            ->assertOk()
            ->assertViewIs('welcomeNotification::welcome');
    }

    public function test_the_user_can_set_their_password()
    {
        $user = User::factory()->create([
            'welcome_valid_until' => now()->addDay(),
        ]);

        $url = URL::temporarySignedRoute(
            'welcome',
            $user->validUntil,
            ['user' => $user->welcomeNotificationKeyValue()]
        );

        $newPassword = 'Una contraseña súper fuerte!123';

        $data = [
            'password' => $newPassword,
            'password_confirmation' => $newPassword,
        ];

        $this->post($url, $data)
            ->assertRedirect('/');

        $user->refresh();

        $this->assertTrue(Hash::check($newPassword, $user->password));
        $this->assertNull($user->welcome_valid_until);
        $this->assertNotNull($user->email_verified_at);
    }
}
