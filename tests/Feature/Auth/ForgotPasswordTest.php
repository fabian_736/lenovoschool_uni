<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use App\Notifications\Auth\ResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Tests\TestCase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    public function test_muestra_el_formulario_de_solicitud_de_restablecimiento_de_contrasena()
    {
        $this
            ->get(route('password.request'))
            ->assertOk()
            ->assertViewIs('modules.auth.forgot');
    }

    public function test_el_usuario_solicita_el_restablecimiento_de_contrasena()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->expectsNotification($user, ResetPasswordNotification::class);

        $this
            ->post(
                route('password.email'),
                ['email' => $user->email]
            )
            ->assertRedirect()
            ->assertSessionHas(
                key: 'status',
                value: '¡Le hemos enviado por correo electrónico el enlace para restablecer su contraseña!'
            );

        $token = DB::table('password_resets')->first();
        $this->assertNotNull($token);
    }

    public function test_el_sistema_no_envia_el_correo_electronico_de_restablecimiento_de_contrasena()
    {
        $this->doesntExpectJobs(ResetPasswordNotification::class);

        $this->post(
            route('password.email'),
            ['email' => 'invalid@email.com']
        )
            ->assertRedirect()
            ->assertInvalid([
                'email' => 'No encontramos ningún usuario con ese correo electrónico.',
            ]);
    }

    public function test_el_usuario_puede_ver_el_formulario_de_restablecimiento_de_contrasena()
    {
        /** @var User */
        $user = User::factory()->create();
        $token = Password::createToken($user);

        $this
            ->get(route('password.reset', ['token' => $token]))
            ->assertOk()
            ->assertViewIs('modules.auth.reset');
    }

    public function test_el_usuario_restablece_su_contrasena()
    {
        /** @var User */
        $user = User::factory()->create();
        $token = Password::createToken($user);
        $newPassword = 'Una contraseña súper fuerte!123';

        $this
            ->postJson(
                route('password.update'),
                [
                    'token' => $token,
                    'email' => $user->email,
                    'password' => $newPassword,
                    'password_confirmation' => $newPassword,
                ]
            )
            ->assertOk();

        $user->refresh();

        $this->assertTrue(Hash::check($newPassword, $user->password));
    }
}
