<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class UpdatePasswordTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_update_password_form()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('profile.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.profile.index');
    }

    public function test_user_can_update_their_password()
    {
        /** @var User */
        $user = User::factory()
            ->set('password', Hash::make($oldPassword = 'old-password'))
            ->create();

        $data = [
            'current_password' => $oldPassword,
            'password' => $newPassword = 'New Password!123',
            'password_confirmation' => $newPassword,
        ];

        $this
            ->actingAs($user)
            ->putJson(route('user-password.update'), $data)
            ->assertOk();

        $user->refresh();

        $this->assertTrue(Hash::check($newPassword, $user->password));
    }

    public function test_user_cannot_update_their_password_if_current_password_is_invalid()
    {
        /** @var User */
        $user = User::factory()
            ->set('password', Hash::make($oldPassword = 'old-password'))
            ->create();

        $data = [
            'current_password' => 'invalid',
            'password' => $newPassword = 'New Password!123',
            'password_confirmation' => $newPassword,
        ];

        $this
            ->actingAs($user)
            ->putJson(route('user-password.update'), $data)
            ->assertUnprocessable()
            ->assertInvalid('current_password');

        $user->refresh();

        $this->assertTrue(Hash::check($oldPassword, $user->password));
    }

    public function test_user_cannot_update_their_password_if_new_password_is_invalid()
    {
        /** @var User */
        $user = User::factory()
            ->set('password', Hash::make($oldPassword = 'old-password'))
            ->create();

        $data = [
            'current_password' => $oldPassword,
            'password' => $newPassword = 'invalid',
            'password_confirmation' => $newPassword,
        ];

        $this
            ->actingAs($user)
            ->putJson(route('user-password.update'), $data)
            ->assertUnprocessable()
            ->assertInvalid('password');

        $user->refresh();

        $this->assertTrue(Hash::check($oldPassword, $user->password));
    }

    public function test_user_cannot_update_their_password_if_new_password_is_not_confirmed()
    {
        /** @var User */
        $user = User::factory()
            ->set('password', Hash::make($oldPassword = 'old-password'))
            ->create();

        $data = [
            'current_password' => $oldPassword,
            'password' => 'New Password!123',
            'password_confirmation' => 'invalid',
        ];

        $this
            ->actingAs($user)
            ->putJson(route('user-password.update'), $data)
            ->assertUnprocessable()
            ->assertInvalid('password');

        $user->refresh();

        $this->assertTrue(Hash::check($oldPassword, $user->password));
    }
}
