<?php

namespace Tests\Feature\Auth;

use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class LoginTest extends TestCase
{
    use RefreshDatabase;

    public function test_el_usuario_se_puede_ver_el_formulario_de_login()
    {
        $this
            ->get('login')
            ->assertOk()
            ->assertViewIs('modules.auth.login');
    }

    public function test_usuario_no_puede_ver_el_formulario_login_cuando_este_autenticado()
    {
        /** @var User */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('login'))
            ->assertRedirect('/');
    }

    public function test_usuario_puede_loguearse_con_credenciales_correctas()
    {
        $user = User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make($password = 'contraseña'),
        ]);

        $data = [
            'email' => 'test@test.com',
            'password' => $password,
        ];

        Event::fake();

        $this->post(route('login'), $data)
            ->assertRedirect('/')
            ->assertStatus(Response::HTTP_FOUND);

        $this->assertAuthenticatedAs($user);

        Event::assertDispatched(Login::class);
    }

    public function test_usuario_no_puede_loguearse_con_credenciales_incorrectas()
    {
        User::factory()->create([
            'email' => 'test@test.com',
            'password' => Hash::make('contraseña'),
        ]);

        $data = [
            'email' => 'test@test.com',
            'password' => 'contraseña inválida',
        ];

        $this->from(route('login'))
            ->post(route('login'), $data)
            ->assertInvalid(['email']);

        $this->assertGuest();
    }

    public function test_user_cannot_login_if_is_inactive()
    {
        $user = User::factory()->inactive()->create([
            'email' => $email = 'test@test.com',
            'password' => Hash::make($password = 'contraseña'),
        ]);

        $data = [
            'email' => $email,
            'password' => $password,
        ];

        $this->from(route('login'))
            ->post(route('login'), $data)
            ->assertInvalid(['email']);

        $this->assertGuest();
    }

    public function test_el_usuario_puede_cerrar_sesion()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->post(route('logout'))
            ->assertRedirect('/');
    }
}
