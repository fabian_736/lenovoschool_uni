<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Events\SyncBadges;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\User;
use App\Notifications\Points\EarnedPointsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class CommentControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_video_comments()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $comment = Comment::factory()->create();

        $video = $comment->gallery;

        $this->actingAs($user)
            ->getJson(route('gallery.comments.index', ['video' => $video->id]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_comment_a_video()
    {
        /** @var User */
        $user = User::factory()->asStudent()->withoutPoints()->create();

        $video = Gallery::factory()->set('points_per_comment', 10)->create();

        $commentData = [
            'comment' => $this->faker->text(500),
        ];

        Event::fake();
        Notification::fake();

        $this
            ->actingAs($user)
            ->postJson(route('gallery.comments.store', ['video' => $video->id]), $commentData, self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, EarnedPointsNotification::class);

        $this->assertDatabaseHas('comments', array_merge($commentData, [
            'author_id' => $user->id,
            'gallery_id' => $video->id,
        ]));

        $comment = Comment::first();
        $this->assertNotNull($comment->pointTransaction);

        $user->refresh();

        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);
    }

    public function test_the_user_cannot_earn_points_if_has_more_than_ten_comments_on_the_same_video()
    {
        /** @var User */
        $user = User::factory()->asStudent()->withoutPoints()->create();

        $video = Gallery::factory()->create();
        Comment::factory(10)->for($user, 'author')->for($video, 'gallery')->create();

        $commentData = [
            'comment' => $this->faker->text(500),
        ];

        Event::fake();
        Notification::fake();

        $this
            ->actingAs($user)
            ->postJson(route('gallery.comments.store', ['video' => $video->id]), $commentData, self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        Event::assertDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $comment = Comment::latest()->first();
        $this->assertNull($comment->pointTransaction);

        $user->refresh();

        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);
    }
}
