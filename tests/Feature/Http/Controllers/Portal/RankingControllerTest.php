<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RankingControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_rankings_view()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        // TODO: Test that the queries return the expected users

        $this->actingAs($user)
            ->get(route('ranking.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.ranking.index');
    }
}
