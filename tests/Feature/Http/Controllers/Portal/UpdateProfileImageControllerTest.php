<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UpdateProfileImageControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_view_the_update_profile_image_form()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('profile.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.profile.index');
    }

    public function test_user_can_update_profile_image()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $file = [
            'file' => UploadedFile::fake()->image('avatar.jpg', '300', '300'),
        ];

        Storage::fake('public');

        $this
            ->actingAs($user)
            ->postJson(route('profile.image.update'), $file, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $user->refresh();

        $avatarPath = $user->getFirstMediaPath('avatar');

        $this->assertFileExists($avatarPath);
    }
}
