<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Models\Badge;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class UserBadgesControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_user_badges_view()
    {
        /** @var \App\Models\User */
        $user = User::factory()->asStudent()->create();

        Badge::factory()->count(3)->create();

        /** @var Collection<int, \App\Models\Badge> */
        $badges = Badge::with([
            'users' => function ($query) {
                $query->wherePivot('user_id', auth()->id());
            },
            'media',
        ])
            ->get();

        $this->actingAs($user)
            ->get(route('badges'))
            ->assertOk()
            ->assertViewIs('modules.landing.medal.index')
            ->assertViewHasAll([
                'badges' => $badges,
                'user' => $user,
            ]);
    }
}
