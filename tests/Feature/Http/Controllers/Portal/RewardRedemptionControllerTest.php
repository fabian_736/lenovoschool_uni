<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Enums\PointTransactionTypes;
use App\Enums\RewardRedemptionsStatus;
use App\Events\SyncBadges;
use App\Models\Departament;
use App\Models\DocumentType;
use App\Models\Reward;
use App\Models\RewardRedemption;
use App\Models\User;
use App\Notifications\Points\EarnedPointsNotification;
use App\Notifications\Rewards\RedeemedRewardNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class RewardRedemptionControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_user_can_view_the_rewards_view()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('rewards.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.reward.index');
    }

    public function test_user_can_fetch_all_rewards()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        Reward::factory()->count(5)->create();
        $testData = Reward::orderByDesc('redeem_points')->first();

        $this->actingAs($user)
            ->getJson(route('rewards.list'), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data'])
            ->assertJson(
                fn (AssertableJson $json) => $json->has(
                    'data',
                    5,
                    fn ($json) => $json
                        ->where('id', $testData->id)
                        ->where('name', $testData->name)
                        ->where('description', $testData->description)
                        ->where('redeem_points', $testData->redeem_points)
                        ->where('quantity_available', $testData->quantity_available)
                        ->where('created_at', $testData->created_at->toISOString())
                        ->where('updated_at', $testData->updated_at->toISOString())
                        ->where('image', $testData->getFirstMediaUrl('image'))
                        ->etc()
                )
                    ->etc()
            );
    }

    public function test_user_check_if_can_redeem_a_reward()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $reward = Reward::factory()->create();

        $this->actingAs($user)
            ->getJson(route('rewards.check_redemption_conditions', ['reward' => $reward]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_user_check_if_cannot_redeem_a_reward()
    {
        /** @var User */
        $user = User::factory()->asStudent()->withoutPoints()->create();

        $reward = Reward::factory()->create();

        $this->actingAs($user)
            ->getJson(route('rewards.check_redemption_conditions', ['reward' => $reward]), self::$HEADERS)
            ->assertUnprocessable()
            ->assertJsonStructure(['message', 'status']);
    }

    public function test_user_can_view_the_redemption_form()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $reward = Reward::factory()->create();
        $documentTypes = DocumentType::all();

        $this->actingAs($user)
            ->get(route('rewards.redeem', ['reward' => $reward]))
            ->assertOk()
            ->assertViewIs('modules.landing.reward.formulario')
            ->assertViewHasAll([
                'reward' => $reward,
                'documentTypes' => $documentTypes,
            ]);
    }

    public function test_user_can_redeem_a_reward()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('total_redemption_points', 10)
            ->set('available_redemption_points', 10)
            ->create();

        $reward = Reward::factory()->create([
            'redeem_points' => 10,
            'quantity_available' => 1,
        ]);

        $documentType = DocumentType::factory()->create();
        $departament = Departament::factory()->create();

        $redemptionData = [
            'receiver_name' => $this->faker->name(),
            'receiver_document_type_id' => $documentType->id,
            'receiver_document_number' => '123456789',
            'receiver_departament_id' => $departament->id,
            'receiver_address' => $this->faker->address(),
            'receiver_email' => 'test@test.com',
            'receiver_phone' => '300000000',
        ];

        Event::fake();
        Notification::fake();

        $this
            ->actingAs($user)
            ->postJson(route('rewards.redeem', ['reward' => $reward]), $redemptionData, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, RedeemedRewardNotification::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $reward->refresh();
        $this->assertEquals(0, $reward->quantity_available);

        $this->assertDatabaseHas('reward_redemptions', $redemptionData);

        $rewardRedemption = RewardRedemption::first();

        $this->assertEquals(RewardRedemptionsStatus::pending, $rewardRedemption->status);

        $this->assertDatabaseHas('point_transactions', [
            'user_id' => $user->id,
            'transactionable_type' => $rewardRedemption->getMorphClass(),
            'transactionable_id' => $rewardRedemption->id,
            'points' => $reward->redeem_points * -1,
            'type' => PointTransactionTypes::redemption->value,
        ]);

        $user->refresh();
        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);
    }

    public function test_user_cannot_redeem_a_reward_if_not_have_enough_points()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        $reward = Reward::factory()->create([
            'redeem_points' => 10,
            'quantity_available' => 1,
        ]);

        $documentType = DocumentType::factory()->create();
        $departament = Departament::factory()->create();

        $redemptionData = [
            'receiver_name' => $this->faker->name(),
            'receiver_document_type_id' => $documentType->id,
            'receiver_document_number' => '123456789',
            'receiver_departament_id' => $departament->id,
            'receiver_address' => $this->faker->address(),
            'receiver_email' => 'test@test.com',
            'receiver_phone' => '300000000',
        ];

        Event::fake();
        Notification::fake();

        $this
            ->actingAs($user)
            ->postJson(route('rewards.redeem', ['reward' => $reward]), $redemptionData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJsonStructure(['message', 'status'])
            ->assertJsonPath('message', 'No podemos redimir tu premio, te faltan 10 puntos para reclamarlo');

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, RedeemedRewardNotification::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $reward->refresh();
        $this->assertEquals(1, $reward->quantity_available);

        $this->assertDatabaseMissing('reward_redemptions', $redemptionData);

        $this->assertDatabaseCount('point_transactions', 0);

        $user->refresh();
        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);
    }

    public function test_user_cannot_redeem_a_reward_if_the_reward_doesnt_have_stock()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('total_redemption_points', 10)
            ->set('available_redemption_points', 10)
            ->create();

        $reward = Reward::factory()->create([
            'redeem_points' => 10,
            'quantity_available' => 0,
        ]);

        $documentType = DocumentType::factory()->create();
        $departament = Departament::factory()->create();

        $redemptionData = [
            'receiver_name' => $this->faker->name(),
            'receiver_document_type_id' => $documentType->id,
            'receiver_departament_id' => $departament->id,
            'receiver_document_number' => '123456789',
            'receiver_address' => $this->faker->address(),
            'receiver_email' => 'test@test.com',
            'receiver_phone' => '300000000',
        ];

        Event::fake();
        Notification::fake();

        $this
            ->actingAs($user)
            ->postJson(route('rewards.redeem', ['reward' => $reward]), $redemptionData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJsonStructure(['message', 'status'])
            ->assertJsonPath('message', 'Este premio ya no está disponible');

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, RedeemedRewardNotification::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $reward->refresh();
        $this->assertEquals(0, $reward->quantity_available);

        $this->assertDatabaseMissing('reward_redemptions', $redemptionData);

        $this->assertDatabaseCount('point_transactions', 0);

        $user->refresh();
        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);
    }
}
