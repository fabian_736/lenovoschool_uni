<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Enums\PointTransactionTypes;
use App\Events\SyncBadges;
use App\Models\Gallery;
use App\Models\User;
use App\Notifications\Points\EarnedPointsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class GalleryControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_user_can_view_the_video_gallery()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $this->actingAs($user)
            ->get(route('gallery.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.gallery.index');
    }

    public function test_the_user_can_view_a_video_details()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $video = Gallery::factory()->create();

        $this->actingAs($user)
            ->get(route('gallery.view_video', ['video' => $video]))
            ->assertOk()
            ->assertViewIs('modules.landing.gallery.video')
            ->assertViewHas('video', $video);
    }

    public function test_the_user_can_download_a_video()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->asStudent()->withoutPoints()->create();

        $video = Gallery::factory()->set('points_for_downloading', 10)->create();
        $video->addMedia(UploadedFile::fake()->create('video.mp4'))->toMediaCollection('file');

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->get(route('gallery.download_video', ['video' => $video]))
            ->assertOk()
            ->assertDownload('video.mp4');

        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, EarnedPointsNotification::class);

        $this->assertNotNull($video->pointTransaction);

        $user->refresh();

        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);
    }

    public function test_the_system_does_not_assign_points_for_downloading_videos_more_than_once()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->asStudent()->withoutPoints()->create();

        $video = Gallery::factory()->create();
        $video->addMedia(UploadedFile::fake()->create('video.mp4'))->toMediaCollection('file');

        $video->pointTransaction()->create([
            'user_id' => $user->id,
            'points' => $video->points_for_downloading,
            'type' => PointTransactionTypes::earnedPoints,
        ]);

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->get(route('gallery.download_video', ['video' => $video]))
            ->assertOk()
            ->assertDownload('video.mp4');

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $count = $video->refresh()->loadCount('pointTransaction')->point_transaction_count;

        $this->assertEquals(1, $count);

        $user->refresh();

        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);
    }
}
