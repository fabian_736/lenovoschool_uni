<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Enums\UserChallengeStatus;
use App\Events\SyncBadges;
use App\Events\UpdatedProgress;
use App\Models\Challenge;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use App\Notifications\Points\EarnedPointsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class UserChallengeControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_all_available_challenges()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $this->actingAs($user)
            ->get(route('challenges.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.challenge.index');
    }

    public function test_the_user_can_fetch_the_user_challenge_data()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        $this->actingAs($user)
            ->getJson(route('challenges.check', $userChallenge), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_view_their_challenge()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        $this->actingAs($user)
            ->get(route('challenges.challenge_index', $userChallenge))
            ->assertOk()
            ->assertViewIs('modules.landing.challenge.test')
            ->assertViewHas('userChallenge', $userChallenge);
    }

    public function test_the_user_can_finish_their_challenge()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->published()->create();

        /** @var Question */
        $question = Question::factory()
            ->set('points', 10)
            ->for($challenge)
            ->create();

        /** @var Question */
        $option = QuestionOption::factory()
            ->asCorrect()
            ->for($question)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($challenge)
            ->for($user)
            ->create();

        $userChallengeAttempt = UserChallengeAttempt::factory()
            ->for($userChallenge)
            ->create();

        $input = [
            "question-$question->id" => $option->id,
        ];

        Event::fake();
        Storage::fake('public');
        Notification::fake();

        $this->actingAs($user)
            ->putJson(
                uri: route('challenges.score_challenge', ['userChallenge' => $userChallenge, 'userChallengeAttempt' => $userChallengeAttempt]),
                data: $input,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [
                    'earned_points',
                    'challenge_points',
                    'max_earned_points',
                ],
            ]);

        $userChallenge->refresh();
        $this->assertEquals(10, $userChallenge->max_earned_points);
        $this->assertNotNull($userChallenge->pointTransaction);
        $this->assertEquals(UserChallengeStatus::finished->value, $userChallenge->status);
        $this->assertFileExists($userChallenge->getFirstMediaPath('diploma'));

        $userChallengeAttempt->refresh();
        $this->assertNotNull($userChallengeAttempt->finished_at);
        $this->assertEquals(10, $userChallengeAttempt->earned_points);

        $user->refresh();
        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, EarnedPointsNotification::class);
    }

    public function test_the_user_can_reach_max_attempts_in_their_challenge()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->published()->create();

        /** @var Question */
        $question = Question::factory()
            ->set('points', 10)
            ->for($challenge)
            ->create();

        /** @var Question */
        $option = QuestionOption::factory()
            ->asIncorrect()
            ->for($question)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($challenge)
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->count(2)
            ->for($userChallenge)
            ->finished()
            ->create();

        $userChallengeAttempt = UserChallengeAttempt::factory()
            ->for($userChallenge)
            ->create();

        $input = [
            "question-$question->id" => $option->id,
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->putJson(
                uri: route('challenges.score_challenge', ['userChallenge' => $userChallenge, 'userChallengeAttempt' => $userChallengeAttempt]),
                data: $input,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [
                    'earned_points',
                    'challenge_points',
                    'max_earned_points',
                ],
            ]);

        $userChallenge->refresh();
        $this->assertEquals(0, $userChallenge->max_earned_points);
        $this->assertEmpty($userChallenge->pointTransaction);
        $this->assertEquals(UserChallengeStatus::maxAttemptsReached->value, $userChallenge->status);

        $userChallengeAttempt->refresh();
        $this->assertNotNull($userChallengeAttempt->finished_at);
        $this->assertEquals(0, $userChallengeAttempt->earned_points);

        $user->refresh();
        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);

        Event::assertDispatched(UpdatedProgress::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);
    }

    public function test_the_system_do_not_duplicate_the_earned_points_in_multiple_selection_questions()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->published()->create();

        /** @var Question */
        $question = Question::factory()
            ->set('points', 10)
            ->typeMultiple()
            ->for($challenge)
            ->create();

        /** @var Question */
        $options = QuestionOption::factory()
            ->count(2)
            ->asCorrect()
            ->for($question)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($challenge)
            ->for($user)
            ->create();

        $userChallengeAttempt = UserChallengeAttempt::factory()
            ->for($userChallenge)
            ->create();

        $input = [
            "question-$question->id" => $options->pluck('id'),
        ];

        Event::fake();
        Storage::fake('public');
        Notification::fake();

        $this->actingAs($user)
            ->putJson(
                uri: route('challenges.score_challenge', ['userChallenge' => $userChallenge, 'userChallengeAttempt' => $userChallengeAttempt]),
                data: $input,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [
                    'earned_points',
                    'challenge_points',
                    'max_earned_points',
                ],
            ]);

        $userChallenge->refresh();
        $this->assertEquals(10, $userChallenge->max_earned_points);
        $this->assertNotNull($userChallenge->pointTransaction);
        $this->assertFileExists($userChallenge->getFirstMediaPath('diploma'));

        $userChallengeAttempt->refresh();
        $this->assertNotNull($userChallengeAttempt->finished_at);
        $this->assertEquals(10, $userChallengeAttempt->earned_points);

        $user->refresh();
        $this->assertEquals(10, $user->available_redemption_points);
        $this->assertEquals(10, $user->total_redemption_points);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, EarnedPointsNotification::class);
    }

    public function test_the_system_do_not_evaluate_a_question_as_correct_if_has_selected_an_incorrect_answer_option()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->published()->create();

        /** @var Question */
        $question = Question::factory()
            ->set('points', 10)
            ->typeMultiple()
            ->for($challenge)
            ->create();

        /** @var QuestionOption */
        $option1 = QuestionOption::factory()
            ->asCorrect()
            ->for($question)
            ->create();

        /** @var QuestionOption */
        $option2 = QuestionOption::factory()
            ->asIncorrect()
            ->for($question)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($challenge)
            ->for($user)
            ->create();

        $userChallengeAttempt = UserChallengeAttempt::factory()
            ->for($userChallenge)
            ->create();

        $input = [
            "question-$question->id" => [
                $option1->id,
                $option2->id,
            ],
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->putJson(
                uri: route('challenges.score_challenge', ['userChallenge' => $userChallenge, 'userChallengeAttempt' => $userChallengeAttempt]),
                data: $input,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data' => [
                    'earned_points',
                    'challenge_points',
                    'max_earned_points',
                ],
            ]);

        $userChallenge->refresh();
        $this->assertEquals(0, $userChallenge->max_earned_points);
        $this->assertNotNull($userChallenge->pointTransaction);

        $userChallengeAttempt->refresh();
        $this->assertNotNull($userChallengeAttempt->finished_at);
        $this->assertEquals(0, $userChallengeAttempt->earned_points);

        $user->refresh();
        $this->assertEquals(0, $user->available_redemption_points);
        $this->assertEquals(0, $user->total_redemption_points);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);
    }

    public function test_the_user_can_view_their_finished_challenges()
    {
        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->published()->create();

        /** @var Question */
        $question = Question::factory()
            ->set('points', 10)
            ->typeMultiple()
            ->for($challenge)
            ->create();

        UserChallenge::factory()
            ->for($challenge)
            ->for($user)
            ->set('max_earned_points', $question->points)
            ->create();

        $userChallenges = UserChallenge::with([
            'challenge' => fn ($query) => $query->withSum('questions', 'points'),
        ])
            ->where('user_id', auth()->id())
            ->get()
            ->filter(function (UserChallenge $userChallenge) {
                return $userChallenge->status == 'finished';
            });

        $this->actingAs($user)
            ->get(route('challenges.my_challenges'))
            ->assertOk()
            ->assertViewIs('modules.landing.challenge.certificate')
            ->assertViewHas('userChallenges', $userChallenges);
    }

    public function test_the_user_can_download_their_diplomas()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        $file = UploadedFile::fake()->create('diploma.pdf', 10, 'application/pdf');

        $userChallenge->addMedia($file)->toMediaCollection('diploma');

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.download_diploma', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertDownload('diploma.pdf');
    }

    public function test_the_user_can_redeem_a_new_attempt()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->finished()
            ->for($userChallenge)
            ->create();

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertCreated()
            ->assertJsonStructure([
                'message',
                'status',
                'data',
            ]);

        Event::assertDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $userChallenge->refresh();
        $this->assertCount(2, $userChallenge->attempts);

        $this->assertNotNull($userChallenge->lastAttempt->pointTransaction);

        $user->refresh();
        $this->assertEquals(0, $user->available_redemption_points);
    }

    public function test_the_user_cannot_redeem_a_new_attempt_if_does_not_have_enough_points()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->withoutPoints()
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->finished()
            ->for($userChallenge)
            ->create();

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'No tienes puntos suficientes para redimir este premio.',
                'status' => 422,
            ]);

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $userChallenge->refresh();
        $this->assertCount(1, $userChallenge->attempts);

        $this->assertDatabaseCount('point_transactions', 0);
    }

    public function test_the_user_cannot_redeem_a_new_attempt_if_the_user_challenge_is_finished()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->finished()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->finished()
            ->for($userChallenge)
            ->create();

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'No se pudo redimir el desafío porque ya está finalizado.',
                'status' => 422,
            ]);

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $userChallenge->refresh();
        $this->assertCount(1, $userChallenge->attempts);

        $this->assertDatabaseCount('point_transactions', 0);

        $this->assertEquals(10, $user->fresh()->available_redemption_points);
    }

    public function test_system_updates_user_challenge_if_user_try_redeem_a_user_challenge_without_the_correct_status()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->count(3)
            ->finished()
            ->for($userChallenge)
            ->create();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'Ya ha alcanzado el número máximo de intentos posible para este desafío.',
                'status' => 422,
            ]);

        $userChallenge->refresh();
        $this->assertCount(3, $userChallenge->attempts);
        $this->assertEquals(UserChallengeStatus::maxAttemptsReached->value, $userChallenge->status);

        $this->assertDatabaseCount('point_transactions', 0);

        $this->assertEquals(10, $user->fresh()->available_redemption_points);
    }

    public function test_the_user_cannot_redeem_a_new_attempt_if_the_user_challenge_has_max_attempts_reatched_status()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->maxAttemptsReached()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->finished()
            ->for($userChallenge)
            ->create();

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'Ya ha alcanzado el número máximo de intentos posible para este desafío.',
                'status' => 422,
            ]);

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $userChallenge->refresh();
        $this->assertCount(1, $userChallenge->attempts);

        $this->assertDatabaseCount('point_transactions', 0);

        $this->assertEquals(10, $user->fresh()->available_redemption_points);
    }

    public function test_the_user_cannot_redeem_a_new_attempt_if_the_challenge_is_unavailable()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $challenge = Challenge::factory()->create();

        $userChallenge = UserChallenge::factory()
            ->maxAttemptsReached()
            ->for($user)
            ->for($challenge)
            ->create();

        UserChallengeAttempt::factory()
            ->finished()
            ->for($userChallenge)
            ->create();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'No se pudo redimir el desafío porque no está disponible.',
                'status' => 422,
            ]);

        $userChallenge->refresh();
        $this->assertCount(1, $userChallenge->attempts);

        $this->assertDatabaseCount('point_transactions', 0);

        $this->assertEquals(10, $user->fresh()->available_redemption_points);
    }

    public function test_the_user_cannot_redeem_a_new_attempt_if_the_user_challenge_has_a_non_finished_attempt()
    {
        config()->set('points-model.redemptions.challenge_attempt', 10);

        /** @var User */
        $user = User::factory()
            ->asStudent()
            ->set('available_redemption_points', 10)
            ->create();

        $userChallenge = UserChallenge::factory()
            ->for($user)
            ->create();

        UserChallengeAttempt::factory()
            ->for($userChallenge)
            ->create();

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->postJson(
                uri: route('challenges.redeem_attempt', ['userChallenge' => $userChallenge]),
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'No se pudo redimir el nuevo intento porque ya tiene un intento sin terminar.',
                'status' => 422,
            ]);

        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, EarnedPointsNotification::class);

        $userChallenge->refresh();
        $this->assertCount(1, $userChallenge->attempts);

        $this->assertDatabaseCount('point_transactions', 0);

        $this->assertEquals(10, $user->fresh()->available_redemption_points);
    }
}
