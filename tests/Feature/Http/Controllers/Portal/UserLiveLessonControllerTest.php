<?php

namespace Tests\Feature\Http\Controllers\Portal;

use App\Events\SyncBadges;
use App\Events\UpdatedProgress;
use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserLiveLesson;
use App\Models\UserLiveLessonTrackingHistory;
use App\Notifications\Challenges\NewChallengeNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class UserLiveLessonControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_live_lessons_view()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('live-lessons.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.class.index');
    }

    public function test_the_user_can_fetch_the_live_lessons_calendar()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->postJson(route('live-lessons.calendar'), [], self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data',
            ]);
    }

    public function test_el_usuario_puede_ver_la_clase_en_vivo_activa()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->toDateString())
            ->create();

        $this->actingAs($user)
            ->get(route('live-lessons.live'))
            ->assertOk()
            ->assertViewHas('liveLesson', $liveLesson)
            ->assertViewIs('modules.landing.class.class');
    }

    public function test_redirects_to_live_lesson_index_view_if_a_live_class_is_not_active()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        LiveLesson::factory()->set('scheduled_date', now()->subDay())->create();

        $this->actingAs($user)
            ->get(route('live-lessons.live'))
            ->assertRedirect(route('live-lessons.index'));
    }

    public function test_el_usuario_puede_ver_las_clases_guardadas()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $liveLessons = LiveLesson::all();

        $this->actingAs($user)
            ->get(route('live-lessons.saved_lessons'))
            ->assertOk()
            // ->assertViewHas('liveLessons', $liveLessons) // TODO: Averiguar por qué falla la aserción con la media
            ->assertViewIs('modules.landing.class.save');
    }

    public function test_the_user_can_fetch_a_live_lesson()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $this->actingAs($user)
            ->get(route('live-lessons.load_lesson', ['liveLesson' => $liveLesson]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data',
            ]);

        $liveLesson->refresh()->load('metadata');

        $this->assertNotNull($liveLesson->metadata);
    }

    public function test_system_do_not_create_user_live_lesson_twice()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($liveLesson)
            ->for($user)
            ->create();

        $this->actingAs($user)
            ->get(route('live-lessons.load_lesson', ['liveLesson' => $liveLesson]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure([
                'message',
                'status',
                'data',
            ]);

        $liveLesson->refresh()->load('metadata');

        $this->assertTrue($liveLesson->metadata->is($userLiveLesson));
    }

    public function test_the_system_creates_a_tracking_record()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->set('watched_time', '0.00')
            ->unwatched()
            ->create();

        $interactionType = $this->faker->randomElement(['play', 'pause', 'seeked', 'timeupdate']);

        $data = [
            'interaction_type' => $interactionType,
            'watched_time' => '1.00',
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->patchJson(route('live-lessons.track_live_lesson', ['userLiveLesson' => $userLiveLesson]), $data, self::$HEADERS)
            ->assertCreated();

        Event::assertNotDispatched(UpdatedProgress::class);
        Notification::assertNothingSent();

        $userLiveLesson->refresh();

        $this->assertEquals('1.00', $userLiveLesson->watched_time);

        $this->assertDatabaseHas(UserLiveLessonTrackingHistory::class, [
            'user_live_lesson_id' => $userLiveLesson->id,
            'interaction_type' => $interactionType,
            'watched_time' => '1.00',
        ]);
    }

    public function test_the_system_register_the_user_to_the_challenge_associated_to_the_live_lesson()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $challenge = Challenge::factory()->for($liveLesson)->published()->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->set('watched_time', '0.00')
            ->unwatched()
            ->create();

        $data = [
            'interaction_type' => 'ended',
            'watched_time' => '10.00',
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->patchJson(route('live-lessons.track_live_lesson', ['userLiveLesson' => $userLiveLesson]), $data, self::$HEADERS)
            ->assertCreated();

        $this->assertDatabaseHas(UserChallenge::class, [
            'user_id' => $user->id,
            'challenge_id' => $challenge->id,
        ]);

        $userLiveLesson->refresh();

        $this->assertTrue($userLiveLesson->watched);
        $this->assertEquals('10.00', $userLiveLesson->watched_time);

        $userChallenge = UserChallenge::whereChallengeId($challenge->id)->whereUserId($user->id)->first();

        $this->assertNotNull($userChallenge);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, NewChallengeNotification::class);
    }

    public function test_the_system_register_the_user_to_the_challenge_associated_to_the_live_lesson_even_if_the_challenge_is_a_draft()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->format('Y-m-d'))
            ->create();

        $challenge = Challenge::factory()->for($liveLesson)->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->set('watched_time', '0.00')
            ->unwatched()
            ->create();

        $data = [
            'interaction_type' => 'ended',
            'watched_time' => '10.00',
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->patchJson(route('live-lessons.track_live_lesson', ['userLiveLesson' => $userLiveLesson]), $data, self::$HEADERS)
            ->assertCreated();

        $userLiveLesson->refresh();

        $this->assertTrue($userLiveLesson->watched);
        $this->assertEquals('10.00', $userLiveLesson->watched_time);

        $this->assertDatabaseHas(UserChallenge::class, [
            'user_id' => $user->id,
            'challenge_id' => $challenge->id,
        ]);

        $userChallenge = UserChallenge::whereChallengeId($challenge->id)->whereUserId($user->id)->first();

        $this->assertNotNull($userChallenge);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertSentTo($user, NewChallengeNotification::class);
    }

    public function test_the_system_system_only_marks_the_live_lesson_as_watched_if_the_live_lesson_does_not_have_a_challenge()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->format('Y-m-d'))
            ->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->set('watched_time', '0.00')
            ->unwatched()
            ->create();

        $data = [
            'interaction_type' => 'ended',
            'watched_time' => '10.00',
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->patchJson(route('live-lessons.track_live_lesson', ['userLiveLesson' => $userLiveLesson]), $data, self::$HEADERS)
            ->assertCreated();

        $userLiveLesson->refresh();

        $this->assertTrue($userLiveLesson->watched);
        $this->assertEquals('10.00', $userLiveLesson->watched_time);

        $this->assertDatabaseCount(UserChallenge::class, 0);

        Event::assertDispatched(UpdatedProgress::class);
        Event::assertDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, NewChallengeNotification::class);
    }

    public function test_the_system_do_not_register_the_user_to_the_challenge_associated_to_the_live_lesson_twice()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();
        $liveLesson = LiveLesson::factory()
            ->set('scheduled_date', now()->subDay()->toDateString())
            ->create();

        $challenge = Challenge::factory()->for($liveLesson)->published()->create();

        $userLiveLesson = UserLiveLesson::factory()
            ->for($user)
            ->for($liveLesson)
            ->watched()
            ->create();

        UserChallenge::factory()
            ->for($user)
            ->for($challenge)
            ->create();

        $data = [
            'interaction_type' => 'ended',
            'watched_time' => '10.00',
        ];

        Event::fake();
        Notification::fake();

        $this->actingAs($user)
            ->patchJson(route('live-lessons.track_live_lesson', ['userLiveLesson' => $userLiveLesson]), $data, self::$HEADERS)
            ->assertCreated();

        $this->assertDatabaseCount(UserChallenge::class, 1);

        Event::assertNotDispatched(UpdatedProgress::class);
        Event::assertNotDispatched(SyncBadges::class);
        Notification::assertNotSentTo($user, NewChallengeNotification::class);
    }
}
