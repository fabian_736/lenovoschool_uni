<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Models\Gallery;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class GalleryControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_gallery_view()
    {
        /** @var User */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.gallery.index'))
            ->assertOk()
            ->assertViewIs('panel.gallery');
    }

    public function test_the_user_can_fetch_all_gallery_videos()
    {
        /** @var User */
        $user = User::factory()->create();

        Gallery::factory()->create();

        $this->actingAs($user)
            ->getJson(route('panel.gallery.list'), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_create_a_video()
    {
        /** @var User */
        $user = User::factory()->create();

        $galleryData = [
            'title' => 'Lorem ipsum dolor sit',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'points_for_downloading' => 10,
            'points_per_comment' => 10,
            'status' => 1,
        ];

        $media = [
            'preview' => UploadedFile::fake()->image('preview.png', 980, 720),
            'file' => UploadedFile::fake()->create('video.mp4'),
        ];

        Storage::fake('public');

        $this
            ->actingAs($user)
            ->postJson(route('panel.gallery.store'), array_merge($galleryData, $media), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('gallery', $galleryData);

        $lesson = Gallery::first();

        $this->assertFileExists($lesson->getFirstMediaPath('preview'));
        $this->assertFileExists($lesson->getFirstMediaPath('file'));
    }

    public function test_the_user_cannot_create_a_video()
    {
        /** @var User */
        $user = User::factory()->create();

        $galleryData = [
            'title' => 'Lorem ipsum dolor sit',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'points_for_downloading' => 10,
            'points_per_comment' => 10,
            'status' => 0,
        ];

        $this
            ->actingAs($user)
            ->postJson(route('panel.gallery.store'), $galleryData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['preview', 'file']);

        $this->assertDatabaseMissing('gallery', $galleryData);
    }

    public function test_the_user_can_edit_a_video()
    {
        /** @var User */
        $user = User::factory()->create();

        $gallery = Gallery::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.gallery.show', $gallery), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_update_a_video()
    {
        /** @var User */
        $user = User::factory()->create();
        $gallery = Gallery::factory()->create();

        $galleryData = [
            'title' => 'Lorem ipsum dolor sit',
            'description' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'points_for_downloading' => 10,
            'points_per_comment' => 10,
            'status' => 0,
        ];

        $media = [
            'preview' => UploadedFile::fake()->image('preview.png', 980, 720),
            'file' => UploadedFile::fake()->create('video.mp4'),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->putJson(
                uri: route('panel.gallery.update', ['gallery' => $gallery]),
                data: array_merge($galleryData, $media),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $gallery->refresh();

        $this->assertEquals($galleryData['title'], $gallery->title);
        $this->assertEquals($galleryData['description'], $gallery->description);
        $this->assertEquals($galleryData['points_for_downloading'], $gallery->points_for_downloading);
        $this->assertEquals($galleryData['points_per_comment'], $gallery->points_per_comment);
        $this->assertEquals($galleryData['status'], $gallery->status);

        $this->assertFileExists($gallery->getFirstMediaPath('preview'));
        $this->assertFileExists($gallery->getFirstMediaPath('file'));
    }
}
