<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\RewardRedemptionsStatus;
use App\Http\Resources\RewardRedemptionResource;
use App\Models\RewardRedemption;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Testing\Fluent\AssertableJson;
use Tests\TestCase;

class RewardRedemptionStatusControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_reward_redemptions_view()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('panel.rewards.redemption-status.index'))
            ->assertViewIs('panel.rewards.follow');
    }

    public function test_user_can_fetch_all_reward_redemptions()
    {
        /** @var User */
        $user = User::factory()->create();

        RewardRedemption::factory()->delivered()->create();
        RewardRedemption::factory()->sent()->create();
        $testData = RewardRedemption::factory()->create();

        $this->actingAs($user)
            ->getJson(route('panel.rewards.redemption-status.list'))
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data'])
            ->assertJson(
                fn (AssertableJson $json) => $json->has('meta')
                    ->has(
                        'data',
                        3,
                        fn ($json) => $json
                            ->where('id', $testData->id)
                            ->where('user_id', $testData->user_id)
                            ->where('reward_id', $testData->reward_id)
                            ->where('receiver_name', $testData->receiver_name)
                            ->where('receiver_document_type_id', $testData->receiver_document_type_id)
                            ->where('receiver_document_number', $testData->receiver_document_number)
                            ->where('receiver_email', $testData->receiver_email)
                            ->where('receiver_departament_id', $testData->receiver_departament_id)
                            ->where('receiver_address', $testData->receiver_address)
                            ->where('receiver_phone', $testData->receiver_phone)
                            ->where('status', $testData->status->value)
                            ->where('status_label', $testData->status->label())
                            ->etc()
                    )
                    ->etc()
            );
    }

    public function test_user_can_view_a_reward_redemption()
    {
        /** @var User */
        $user = User::factory()->create();

        $redemption = RewardRedemption::factory()->create();

        $redemptions = new RewardRedemptionResource($redemption);

        $this->actingAs($user)
            ->getJson(route('panel.rewards.redemption-status.show', ['rewardRedemption' => $redemption]))
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data'])
            ->assertJsonFragment($redemptions->resolve());
    }

    public function test_user_can_update_a_reward_redemption_status()
    {
        /** @var User */
        $user = User::factory()->create();

        $redemption = RewardRedemption::factory()->create();

        $data = [
            'status' => RewardRedemptionsStatus::delivered->value,
        ];

        $this->actingAs($user)
            ->patchJson(route('panel.rewards.redemption-status.patch', ['rewardRedemption' => $redemption]), $data)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $redemption->refresh();

        $this->assertEquals(RewardRedemptionsStatus::delivered, $redemption->status);
    }

    public function test_user_cannot_update_a_reward_redemption_status()
    {
        /** @var User */
        $user = User::factory()->create();

        $redemption = RewardRedemption::factory()->create();

        $data = [
            'status' => 'Invalid',
        ];

        $this->actingAs($user)
            ->patchJson(route('panel.rewards.redemption-status.patch', ['rewardRedemption' => $redemption]), $data)
            ->assertUnprocessable()
            ->assertInvalid(['status']);

        $redemption->refresh();

        $this->assertEquals(RewardRedemptionsStatus::pending, $redemption->status);
    }
}
