<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Models\Challenge;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class QuestionOptionControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_el_usuario_puede_consultar_todas_las_opciones_de_respuesta_de_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        /** @var Question */
        $question = Question::factory()->for($challenge)->create();

        /** @var Question */
        QuestionOption::factory()->count(3)->for($question)->create();

        $this->actingAs($user)
            ->getJson(
                uri: route(
                    name: 'panel.challenges.questions.options.index',
                    parameters: ['challenge' => $challenge, 'question' => $question]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_el_usuario_puede_crear_una_opcion_de_respuesta_con_imagen()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        /** @var Question */
        $question = Question::factory()->for($challenge)->create();

        $optionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'is_correct' => 1,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('image.jpg', 720, 480),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->postJson(
                uri: route(
                    name: 'panel.challenges.questions.options.store',
                    parameters: ['challenge' => $challenge, 'question' => $question]
                ),
                data: array_merge($optionData, $image),
                headers: self::$HEADERS
            )
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('question_options', $optionData);

        $option = QuestionOption::first();

        $this->assertFileExists($option->getFirstMediaPath('image'));
    }

    public function test_el_usuario_puede_crear_una_opcion_de_respuesta_sin_imagen()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        /** @var Question */
        $question = Question::factory()->for($challenge)->create();

        $optionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'is_correct' => 1,
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->postJson(
                uri: route(
                    name: 'panel.challenges.questions.options.store',
                    parameters: ['challenge' => $challenge, 'question' => $question]
                ),
                data: $optionData,
                headers: self::$HEADERS
            )
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('question_options', $optionData);

        $option = QuestionOption::first();

        $this->assertNull($option->getFirstMedia('image'));
    }

    public function test_el_usuario_no_puede_crear_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        /** @var Question */
        $question = Question::factory()->for($challenge)->create();

        $optionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'is_correct' => 2,
        ];

        $this->actingAs($user)
            ->postJson(
                uri: route(
                    name: 'panel.challenges.questions.options.store',
                    parameters: ['challenge' => $challenge, 'question' => $question]
                ),
                data: $optionData,
                headers: self::$HEADERS
            )
            ->assertUnprocessable()
            ->assertInvalid(['is_correct']);

        $this->assertDatabaseMissing('question_options', $optionData);
    }

    public function test_el_usuario_puede_editar_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        $option = QuestionOption::factory()->create();
        $question = $option->question;
        $challenge = $option->question->challenge;

        $this->actingAs($user)
            ->getJson(
                uri: route(
                    name: 'panel.challenges.questions.options.show',
                    parameters: ['challenge' => $challenge, 'question' => $question, 'option' => $option]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_el_usuario_puede_actualizar_una_opcion_de_respuesta()
    {
        /** @var User */
        $user = User::factory()->create();

        $option = QuestionOption::factory()->asCorrect()->create();
        $question = $option->question;
        $challenge = $option->question->challenge;

        $optionData = [
            'label' => '¿Opción de respuesta?',
            'is_correct' => 0,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('image.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->putJson(
                uri: route(
                    name: 'panel.challenges.questions.options.update',
                    parameters: ['challenge' => $challenge, 'question' => $question, 'option' => $option]
                ),
                data: array_merge($optionData, $image),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $option->refresh();

        $this->assertEquals($optionData['label'], $option->label);
        $this->assertEquals($optionData['is_correct'], $option->getRawOriginal('is_correct'));
    }

    public function test_el_usuario_puede_eliminar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        $option = QuestionOption::factory()->asCorrect()->create();
        $question = $option->question;
        $challenge = $option->question->challenge;

        $this->actingAs($user)
            ->deleteJson(
                uri: route(
                    name: 'panel.challenges.questions.options.destroy',
                    parameters: ['challenge' => $challenge, 'question' => $question, 'option' => $option]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertModelMissing($option);
    }
}
