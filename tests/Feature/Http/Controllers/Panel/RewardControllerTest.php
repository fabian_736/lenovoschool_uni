<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Models\Reward;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class RewardControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_listen_of_rewards()
    {
        /** @var User */
        $user = User::factory()->create();
        Reward::factory(5)->create();
        Reward::with('media')->paginate();

        $response = $this->followingRedirects()
            ->actingAs($user)
            ->get(route('panel.rewards.all'));

        $response->assertOk()
            ->assertViewIs('panel.rewards.all');

        $this->assertInstanceOf(\Illuminate\Contracts\Pagination\LengthAwarePaginator::class, $response->viewData('rewards'));
    }

    public function test_the_user_can_create_a_reward()
    {
        /** @var User */
        $user = User::factory()->create();

        $data = [
            'name' => 'Prueba',
            'description' => 'Descripción de prueba',
            'redeem_points' => 10,
            'quantity_available' => 10,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('cover.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->post(route('panel.rewards.store'), array_merge($data, $image))
            ->assertStatus(302)
            ->assertSessionDoesntHaveErrors()
            ->assertSessionHas('success', 'Premio creado correctamente');

        $this->assertDatabaseHas('rewards', $data);

        $reward = Reward::first();

        $this->assertFileExists($reward->getFirstMediaPath('image'));
    }

    public function test_the_user_can_edit_a_reward()
    {
        /** @var User */
        $user = User::factory()->create();

        $reward = Reward::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.rewards.edit', $reward))
            ->assertOk()
            ->assertViewIs('panel.rewards.edit')
            ->assertViewHas('reward', $reward);
    }

    public function test_the_user_can_update_a_reward()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->create();
        $reward = Reward::factory()->create();
        $reward->addMedia(UploadedFile::fake()->image('cover.png', 980, 720)->size('1000'))
            ->toMediaCollection('image');

        $previousImagePath = $reward->getFirstMediaPath('image');

        $data = [
            'name' => 'Prueba',
            'description' => 'Descripción de prueba',
            'redeem_points' => 10,
            'quantity_available' => 10,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('cover.png', 980, 720)->size('1000'),
        ];

        $this->actingAs($user)
            ->put(route('panel.rewards.update', $reward), array_merge($data, $image))
            ->assertStatus(302)
            ->assertSessionDoesntHaveErrors()
            ->assertSessionHas('success', 'Premio actualizado correctamente');

        $reward->refresh();

        $this->assertEquals($data['name'], $reward->name);
        $this->assertEquals($data['description'], $reward->description);
        $this->assertEquals($data['redeem_points'], $reward->redeem_points);
        $this->assertEquals($data['quantity_available'], $reward->quantity_available);

        $this->assertFileDoesNotExist($previousImagePath);
        $this->assertFileExists($reward->getFirstMediaPath('image'));
    }

    public function test_the_user_can_delete_a_reward()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->create();
        $reward = Reward::factory()->create();
        $reward->addMedia(UploadedFile::fake()->image('cover.png', 980, 720)->size('1000'))
            ->toMediaCollection('image');

        $previousImagePath = $reward->getFirstMediaPath('image');

        $this->actingAs($user)
            ->delete(route('panel.rewards.destroy', $reward))
            ->assertStatus(302)
            ->assertSessionDoesntHaveErrors()
            ->assertSessionHas('success', 'Premio eliminado correctamente');

        $this->assertModelMissing($reward);
        $this->assertFileDoesNotExist($previousImagePath);
    }
}
