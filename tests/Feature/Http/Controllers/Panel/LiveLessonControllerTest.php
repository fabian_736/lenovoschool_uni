<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Models\LiveLesson;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class LiveLessonControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_live_lessons_view()
    {
        /** @var User */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.live-lessons.index'))
            ->assertOk()
            ->assertViewIs('panel.live-lessons.index');
    }

    public function test_the_user_can_fetch_all_live_lessons()
    {
        /** @var User */
        $user = User::factory()->create();

        LiveLesson::factory()->create();

        $this->actingAs($user)
            ->getJson(route('panel.live-lessons.list'), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_create_a_live_lesson()
    {
        Carbon::setTestNow(now());

        /** @var User */
        $user = User::factory()->create();

        $liveLessonData = [
            'title' => 'Clase en vivo de prueba',
            'subtitle' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, veniam ut. Officiis, aliquam alias quisquam quo facilis aspernatur delectus error, debitis illo tempora veniam rem voluptatem culpa minus, necessitatibus velit.',
            'teacher' => 'Profesor',
            'scheduled_date' => now()->addDay()->format('Y-m-d'),
            'scheduled_time' => now()->addDay()->format('H:i:s'),
            'vimeo_live_event_id' => '2083897',
            'vimeo_video_id' => '705838785',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
        ];

        Storage::fake('public');

        $this
            ->actingAs($user)
            ->postJson(route('panel.live-lessons.store'), array_merge($liveLessonData, $cover), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('live_lessons', $liveLessonData);

        $lesson = LiveLesson::first();

        $this->assertFileExists($lesson->getFirstMediaPath('cover'));
    }

    public function test_the_user_cannot_create_a_live_lesson()
    {
        /** @var User */
        $user = User::factory()->create();

        $liveLessonData = [
            'title' => 'Clase en vivo de prueba',
            'subtitle' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, veniam ut. Officiis, aliquam alias quisquam quo facilis aspernatur delectus error, debitis illo tempora veniam rem voluptatem culpa minus, necessitatibus velit.',
            'teacher' => 'Profesor',
            'scheduled_date' => now()->addDay()->format('Y-m-d'),
            'scheduled_time' => now()->addDay()->format('H:i:s'),
        ];

        $this
            ->actingAs($user)
            ->postJson(route('panel.live-lessons.store'), $liveLessonData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['vimeo_live_event_id', 'vimeo_video_id', 'cover']);

        $this->assertDatabaseMissing('live_lessons', $liveLessonData);
    }

    public function test_the_user_cannot_create_a_live_lesson_if_another_live_lesson_has_the_same_scheduled_date()
    {
        $lessonDate = now()->addDay()->format('Y-m-d');

        /** @var User */
        $user = User::factory()->create();
        LiveLesson::factory()->set('scheduled_date', $lessonDate)->create();

        $liveLessonData = [
            'title' => 'Clase en vivo de prueba',
            'subtitle' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, veniam ut. Officiis, aliquam alias quisquam quo facilis aspernatur delectus error, debitis illo tempora veniam rem voluptatem culpa minus, necessitatibus velit.',
            'teacher' => 'Profesor',
            'scheduled_date' => $lessonDate,
            'scheduled_time' => now()->addDay()->format('H:i:s'),
            'vimeo_live_event_id' => '2083897',
            'vimeo_video_id' => '705838785',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
        ];

        Storage::fake('public');

        $this
            ->actingAs($user)
            ->postJson(route('panel.live-lessons.store'), array_merge($liveLessonData, $cover), self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['scheduled_date' => "Ya existe una clase en vivo programada para el $lessonDate"]);

        $this->assertDatabaseMissing('live_lessons', $liveLessonData);
    }

    public function test_the_user_can_edit_a_live_lesson()
    {
        /** @var User */
        $user = User::factory()->create();

        $liveLesson = LiveLesson::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.live-lessons.show', $liveLesson), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_update_a_live_lesson()
    {
        Carbon::setTestNow(now()->addDay());

        /** @var User */
        $user = User::factory()->create();
        $liveLesson = LiveLesson::factory()->create();

        $liveLessonData = [
            'title' => 'Clase en vivo de prueba',
            'subtitle' => 'Lorem ipsum dolor, sit amet consectetur adipisicing elit.',
            'description' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Maiores, veniam ut. Officiis, aliquam alias quisquam quo facilis aspernatur delectus error, debitis illo tempora veniam rem voluptatem culpa minus, necessitatibus velit.',
            'teacher' => 'Profesor',
            'scheduled_date' => now()->addDay()->format('Y-m-d'),
            'scheduled_time' => now()->addDay()->format('H:i:s'),
            'vimeo_live_event_id' => '2083897',
            'vimeo_video_id' => '705838785',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->putJson(
                uri: route('panel.live-lessons.update', ['liveLesson' => $liveLesson]),
                data: array_merge($liveLessonData, $cover),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $liveLesson->refresh();

        $this->assertEquals($liveLessonData['title'], $liveLesson->title);
        $this->assertEquals($liveLessonData['subtitle'], $liveLesson->subtitle);
        $this->assertEquals($liveLessonData['description'], $liveLesson->description);
        $this->assertEquals($liveLessonData['teacher'], $liveLesson->teacher);
        $this->assertEquals($liveLessonData['scheduled_date'], $liveLesson->scheduled_date);
        $this->assertEquals($liveLessonData['scheduled_time'], $liveLesson->scheduled_time);
        $this->assertEquals($liveLessonData['vimeo_live_event_id'], $liveLesson->vimeo_live_event_id);
        $this->assertEquals($liveLessonData['vimeo_video_id'], $liveLesson->vimeo_video_id);

        $this->assertFileExists($liveLesson->getFirstMediaPath('cover'));
    }

    public function test_the_user_can_delete_a_live_lesson()
    {
        /** @var User */
        $user = User::factory()->create();
        $liveLesson = LiveLesson::factory()->create();

        $this->actingAs($user)
            ->deleteJson(
                uri: route('panel.live-lessons.destroy', ['liveLesson' => $liveLesson]),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertModelMissing($liveLesson);
    }
}
