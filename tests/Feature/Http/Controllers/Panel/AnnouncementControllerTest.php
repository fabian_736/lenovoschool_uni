<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AnnouncementControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_announcements_view()
    {
        /** @var User */
        $user = User::factory()->create();

        Storage::fake('public');

        Storage::disk('public')->putFile('announcements', UploadedFile::fake()->image('announcement.jpg'));

        $files = collect(Storage::disk('public')->files('announcements'))
            ->mapWithKeys(function ($file) {
                return [
                    File::basename($file) => Storage::disk('public')->url($file),
                ];
            });

        $this
            ->actingAs($user)
            ->get(route('panel.announcements.index'))
            ->assertOk()
            ->assertViewHas('files', $files);
    }

    public function test_the_user_can_upload_an_announcement()
    {
        /** @var User */
        $user = User::factory()->create();

        $image = UploadedFile::fake()->image('announcement.jpg');

        Storage::fake('public');

        $this->actingAs($user)
            ->postJson(route('panel.announcements.store'), ['file' => $image], self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_delete_an_announcement()
    {
        /** @var User */
        $user = User::factory()->create();

        Storage::fake('public');

        $path = Storage::disk('public')->putFile('announcements', UploadedFile::fake()->image('announcement.jpg'));
        $filename = File::basename($path);

        $this->actingAs($user)
            ->deleteJson(route('panel.announcements.destroy'), ['filename' => $filename], self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        Storage::disk('public')->assertMissing($path);
    }

    public function test_the_user_cannot_delete_an_invalid_announcement()
    {
        /** @var User */
        $user = User::factory()->create();

        Storage::fake('public');

        $path = Storage::disk('public')->putFile('announcements', UploadedFile::fake()->image('announcement.jpg'));

        $this->actingAs($user)
            ->deleteJson(route('panel.announcements.destroy'), ['filename' => '../../file.jpg'], self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['filename']);

        Storage::disk('public')->assertExists($path);
    }

    public function test_the_user_cannot_delete_an_announcement_that_not_exists()
    {
        /** @var User */
        $user = User::factory()->create();

        Storage::fake('public');

        $path = Storage::disk('public')->putFile('announcements', UploadedFile::fake()->image('announcement.jpg'));

        $this->actingAs($user)
            ->deleteJson(route('panel.announcements.destroy'), ['filename' => 'file.jpg'], self::$HEADERS)
            ->assertUnprocessable()
            ->assertJsonStructure(['message', 'status']);

        Storage::disk('public')->assertExists($path);
    }
}
