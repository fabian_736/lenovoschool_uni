<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\PointTransactionTypes;
use App\Models\Penalization;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PenalizationControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_penalizations_view()
    {
        /** @var User */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.penalizations.index'))
            ->assertOk()
            ->assertViewIs('panel.penalizations');
    }

    public function test_the_user_can_penalize_a_user()
    {
        /** @var User */
        $penalizingUser = User::factory()->create();

        /** @var User */
        $penalizedUser = User::factory()->create([
            'total_redemption_points' => 10,
            'available_redemption_points' => 10,
        ]);

        $penalizationData = [
            'reason' => $this->faker->text(),
            'deducted_points' => 10,
        ];

        $this->actingAs($penalizingUser)
            ->postJson(route('panel.penalizations.store', ['user' => $penalizedUser]), $penalizationData, self::$HEADERS)
            ->assertCreated()
            ->assertJson([
                'message' => 'Usuario penalizado correctamente.',
                'status' => 201,
                'data' => [],
            ]);

        $penalizationDB = array_merge($penalizationData, [
            'penalizing_user_id' => $penalizingUser->id,
            'penalized_user_id' => $penalizedUser->id,
        ]);
        $this->assertDatabaseHas('penalizations', $penalizationDB);

        $penalization = Penalization::first();
        $this->assertDatabaseHas('point_transactions', [
            'user_id' => $penalizedUser->id,
            'transactionable_type' => $penalization->getMorphClass(),
            'transactionable_id' => $penalization->id,
            'points' => -10,
            'type' => PointTransactionTypes::penalization->value,
        ]);

        $penalizedUser->refresh();
        $this->assertEquals(10, $penalizedUser->total_redemption_points);
        $this->assertEquals(0, $penalizedUser->available_redemption_points);
    }

    public function test_the_user_cannot_penalize_a_user_if_data_is_invalid()
    {
        /** @var User */
        $penalizingUser = User::factory()->create();

        /** @var User */
        $penalizedUser = User::factory()->create([
            'total_redemption_points' => 10,
            'available_redemption_points' => 5,
        ]);

        $this->actingAs($penalizingUser)
            ->postJson(route('panel.penalizations.store', ['user' => $penalizedUser]), [], self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['reason', 'deducted_points']);

        $this->assertDatabaseCount('penalizations', 0);
        $this->assertDatabaseCount('point_transactions', 0);

        $penalizedUser->refresh();
        $this->assertEquals(10, $penalizedUser->total_redemption_points);
        $this->assertEquals(5, $penalizedUser->available_redemption_points);
    }

    public function test_the_user_cannot_penalize_a_user_if_the_penalized_user_not_have_enough_available_redemption_points()
    {
        /** @var User */
        $penalizingUser = User::factory()->create();

        /** @var User */
        $penalizedUser = User::factory()->create([
            'total_redemption_points' => 10,
            'available_redemption_points' => 5,
        ]);

        $penalizationData = [
            'reason' => $this->faker->text(),
            'deducted_points' => 10,
        ];

        $this->actingAs($penalizingUser)
            ->postJson(route('panel.penalizations.store', ['user' => $penalizedUser]), $penalizationData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'No se pudo penalizar al usuario porque no tiene suficientes puntos para deducir. Se pueden deducir un máximo de 5 para este usuario',
                'status' => 422,
            ]);

        $penalizationDB = array_merge($penalizationData, [
            'penalizing_user_id' => $penalizingUser->id,
            'penalized_user_id' => $penalizedUser->id,
        ]);
        $this->assertDatabaseMissing('penalizations', $penalizationDB);

        $this->assertDatabaseCount('point_transactions', 0);

        $penalizedUser->refresh();
        $this->assertEquals(10, $penalizedUser->total_redemption_points);
        $this->assertEquals(5, $penalizedUser->available_redemption_points);
    }
}
