<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\ChallengeStatus;
use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\User;
use App\Models\UserChallengeAttempt;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class ChallengeControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_all_challenges()
    {
        /** @var User */
        $user = User::factory()->create();

        Challenge::factory()
            ->count(3)
            ->state(new Sequence(
                ['status' => ChallengeStatus::draft],
                ['status' => ChallengeStatus::published],
            ))
            ->create();
        $challenges = Challenge::all();

        LiveLesson::factory()->create();
        $liveLessons = LiveLesson::doesntHave('challenge')->get();

        $this
            ->actingAs($user)
            ->get(route('panel.challenges.index'))
            ->assertOk()
            ->assertViewIs('panel.challenges.index')
            ->assertViewHasAll([
                'challenges' => $challenges,
                'liveLessons' => $liveLessons,
            ]);
    }

    public function test_the_user_can_create_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();

        $challengeData = [
            'live_lesson_id' => LiveLesson::factory()->create()->id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
            'teacher_signature' => UploadedFile::fake()->image('signature.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->postJson(route('panel.challenges.store'), array_merge($challengeData, $cover), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data', 'url']);

        $this->assertDatabaseHas('challenges', $challengeData);

        $challenge = Challenge::first();

        $this->assertFileExists($challenge->getFirstMediaPath('cover'));
        $this->assertFileExists($challenge->getFirstMediaPath('teacher_signature'));
    }

    public function test_the_user_cannot_create_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();

        $challengeData = [
            'name' => 'Desafío de prueba',
        ];

        $this->actingAs($user)
            ->postJson(route('panel.challenges.store'), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['live_lesson_id', 'teacher_name', 'teacher_signature', 'cover']);

        $this->assertDatabaseMissing('challenges', $challengeData);
    }

    public function test_the_user_cannot_create_a_challenge_if_live_lesson_is_not_unique()
    {
        /** @var User */
        $user = User::factory()->create();

        $challenge = Challenge::factory()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
            'teacher_signature' => UploadedFile::fake()->image('signature.png', 980, 720),
        ];

        $this->actingAs($user)
            ->postJson(route('panel.challenges.store'), array_merge($challengeData, $cover), self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['live_lesson_id' => 'La Clase en vivo asociada seleccionada ya posee un desafío asociado.']);

        $this->assertDatabaseMissing('challenges', $challengeData);
    }

    public function test_the_user_can_edit_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();

        $challenge = Challenge::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.challenges.show', $challenge))
            ->assertOk()
            ->assertViewIs('panel.challenges.edit')
            ->assertViewHas('challenge', $challenge);
    }

    public function test_the_user_can_update_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $cover = [
            'cover' => UploadedFile::fake()->image('cover.png', 980, 720),
            'teacher_signature' => UploadedFile::fake()->image('signature.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->putJson(route('panel.challenges.update', $challenge), array_merge($challengeData, $cover), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $challenge->refresh();

        $this->assertEquals($challengeData['name'], $challenge->name);
    }

    public function test_the_user_can_publish_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();
        $question = Question::factory()->for($challenge)->create();
        QuestionOption::factory()->for($question)->asCorrect()->create();
        QuestionOption::factory()->for($question)->asIncorrect()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $challenge->refresh();

        $this->assertEquals($challengeData['name'], $challenge->name);
        $this->assertEquals(ChallengeStatus::published->value, $challenge->status);
        $this->assertEquals(now()->format('Y-m-d H:i:m'), $challenge->published_at->format('Y-m-d H:i:m'));
    }

    public function test_the_user_cannot_publish_a_challenge_without_questions()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => 'El desafío no se pudo publicar porque no tiene preguntas.',
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_cannot_publish_a_challenge_without_question_options()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();
        Question::factory()
            ->for($challenge)
            ->set('label', $label = '¿Preguna de prueba?')
            ->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => sprintf(
                    "El desafío no se pudo publicar porque la pregunta \"$label\" no tiene opciones de respuesta."
                ),
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_cannot_publish_a_challenge_with_questions_that_only_has_one_option()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();
        $question = Question::factory()
            ->for($challenge)
            ->set('label', $label = '¿Preguna de prueba?')
            ->create();
        QuestionOption::factory()->for($question)->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => sprintf(
                    "El desafío no se pudo publicar porque la pregunta \"$label\" necesita al menos dos (2) opciones de respuesta."
                ),
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_cannot_publish_a_challenge_if_at_least_one_unique_question_has_not_exactly_one_correct_option()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();
        $question = Question::factory()
            ->for($challenge)
            ->set('label', $label = '¿Preguna de prueba?')
            ->create();
        QuestionOption::factory()->for($question)->asCorrect()->create();
        QuestionOption::factory()->for($question)->asCorrect()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => sprintf(
                    "El desafío no se pudo publicar porque la pregunta \"$label\" requiere una (1) opción de respuesta correcta. Se encontraron 2 opciones de respuesta correctas."
                ),
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_cannot_publish_a_challenge_if_at_least_one_multiple_question_has_not_at_least_two_correct_options()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();
        $question = Question::factory()
            ->for($challenge)
            ->typeMultiple()
            ->set('label', $label = '¿Preguna de prueba?')
            ->create();
        QuestionOption::factory()->for($question)->asCorrect()->create();
        QuestionOption::factory()->count(2)->for($question)->asIncorrect()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.publish', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => sprintf(
                    "El desafío no se pudo publicar porque la pregunta \"$label\" requiere dos o más opciones de respuesta correctas."
                ),
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_can_move_to_draft_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->published()->create();

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.move_to_draft', $challenge), $challengeData, self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $challenge->refresh();

        $this->assertEquals($challengeData['name'], $challenge->name);
        $this->assertEquals(ChallengeStatus::draft->value, $challenge->status);
        $this->assertNull($challenge->published_at);
    }

    public function test_the_user_cannot_move_to_draft_a_challenge_if_has_user_attempts()
    {
        /** @var User */
        $user = User::factory()->create();
        $userChallengeAttempt = UserChallengeAttempt::factory()->create();
        $challenge = $userChallengeAttempt->userChallenge->challenge;

        $challengeData = [
            'live_lesson_id' => $challenge->live_lesson_id,
            'name' => 'Desafío de prueba',
            'teacher_name' => 'Profesor de prueba',
        ];

        $this->actingAs($user)
            ->putJson(route('panel.challenges.move_to_draft', $challenge), $challengeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertJson([
                'message' => sprintf(
                    'El desafío no se pudo mover a borrador porque ya hay estudiantes inscritos en el.'
                ),
                'status' => 422,
            ]);

        $challenge->refresh();

        $this->assertNotEquals($challengeData['name'], $challenge->name);
        $this->assertEquals(ChallengeStatus::published->value, $challenge->status);
        $this->assertNotNull($challenge->published_at);
    }

    public function test_the_user_can_delete_a_challenge()
    {
        /** @var User */
        $user = User::factory()->create();
        $challenge = Challenge::factory()->create();

        $this->actingAs($user)
            ->deleteJson(route('panel.challenges.destroy', $challenge), [], self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertSoftDeleted($challenge);
    }
}
