<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\UserTypes;
use App\Models\DocumentType;
use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class UserControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_the_users_view()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('panel.users.index'))
            ->assertOk()
            ->assertViewIs('panel.users');
    }

    public function test_the_user_can_fetch_all_users()
    {
        /** @var User */
        $user = User::factory()->create();

        User::factory()->count(2)->create();

        $this->actingAs($user)
            ->getJson(route('panel.users.list'), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_user_can_create_a_new_user()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var DocumentType */
        $documentType = DocumentType::factory()->create();

        $userData = [
            'name' => 'test name',
            'lastname' => 'test lastname',
            'document_type_id' => $documentType->id,
            'document_number' => '12345678',
            'email' => 'test@test.com',
            'position' => 'test position',
            'company' => 'test company',
            'active' => 1,
        ];

        Event::fake();

        $this
            ->actingAs($user)
            ->postJson(route('panel.users.store'), $userData, self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('users', array_merge($userData, ['user_type' => UserTypes::administrator->value]));

        $newUser = User::whereEmail('test@test.com')->first();
        Event::assertDispatched(Registered::class);
    }

    public function test_user_cannot_create_a_new_user()
    {
        /** @var User */
        $user = User::factory()->create();

        $userData = [
            'email' => 'test@test.com',
        ];

        $this
            ->actingAs($user)
            ->postJson(route('panel.users.store'), $userData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['name', 'lastname', 'document_type_id', 'document_number', 'position', 'company', 'active']);

        $this->assertDatabaseMissing('users', $userData);
    }

    public function test_the_user_can_edit_a_user()
    {
        /** @var User */
        $user = User::factory()->create();

        $testUser = User::factory()->create();

        $this->actingAs($user)
            ->get(route('panel.users.show', $testUser), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_user_can_update_a_user()
    {
        /** @var User */
        $user = User::factory()->create();
        $testUser = User::factory()->create();

        /** @var DocumentType */
        $documentType = DocumentType::factory()->create();

        $userData = [
            'name' => 'test name',
            'lastname' => 'test lastname',
            'document_type_id' => $documentType->id,
            'document_number' => '12345678',
            'email' => 'test@test.com',
            'position' => 'test position',
            'company' => 'test company',
            'active' => 0,
        ];

        $this->actingAs($user)
            ->putJson(
                uri: route('panel.users.update', ['user' => $testUser]),
                data: $userData,
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $testUser->refresh();

        $this->assertEquals($userData['name'], $testUser->name);
        $this->assertEquals($userData['lastname'], $testUser->lastname);
        $this->assertEquals($userData['document_type_id'], $testUser->document_type_id);
        $this->assertEquals($userData['document_number'], $testUser->document_number);
        $this->assertEquals($userData['email'], $testUser->email);
        $this->assertEquals($userData['position'], $testUser->position);
        $this->assertEquals($userData['company'], $testUser->company);
        $this->assertEquals($userData['active'], $testUser->active);
    }
}
