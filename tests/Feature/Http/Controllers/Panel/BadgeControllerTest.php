<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\BadgeTypes;
use App\Events\SyncBadges;
use App\Models\Badge;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class BadgeControllerTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_the_user_can_view_all_challenges()
    {
        /** @var User */
        $user = User::factory()->create();

        $this
            ->actingAs($user)
            ->get(route('panel.badges.index'))
            ->assertOk()
            ->assertViewIs('panel.badges');
    }

    public function test_the_user_can_fetch_all_badges()
    {
        /** @var User */
        $user = User::factory()->create();

        Badge::factory()->count(10)->create();

        $this->actingAs($user)
            ->getJson(route('panel.badges.list'), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_create_a_badge()
    {
        /** @var User */
        $user = User::factory()->create();

        $badgeData = [
            'name' => $this->faker->realTextBetween(3, 100),
            'description' => $this->faker->realTextBetween(10, 200),
            'type' => $this->faker->randomElement(BadgeTypes::cases())->value,
            'total' => $this->faker->numberBetween(1, 16777215),
            'status' => 1,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('badge.png', 980, 720),
        ];

        Storage::fake('public');
        Event::fake();

        $this->actingAs($user)
            ->postJson(route('panel.badges.store'), array_merge($badgeData, $image), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('badges', $badgeData);

        $badge = Badge::first();

        $this->assertFileExists($badge->getFirstMediaPath('image'));

        Event::assertDispatched(SyncBadges::class);
    }

    public function test_create_a_inactive_badge_do_not_dispatch_the_sync_badges_event()
    {
        /** @var User */
        $user = User::factory()->create();

        $badgeData = [
            'name' => $this->faker->realTextBetween(3, 100),
            'description' => $this->faker->realTextBetween(10, 200),
            'type' => $this->faker->randomElement(BadgeTypes::cases())->value,
            'total' => $this->faker->numberBetween(1, 16777215),
            'status' => 0,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('badge.png', 980, 720),
        ];

        Storage::fake('public');
        Event::fake();

        $this->actingAs($user)
            ->postJson(route('panel.badges.store'), array_merge($badgeData, $image), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('badges', $badgeData);

        $badge = Badge::first();

        $this->assertFileExists($badge->getFirstMediaPath('image'));

        Event::assertNotDispatched(SyncBadges::class);
    }

    public function test_the_user_cannot_create_a_badge()
    {
        /** @var User */
        $user = User::factory()->create();

        $badgeData = [
            'name' => $this->faker->realTextBetween(),
            'description' => $this->faker->text(9),
            'type' => 'Invalid type',
            'total' => 0,
            'status' => 'Inactivo',
        ];

        Event::fake();

        $this->actingAs($user)
            ->postJson(route('panel.badges.store'), $badgeData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['name', 'description', 'type', 'total', 'status', 'image']);

        $this->assertDatabaseMissing('badges', $badgeData);

        Event::assertNotDispatched(SyncBadges::class);
    }

    public function test_the_user_can_edit_a_badge()
    {
        /** @var User */
        $user = User::factory()->create();

        $badge = Badge::factory()->create();

        $this->actingAs($user)
            ->getJson(route('panel.badges.show', $badge), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_the_user_can_update_a_badge()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->create();
        $badge = Badge::factory()->create();
        $badge->addMedia(UploadedFile::fake()->image('badge.png', 980, 720)->size('1000'))
            ->toMediaCollection('image');

        $previousImagePath = $badge->getFirstMediaPath('image');

        $badgeData = [
            'name' => $this->faker->realTextBetween(3, 100),
            'description' => $this->faker->realTextBetween(10, 200),
            'type' => $this->faker->randomElement(BadgeTypes::cases())->value,
            'total' => $this->faker->numberBetween(1, 16777215),
            'status' => 1,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('new badge.png', 980, 720),
        ];

        Event::fakeFor(function () use ($user, $badge, $badgeData, $image) {
            $this->actingAs($user)
                ->putJson(route('panel.badges.update', $badge), array_merge($badgeData, $image), self::$HEADERS)
                ->assertOk()
                ->assertJsonStructure(['message', 'status', 'data']);
        }, [SyncBadges::class]);

        $badge->refresh();

        $this->assertEquals($badgeData['name'], $badge->name);
        $this->assertEquals($badgeData['description'], $badge->description);
        $this->assertEquals(BadgeTypes::from($badgeData['type']), $badge->type);
        $this->assertEquals($badgeData['total'], $badge->total);
        $this->assertEquals($badgeData['status'], $badge->status);

        $this->assertFileDoesNotExist($previousImagePath);
        $this->assertFileExists($badge->getFirstMediaPath('image'));
    }

    public function test_update_a_badge_as_disabled_do_not_dispatch_the_sync_badge_event()
    {
        Storage::fake('public');

        /** @var User */
        $user = User::factory()->create();
        $badge = Badge::factory()->create();
        $badge->addMedia(UploadedFile::fake()->image('badge.png', 980, 720)->size('1000'))
            ->toMediaCollection('image');

        $previousImagePath = $badge->getFirstMediaPath('image');

        $badgeData = [
            'name' => $this->faker->realTextBetween(3, 100),
            'description' => $this->faker->realTextBetween(10, 200),
            'type' => $this->faker->randomElement(BadgeTypes::cases())->value,
            'total' => $this->faker->numberBetween(1, 16777215),
            'status' => 0,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('new badge.png', 980, 720),
        ];

        Event::fake(SyncBadges::class);

        $this->actingAs($user)
            ->putJson(route('panel.badges.update', $badge), array_merge($badgeData, $image), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $badge->refresh();

        $this->assertEquals($badgeData['name'], $badge->name);
        $this->assertEquals($badgeData['description'], $badge->description);
        $this->assertEquals(BadgeTypes::from($badgeData['type']), $badge->type);
        $this->assertEquals($badgeData['total'], $badge->total);
        $this->assertEquals($badgeData['status'], $badge->status);

        $this->assertFileDoesNotExist($previousImagePath);
        $this->assertFileExists($badge->getFirstMediaPath('image'));

        Event::assertNotDispatched(SyncBadges::class);
    }
}
