<?php

namespace Tests\Feature\Http\Controllers\Panel;

use App\Enums\ChallengeQuestionTypes;
use App\Models\Challenge;
use App\Models\Question;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class QuestionControllerTest extends TestCase
{
    use RefreshDatabase;

    public static $HEADERS = [
        'X-Requested-With' => 'XMLHttpRequest',
    ];

    public function test_el_usuario_puede_consultar_todas_las_preguntas_de_un_desafio()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        Question::factory()->for($challenge)->count(3)->create();

        $this->actingAs($user)
            ->getJson(route('panel.challenges.questions.index', $challenge), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_el_usuario_puede_crear_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        $questionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'points' => 50,
            'type' => ChallengeQuestionTypes::unique->value,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('image.jpg', 720, 480),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->postJson(route('panel.challenges.questions.store', $challenge), array_merge($questionData, $image), self::$HEADERS)
            ->assertCreated()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertDatabaseHas('questions', $questionData);
    }

    public function test_el_usuario_no_puede_crear_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        /** @var Challenge */
        $challenge = Challenge::factory()->create();

        $questionData = [
            'label' => '¿Lorem ipsum, dolor sit amet consectetur adipisicing elit?',
            'points' => 50,
        ];

        $this->actingAs($user)
            ->postJson(route('panel.challenges.questions.store', $challenge), $questionData, self::$HEADERS)
            ->assertUnprocessable()
            ->assertInvalid(['type']);

        $this->assertDatabaseMissing('questions', $questionData);
    }

    public function test_el_usuario_puede_editar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();

        $question = Question::factory()->create();

        /** @var Challenge */
        $challenge = $question->challenge;

        $this->actingAs($user)
            ->getJson(route('panel.challenges.questions.show', ['challenge' => $challenge, 'question' => $question]), self::$HEADERS)
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);
    }

    public function test_el_usuario_puede_actualizar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();
        $question = Question::factory()->create();
        $challenge = $question->challenge;

        $questionData = [
            'label' => '¿Pregunta de prueba?',
            'points' => 500,
            'type' => ChallengeQuestionTypes::multiple->value,
        ];

        $image = [
            'image' => UploadedFile::fake()->image('image.png', 980, 720),
        ];

        Storage::fake('public');

        $this->actingAs($user)
            ->putJson(
                uri: route('panel.challenges.questions.update', ['challenge' => $challenge->id, 'question' => $question->id]),
                data: array_merge($questionData, $image),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $question->refresh();

        $this->assertEquals($questionData['label'], $question->label);
        $this->assertEquals($questionData['points'], $question->points);
        $this->assertEquals($questionData['type'], $question->type);
    }

    public function test_el_usuario_puede_eliminar_una_pregunta()
    {
        /** @var User */
        $user = User::factory()->create();
        $question = Question::factory()->create();
        $challenge = $question->challenge;

        $this->actingAs($user)
            ->deleteJson(
                uri: route(
                    name: 'panel.challenges.questions.destroy',
                    parameters: ['challenge' => $challenge, 'question' => $question]
                ),
                headers: self::$HEADERS
            )
            ->assertOk()
            ->assertJsonStructure(['message', 'status', 'data']);

        $this->assertModelMissing($question);
    }
}
