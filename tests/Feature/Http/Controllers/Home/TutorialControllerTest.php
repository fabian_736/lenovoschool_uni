<?php

namespace Tests\Feature\Http\Controllers\Home;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class TutorialControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_tutorial()
    {
        /** @var User */
        $user = User::factory()->asStudent()->withoutTutorial()->create();

        $this
            ->actingAs($user)
            ->get(route('tutorial.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.tutorial.index');
    }

    public function test_user_can_finish_the_tutorial()
    {
        /** @var User */
        $user = User::factory()->asStudent()->withoutTutorial()->create();

        $this
            ->actingAs($user)
            ->post(route('tutorial.finish'))
            ->assertRedirect(route('portal.index'));
    }
}
