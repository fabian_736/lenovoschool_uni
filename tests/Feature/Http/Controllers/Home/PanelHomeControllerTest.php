<?php

namespace Tests\Feature\Http\Controllers\Home;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PanelHomeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_user_can_view_the_panel_index()
    {
        /** @var User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('panel.index'))
            ->assertOk()
            ->assertViewIs('panel.home.index');
    }
}
