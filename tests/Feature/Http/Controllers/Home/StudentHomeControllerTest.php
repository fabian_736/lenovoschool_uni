<?php

namespace Tests\Feature\Http\Controllers\Home;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class StudentHomeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_user_can_view_the_student_home_page()
    {
        /** @var User */
        $user = User::factory()->asStudent()->create();

        Storage::fake('public');

        Storage::disk('public')->putFile('announcements', UploadedFile::fake()->image('announcement.jpg'));

        $files = collect(Storage::disk('public')->files('announcements'))
            ->mapWithKeys(function ($file) {
                return [
                    File::basename($file) => Storage::disk('public')->url($file),
                ];
            });

        $this
            ->actingAs($user)
            ->get(route('portal.index'))
            ->assertOk()
            ->assertViewIs('modules.landing.home.index')
            ->assertViewHas('files', $files);
    }
}
