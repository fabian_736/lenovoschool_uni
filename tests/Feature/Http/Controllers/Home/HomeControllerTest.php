<?php

namespace Tests\Feature\Http\Controllers\Home;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HomeControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_super_administrator_user_is_redirected_to_panel_route()
    {
        /** @var \App\Models\User */
        $user = User::factory()->asSuperAdmin()->create();

        $this->actingAs($user)
            ->get(route('index'))
            ->assertRedirect(route('panel.index'));
    }

    public function test_the_administrator_user_is_redirected_to_panel_route()
    {
        /** @var \App\Models\User */
        $user = User::factory()->create();

        $this->actingAs($user)
            ->get(route('index'))
            ->assertRedirect(route('panel.index'));
    }

    public function test_the_student_user_is_redirected_to_portal_route()
    {
        /** @var \App\Models\User */
        $user = User::factory()->asStudent()->create();

        $this->actingAs($user)
            ->get(route('index'))
            ->assertRedirect(route('portal.index'));
    }
}
