<?php

namespace Tests\Feature\Http\Middleware;

use App\Http\Middleware\OnlyAjaxRequest;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Tests\TestCase;

class OnlyAjaxRequestTest extends TestCase
{
    use RefreshDatabase;

    public function test_system_throws_not_found_in_not_ajax_requests()
    {
        /** @var \App\Models\User */
        $user = User::factory()->create();

        $this->actingAs($user);

        $this->expectException(\Symfony\Component\HttpKernel\Exception\NotFoundHttpException::class);

        $request = Request::create(route('panel.live-lessons.list'), 'GET');

        $middleware = new OnlyAjaxRequest();

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response->getStatusCode(), Response::HTTP_NOT_FOUND);
    }

    public function test_system_dont_throws_not_found_in_ajax_requests()
    {
        /** @var \App\Models\User */
        $user = User::factory()->create();

        $this->actingAs($user);

        $request = Request::create(route('panel.live-lessons.list'), 'GET');
        $request->headers->set('X-Requested-With', 'XMLHttpRequest');

        $middleware = new OnlyAjaxRequest();

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals($response, null);
    }
}
