<?php

namespace Tests\Feature\Http\Middleware;

use App\Http\Middleware\TutorialMiddleware;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;

class TutorialMiddlewareTest extends TestCase
{
    use RefreshDatabase;

    public function test_redirects_the_user_to_the_tutorial()
    {
        /** @var \App\Models\User */
        $user = User::factory()->asStudent()->withoutTutorial()->create();

        $this->actingAs($user);

        $request = Request::create(route('portal.index'), 'GET');

        $middleware = new TutorialMiddleware();

        $response = $middleware->handle($request, function () {
        });

        $this->assertEquals('302', $response->getStatusCode());

        $this->assertEquals(route('tutorial.index'), $response->getTargetUrl());
    }
}
