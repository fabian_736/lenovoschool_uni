<?php

namespace Tests\Feature\Notifications\Challenges;

use App\Models\User;
use App\Models\UserChallenge;
use App\Notifications\Challenges\NewChallengeNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class NewChallengeNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $user = User::factory()->create();
        $userChallenge = UserChallenge::factory()->for($user)->create();

        $notification = new NewChallengeNotification($userChallenge);

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('Nuevo desafío disponible en Lenovo School', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        $this->assertStringContainsString(
            "Atrévete y participa superando el nuevo desaio <em>\"{$userChallenge->challenge->name}\"</em> que ya se encuentra listo en Lenovo School",
            $rendered
        );
        $this->assertStringContainsString(
            "Y si respondes correctamente podrás ganar hasta {$userChallenge->challenge->questions_sum_points} puntos para canjear los mejores premios.",
            $rendered
        );
    }

    public function test_the_database_notification_has_the_correct_content()
    {
        $user = User::factory()->create();
        $userChallenge = UserChallenge::factory()->for($user)->create();

        $notification = new NewChallengeNotification($userChallenge);

        $user->notify($notification);

        $arrayNotification = $notification->toDatabase($user);

        $notification = $user->notifications->first();

        $this->assertEquals($arrayNotification['main_text'], $notification->data['main_text']);
        $this->assertEquals($arrayNotification['image'], $notification->data['image']);
        $this->assertEquals($arrayNotification['url'], $notification->data['url']);
    }
}
