<?php

namespace Tests\Feature\Notifications\Rewards;

use App\Models\Reward;
use App\Models\User;
use App\Notifications\Rewards\RedeemedRewardNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class RedeemedRewardNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $user = User::factory()->create();

        $reward = Reward::factory()->create();

        $notification = new RedeemedRewardNotification($reward);

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('¡Felicidades! has redimido un premio', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        $this->assertStringContainsString(htmlspecialchars($user->name), $rendered);

        $this->assertStringContainsString("Canjeaste {$reward->redeem_points} puntos en <b>Lenovo School</b> para <br> redimir tu bono digital.", $rendered);

        $this->assertStringContainsString($reward->name, $rendered);
    }

    public function test_the_database_notification_has_the_correct_content()
    {
        $user = User::factory()->create();

        $reward = Reward::factory()->create();

        $notification = new RedeemedRewardNotification($reward);

        $user->notify($notification);

        $arrayNotification = $notification->toDatabase($user);

        $notification = $user->notifications->first();

        $this->assertEquals($arrayNotification['main_text'], $notification->data['main_text']);
        $this->assertEquals($arrayNotification['image'], $notification->data['image']);
        $this->assertEquals($arrayNotification['url'], $notification->data['url']);
    }
}
