<?php

namespace Tests\Feature\Notifications\Auth;

use App\Models\User;
use App\Notifications\Auth\ResetPasswordNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ResetPasswordNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $user = User::factory()->create();
        $token = 'test';

        $notification = new ResetPasswordNotification($token);

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('Solicitud de restablecimiento de contraseña', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        // This is the logic for create the reset link.
        // Was taken from \Illuminate\Auth\Notifications\ResetPassword::resetUrl()
        $url = url(route('password.reset', [
            'token' => $token,
            'email' => $user->getEmailForPasswordReset(),
        ], false));

        $this->assertStringContainsString(htmlspecialchars($url), $rendered);
    }
}
