<?php

namespace Tests\Feature\Notifications\Auth;

use App\Models\User;
use App\Notifications\Auth\WelcomeNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class WelcomeNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $user = User::factory()->create();

        $notification = new WelcomeNotification(now()->addDay());

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('¡Bienvenido a Lenovo School!', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        $this->assertStringContainsString(htmlspecialchars($user->name), $rendered);
        $this->assertStringContainsString(htmlspecialchars($notification->showWelcomeFormUrl), $rendered);
    }
}
