<?php

namespace Tests\Feature\Notifications\Points;

use App\Enums\PointTransactionTypes;
use App\Models\User;
use App\Notifications\Points\EarnedPointsNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EarnedPointsNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $user = User::factory()->create();

        $transaction = $user->pointTransaction()->create([
            'user_id' => $user->id,
            'type' => PointTransactionTypes::earnedPoints,
            'points' => 1000,
        ]);

        $notification = new EarnedPointsNotification($transaction);

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('¡Felicidades! Has ganado 1000 puntos', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        $this->assertStringContainsString(htmlspecialchars($user->name), $rendered);

        $this->assertStringContainsString('Ganaste 1000 puntos por completar tu registro', $rendered);

        $this->assertStringContainsString(
            'Ahora tienes 1000 puntos en tu cuenta. Úsalos para canjear los mejores premios',
            $rendered
        );
    }

    public function test_the_database_notification_has_the_correct_content()
    {
        $user = User::factory()->create();

        $transaction = $user->pointTransaction()->create([
            'user_id' => $user->id,
            'type' => PointTransactionTypes::earnedPoints,
            'points' => 1000,
        ]);

        $notification = new EarnedPointsNotification($transaction);

        $user->notify($notification);

        $arrayNotification = $notification->toDatabase($user);

        $notification = $user->notifications->first();

        $this->assertEquals($arrayNotification['main_text'], $notification->data['main_text']);
        $this->assertEquals($arrayNotification['image'], $notification->data['image']);
        $this->assertEquals($arrayNotification['url'], $notification->data['url']);
    }
}
