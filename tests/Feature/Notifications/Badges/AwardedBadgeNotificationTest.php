<?php

namespace Tests\Feature\Notifications\Badges;

use App\Models\Badge;
use App\Models\User;
use App\Notifications\Badges\AwardedBadgeNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AwardedBadgeNotificationTest extends TestCase
{
    use RefreshDatabase;

    public function test_the_mail_has_the_correct_content()
    {
        $badge = Badge::factory()->create();
        $user = User::factory()->create();

        $notification = new AwardedBadgeNotification($badge);

        $arrayNotification = $notification->toMail($user)->toArray();

        $this->assertEquals('¡Felicidades! Has ganado una insignia', $arrayNotification['subject']);

        $rendered = $notification->toMail($user)->render();

        $this->assertStringContainsString(htmlspecialchars($user->name), $rendered);
        $this->assertStringContainsString(htmlspecialchars($badge->name), $rendered);
    }

    public function test_the_database_notification_has_the_correct_content()
    {
        $badge = Badge::factory()->create();
        $user = User::factory()->create();

        $notification = new AwardedBadgeNotification($badge);

        $user->notify($notification);

        $arrayNotification = $notification->toDatabase($user);

        $notification = $user->notifications->first();

        $this->assertEquals($arrayNotification['main_text'], $notification->data['main_text']);
        $this->assertEquals($arrayNotification['image'], $notification->data['image']);
        $this->assertEquals($arrayNotification['url'], $notification->data['url']);
    }
}
