@extends('layouts.auth.app')
@section('content')
    <div class="container " id="container_auth">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-8 mr-autop-3 rounded bg-white" id="card_auth">
                <form action="{{ route('login') }}" method="POST">
                    @csrf
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center pt-5 pb-3">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="" class="w-50">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3">
                            <label for="" class="h3 text-success font-weight-bold">Bienvenido</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3">
                            @if (session('registerStatus'))
                            <div class="row">
                                <div class="col">
                                    <div class="alert alert-success">
                                        {{ session('registerStatus') }}
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                        <div class="col mb-3">
                            @error('email')
                                <div class="row">
                                    <div class="col text-center">
                                        <p class="text-danger font-weight-bold">{{ $message }}</p>
                                    </div>
                                </div>
                            @enderror
                            <div class="input-group rounded" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="clarity:email-solid"></span>
                                    </span>
                                </div>
                                <input type="email" name="email" value="{{ old('email') }}"
                                    class="form-control " placeholder="Correo electrónico">
                            </div>
                        </div>
                        <div class="col mb-4">
                            @error('password')
                                <div class="row">
                                    <div class="col-12 text-center">
                                        <p class="text-danger font-weight-bold">{{ $message }}</p>
                                    </div>
                                </div>
                            @enderror
                            <div class="input-group rounded" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="bxs:lock"></span>
                                    </span>
                                </div>
                                <input type="password" name="password" class="form-control"
                                    placeholder="Contraseña">
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-success rounded w-100">INGRESAR</button>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3 ">
                            <a href="{{ route('register') }}" class="btn btn-white text-success border border-success rounded w-100">CREAR CUENTA</a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3 ">
                            <a href="{{ route('password.request') }}">
                                <span class="text-success font-weight-bold">Olvidaste tu contraseña</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
