@extends('layouts.auth.app')
@section('content')
<div class="container" id="container_auth">
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-8 mr-autop-3 rounded register py-4 bg-white" id="card_auth">
            <form action="{{ route('register') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center pb-3">
                        <img src="{{ asset('assets/img/logo.png') }}" alt="" class="w-50">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center">
                        <label for="" class="lead text-success font-weight-bold">Datos adicionales</label>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center mb-3">
                        @if (session('registerStatus'))
                        <div class="col-10 mx-auto">
                            <div class="alert alert-success">
                                {{ session('registerStatus') }}
                            </div>
                        </div>
                        @endif
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('name') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="fa:address-book"></span>
                                </span>
                            </div>
                            <input type="text" name="name"
                                class="form-control @error('name') border border-danger @enderror" placeholder="Nombres"
                                value="{{ old('name') }}" autocomplete="off" required>
                        </div>
                        @error('name')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('lastname') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="fa:address-book"></span>
                                </span>
                            </div>
                            <input type="text" name="lastname"
                                class="form-control @error('lastname') border border-danger @enderror"
                                placeholder="Apellidos" value="{{ old('lastname') }}" autocomplete="off" required>
                        </div>
                        @error('lastname')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="bxs:id-card"></span>
                                </span>
                            </div>
                            <select class="custom-select form-control" name="document_type_id" autocomplete="off"
                                required>
                                <option value="" disabled @selected(empty(old('document_type_id')))>Seleccione una
                                    opción</option>
                                @foreach ($documentTypes as $documentType)
                                <option value="{{ $documentType->id }}"
                                    @selected(old('document_type_id')==$documentType->id)>{{ $documentType->name }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="row">
                            <div class="col">
                                <span class="text-danger font-weight-bold">
                                    @error('document_type_id')
                                    {{ $message }}
                                    @enderror
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('document_number') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="bxs:id-card"></span>
                                </span>
                            </div>
                            <input type="text" name="document_number"
                                class="form-control @error('document_number') border border-danger @enderror"
                                placeholder="Número de documento" value="{{ old('document_number') }}"
                                autocomplete="off" required>
                        </div>
                        @error('document_number')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('cellphone_number') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="bxs:phone-call"></span>
                                </span>
                            </div>
                            <input type="number" name="cellphone_number"
                                class="form-control @error('cellphone_number') border border-danger @enderror"
                                placeholder="Télefono" value="{{ old('cellphone_number') }}" autocomplete="off"
                                required>
                        </div>
                        @error('cellphone_number')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('email') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="clarity:email-solid"></span>
                                </span>
                            </div>
                            <input type="email" name="email"
                                class="form-control @error('email') border border-danger @enderror"
                                placeholder="Correo electronico" value="{{ old('email') }}" autocomplete="off" required>
                        </div>
                        @error('email')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('position') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="material-symbols:engineering"></span>
                                </span>
                            </div>

                            <select class="custom-select form-control @error('position') border border-danger @enderror" name="position" required autocomplete="off">
                                <option value="Gerente" @if(old('position') == "Gerente") selected @endif>Gerente</option>
                                <option value="Accionista" @if(old('position') == "Accionista") selected @endif>Accionista</option>
                                <option value="Ejecutivo de cuenta" @if(old('position') == "Ejecutivo de cuenta") selected @endif>Ejecutivo de cuenta</option>
                            </select>
                        </div>
                        @error('position')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('company') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="carbon:enterprise"></span>
                                </span>
                            </div>
                            <input type="text" name="company"
                                class="form-control @error('company') border border-danger @enderror"
                                placeholder="Empresa a la que pertenece" value="{{ old('company') }}" autocomplete="off"
                                required>
                        </div>
                        @error('company')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col mb-3">
                        <div class="input-group rounded">
                            <div class="input-group-prepend @error('date_of_birth') border border-danger @enderror">
                                <span class="input-group-text">
                                    <span class="iconify" data-icon="carbon:enterprise"></span>
                                </span>
                            </div>
                            <input type="date" name="date_of_birth"
                                class="form-control @error('date_of_birth') border border-danger @enderror"
                                placeholder="Fecha de nacimiento" value="{{ old('date_of_birth') }}" autocomplete="off"
                                required>
                        </div>
                        @error('date_of_birth')
                        <div class="invalid-feedback d-block">
                            {{ $message }}
                        </div>
                        @enderror
                    </div>
                    <div class="col">
                        @error('file')
                        <p style="color: red">{{ $message }}</p>
                        @enderror
                        <div class="row">
                            <div class="col">
                                <label for="" class="text-dark" id="change_file"></label>
                            </div>
                        </div>
                        <div class="fileinput fileinput-new text-center m-0 p-0" data-provides="fileinput">
                            <span
                                class="btn btn-default btn-file btn-block d-flex justify-content-center align-items-center">
                                <span class="iconify mr-2" data-icon="bxs:camera" data-width="20"></span>
                                <span class="fileinput-new">Subir foto</span>
                                <span class="fileinput-exists">Cambiar foto</span>
                                <input type="file" name="file" id="file_register" accept="image/png,image/jpeg"
                                    autocomplete="off">
                            </span>
                        </div>
                    </div>
                    <script>
                        document.getElementById('file_register').onchange = function () {
                            console.log(this.value);
                            document.getElementById('change_file').innerHTML = '<b>Archivo seleccionado:</b> ' +
                                document.getElementById('file_register').files[0].name;
                        }

                    </script>

                    <div class="col d-flex justify-content-center align-items-center">
                        <button type="submit" class="btn btn-success rounded w-100">REGISTRAR</button>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center mb-3">
                        <a href="{{ route('login') }}">
                            <span class="text-success font-weight-bold">Volver al login</span>
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
