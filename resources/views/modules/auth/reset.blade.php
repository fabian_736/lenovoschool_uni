@extends('layouts.auth.app')
@section('content')
    <div class="container " id="container_auth">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-8 mr-autop-3 rounded bg-white" id="card_auth">
                <form action="{{ route('password.update') }}" method="POST">
                    @csrf


                    <input type="hidden" name="token" value="{{ request()->token }}">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center pt-5 pb-3">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="" class="w-50">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="h3 text-success font-weight-bold">Reestablece tu contraseña</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3">
                            <label for="" class="text-dark text-center">Ingresa una combinación de 8 carácteres mínimo.</label>
                        </div>
                        <div class="col mb-3">
                            <div class="input-group rounded" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="clarity:email-solid"></span>
                                    </span>
                                </div>
                                <input id="email" type="email" class="form-control"
                                    name="email" value="{{ $email ?? old('email') }}" required autocomplete="email"
                                    autofocus placeholder="Correo electronico">
                            </div>
                            @error('email')
                                <div class="row">
                                    <div class="col">
                                        <p class="text-danger font-weight-bold">{{ $message }}</p>
                                    </div>
                                </div>
                            @enderror
                        </div>
                        <div class="col mb-3">
                            <div class="input-group rounded" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="bxs:lock"></span>
                                    </span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control" name="password" required
                                    autocomplete="new-password" placeholder="Contraseña">
                            </div>
                            @error('password')
                                <div class="row">
                                    <div class="col">
                                        <p class="text-danger font-weight-bold">{{ $message }}</p>
                                    </div>
                                </div>
                            @enderror
                        </div>
                        <div class="col mb-4">
                            <div class="input-group rounded" >
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="bxs:lock"></span>
                                    </span>
                                </div>
                                <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password" placeholder="Repite contraseña">
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-success rounded w-100">RESTABLECER CONTRASEÑA</button>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3 ">
                            <a href="{{ URL::to('/') }}">
                                <span class="text-success font-weight-bold">Volver al login</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
