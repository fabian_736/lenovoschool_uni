@extends('layouts.app')
@push('scripts')
<script src="{{ mix('js/portal/profile.js') }}" defer></script>
@endpush
@section('content')
@include('layouts.utils.canvas.index')

<div style="background-image:url({{asset('assets/img/background_profile.png')}})" class="p-4 rounded">

    <div class="bg-white">
        <div class="row p-3 align-items-center justify-content-center">
            <div class="col-3 align-self-center">

                <div class="position-relative text-center" style="max-width: 200px;height: 200px">


                    <img src="{{ auth()->user()->getFirstMediaUrl('avatar') }}"
                        class="border border-5 rounded-circle border-success w-100 h-100" alt=""
                         id="current-profile-image">

                         <button type="button" id="show-profile-image-modal"
                         class="btn btn-white border border-primary my-3 font-weight-bold text-primary rounded">Editar
                         foto</button>

                    <span class="position-absolute" style="bottom:50px;right:0px">
                        <span class="bg-warning rounded-circle p-2">
                            {{$user->rank}}
                        </span>
                    </span>
                </div>

            </div>
            <div class="col-lg-6 col-md-12">
                @include('modules.landing.profile.form_date')
            </div>
            <div class="col-lg-3 col-md-12 align-self-start py-4">

                <div class="border rounded border-primary py-2 px-3">
                    <span class="text-primary fw-bold">Posición</span>
                    <div class="bg-primary p-2 text-white rounded mb-3">
                        Posición ranking general: {{$user->rank}}
                    </div>

                    <span class="text-primary fw-bold">Mi avance</span>
                    <div class="progress rounded m-0" style="height: 40px">
                        <div class="progress-bar" role="progressbar" aria-label="Basic example"
                            style="width: {{$user->total_progress}}%" aria-valuenow="{{$user->total_progress}}"
                            aria-valuemin="1" aria-valuemax="100"><span
                                class="font-weight-bold">{{$user->total_progress}}%</span></div>
                    </div>
                </div>


            </div>
        </div>

        <div class="d-flex justify-content-end p-3">

            <button id="show-update-password-modal" type="button"
                class="btn btn-white border border-primary text-primary rounded-pill mx-2">Cambiar
                contraseña</button>
        </div>

    </div>
</div>

@include('layouts.utils.modal.profile.index')
@endsection
