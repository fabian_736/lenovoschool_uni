<form action="#" method="post" id="user-profile-form">
    <div class="row">
        <div class="col-6">
            <div class="form-floating mb-3">
                <label for="name">Nombres:</label>
                <input type="text" class="form-control rounded input_form" id="name" placeholder="Nombre" name="name"
                    value="{{ $user->name }}" required disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="lastname">Apellidos:</label>
                <input type="text" class="form-control rounded input_form" placeholder="Apellido" name="lastname"
                    value="{{ $user->lastname }}" required id="lastname" disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="document_type">Tipo de documento</label>
                <select class="form-select custom-select rounded w-100 p-1 border-secondary" disabled
                    style="--bs-border-opacity: .9;" id="document_type" name="document_type_id" autocomplete="off"
                    required>
                    <option value="" disabled @selected(empty($user->document_type_id))>Seleccione una opción</option>
                    @foreach ($documentTypes as $documentType)
                    <option value="{{ $documentType->id }}" @selected($user->document_type_id ==
                        $documentType->id)>{{ $documentType->name }}</option>
                    @endforeach
                </select>
                <span class="invalid-feedback"></span>
            </div>
            <div class="form-floating mb-3">
                <label for="document_number">Número de documento:</label>
                <input type="number" class="form-control rounded input_form" placeholder="Número de documento"
                    name="document_number" value="{{ $user->document_number }}" required id="document_number" disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="email">Correo electronico:</label>
                <input type="email" class="form-control rounded input_form" name="email"
                    placeholder="Correo electrónico" value="{{ $user->email }}" required id="email" disabled>
                <span class="invalid-feedback"></span>
            </div>

        </div>
        <div class="col-6">



            <div class="form-floating mb-3">
                <label for="phone">Celular:</label>
                <input type="number" class="form-control rounded input_form" placeholder="Teléfono"
                    name="cellphone_number" value="{{ $user->cellphone_number }}" required id="phone" disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="company">Empresa a la que pertenece:</label>
                <input type="text" class="form-control rounded input_form" placeholder="Empresa" name="company"
                    value="{{ $user->company }}" required id="company" disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="position">Cargo:</label>
                <input type="text" class="form-control rounded input_form" placeholder="Cargo" name="position"
                    value="{{ $user->position }}" required id="position" disabled>
                <span class="invalid-feedback"></span>
            </div>

            <div class="form-floating mb-3">
                <label for="date_of_birth">Fecha de nacimiento:</label>
                <input type="date" class="form-control rounded input_form" placeholder="Fecha de nacimiento" name="date_of_birth"
                    value="{{ $user->date_of_birth }}" required id="date_of_birth" disabled>
                <span class="invalid-feedback"></span>
            </div>

        </div>
    </div>
</form>


{{-- <form action="#" method="post" id="user-profile-form">
    <div class="row-reverse">
        <div class="col mb-2">
            <div class="input-group">
                <div class="input-group-prepend">
                    <span class="input-group-text">
                        <span class="iconify" data-icon="bxs:user-detail" data-width="30"></span>
                    </span>
                </div>
                <input type="text" class="form-control input_form" placeholder="Nombre" name="name"
                    value="{{ $user->name }}" required>
<span class="invalid-feedback"></span>
</div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="bxs:user-detail" data-width="30"></span>
            </span>
        </div>
        <input type="text" class="form-control input_form" placeholder="Apellido" name="lastname"
            value="{{ $user->lastname }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="bxs:id-card" data-width="30"></span>
            </span>
        </div>
        <select class="custom-select form-control" name="document_type_id" autocomplete="off" required>
            <option value="" disabled @selected(empty($user->document_type_id))>Seleccione una opción</option>
            @foreach ($documentTypes as $documentType)
            <option value="{{ $documentType->id }}" @selected($user->document_type_id ==
                $documentType->id)>{{ $documentType->name }}</option>
            @endforeach
        </select>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="bxs:id-card" data-width="30"></span>
            </span>
        </div>
        <input type="number" class="form-control input_form" placeholder="Número de documento" name="document_number"
            value="{{ $user->document_number }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="carbon:phone-voice-filled" data-width="30"></span>
            </span>
        </div>
        <input type="number" class="form-control input_form" placeholder="Teléfono" name="cellphone_number"
            value="{{ $user->cellphone_number }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="bx:mail-send" data-width="30"></span>
            </span>
        </div>
        <input type="email" class="form-control input_form" name="email" placeholder="Correo electrónico"
            value="{{ $user->email }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="ph:student-fill" data-width="30"></span>
            </span>
        </div>
        <input type="text" class="form-control input_form" placeholder="Cargo" name="position"
            value="{{ $user->position }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
<div class="col mb-2">
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
                <span class="iconify" data-icon="carbon:enterprise" data-width="30"></span>
            </span>
        </div>
        <input type="text" class="form-control input_form" placeholder="Empresa" name="company"
            value="{{ $user->company }}" required>
        <span class="invalid-feedback"></span>
    </div>
</div>
</div>
</form> --}}
