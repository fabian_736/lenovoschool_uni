@extends('layouts.app')
@section('content')
@include('layouts.utils.canvas.index')
    <div class="row-reverse">
        <div class="col mb-4">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">PREMIOS</label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">
                                Canjea tus puntos por maravillosos premios
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-auto d-flex justify-content-end">
                    <a href="{{ route('badges') }}" class="h3 font-weight-bold m-0" style="text-decoration: underline; color: #4C7421">Insignias</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card rounded" style="min-height: 60vh; max-height: 60vh; overflow: auto; overflow-x: hidden">
                <div id="rewards-container" class="card-body"></div>
            </div>
        </div>
    </div>

    <template id="reward-template">
        <div class="col">
            <div style="min-height: 500px; max-height: 500px; min-width: 300px; max-width: 300px; filter: drop-shadow(0px 4px 8px rgba(0, 0, 0, 0.17)); background: url('/assets/premios/ticket.png'); background-size: cover">
                <div class="row-reverse pb-2">
                    <div class="col p-3">
                        <div id="container" style=" width:100%;
                    height:250px;">
                            <img class="d-block w-100 h-100 reward-image" src="" />
                        </div>
                    </div>
                    <div class="col mt-4">
                        <div class="row-reverse">
                            <div class="col d-flex justify-content-center">
                                <label for="" class="text-dark text-center font-weight-bold reward-name"></label>
                            </div>
                            <div class="col d-flex justify-content-center">
                                <label for="" class="lead text-dark font-weight-bold reward-points"></label>
                            </div>
                            <div class="col d-flex justify-content-center">
                                <button data-id="" class="btn btn-success border-radius-10 redeem-button">REDIMIR</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
    @include('layouts.utils.modal.reward.index')
    @push('scripts')
        <script src="{{ mix('js/portal/redemptions/index.js') }}" defer></script>
    @endpush
@endsection
