@extends('layouts.app')
@section('content')

    <div class="row-reverse">
        <div class="col mb-4">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <span class="h3 m-0 font-weight-bold">REDENCIÓN PREMIO</span>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <span class="lead">Para concluir la redención de tu premio llena estos campos:</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col p-0">
            <div class="row mr-0">
                <div class="col-3 card card-body mx-auto card_one">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center  my-5 card_title">
                            <span class="h3 font-weight-bold text-center">{{ $reward->name }}</span>
                        </div>
                        <div class="col d-flex justify-content-center">
                            <img src="{{ $reward->getFirstMediaUrl('image') }}" class="img-fluid" alt="">
                        </div>
                    </div>
                </div>
                <div class="col-8 card card-body mx-auto px-5 card_two">
                    <span class="h3 my-5 title_form card_title_two font-weight-bold">Formulario</span>
                    <form action="{{ route('rewards.redeem', ['reward' => $reward]) }}" method="post" id="redemption-form">
                        @csrf
                        <div class="row-reverse">
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="heroicons-outline:identification" data-width="30"></span>
                                        </span>
                                    </div>
                                    <select name="receiver_document_type_id" id="receiver_document_type_id" class="form-control cursor-pointer">
                                        <option value="" selected disabled>Tipo de identificación</option>
                                        @foreach ($documentTypes as $documentType)
                                            <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="fluent:book-number-16-filled" data-width="30"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="receiver_document_number" id="receiver_document_number" placeholder="Número de identificación">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="bxs:user-detail" data-width="30"></span>
                                        </span>
                                    </div>
                                    <input type="text" name="receiver_name" id="receiver_name" class="form-control" placeholder="Nombre completo">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="arcticons:city-transit" data-width="30"></span>
                                        </span>
                                    </div>
                                    <select name="receiver_departament_id" id="receiver_departament_id" class="form-control cursor-pointer">
                                        <option value="" selected disabled>Departamento de entrega</option>
                                        @foreach ($departaments as $departament)
                                            <option value="{{ $departament->id }}">{{ $departament->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="bxs:map-pin" data-width="30"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control " name="receiver_address" id="receiver_address" placeholder="Dirección">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="bx:mail-send" data-width="30"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control " name="receiver_email" id="receiver_email" placeholder="Correo electrónico">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col mb-2">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <span class="iconify" data-icon="carbon:phone-voice-filled" data-width="30"></span>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control" name="receiver_phone" id="receiver_phone" placeholder="Teléfono">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col-6 offset-6 mb-2">
                                <button class="btn btn-success btn-block" id="button_submit">Redimir</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="{{ mix('js/portal/redemptions/redemption.js') }}" defer></script>
    @endpush
@endsection
