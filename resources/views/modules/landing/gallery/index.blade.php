@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col">
            <div class="row-reverse">
                <div class="col p-0">
                    <div class="row mx-auto">
                        <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                            <a href="{{ url()->previous() }}" class="mt-1">
                                <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25"
                                    style="color: #7DBE38"></i>
                            </a>
                        </div>
                        <div class="col p-0">
                            <label for="" class="h3 m-0">GALERÍA <b class="font-weight-bold">LENOVO</b></label>
                        </div>
                    </div>
                </div>
                <div class="col p-0">
                    <label for="" class="lead">Repasa y comparte con otros vededores tus opiniones, dudas y/o tips
                        sobre lo aprendido.</label>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                @forelse($gallery as $video)
                    <div class="col-4">
                        <a href="{{ route('gallery.view_video', $video) }}">
                            <div class="card" id="challenge_card"
                                style="background-image: url('{{ $video->getFirstMediaUrl('preview') }}'); background-size: cover;">
                                <div class="card-body d-flex justify-content-center align-items-end">
                                    <div class="row-reverse">
                                        <div class="col d-flex justify-content-center">
                                            <label for=""
                                                class="lead text-white font-weight-bold">{{ $video->title }}</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                @empty
                    <!-- Modal ¿VISUAL EN PROCESO? -->
                    <div class="modal fade" id="empty" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalCenterTitle" aria-hidden="" data-backdrop="static"
                        data-keyboard="false">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content backgroundModal">
                                <div class="modal-body">
                                    <div class="row-reverse">
                                        <div class="col d-flex justify-content-end align-items-end">
                                            <a href="{{ route('portal.index') }}">
                                                <i class="far fa-times-circle fa-2x"></i>
                                            </a>
                                        </div>
                                        <div class="col d-flex justify-content-center align-items-center my-5">
                                            <span class="iconify" data-icon="emojione:warning" data-width="100"></span>
                                        </div>
                                        <div class="col d-flex justify-content-center align-items-center px-5">
                                            <h3 class="text-center font-weight-bold">¡ATENCIÓN!</h3>
                                        </div>
                                        <div class="col d-flex justify-content-center align-items-center px-5">
                                            <hr class="text-secundary"
                                                style="border-width: 1px; border-style: solid; width: 100%">
                                        </div>
                                        <div class="col d-flex justify-content-center align-items-center px-5 mb-3">
                                            <span class="lead text-center font-weight-bold" style="color: black;">No hay
                                                videos disponibles <br> Vuelve mas tarde</span>
                                        </div>
                                        <div class="col d-flex justify-content-center align-items-center">
                                            <a href="{{ route('portal.index') }}"
                                                class="btn w-50 bg-white text-dark font-weight-bold font16"
                                                style="border: 2px solid #8246AF;">IR Al INICIO</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <script>
                        $('#empty').modal('show')
                    </script>
                @endforelse
            </div>
        </div>
    </div>


    <style>
        #challenge_card {
            transition: transform .2s;
            cursor: pointer;
            border-radius: 20px;
            min-height: 25vh;
            max-height: 25vh;
            background: linear-gradient(0deg, rgba(0, 0, 0, 0.6) 0%, rgba(0, 0, 0, 0) 100%), #C4C4C4;
        }

        #challenge_card:hover {
            transform: scale(1.05);
            border: solid 2px #7DBE38;
        }
    </style>
@endsection
