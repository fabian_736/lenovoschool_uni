@extends('layouts.app')
@section('content')

    <div class="row-reverse">
        <div class="col">
            <div class="row-reverse">
                <div class="col p-0">
                    <div class="row mx-auto">
                        <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                            <a href="{{ url()->previous() }}" class="mt-1">
                                <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25"
                                    style="color: #7DBE38"></i>
                            </a>
                        </div>
                        <div class="col p-0">
                            <label for="" class="h3 m-0 font-weight-bold">{{ $video->title }}</label>
                        </div>
                    </div>
                </div>
                <div class="col p-0">
                    <label for="" class="lead">{{ $video->description }}</label>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row mb-5">
                <div class="col-8">
                    <video controls poster="{{ $video->getFirstMediaUrl('preview') }}" width="100%" height="100%" style="max-height: 60vh">
                        <source src="{{ $video->getFirstMEdiaUrl('file') }}" type="video/mp4" />
                        Tu navegador no soporta video.
                    </video>

                    <div class="text-right">
                        <a class="btn btn-success" href="{{route('gallery.download_video',$video->id)}}">Descargar</a>
                    </div>
                </div>
                <div class="col-4 p-0">
                    <div class="card"
                        style="background: url('/assets/img/background_sidebar.png'); background-size: cover">
                        <div class="card-body p-0" style=" min-height: 60vh; max-height: 60vh">
                            <div class="row-reverse">
                                <div class="col p-3 rounded " style="background: #ffffff;">
                                    <form id="comment-form" class="row" method="post" action="#">
                                        <div class="col-10 col_box_text">
                                            <input type="text" name="comment" id="input-comment-form"  minlength="10" required class="form-control"
                                                placeholder="Escribe aquí tus preguntas u opiniones..." />
                                        </div>
                                        <button type="submit"
                                            class="btn btn-success btn-sm" id="button-submit-comment">
                                            Enviar
                                        </button>
                                    </form>
                                </div>
                                <div class="col" id="comments" style=" min-height: 40vh; max-height: 40vh; overflow: auto">
                                        <ul class=""></ul>
                                </div>
                                <div class="col">
                                    <template id="comments-template">
                                        <li class="row">
                                            <div class="col border-bottom py-2">
                                                <div class="row align-items-center">
                                                    <div class="col-3">
                                                        <div style="width: 55px; height: 55px"
                                                            class="bg-primary rounded-circle p-1 d-block m-auto">
                                                            <img class="w-100 h-100 rounded-circle"
                                                                src=""
                                                                alt="">
                                                        </div>
                                                    </div>
                                                    <div class="col-9">
                                                        <label for="" class="text-white d-block" id="comment-text"></label>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </template>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

        window.addEventListener("DOMContentLoaded", async () => {
            const getComments = async () => {
                const comments = await axios.get(
                        "{{ route('gallery.comments.index', ['video' => $video->id]) }}")
                    .then(response => response.data.data);

                const template = document.getElementById("comments-template");
                const list = document.createElement("ul");
                list.classList.add("pl-0");

                comments.forEach(comment => {
                    const li = template.content.cloneNode(true);

                    li.querySelector("img").src = comment.author.avatar_url;
                    li.getElementById("comment-text").textContent = comment.comment;


                    list.appendChild(li);
                });

                document.querySelector("#comments ul").replaceWith(list);
            }

            const commentForm = document.getElementById("comment-form");

            const createComment = e => {
                let input_form_comment = document.querySelector("#input-comment-form");
                let button_form_comment = document.querySelector("#button-submit-comment");

                button_form_comment.disabled = true;

                e.preventDefault();

                const createCommentData = {
                    comment: commentForm.elements["comment"].value
                };

                axios.post("{{ route('gallery.comments.store', ['video' => $video->id]) }}", createCommentData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });

                        const template = document.getElementById("comments-template");

                        const li = template.content.cloneNode(true);

                        li.querySelector("img").src = response.data.data.author.avatar_url;
                        li.getElementById("comment-text").textContent = response.data.data.comment;

                        list = document.querySelector("#comments ul");

                        list.insertBefore(li, list.firstChild);

                        input_form_comment.value = "";

                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(commentForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = commentForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });

                    setTimeout(function(){
            button_form_comment.disabled = false;
            }, 2000);

            }
            commentForm.addEventListener("submit", createComment);

            getComments();
        });
    </script>
@endsection
