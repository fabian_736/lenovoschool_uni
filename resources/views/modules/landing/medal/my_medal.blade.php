@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">MIS INSIGNIAS</label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">
                                Aquí podrás revisar todas tus medallas obtenidas en Lenovo School
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card rounded" style="min-height: 60vh; max-height: 60vh; overflow: auto">
                <div class="card-body ">
                    <div class="row-reverse">
                        <div class="col">
                            <div class="row">

                                @foreach(auth()->user()->badges as $badge)
                                <div class="col-6">
                                    <div class="card" style="background-color: #c4beb631">
                                        <div class="card-body">
                                            <div class="row">
                                                <div class="col-auto">
                                                    <div style="width: 75px; height: 75px" class="rounded-circle">
                                                        <img class="w-100 h-100 rounded-circle"
                                                            src="{{ $badge->getFirstMediaUrl('image') }}" alt="">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="row-reverse">
                                                        <div class="col">
                                                            <label for=""
                                                                class="lead text-dark font-weight-bold">{{$badge->name}}</label>
                                                        </div>
                                                        <div class="col">
                                                            <label for="" class="text-dark">{{$badge->description}}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
