<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/favicon.svg') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.svg') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Lenovo School
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fullcalendar@5.11.0/main.css"
        integrity="sha256-jLWPhwkAHq1rpueZOKALBno3eKP3m4IMB131kGhAlRQ=" crossorigin="anonymous">

    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>

    {{--Boostrap 5 --}}
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-gH2yIJqKdNHPEq0n4Mqa/HGKIhSkIHeL5AyhkYV8i59U5AR6csBvApHHNl/vI1Bx" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-A3rJD856KowSb7dwlZdYEkO39Gagi7vIsF0jrRAoQmDKKtQBHUuLZ9AsSv4jD4Xa" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.9.1/font/bootstrap-icons.css">



    @stack('scripts')
</head>

<style>
    body {
        background-image: url('{{asset('assets/img/Fondo Tutoriales 1.png')}}');
        background-position: center;
        background-repeat: no-repeat;
        background-size:100vw;
        background-attachment: fixed;
    }

    .btn-success{
        background-color: #7DBE38 !important;
    }

    .text-success{
        color: #7DBE38 !important;
    }

</style>


<div class="container">

    <div class="d-flex align-items-center justify-content-center vh-100">

            <div class="align-self-center bg-white rounded w-75 p-4">
                <div class=" d-flex align-items-center justify-content-center mb-3">

                        <img src="{{asset('assets/img/Icono lenovo school 1.svg')}}" width="60px" alt="Logo Lenovo School">
                        <span class="fs-3"><b>TUTORIAL</b> LENOVO SCHOOL</span>
                </div>


            <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="false">
                <div class="carousel-indicators">
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active"
                        aria-current="true" aria-label="Slide 1"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1"
                        aria-label="Slide 2"></button>
                    <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2"
                        aria-label="Slide 3"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3"
                        aria-label="Slide 4"></button>
                        <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4"
                        aria-label="Slide 5"></button>
                </div>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <img src="{{asset('assets/tutorial/1.png')}}" class="d-block" style="width: 100%; height:70vh" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5></h5>
                            <p></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('assets/tutorial/2.png')}}" class="d-block w-100" style="width: 100%; height:70vh" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5></h5>
                            <p></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('assets/tutorial/3.png')}}" class="d-block w-100" style="width: 100%; height:70vh" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5></h5>
                            <p></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('assets/tutorial/4.png')}}" class="d-block w-100" style="width: 100%; height:70vh" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5></h5>
                            <p></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{asset('assets/tutorial/5.png')}}" class="d-block w-100" style="width: 100%; height:70vh" alt="...">
                        <div class="carousel-caption d-none d-md-block">
                            <h5></h5>
                            <div class="text-end">
                                <form action="{{route('tutorial.finish')}}" method="POST">
                                @csrf
                                <button class="btn btn-success w-25" type="submit">Finalizar</button>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="prev" >
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions"
                    data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
        </div>


    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="videoTutorial" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
    <div class="modal-dialog modal-fullscreen">
        <div class="modal-content">

            <div class="modal-body position-relative">

                <div class="position-absolute end-0 px-5" style="z-index: 1">

                    <button type="button" id="closeVideo" class="bg-white border-0 display-4 text-secondary" disabled
                        data-bs-dismiss="modal" aria-label="Close"> <i
                            class="bi bi-arrow-right-circle-fill"></i></button>

                </div>



                <video autoplay muted controls class="w-100 h-100" id="video">
                    <source src="{{ asset('/assets/video/initial.mp4') }}">
                </video>

            </div>

        </div>
    </div>
</div>


<script>

    var modalVideo = new bootstrap.Modal('#videoTutorial', {
    keyboard: false
    })

    modalVideo.show();

    $('#closeVideo').click(function(){
    $('#video').trigger('pause');
      });


    setInterval('nextBoton()', 60000);

    function nextBoton() {

        $('#closeVideo').addClass('text-success');
        $('#closeVideo').removeAttr("disabled");
    }

</script>

</html>
