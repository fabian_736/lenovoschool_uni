@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col">
            <div class="row-reverse">
                <div class="col p-0">
                    <div class="row mx-auto">
                        <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                            <a href="{{ url()->previous() }}" class="mt-1">
                                <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25" style="color: #7DBE38"></i>
                            </a>
                        </div>
                        <div class="col p-0">
                            <span class="h3 m-0 font-weight-bold">RANKING</span>
                        </div>
                    </div>
                </div>
                <div class="col p-0">
                    <span class="lead">Logra estar entre los primeros lugares de la tabla general asistiendo a tus sesiones y completando los desafíos Lenovo.</span>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card p-3" style="background: url('/assets/img/background_profile.png'); background-size: cover">
                <div class="row-reverse">
                    <div class="col">
                        <div class="row">
                            <div class="col">
                                <div class="card-body bg-white rounded">
                                    <div class="row-reverse">
                                        <div class="col d-flex justify-content-center">
                                            <span class="h3 font-weight-bold text-center">PRIMEROS LUGARES TABLA GENERAL</span>
                                        </div>
                                        <div class="col-auto ">
                                            <ul class="list_ranking">
                                                @forelse ($generalRanking as $user)
                                                    @if ($user->rank <= 3)
                                                        <li class="row">
                                                            <div class="col p-0">
                                                                <div class="card p-3 m-0"
                                                                    style="border-radius: 20px; background: url('/assets/img/background_profile.png'); background-size: cover">
                                                                    <div class="card-body bg-white rounded">
                                                                        <div class="row" style="position: absolute; top: 0; right: 0">
                                                                            <div class="col">
                                                                                @switch($user->rank)
                                                                                    @case(1)
                                                                                        <span class="iconify" data-icon="noto:1st-place-medal" data-width="75"></span>
                                                                                    @break

                                                                                    @case(2)
                                                                                        <span class="iconify" data-icon="noto:2nd-place-medal" data-width="75"></span>
                                                                                    @break

                                                                                    @default
                                                                                        <span class="iconify" data-icon="noto:3rd-place-medal" data-width="75"></span>
                                                                                @endswitch
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-auto">
                                                                                <div style="width: 75px; height: 75px"
                                                                                    class="bg-primary rounded-circle p-1">
                                                                                    <img class="w-100 h-100 rounded-circle" src="{{ $user->getFirstMediaUrl('avatar') }}" alt="">
                                                                                </div>
                                                                            </div>
                                                                            <div class="col p-0 m-0 d-flex justiy-content-center align-items-center">
                                                                                <div class="row-reverse">
                                                                                    <div class="col">
                                                                                        <span class="lead text-dark font-weight-bold">{{ $user->fullname }}</span>
                                                                                    </div>
                                                                                    <div class="col">
                                                                                        <span class="h6 text-dark">{{ $user->position }}</span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    @endif
                                                    @empty
                                                        @include('layouts.utils.modal.challengue.empty')
                                                    @endforelse
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col ">
                                    <div class="card-body p-0 bg-white rounded">
                                        <div class="row-reverse">
                                            <div class="col p-0 rounded">
                                                <div class="card card-nav-tabs card-plain rounded">
                                                    <div class="card-header card-header-success">
                                                        <div class="nav-tabs-navigation">
                                                            <div class="nav-tabs-wrapper">
                                                                <ul class="nav nav-tabs" data-tabs="tabs">
                                                                    <li class="nav-item">
                                                                        <a class="nav-link active" href="#all" data-toggle="tab">General</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="#class" data-toggle="tab">Clases</a>
                                                                    </li>
                                                                    <li class="nav-item">
                                                                        <a class="nav-link" href="#challenge" data-toggle="tab">Desafios</a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body p-3" style="min-height: 50vh; max-height: 50vh; overflow: auto">
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="all">
                                                                <ul class="list_ranking">
                                                                    @foreach ($generalRanking as $user)
                                                                        <li class="row">
                                                                            <div class="col p-0">
                                                                                <div class="row" style="position: absolute; top: 0; right: 0">
                                                                                    <div class="col">
                                                                                        <div class="bg-secondary rounded-circle d-flex justify-content-center align-items-center" style="width:40px; height: 40px;">
                                                                                            <span class="m-0 text-white font-weight-bold">{{ $user->rank }}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-auto">
                                                                                        <div style="width: 75px; height: 75px" class="bg-primary rounded-circle p-1">
                                                                                            <img class="w-100 h-100 rounded-circle"
                                                                                                src="{{ $user->getFirstMediaUrl('avatar') }}"
                                                                                                alt="">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col p-0 m-0 d-flex justiy-content-center align-items-center">
                                                                                        <div class="row-reverse">
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="lead text-dark font-weight-bold">{{ $user->fullname }}</span>
                                                                                            </div>
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="h6 text-dark">{{ $user->position }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                            <div class="tab-pane" id="class">
                                                                <ul class="list_ranking">
                                                                    @foreach ($challengesRanking as $user)
                                                                        <li class="row">
                                                                            <div class="col p-0">
                                                                                <div class="row" style="position: absolute; top: 0; right: 0">
                                                                                    <div class="col">
                                                                                        <div class="bg-secondary rounded-circle d-flex justify-content-center align-items-center" style="width:40px; height: 40px;">
                                                                                            <span class="m-0 text-white font-weight-bold">{{ $user->rank }}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-auto">
                                                                                        <div style="width: 75px; height: 75px" class="bg-primary rounded-circle p-1">
                                                                                            <img class="w-100 h-100 rounded-circle"
                                                                                                src="{{ $user->getFirstMediaUrl('avatar') }}"
                                                                                                alt="">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col p-0 m-0 d-flex justiy-content-center align-items-center">
                                                                                        <div class="row-reverse">
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="lead text-dark font-weight-bold">{{ $user->fullname }}</span>
                                                                                            </div>
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="h6 text-dark">{{ $user->position }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                            <div class="tab-pane" id="challenge">
                                                                <ul class="list_ranking">
                                                                    @foreach ($liveLessonsRanking as $user)
                                                                        <li class="row">
                                                                            <div class="col p-0">
                                                                                <div class="row" style="position: absolute; top: 0; right: 0">
                                                                                    <div class="col">
                                                                                        <div class="bg-secondary rounded-circle d-flex justify-content-center align-items-center" style="width:40px; height: 40px;">
                                                                                            <span class="m-0 text-white font-weight-bold">{{ $user->rank }}</span>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="row">
                                                                                    <div class="col-auto">
                                                                                        <div style="width: 75px; height: 75px" class="bg-primary rounded-circle p-1">
                                                                                            <img class="w-100 h-100 rounded-circle"
                                                                                                src="{{ $user->getFirstMediaUrl('avatar') }}"
                                                                                                alt="">
                                                                                        </div>
                                                                                    </div>
                                                                                    <div
                                                                                        class="col p-0 m-0 d-flex justiy-content-center align-items-center">
                                                                                        <div class="row-reverse">
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="lead text-dark font-weight-bold">{{ $user->fullname }}</span>
                                                                                            </div>
                                                                                            <div class="col">
                                                                                                <span
                                                                                                    class="h6 text-dark">{{ $user->position }}</span>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    @endforeach
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .list_ranking {
                margin: 0 !important;
                padding: 0 !important;
            }

            .list_ranking li {
                list-style: none;
                margin: 10px 0px !important;
                padding: 0 !important;
            }
        </style>
    @endsection
