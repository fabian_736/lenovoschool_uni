@extends('layouts.app')
@section('content')
    <div class="row-reverse">
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">MIS <b
                                            class="font-weight-bold">NOTIFICACIONES</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">
                                Acá encontrarás todas las notificaciones que has tenido en el mes
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card" style="max-height: 60vh; overflow: auto">
                <div class="card-body shadow">
                    <ul class="ul_notification">
                        <li class="li_notification">
                            <div class="alert alert-success" role="alert">
                                <a href="#">
                                    Notificacion 1
                                </a>
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert alert-success" role="alert">
                                <a href="#">
                                    Notificacion 2
                                </a>
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert alert-success" role="alert">
                                Notificacion 3
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert alert-success" role="alert">
                                Notificacion 4
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert alert-success" role="alert">
                                Notificacion 5
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert border border-secondary border-3 rounded" role="alert">
                                Notificacion 6
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert border border-secondary border-3 rounded" role="alert">
                                Notificacion 7
                            </div>
                        </li>
                        <li class="li_notification">
                            <div class="alert border border-secondary border-3 rounded" role="alert">
                                Notificacion 8
                            </div>
                        </li>
                    </ul>
                    <style>
                        .ul_notification{
                            margin: 0 !important;
                            padding: 0 !important;
                        }
                        .ul_notification .li_notification{
                            list-style: none;
                        }
                    </style>
                </div>
            </div>

        </div>
    </div>
@endsection
