@extends('layouts.app')
@section('content')

    <div class="row-reverse">
        <div class="col">
            <div class="row-reverse">
                <div class="col p-0">
                    <div class="row mx-auto">
                        <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                            <a href="{{ url()->previous() }}" class="mt-1">
                                <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25"
                                    style="color: #7DBE38"></i>
                            </a>
                        </div>
                        <div class="col p-0">
                            <label for="" class="h3 m-0">CLASES <b class="font-weight-bold">EN VIVO</b></label>
                        </div>
                    </div>
                </div>
                <div class="col p-0">
                    <label for="" class="lead">Bienvenido a tus clases en vivo, elige la fecha de la clase que
                        quieres ver e ingresa a ella.
                    </label>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-lg-5 col-md-5 col-sm-10">
                    <div class="card">
                        <div class="card-body">
                            <div class="row-reverse">
                                <div class="col">
                                    <div id='calendar'></div>
                                </div>
                                <div class="col p-0 my-3">
                                    <ul class="list_indication">
                                        <li class="one">
                                            <span>Clase actual</span>
                                        </li>
                                        <li class="two">
                                            <span>Próximas clases</span>
                                        </li>
                                        <li class="three">
                                            <span>Clases anteriores</span>
                                        </li>
                                    </ul>
                                    <style>
                                        .list_indication {
                                            list-style: none;
                                            /* Remove default bullets */
                                        }

                                        .list_indication .one::before {
                                            content: url('https://api.iconify.design/akar-icons/circle-fill.svg?color=%237dbe38');
                                            display: inline-block;
                                            width: 1em;
                                            margin-left: -2em;
                                        }

                                        .list_indication .two::before {
                                            content: url('https://api.iconify.design/akar-icons/circle-fill.svg?color=%238246af');
                                            display: inline-block;
                                            width: 1em;
                                            margin-left: -2em;
                                        }

                                        .list_indication .three::before {
                                            content: url('https://api.iconify.design/akar-icons/circle-fill.svg?color=%23c4beb6');
                                            display: inline-block;
                                            width: 1em;
                                            margin-left: -2em;
                                        }
                                    </style>
                                </div>
                                <div class="col">
                                    <a href="{{ route('live-lessons.saved_lessons') }}" class="btn btn-secundary btn-block">VER CLASES
                                        GRABADAS</a>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="card">
                        <div class="card-body">

                            {{-- PROXIMA CLASE --}}
                            <div class="row-reverse" id="lesson-data" style="display: none">
                                <div class="col">
                                    <img id="lesson-thumbnail" src="" alt=""
                                        class="w-100">
                                </div>
                                <div class="col py-4 px-0">

                                    <div class="row-reverse">
                                        <div class="col">
                                            <h3 id="lesson-title" class="font-weight-bold title_live"></h3>
                                        </div>
                                        <div class="col">
                                            <h5 id="lesson-subtitle"></h5>
                                            <h5 id="lesson-schedule"></h5>
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <div class="col-4">
                                                    <h4 class="font-weight-bold">Profesor:</h4>
                                                </div>
                                                <div class="col">
                                                    <h5 id="lesson-teacher"></h5>
                                                    <h5></h5>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="row">
                                                <div class="col-4">
                                                    <h4 class="font-weight-bold">Descripción:</h4>
                                                </div>
                                                <div class="col">
                                                    <h5 id="lesson-description"></h5>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col">
                                            <a href="" id="lesson-url" class="btn w-100">ENTRAR</a>
                                            <span id="lesson-button" class="btn btn-disabled w-100">LA CLASE AÚN NO HA
                                                COMENZADO</button>
                                        </div>
                                    </div>


                                </div>
                            </div>

                            {{-- ESCOGER CLASE --}}
                            <div class="row-reverse my-3" id="no-selected-lesson">
                                <div class="col d-flex justify-content-center align-items-center mb-4">
                                    <img src="{{ url('/assets/img/background_class.png') }}" alt="" style="opacity: 0.5"
                                        class="w-100">
                                    <div class="row" style="position: absolute">
                                        <div class="col-auto d-flex justify-content-center align-items-center">
                                            <span class="iconify mt-1" data-width="50" data-icon="akar-icons:circle-triangle-left-fill"
                                                style="color: #7DBE38;"></span>
                                        </div>
                                        <div class="col">
                                            <label for="" class="h3 font-weight-bold animate__animated animate__backInLeft text-center">Por
                                                favor seleccione una opción <br> del calendario</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

        document.addEventListener('DOMContentLoaded', function() {
            const lessonData = document.getElementById("lesson-data");
            const noSelectedLesson = document.getElementById("no-selected-lesson");
            const lessonThumbnail = document.getElementById("lesson-thumbnail");
            const lessonTitle = document.getElementById("lesson-title");
            const lessonSubtitle = document.getElementById("lesson-subtitle");
            const lessonSchedule = document.getElementById("lesson-schedule");
            const lessonTeacher = document.getElementById("lesson-teacher");
            const lessonDescription = document.getElementById("lesson-description");
            const lessonUrl = document.getElementById("lesson-url");
            const lessonButton = document.getElementById("lesson-button");
            const calendarEl = document.getElementById('calendar');

            const calendar = new FullCalendar.Calendar(calendarEl, {
                locale: 'es',
                headerToolbar: {
                    start: 'title',
                    center: '',
                    end: 'prev,next'
                },
                initialView: 'dayGridMonth',
                dayMaxEvents: true,
                events: function(info, successCallback, failureCallback) {
                    const start = moment(info.start.valueOf()).format('YYYY-MM-DD');
                    const end = moment(info.end.valueOf()).format('YYYY-MM-DD');

                    axios.post("{{ route('live-lessons.calendar') }}", {
                            start,
                            end
                        })
                        .then(response => {
                            const events = response.data.data.map(event => {
                                let backgroundColor;
                                let borderColor;
                                const currentDate = moment();
                                const eventStart = moment(event.scheduled_date);
                                if (eventStart.isSame(currentDate, "day")) {
                                    backgroundColor = "green";
                                    borderColor = "green";
                                } else if (eventStart.isBefore(currentDate, "day")) {
                                    backgroundColor = "black";
                                    borderColor = "black";
                                } else {
                                    backgroundColor = "red";
                                    borderColor = "red";
                                }

                                return {
                                    id: event.id,
                                    title: event.title,
                                    start: event.scheduled_date,
                                    backgroundColor: backgroundColor,
                                    borderColor: borderColor,
                                    textColor: "white",
                                }
                            });

                            successCallback(events);
                        })
                        .catch(failureCallback);
                },
                eventClick: function(info) {
                    axios.get(`/live-lessons/load-lesson/${info.event.id}`)
                        .then(response => {
                            const lesson = response.data.data;

                            lessonData.style.removeProperty("display");
                            noSelectedLesson.style.display = "none";

                            lessonThumbnail.src = lesson.cover;
                            lessonTitle.textContent = lesson.title;
                            lessonSubtitle.textContent = lesson.subtitle;
                            lessonSchedule.textContent = lesson.shedule_date;
                            lessonTeacher.textContent = lesson.teacher;
                            lessonDescription.textContent = lesson.description;

                            const currentDate = moment();
                            const eventStart = moment(lesson.scheduled_date);

                            if (eventStart.isSame(currentDate, "day")) {
                                lessonUrl.href = "{{ route('live-lessons.live') }}";
                                lessonUrl.style.removeProperty("display");
                                lessonButton.style.display = "none";
                            } else if (eventStart.isBefore(currentDate, "day")) {
                                lessonUrl.href = `/live-lessons/saved-lessons?view=${lesson.id}`;
                                lessonUrl.style.removeProperty("display");
                                lessonButton.style.display = "none";
                            } else {
                                lessonUrl.href = "";
                                lessonButton.style.removeProperty("display");
                                lessonUrl.style.display = "none";
                            }
                        });
                }
            });

            calendar.render();
        });
    </script>
@endsection
