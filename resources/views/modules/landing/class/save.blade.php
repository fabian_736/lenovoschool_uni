@extends('layouts.app')
@section('content')
@include('layouts.utils.canvas.index')

    <div class="row-reverse">
        <div class="col mb-3">
            <div class="row-reverse">
                <div class="col p-0">
                    <div class="row mx-auto">
                        <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                            <a href="{{ url()->previous() }}" class="mt-1">
                                <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill" data-width="25"
                                    style="color: #7DBE38"></i>
                            </a>
                        </div>
                        <div class="col p-0">
                            <label for="" class="h3 m-0">CLASES <b class="font-weight-bold">GRABADAS</b></label>
                        </div>
                    </div>
                </div>
                <div class="col p-0">
                    <label for="" class="lead">Consulta las clases grabadas para reforzar tus conocimientos de
                        Lenovo School.
                    </label>
                </div>
            </div>
        </div>

        <div class="col">
            <div class="row">

                <div class="col-8 d-flex justify-content-center align-items-center" style="min-height: 50vh;">
                    <div class="row-reverse">

                        {{-- TEXTO ANTES DE LA CARGA --}}
                        <div class="col" id="classSave">
                            <div class="row ">
                                <div class="col">
                                    <label for="" class="h3 font-weight-bold animate__animated animate__backInLeft">Por
                                        favor seleccione una opción</label>
                                </div>
                                <div class="col-auto d-flex justify-content-center align-items-center">
                                    <span class="iconify mt-1" data-width="50" data-icon="akar-icons:circle-triangle-right-fill"
                                        style="color: #c4beb6;"></span>
                                </div>
                            </div>
                        </div>

                        {{-- VIDEO + INFO --}}
                        <div class="col p-0" style="display: none" id="content_video">
                            <div class="row-reverse">

                                {{-- VIDEO --}}
                                <div class="col bg-dark p-0">
                                    <div class="embed-responsive embed-responsive-16by9">
                                        <div id="player" class="embed-responsive-item"></div>
                                    </div>
                                </div>

                                {{-- INFO --}}
                                <div class="col p-0">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="row-reverse">
                                                <div class="col">
                                                    <h3 id="video-title" class="titleIntroduction font-weight-bold"></h3>
                                                </div>
                                                <div class="col ">
                                                    <p id="video-subtitle" class="subtitledIntroduction"></p>
                                                </div>
                                                <div class="col">
                                                    <p>
                                                        <b class="subtitledIntroduction font-weight-bold">Duración:</b> <span id="video-duration"></span>
                                                    </p>
                                                </div>
                                                <div class="col">
                                                    <hr>
                                                </div>
                                                <div class="col">
                                                    <p>
                                                        <b class="subtitledIntroduction font-weight-bold">Profesor:</b> <span id="video-teacher"></span>
                                                    </p>
                                                </div>
                                                <div class="col">
                                                    <p>
                                                        <b class="subtitledIntroduction font-weight-bold">Descripción:</b> <span id="video-description"></span>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                {{-- LISTADO --}}
                <div class="col-4 p-0">
                    <div class="card m-0"
                        style="background: url('/assets/img/background_sidebar.png'); background-size: cover">
                        <div class="card-body" style="min-height: 60vh; max-height: 60vh; overflow: auto">
                            <div class="row-reverse ">
                                <ul id="class-selector" class="p-0 m-0" style="list-style: none">
                                    @foreach ($liveLessons as $liveLesson)
                                        <li style="cursor: pointer;" data-id="{{ $liveLesson->id }}" class="animate__animated animate__bounceInDown ">
                                            <div class="row">
                                                <div class="col-auto d-flex align-items-center">
                                                    <div style="width: 75px; height: 75px"
                                                        class="bg-primary rounded-circle p-1">
                                                        <img class="w-100 h-100 rounded-circle"
                                                            src="{{ $liveLesson->getFirstMediaUrl('cover') }}"
                                                            alt="">
                                                    </div>
                                                </div>
                                                <div class="col d-flex align-items-center">
                                                    <div class="row-reverse">
                                                        <div class="col">
                                                            <label for="" class="lead font-weight-bold text-white">{{ $liveLesson->title }}</label>
                                                        </div>
                                                        <div class="col">
                                                            <label for="" class="tetx-white">{{ $liveLesson->subtitle }}</label>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="{{ mix('js/Portal/LiveLessons/vendor.js') }}" defer></script>
        <script src="{{ mix('js/Portal/LiveLessons/saved-lessons.js') }}" defer></script>
    @endpush
@endsection
