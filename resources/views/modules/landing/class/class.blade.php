@extends('layouts.app')
@section('content')
@include('layouts.utils.canvas.index')

    <div class="row-reverse">
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="javascript:;" class="mt-1" data-toggle="modal"
                                        data-target="#modalClassExit">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">{{ $liveLesson->title }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">{{ $liveLesson->subtitle }}</label>
                        </div>
                    </div>
                </div>
                <div class="col d-flex justify-content-end">
                    <label for="" class="lead text-dark m-0 ">DURACIÓN: 1h 30m</label>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                <div class="col-md-8 col-sm-12" >
                    <div class="embed-responsive embed-responsive-16by9">
                        <div id="player" class="embed-responsive-item" data-class="{{ $liveLesson->id }}"></div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-12 p-0">
                    <div class="card m-0">
                        <div class="row">
                            <div class="col">
                                <iframe src="{{ $liveLesson->vimeo_event_chat_url }}" frameborder="0"></iframe>
                                <style>
                                    iframe {
                                        display: block;
                                        width: 100%;
                                        height: 60vh;

                                    }
                                </style>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @include('layouts.utils.modal.class.index')


    @push('scripts')
        <script src="{{ mix('js/Portal/LiveLessons/vendor.js') }}"></script>
        <script src="{{ mix('js/Portal/LiveLessons/live.js') }}"></script>
    @endpush

    
@endsection
