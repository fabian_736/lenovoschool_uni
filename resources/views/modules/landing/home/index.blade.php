@extends('layouts.app')
@section('content')

   

    <div class="row-reverse">
        <div class="col">
            <div class="row-reverse">
                <div class="col p-0">
                    <label for="" class="h3">BIENVENIDO A <span class="font-weight-bold">LENOVO SCHOOL</span></label>
                </div>
                <div class="col p-0">
                    @if ($files->count() > 0)
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="z-index: 1 !important;">
                            <ol class="carousel-indicators">
                                @foreach ($files as $file)
                                    <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->index }}" class="@if ($loop->first) active @endif"></li>
                                @endforeach
                            </ol>
                            <div class="carousel-inner">
                                @foreach ($files as $file)
                                    <div class="carousel-item @if ($loop->first) active @endif">
                                        <img class="d-block w-100" src="{{ $file }}" alt="First slide">
                                    </div>
                                @endforeach
                            </div>
                            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    @endif
                </div>
            </div>
        </div>

        <div class="col">
            <div class="row">
                <div class="col-4">
                    <div class="card" id="class_card" onclick="window.location='{{ route('live-lessons.index') }}'">
                        <div class="card-body d-flex align-items-end justify-content-center text-center">
                            <div class="row-reverse">
                                <div class="col">
                                    <label for="" class="h3 text-white font-weight-bold">Clases en vivo</label>
                                </div>
                                <div class="col">
                                    <label for="" class="text-white">Entrena tus habilidades</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" id="challenge_card" onclick="window.location='{{ route('challenges.index') }}'">
                        <div class="card-body d-flex align-items-end justify-content-center text-center">
                            <div class="row-reverse">
                                <div class="col">
                                    <label for="" class="h3 text-white font-weight-bold">Desafios Lenovo</label>
                                </div>
                                <div class="col">
                                    <label for="" class="text-white">Prueba tus conocimientos</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-4">
                    <div class="card" id="gallery_card" onclick="window.location='{{ route('gallery.index') }}'">
                        <div class="card-body d-flex align-items-end justify-content-center text-center">
                            <div class="row-reverse">
                                <div class="col">
                                    <label for="" class="h3 text-white font-weight-bold">Galería</label>
                                </div>
                                <div class="col">
                                    <label for="" class="text-white">Refresca tus aprendizajes</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        #class_card {
            transition: transform .2s;
            cursor: pointer;
            border-radius: 20px;
            min-height: 55vh;
            max-height: 55vh;
            background: url('/assets/img/class.png');
            background-size: cover
        }

        #class_card:hover {
            transform: scale(1.05);
            border: solid 2px #7DBE38;
        }

        #challenge_card {
            transition: transform .2s;
            cursor: pointer;
            border-radius: 20px;
            min-height: 55vh;
            max-height: 55vh;
            background: url('/assets/img/challenge.png');
            background-size: cover
        }

        #challenge_card:hover {
            transform: scale(1.05);
            border: solid 2px #7DBE38;
        }

        #gallery_card {
            transition: transform .2s;
            cursor: pointer;
            border-radius: 20px;
            min-height: 55vh;
            max-height: 55vh;
            background: url('/assets/img/gallery.png');
            background-size: cover
        }

        #gallery_card:hover {
            transform: scale(1.05);
            border: solid 2px #7DBE38;
        }
    </style>


@endsection
