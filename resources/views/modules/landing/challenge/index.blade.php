@extends('layouts.app')
@section('content')
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script type="module" src="{{ asset('other/js/scripts/land_chall_index.js') }}"></script>
    @endpush

    <div class="row-reverse">
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">DESAFIOS <b
                                            class="font-weight-bold">LENOVO</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">
                                Desafía tus conocimientos, prueba qué tanto sabes de Lenovo y gana puntos para canjear
                                premios y recompensas por tus logros.
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-auto d-flex justify-content-end">
                    <a href="{{ route('challenges.my_challenges') }}" class="h3 font-weight-bold m-0" style="text-decoration: underline; color: #4C7421">MIS
                        DESAFIOS</a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                @forelse ($userChallenges as $userChallenge)
                    @php
                        $challenge = $userChallenge->challenge;
                    @endphp
                    <div class="col-4">
                        <div id="challenge_card" class="card cursor-pointer card_challenge {{ $userChallenge->status == 'In progress' ? '' : 'card_challenge_inactive' }}" data-id="{{ $userChallenge->id }}" style="background-image: url('{{ $challenge->getFirstMediaUrl('cover') }}');">
                            <div class="card-body d-flex justify-content-center align-items-end">
                                <div class="row-reverse">
                                    <div class="col d-flex justify-content-center">
                                        <label for="" class="lead text-white font-weight-bold">{{ $challenge->name }}</label>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <label for="" class="text-white font-weight-bold ">Puntos ganados: {{ $userChallenge->max_earned_points ?? 0 }} de {{ $userChallenge->challenge->questions_sum_points ?? 0 }}</label>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <label for="" class="text-white font-weight-bold ">N° de intentos: {{ $userChallenge->attempts_count }}</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    @include('layouts.utils.modal.challengue.empty')
                @endforelse
            </div>
        </div>
    </div>

    @include('layouts.utils.modal.challengue.index')

@endsection
