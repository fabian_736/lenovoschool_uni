@extends('layouts.app')
@section('content')
    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script src="{{ url('js/scripts/land_chall_test.js') }}" defer></script>
    @endpush
    <div class="row pr-0 mr-0">
        <div class="col-1 d-flex justify-content-end align-items-center">
            <a href="{{ route('challenges.index') }}"><i class="fas fa-arrow-circle-left fa-2x text-success"></i></a>
        </div>
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="h3 title_prueba">
                        <span class="font-weight-bold">DESAFIO</span> {{ $userChallenge->challenge->name }}
                    </div>
                </div>
            </div>
        </div>
    </div>


        <div class="text-right">Tiempo restante <span id="time" class="btn btn-success h5 font-weight-bold">10:00</span></div>


    <script>

function startTimer(minutos, segundos, display) {
    var minutes = minutos, seconds=segundos;

    var id = setInterval(function () {
        seconds = seconds -1;

        if(seconds <= 0){
            seconds = 60;
            minutes = minutes - 1;
        }

        display.text(minutes + ":" + ('0' + seconds).slice(-2)  );

        if(minutes < 0){

    var request = $.ajax({
    method: "put",
    url: "/challenges/"+{{$userChallenge->id}}+"/"+{{$attempt->id}}+"/score",
    data:$('#challenge-form').serialize(),
    }).done(function(data) {
    var modalError = new bootstrap.Modal("#modal_1", {
            keyboard: false
        });
        modalError.show();


    if(data.message == "Exedio el tiempo para responder."){
            $("#modal_title").text('Lo sentimos');
        }
        $("#modal_message").text(data.message);
        $("#modal_1").on("hidden.bs.modal", function (e) {
            location.replace("/challenges");
        });
    });
    clearInterval(id);

    }}, 1000);}

    jQuery(function ($) {
    var minutes = @php echo date('i', $minutos_res)@endphp;
    var seconds = @php echo date('s', $minutos_res)@endphp;
        display = $('#time');
    startTimer(minutes, seconds,display);
    });
    </script>

    <!-- CUESTIONARIOS -->
    <form id="challenge-form" class="row mx-auto pr-0 mr-0 mt-5"
        action="{{ route('challenges.score_challenge', ['userChallenge' => $userChallenge, 'userChallengeAttempt' => $attempt]) }}"
        method="POST">
        @csrf
        @method('PUT')
        <div class="col">
            @foreach ($userChallenge->challenge->questions as $question)
                <div class="card card-body">
                    @php
                        $inputType = $question->type == 'unique' ? 'radio' : 'checkbox';
                    @endphp
                    <div class="row-reverse">
                        <div class="col mb-4 ">
                            <span class="h3 titleprueba_cuestionario" style="color: black">{{ $question->label }}</span>
                        </div>
                        <div class="row">
                            <div class="col">
                                <ul style="list-style: none">
                                    @foreach ($question->options as $option)
                                        <li class="mb-5">
                                            @if ($inputType == 'radio')
                                                <div class="form-check form-check-radio">
                                                    <label class="form-check-label mx-5 lead" style="color: black">
                                                        <input class="form-check-input" type="radio" name="question-{{ $question->id }}" id="question-{{ $question->id }}-{{ $option->id }}" required autocomplete="off" value="{{ $option->id }}"/>
                                                        {{ $option->label }}
                                                        <span class="circle">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @else
                                                <div class="form-check">
                                                    <label class="form-check-label mx-5 lead" style="color: black">
                                                        <input class="form-check-input" type="checkbox" name="question-{{ $question->id }}[]" id="question-{{ $question->id }}-{{ $option->id }}" autocomplete="off" value="{{ $option->id }}" data-required="" />
                                                        {{ $option->label }}
                                                        <span class="form-check-sign">
                                                            <span class="check"></span>
                                                        </span>
                                                    </label>
                                                </div>
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>

                            @if ($question->hasMedia('image'))
                                <div class="col">
                                    <img src="{{ $question->getFirstMediaUrl('image') }}" alt="" class="w-100">
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            @endforeach
            <div class="row d-flex justify-content-center">
                <button type="submit" id="button-finish" class="btn btn-success text-white">Finalizar</button>
            </div>
        </div>
    </form>

    @include('layouts.utils.modal.challengue.test')
@endsection
