@extends('layouts.app')
@section('content')

    @push('scripts')
        <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
        <script type="module" src="{{ asset('other/js/scripts/land_chall_my.js') }}"></script>
    @endpush

    <div class="row-reverse">
        <div class="col">
            <div class="row">
                <div class="col">
                    <div class="row-reverse">
                        <div class="col p-0">
                            <div class="row mx-auto">
                                <div class="col-auto d-flex align-items-center justify-content-center p-0 mr-2">
                                    <a href="{{ url()->previous() }}" class="mt-1">
                                        <i class="iconify cursor-pointer" data-icon="bi:arrow-left-circle-fill"
                                            data-width="25" style="color: #7DBE38"></i>
                                    </a>
                                </div>
                                <div class="col p-0">
                                    <label for="" class="h3 m-0 font-weight-bold">MIS <b
                                            class="font-weight-bold">DESAFÍOS</b></label>
                                </div>
                            </div>
                        </div>
                        <div class="col p-0">
                            <label for="" class="lead">
                                Acá encontrarás los desafios que has culminado
                                satisfactoriamente
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="row">
                @forelse ($userChallenges as $userChallenge)
                    @php
                        $challenge = $userChallenge->challenge;
                    @endphp
                    <div class="col-4">
                        <div class="card challenge-card cursor-pointer" id="challenge_card" data-id="{{ $userChallenge->id }}" style="background-image: url('{{ $challenge->getFirstMediaUrl('cover') }}');">
                            <div class="card-body d-flex justify-content-center align-items-end">
                                <div class="row-reverse">
                                    <div class="col d-flex justify-content-center">
                                        <span class="iconify text-success" data-icon="la:check-circle-solid" data-width="150"></span>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <label for="" class="h3 text-white font-weight-bold">Descargar diploma</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class="px-5">
                        <h2>No hay desafios para ver.</h2>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
@endsection
