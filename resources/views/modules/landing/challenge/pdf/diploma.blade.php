<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Desafío {{ $userChallenge->challenge->name }}</title>
</head>

<body>
    <main>
        <div
            style="background-image: url('{{ public_path('other/panel/img/diploma.png') }}'); background-size: 100% 100%; width: 100%; height: 100%; text-align: center;">
            <img src="{{ public_path('other/panel/img/logo_blanco.png') }}" alt="" style="margin-top: 10%; width: 200px; height: 80px"> <br>
            <h1 style="color: white; font-weight: bold; padding-top: 30px">Desafío {{ $userChallenge->challenge->name }}</h1>
            <h2 style="color: white; font-weight: bold">Certificado otorgado a</h2>
            <h2 style="color: white; font-weight: bold">{{ $userChallenge->user->fullname }}</h2>
            <h3 style="padding-left: 20%; padding-right: 20%; padding-top: 3%; color: white">Por haber completado satisfactoriamente su curso en la plataforma de Lenovo School</h3>
            <p style="padding: 30px; color: white">{{ $currentDay }}</p>

            {{-- FIRMA USUARIO --}}

            <table style="min-width: 80%; margin:auto; ">
                <tr>
                    <td style="padding: 20px; max-width: 200px !important; min-width: 200px !important; vertical-align: bottom;">
                        <div style="text-align: center; color: white;">
                            <img src="{{ $userChallenge->challenge->getFirstMediaPath('teacher_signature') }}" style="max-width: 150px; max-height: 50px;"><br />
                            <hr>
                            {{ $userChallenge->challenge->teacher_name }}<br>
                            Profesor
                        </div>
                    </td>
                    <td style="padding: 20px; max-width:200px; min-width: 200px; vertical-align: bottom;">
                        <div style="text-align: center; color: white;">
                            <img src="{{ storage_path('app/challenge-signatures/gabriel-missaglia.png') }}" style="max-width: 150px; max-height: 50px;"><br />
                            <hr>
                            Gabriel Missaglia<br>
                            Gerente general
                        </div>
                    </td>
                </tr>
            </table>

        </div>
    </main>
</body>
<style>
    @page {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    @font-face {
        font-family: 'Helvetica';
        font-weight: normal;
        font-style: normal;
        font-variant: normal;
        src: url("font url");
    }

    body {
        font-family: Helvetica, sans-serif;
    }
</style>

</html>
