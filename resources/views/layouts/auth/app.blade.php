<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/img/favicon.svg') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/img/favicon.svg') }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>
        Lenovo School
    </title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    <script src="https://code.iconify.design/2/2.2.1/iconify.min.js"></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
    <!--   Core JS Files   -->
    <script src="{{ asset('js/app.js') }}"></script>
</head>

<body class="">
    <div class="wrapper wrapper-full-page ">
        <div class="page-header login-page header-filter " filter-color="black">
            <img class="imgA1" src="{{ asset('assets/img/login_background.png') }}">
            <img class="imgB1" src="{{ asset('assets/img/copyright_white.png') }}">
            @yield('content')
        </div>
    </div>


</body>

</html>
