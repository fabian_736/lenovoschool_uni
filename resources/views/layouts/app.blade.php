<!DOCTYPE html>
<html lang="en">
@include('layouts.components.head')

<body class="">
    <div class="wrapper ">
        @include('layouts.components.sidebar')
        <div class="main-panel">
            @include('layouts.components.navbar')
            <div class="content">
                <div class="content">
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                </div>
            </div>
            @include('layouts.components.footer')
        </div>
    </div>

</body>

</html>
