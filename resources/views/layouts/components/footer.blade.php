        <footer class="footer p-0">
            <div class="container-fluid bg-dark py-1">

                <div class="d-flex justify-content-center text-white">
                    <a class="text-white" href="{{asset('assets/manual/Términos y condiciones Lenovo School.pdf')}}" target="_blank">Términos y condiciones</a>
                     <span class="px-3">|</span>
                     <a class="text-white" href="{{asset('assets/manual/manual.pdf')}}" target="_blank">Manual de ayuda</a>

                </div>
            </div>
        </footer>
