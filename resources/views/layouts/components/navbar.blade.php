<nav class="shadow-none navbar navbar-expand-lg " style="background-color: #eee !important; z-index: 9 !important">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
        </button>
        <div class="row mx-auto">
            <div class="col ml-3">
                <a href="" class="btn btn-primary mr-3 rounded font-weight-bold">Puntos:
                    {{ auth()->user()->available_redemption_points }}</a>
                <a href="" class="btn btn-primary rounded font-weight-bold">Avance Total:
                    {{ auth()->user()->total_progress }}%</a>
            </div>
        </div>

        <div class="collapse navbar-collapse justify-content-end">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="dropdown">
                                <a class="d-block" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
                                    aria-haspopup="true" aria-expanded="false">
                                    <div>
                                        <span class="badge badge-primary rounded-circle p-0 p-1"
                                            style="position: absolute">
                                            {{ auth()->user()->unreadNotifications()->count() }}
                                        </span>
                                        <span class="iconify text-success" data-icon="bxs:bell" data-width="30"></span>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right p-3"
                                    aria-labelledby="navbarDropdownMenuLink">
                                    <style>
                                        .dropdown-menu {
                                            min-width: 25vw !important;
                                            max-width: 25vw !important;
                                        }

                                    </style>
                                    <div class="row">
                                        <div class="col justify-content-end">
                                            <label for="" class="lead text-dark font-weight-bold">Notificaciones</label>
                                        </div>
                                    </div>
                                    <div class="notificaciones-menu">
                                        @forelse (auth()->user()->unreadNotifications as $notification)
                                        <a class="dropdown-item"
                                            href="{{route('read_notification', $notification->id)}}">
                                            <div class="row align-items-center">
                                                <div class="col-3">
                                                    <i class="bi bi-bell-fill display-4"></i>
                                                </div>
                                                <div class="col-9">
                                                    <p class="d-block">
                                                        {{ $notification->data['main_text'] }}
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <hr>
                                        @empty
                                        <div class="px-3 text-center my-5">
                                            <i class="bi bi-check-circle text-success h4"></i>
                                            <h4>Estas al día, no hay notificaciones.</h4>
                                        </div>

                                        @endforelse
                                    </div>


                                    <div class="row">
                                        <div class="col d-flex justify-content-center">
                                            <a href="javascript:;" class="btn btn-success" data-bs-toggle="offcanvas"
                                                data-bs-target="#offcanvasRight" aria-controls="offcanvasRight">Ver
                                                todas las notificaciones</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto p-0">
                            <a href="{{ route('profile.index') }}">
                                <div style="width: 75px; height: 75px" class="bg-primary rounded-circle p-1">
                                    <img id="avatar" class="w-100 h-100 rounded-circle"
                                        src="{{ auth()->user()->getFirstMediaUrl('avatar') }}" alt="">
                                </div>
                            </a>
                        </div>
                        <div class="col-auto">
                            <div class="row-reverse">
                                <div class="col text-left">
                                    <span class="font-weight-bold">{{ auth()->user()->fullname }}</span>
                                </div>
                                <div class="col text-left">
                                    <span>{{ auth()->user()->position }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>


<div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasRight" aria-labelledby="offcanvasRightLabel">
    <div class="offcanvas-header">
        <h5 id="offcanvasRightLabel" class="h3 font-weight-bold">MIS NOTIFICACIONES</h5>
        <a href="javascript:;" data-bs-dismiss="offcanvas" aria-label="Close">
            <span class="iconify" data-icon="ant-design:close-outlined" data-width="30"></span>
        </a>
    </div>
    <div class="offcanvas-body">
        @include('layouts.utils.notification.new')

        @include('layouts.utils.notification.old')
    </div>
</div>
