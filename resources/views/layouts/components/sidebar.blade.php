<div class="sidebar" style="z-index: 10">

    <style>
        .sidebar {
            background: url('/assets/img/background_sidebar.png');
        }
    </style>
    <div class="logo">
        <div class="row">
            <div class="col d-flex justify-content-center ">
                <img src="{{ asset('assets/img/logo_blanco.png') }}" alt="" class="logo_sidebar"
                    style="width: 60%">
                <img src="{{ asset('assets/img/favicon.svg') }}" alt="" class="logo_sidebar w-50"
                    style="display: none;">
            </div>
        </div>
    </div>
    <button id="minimizeSidebar"
        class=" btn btn-just-icon btn-white btn-fab btn-round d-flex align-items-center justify-content-center"
        style="background-color: #7DBE38; position: absolute; right: -20px; z-index: 10000">
        <i class="iconify text_align-center visible-on-sidebar-regular" data-icon="bxs:left-arrow"
            style="color: #4C7421"></i>
        <i class="iconify design_bullet-list-67 visible-on-sidebar-mini" data-icon="bxs:right-arrow"
            style="color: #4C7421"></i>
    </button>
    <script>
        $('#minimizeSidebar').on('click', function() {
            $('.logo_sidebar').toggle();
            var menu = document.querySelector('body')
            menu.classList.toggle('sidebar-mini');
        });
    </script>
    <div class="sidebar-wrapper">
        <ul class="nav pb-5">
            <li class="nav-item {{ request()->is('home') ? 'active' : '' }} ">
                <a class="nav-link" href="{{ route("portal.index") }}">
                    <div class="row">
                        <div class="col-auto">
                            <i class="iconify" data-icon="ant-design:home-filled" data-width="25"></i>
                        </div>
                        <div class="col">
                            <p class=""> INICIO </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('live-lessons', 'live-lessons/live', 'live-lessons/saved-lessons') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('live-lessons.index') }}">
                    <div class="row">
                        <div class="col-auto">
                            <i class="iconify" data-icon="bi:camera-reels-fill" data-width="25"></i>
                        </div>
                        <div class="col">
                            <p class=""> CLASES EN VIVO </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('challenges', 'challenges/my-challenges', 'challenges/*') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('challenges.index') }}">
                    <div class="row">
                        <div class="col-auto"><span class="iconify" data-icon="carbon:progress-bar-round"
                                data-width="25"></span></div>
                        <div class="col">
                            <p> DESAFIOS </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('gallery', 'gallery/video') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route("gallery.index") }}">
                    <div class="row">
                        <div class="col-auto"><span class="iconify" data-icon="bi:card-image" data-width="25"></span>
                        </div>
                        <div class="col">
                            <p> GALERIA </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('profile') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route('profile.index') }}">
                    <div class="row">
                        <div class="col-auto"><span class="iconify" data-icon="bxs:user" data-width="25"></span></div>
                        <div class="col">
                            <p> PERFIL </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('ranking') ? 'active' : '' }}">
                <a class="nav-link" href="{{ route("ranking.index") }}">
                    <div class="row">
                        <div class="col-auto"><span class="iconify" data-icon="fa6-solid:ranking-star"
                                data-width="25"></span></div>
                        <div class="col">
                            <p> RANKING </p>
                        </div>
                    </div>
                </a>
            </li>
            <li class="nav-item {{ request()->is('rewards', 'medal', 'rewards/*', 'badges/*', 'badges') ? 'active' : '' }}">
                <a class="nav-link " data-toggle="collapse" href="#pagesExamples">
                    <div class="row">
                        <div class="col-auto"><span class="iconify" data-icon="icomoon-free:trophy"
                                data-width="25"></span></div>
                        <div class="col">
                            <p> RECOMPENSAS <b class="caret"></b></p>
                        </div>
                    </div>
                </a>
                <div class="collapse {{ request()->is('rewards', 'badges', 'rewards/*', 'badges/*') ? 'show' : '' }}" id="pagesExamples">
                    <ul class="nav">
                        <li class="nav-item {{ request()->is('badges/*', 'badges') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('badges') }}">
                                <span class="sidebar-mini"> <span class="iconify" data-icon="bxs:medal" data-width="20"></span> </span>
                                <span class="sidebar-normal"> INSIGNIAS </span>
                            </a>
                        </li>
                        <li class="nav-item {{ request()->is('rewards', 'rewards/*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('rewards.index') }}">
                                <span class="sidebar-mini"> <span class="iconify" data-icon="icomoon-free:gift" data-width="20"></span> </span>
                                <span class="sidebar-normal"> PREMIOS </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item ">
                <a class="nav-link" href="#">
                    <form action="{{ route('logout') }}" method="POST" name="form_logout">
                        @csrf
                        <div class="row">
                            <div class="col-auto d-flex justify-content-center">
                                <span class="iconify" data-icon="fluent:sign-out-24-filled"
                                    data-width="25"></span>
                            </div>
                            <div class="col d-flex align-items-center" onclick="logout()">
                                <p>CERRAR SESIÓN</p>
                            </div>
                        </div>
                    </form>
                    <script>
                        function logout() {
                            document.form_logout.submit();
                        }
                    </script>
                </a>
            </li>
        </ul>
    </div>
</div>
