<div class="modal fade" id="modalClassExit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-primary border-2 border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">FINALIZAR CLASE</h5>
                <button type="button" class="close btnClassExit" data-dismiss="modal" aria-label="Close">
                    <span class="iconify" data-icon="carbon:close-filled"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnClassExit').on('click', function() {
                            $("#modalClassExit").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center mb-3">
                        <span class="iconify text-primary" data-icon="akar-icons:triangle-alert-fill" data-width="100"></span>
                    </div>
                    <div class="col d-flex justify-content-center align-center mb-3">
                        <label for="" class="h4 text-center">Aun no has terminado la <br> Introducción a Lenovo</label>
                    </div>
                    <div class="col p-0 m-0">
                        <hr class="p-0 m-0">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center">
                        <label for="" class="h3 text-center font-weight-bold">
                            ¿Estas seguro de que <br> quieres salir?
                        </label>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center">
                        <div class="row-reverse">
                            <div class="col">
                                <a href="{{ url()->previous() }}" class="btn btn-primary btn-block">Salir de la clase</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

 <!-- Modal HAS FINALIZADO LA CLASE -->
 <div class="modal fade" id="modal_two" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
 aria-hidden="true">
 <div class="modal-dialog " role="document">
     <div class="modal-content backgroundModal">
         <div class="modal-body ">
             <div class="row-reverse">
                 <div class="col d-flex justify-content-center align-items-center my-5">
                     <img src="{{ url('landing/svg/users.svg') }}" alt="" class="icon_modal">
                 </div>
                 <div class="col d-flex justify-content-center align-items-center px-5">
                     <h3 class="text-center" style="font-weight: bold">¡Has finalizado la clase!</h3>
                 </div>
                 <div class="col d-flex justify-content-center align-items-center px-5">
                     <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                 </div>
                 <div class="col d-flex justify-content-center align-items-center px-5">
                     <label for="" class="lead text-center" style="color: black">Desafia tus nuevos conociemientos y prepárate para la siguiente clase.</label>
                 </div>
                 <div class="col d-flex justify-content-center align-items-center mt-5">
                     <a href="{{ route('challenges.index') }}" class="btn text-white btn_modal purpleBtn font16">Cumplir el desafio</a>
                 </div>
                 <div class="col d-flex justify-content-center align-items-center">
                     <a href="{{ route('live-lessons.saved_lessons') }}" class="btn text-dark btn_modal font16" style="border-style: solid; border-width: 2px; border-color: #8246AF;background: white;">Ver clases grabadas</a>
                 </div>
             </div>
         </div>
     </div>
 </div>
</div>