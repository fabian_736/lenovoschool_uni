<div class="modal fade" id="modalProfile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">Más información</h5>
                <button type="button" class="close btnProfile" data-dismiss="modal" aria-label="Close">
                    <span class="iconify" data-icon="carbon:close-filled"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnProfile').on('click', function() {
                            $("#modalProfile").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <ul>
                            <li>Puntos totales: {{ auth()->user()->available_redemption_points }}</li>
                            <li>Avance total: {{ auth()->user()->total_progress }}%</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="password-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold text-primary" id="exampleModalLabel">Cambiar contraseña</h5>
                <button type="button" class="border-0 bg-white btnProfilePass" data-dismiss="modal" aria-label="Close">
                    <i class="bi bi-x-circle text-primary h3"></i>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnProfilePass').on('click', function() {
                            $("#password-modal").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col">
                        <form id="password-form" action="#" method="POST">
                            <div class="row-reverse">
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <label for="current-password" class="w-100 text-dark font-weight-bold">Contraseña actual:</label>
                                        <input type="password" class="form-control rounded" id="current-password"
                                            placeholder="Contraseña actual">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <label for="password" class="w-100 text-dark font-weight-bold">Nueva contraseña:</label>
                                        <input type="password" class="form-control rounded" id="password"
                                            placeholder="Nueva contraseña">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="col mb-3">
                                    <div class="input-group">
                                        <label for="password_confirmation" class="w-100 text-dark font-weight-bold">Vuelve a escribir la nueva contraseña:</label>
                                        <input type="password" class="form-control rounded" id="password_confirmation"
                                            placeholder="Repite contraseña">
                                        <span class="invalid-feedback"></span>
                                    </div>
                                </div>
                                <div class="col mb-3 text-right">

                                    <button type="button" class="btn btn-outline-primary rounded btnProfilePass" data-dismiss="modal" aria-label="Close">
                                        Cancelar
                                    </button>
                                    <button type="submit" class="btn btn-primary">Actualizar contraseña</button>

                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade"  id="modalProfilePhoto" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-primary" style="background-image: url('{{asset('assets/img/bg-gris-textura-lenovo.jpg')}}');background-repeat:no-repeat;border-none">
            <div class="modal-header">
                <button type="button" class="close btnProfilePhoto" data-dismiss="modal" aria-label="Close">
                    <span class="iconify" data-icon="carbon:close-filled"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnProfilePhoto').on('click', function() {
                            $("#modalProfilePhoto").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex align-items-center justify-content-center">
                        <div style="width: 225px; height: 225px" class="bg-primary rounded-circle p-1">
                            <img class="w-100 h-100 rounded-circle" id="profile-image-preview"
                                src="{{ auth()->user()->getFirstMediaUrl('avatar') }}" alt="">

                            <form action="#" method="POST" id="profile-image-form"
                                enctype="multipart/form-data">
                                <input type="file" name="file" id="file" class="d-none">
                            </form>
                        </div>
                    </div>
                    <div class="col d-flex align-items-center justify-content-center my-3">

                        <label for="file" class="btn btn-primary">
                            SUBIR FOTO
                        </label>
                        <div class="text-end">
                            <button type="submit" form="profile-image-form" class="btn btn-outline-primary">GUARDAR</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
