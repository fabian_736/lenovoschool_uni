<div class="modal fade" id="modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-2 border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">DESAFIO</h5>
                <button type="button" class="close btnChallenge1" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnChallenge1').on('click', function() {
                            $("#modal_1").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center">
                        <span class="iconify" data-icon="fa6-solid:people-robbery" data-width="100"></span>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <h3 class="text-center font-weight-bold" id="modal_title"></h3>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <span class="lead text-center" style="color: black" id="modal_message"></span>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center mt-5">
                        <a href="{{ route('challenges.index') }}" data-dismiss="modal" class="btn text-white purpleBtn font16">SALIR</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="redeem-attempt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-2 border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">DESAFIO</h5>
                <button type="button" class="close btnChallengeRedeem" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnChallengeRedeem').on('click', function() {
                            $("#redeem-attempt").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <form id="redeem-attempt-form" class="backgroundModal" method="POST" action="#">
                @csrf
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <span class="iconify" data-icon="ant-design:warning-filled" data-width="100"></span>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center font-weight-bold">¡Ups, fallaste!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100% ">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <div class="lead text-center" style="color: black">
                                <b class="font-weight-bold colorPurple">¿Quieres volverlo a intentar y ganar hasta <span id="retry-points"></span> puntos más?</b><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="{{ route('challenges.index') }}" class="btn btn-secondary">No</a>
                    <button type="submit" class="btn btn-primary">Sí</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="error-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-2 border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">DESAFIO</h5>
                <button type="button" class="close btnChallengeError" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnChallengeError').on('click', function() {
                            $("#error-modal").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center my-5">
                        <img src="{{ url('landing/svg/error.svg') }}" alt="" class="icon_modal">
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <h3 class="text-center font-weight-bold">Ocurrio un problema</h3>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
