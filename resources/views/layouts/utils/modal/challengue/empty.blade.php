@if (Request::is('challenges'))
    <div class="modal fade" id="empty" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content backgroundModal">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="{{ route('portal.index') }}">
                                <i class="far fa-times-circle fa-2x"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <span class="iconify" data-icon="emojione:warning" data-width="100"></span>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center font-weight-bold">¡ATENCIÓN!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100%">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5 mb-3">
                            <span class="lead text-center font-weight-bold" style="color: black;">No hay desafíos disponibles <br> Vuelve más tarde</span>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <a href="{{ route('portal.index') }}" class="btn w-50 text-white purpleBtn font16">CONTINUAR</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#empty').modal('show')
    </script>
@endif

@if (Request::is('challenges/my-challenges'))
    <div class="modal fade" id="empty_certificate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content backgroundModal">
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-end align-items-end">
                            <a href="{{ route('portal.index') }}">
                                <i class="far fa-times-circle fa-2x"></i>
                            </a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-5">
                            <span class="iconify" data-icon="emojione:warning" data-width="100"></span>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <h3 class="text-center font-weight-bold">¡ATENCIÓN!</h3>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <hr class="text-secundary" style="border-width: 1px; border-style: solid; width: 100%">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center px-5 mb-3">
                            <span class="lead text-center font-weight-bold" style="color: black;">No hay certificados disponibles <br> Vuelve mas tarde</span>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <a href="{{ route('portal.index') }}" class="btn w-50 text-white purpleBtn font16">CONTINUAR</a>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <a href="{{ route('portal.index') }}" class="btn w-50 bg-white text-dark font-weight-bold font16" style="border: 2px solid #8246AF;">Volver al inicio</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#empty_certificate').modal('show')
    </script>
@endif
