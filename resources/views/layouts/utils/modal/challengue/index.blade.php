<div class="modal fade" id="challenge-modal-warning" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-2 border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">DESAFIO</h5>
                <button type="button" class="close btnChallengue" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnChallengue').on('click', function() {
                            $("#challenge-modal-warning").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center align-items-center">
                        <span class="iconify text-primary" data-icon="mdi:lightbulb-on-90" data-width="100"></span>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center">
                        <span id="challenge-modal-warning-challenge-title" class="lead text-center h3 font-weight-bold purpleBtn"></span>
                    </div>
                    <div class="col d-flex justify-content-center align-items-center px-5 my-3">
                        <p id="challenge-modal-warning-challenge-message" class="text-center fa-lg" style="line-height: 1.5em;"></p>
                    </div>
                    <div id="challenge-modal-warning-ready-message" class="col d-flex justify-content-center align-items-center px-5 my-3">
                        <p class="text-center fa-lg" style="line-height: 1.5em;">¿Estás listo para comenzar?</p>
                    </div>
                    <div id="challenge-modal-warning-challenge-link-container"
                        class="col d-flex justify-content-center align-items-center my-3">
                        <a id="challenge-modal-warning-challenge-link" href="" class="btn btn-success ">INICIAR PRUEBA</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="redeem-attempt" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-2 border-primary border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="exampleModalLabel">DESAFIO</h5>
                <button type="button" class="close btnChallengeRedeem" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnChallengeRedeem').on('click', function() {
                            $("#redeem-attempt").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <form id="redeem-attempt-form" class="backgroundModal" method="POST" action="#">
                @csrf
                <div class="modal-body">
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center px-5">
                            <div class="lead text-center" style="color: black">
                                <b class="font-weight-bold colorPurple">¿Quieres redimir 1 punto para tener otro intento y ganar hasta <span id="redeem-attempt-retry-points"></span> puntos más?</b><br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
                    <button type="submit" class="btn btn-primary">Sí</button>
                </div>
            </form>
        </div>
    </div>
</div>
