<div class="modal fade" id="redeem-modal" tabindex="-1" role="dialog" aria-labelledby="redeem-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-primary border-2 border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="redeem-modal-label">Redimir premio</h5>
                <button type="button" class="close btnReward" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnReward').on('click', function() {
                            $("#redeem-modal").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <div style="min-height: 500px; max-height: 500px; min-width: 300px; max-width: 300px; filter: drop-shadow(0px 4px 8px rgba(0, 0, 0, 0.17)); background: url('/assets/premios/ticket.png'); background-size: cover">
                            <div class="row-reverse pb-2">
                                <div class="col p-3">
                                    <div id="container" style=" width:100%; height:250px;">
                                        <img class="d-block w-100 h-100" src="" id="reward-image" />
                                    </div>
                                </div>
                                <div class="col mt-4">
                                    <div class="row-reverse">
                                        <div class="col d-flex justify-content-center">
                                            <span class="text-dark text-center font-weight-bold" id="reward-name"></span>
                                        </div>
                                        <div class="col d-flex justify-content-center">
                                            <span class="lead text-dark font-weight-bold" id="required-redemable-points">Puntos: </span>
                                        </div>
                                        <div class="col d-flex justify-content-center">
                                            <a href="" class="btn btn-sm btn-block rounded" id="redemption-link">REDIMIR</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="redemption-error-modal" tabindex="-1" role="dialog" aria-labelledby="redemption-error-modal-label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content border border-primary border-2 border-radius-40">
            <div class="modal-header">
                <h5 class="modal-title font-weight-bold" id="redemption-error-modal-label">No puedes redimir</h5>
                <button type="button" class="close btnRewardError" data-dismiss="modal" aria-label="Close">
                    <span class="iconify text-primary" data-width="30" data-icon="ant-design:close-circle-outlined"></span>
                </button>
                <script>
                    $(document).ready(function() {
                        $('.btnRewardError').on('click', function() {
                            $("#redemption-error-modal").modal('toggle');
                            if ($('.modal-backdrop').is(':visible')) {
                                $('body').removeClass('modal-open');
                                $('.modal-backdrop').remove();
                                $('body').addClass('scroll');
                            };
                        });
                    });
                </script>
            </div>
            <div class="modal-body">
                <div class="row-reverse">
                    <div class="col d-flex justify-content-center">
                        <span class="iconify text-primary" data-width="100" data-icon="clarity:warning-standard-solid"></span>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <span class="h3 font-weight-bold">¡TE FALTAN PUNTOS!</span>
                    </div>
                    <div class="col my-3">
                        <hr class="p-0 m-0">
                    </div>
                    <div class="col d-flex justify-content-center">
                        <span id="redemption-error-message" class="h4 text-center"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
