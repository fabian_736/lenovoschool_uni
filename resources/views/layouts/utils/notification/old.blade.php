<div class="row-reverse">
    <div class="col">
        <div class="card border-radius-20 bg-secondaryhigh">
            <div class="card-header bg-secondaryhigh d-flex align-items-center">
                <label for="" class="text-white h5 text-dark font-weight-bold m-0">Antiguas</label>
            </div>
            <div class="card-body" style="overflow-y: auto; max-height: 30vh">
                @forelse (auth()->user()->readNotifications as $notification)
                    <a class="dropdown-item dropdown_item_old" href="{{route('read_notification', $notification->id)}}">
                        <div class="row">
                            <div class="col-auto">
                                <div class="p-2 text-center">
                                    <i class="bi bi-bell-fill h2"></i>
                                </div>
                            </div>
                            <div class="col d-flex justify-content-center align-items-center">

                                    {{ $notification->data['main_text'] }}

                            </div>
                        </div>
                    </a>
                    <hr>
                    @empty
                    <div class="px-3 text-center my-5">
                        <i class="bi bi-check-circle text-success h4"></i>
                        <h4>Estas al día, no hay notificaciones.</h4>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</div>


<style>
    .dropdown_item_old:hover,
    .dropdown_item_old:focus {
        color: #ffffff !important;
        text-decoration: none !important;
        background-color: rgb(159, 159, 159) !important;
        border-radius: 20px;
        transition: 0.3s;
    }
</style>
