<div class="row-reverse">
    <div class="col">
        <div class="card border-radius-20">
            <div class="card-header bg-secondarylight d-flex align-items-center">
                <label for="" class="text-white h5 text-success font-weight-bold m-0">Nuevas</label>
            </div>
            <div class="card-body" style="overflow-y: auto; max-height: 30vh">
                @forelse (auth()->user()->unreadNotifications as $notification)
                    <a class="dropdown-item dropdown_item_new" href="{{route('read_notification', $notification->id)}}">
                        <div class="row align-items-center">
                            <div class="col-auto">
                                <div class="p-2 text-center">
                                    <i class="bi bi-bell-fill h2"></i>
                                </div>


                            </div>
                            <div class="col d-flex justify-content-center align-items-center">

                                    {{ $notification->data['main_text'] }}

                            </div>
                        </div>
                    </a>
                    <hr>
                    @empty
                    <div class="px-3 text-center my-5">
                        <i class="bi bi-check-circle text-success h4"></i>
                        <h4>Estas al día, no hay notificaciones.</h4>
                    </div>
                @endforelse
            </div>
        </div>
    </div>
</div>

<style>
    .dropdown_item_new:hover,
    .dropdown_item_new:focus {
        color: #ffffff !important;
        text-decoration: none !important;
        background-color: #7DBE38 !important;
        border-radius: 20px;
        transition: 0.3s;
    }
</style>
