@extends('layouts.auth.app')
@section('content')
    <div class="container " id="container_auth">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-8 mr-autop-3 rounded bg-white" id="card_auth">
                <form method="POST">
                    @csrf
                    <input type="hidden" name="email" value="{{ $user->email }}" />
                    <div class="row-reverse">
                        <div class="col d-flex justify-content-center align-items-center pt-5 pb-3">
                            <img src="{{ asset('assets/img/logo.png') }}" alt="" class="w-50">
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <label for="" class="h3 text-success font-weight-bold">Crear tu contraseña</label>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center mb-3">
                            <label for="" class="text-dark text-center">Te recomendamos crear una contraseña segura incluyendo números, mayúsculas o caracteres especiales.
                                </label>
                        </div>
                        <div class="col mb-3">
                            <div class="input-group rounded" id="input_rounded">
                                <div class="input-group-prepend @error('password') border border-danger @enderror">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="bxs:lock"></span>
                                    </span>
                                </div>
                                <input id="password" type="password"
                                    class="form-control @error('password') border border-danger @enderror" name="password" required
                                    autocomplete="new-password" placeholder="Contraseña">
                            </div>
                            @error('password')
                            <div class="invalid-feedback d-block">
                                {{ $message }}
                            </div>
                            @enderror
                        </div>
                        <div class="col mb-4">
                            <div class="input-group rounded" id="input_rounded">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">
                                        <span class="iconify" data-icon="bxs:lock"></span>
                                    </span>
                                </div>
                                <input id="password-confirm" type="password" class="form-control"
                                name="password_confirmation" required autocomplete="new-password"
                                placeholder="Repetir contraseña">
                            </div>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center">
                            <button type="submit" class="btn btn-success rounded w-100">GUARDAR CONTRASEÑA</button>
                        </div>
                        <div class="col d-flex justify-content-center align-items-center my-3 ">
                            <a href="{{ URL::to('/') }}">
                                <span class="text-success font-weight-bold">Volver al login</span>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
