<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f4f4f4">
    <tbody>
        <!-- HEAD -->
        <tr>
            <td align="center">
                <img src="{{ asset('assets/img/LogoMail-Lenovo.jpg') }}" style="margin-top: 5%; margin-bottom: 5%;">
            </td>
        </tr>
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="center" valign="top" bgcolor="#f4f4f4">
                                <table class="col-600" width="400" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img src="{{ asset('assets/img/new-challenge-mail-header.png') }}" style="margin-top: 1em; margin-bottom: 1em;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END HEAD -->

        <!-- BODY -->
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px;">
                    <tbody>
                        <tr>
                            <td height="35"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: 'Gotham', sans-serif; font-size:22px; font-weight: 700; color:#2a3a4b;">¿Quieres probar qué tanto sabes como profesional?</td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: 'Gotham', sans-serif; font-size:18px; color:#00000; line-height:24px; font-weight: 300;">
                                <p>Atrévete y participa superando el nuevo desaio <em>"{{ $userChallenge->challenge->name }}"</em> que ya se encuentra listo en Lenovo School</p>
                                <p>Y si respondes correctamente podrás ganar hasta {{ $userChallenge->challenge->questions_sum_points }} puntos para canjear los mejores premios.</p>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: 'Gotham', sans-serif; font-size:18px; color:#00000; line-height:24px; font-weight: 300;">
                                <br>
                                <p><b style="font-size: 25px; font-weight: 700">¿Estás listo/a?</b></p>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="5"></td>
        </tr>
        <!-- END BODY -->

        <!-- FOOTER -->
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px;">
                    <tbody>
                        <tr>
                            <td height="35"></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a href="{{ route('challenges.challenge_index', ['userChallenge' => $userChallenge]) }}" style="background-color: #7DBE38; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 20px;">¡QUIERO HACERLO!</a>
                            </td>
                        </tr>
                        <tr>
                            <td height="20"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END FOOTER -->
    </tbody>
</table>
