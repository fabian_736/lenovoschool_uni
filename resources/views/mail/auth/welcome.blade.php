<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" bgcolor="#f4f4f4">
    <tbody>
        <!-- HEAD -->
        <tr>
            <td align="center">
                <img src="{{ asset('assets/img/LogoMail-Lenovo.jpg') }}" style="margin-top: 5%; margin-bottom: 5%;">
            </td>
        </tr>
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td align="center" valign="top" bgcolor="#f4f4f4" height="400">
                                <table class="col-600" width="600" height="400" border="0" align="center" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td>
                                            <img src="{{ asset('assets/img/welcome-mail-header.png') }}" style="margin-top: 1em; margin-bottom: 1em;">
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END HEAD -->

        <!-- BODY -->
        <tr>
            <td align="center">
                <table class="col-600" bgcolor="#f4f4f4" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px;">
                    <tbody>
                        <tr>
                            <td height="35"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: 'Gotham', sans-serif; font-size:22px; font-weight: 700; color:#2a3a4b;">¡Hola {{ $user->name }}!</td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td align="center" style="font-family: 'Gotham', sans-serif; font-size:18px; color:#00000; line-height:24px; font-weight: 300;">
                                <b>Te damos la bienvenida a Lenovo School</b> aquí encontrarás todas <br> las herramientas para conocer la marca, distribuirla y enamorar a <br> tus clientes con ella. <br><br>
                                Empieza a disfrutar de todas nuestras sorpresas y accede a:
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                    <tbody>
                        <tr>
                            <td height="10"></td>
                        </tr>
                        <tr>
                            <td>
                                <table class="col3" width="600" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tbody>
                                        <tr>
                                            <td height="30"></td>
                                        </tr>
                                        <tr>
                                            <td align="center">
                                                <table class="insider" width="600" border="0" align="center" cellpadding="0" cellspacing="0">
                                                    <tbody>
                                                        <tr align="center" style="line-height:0px;">
                                                            <td>
                                                                <img style="display:block; line-height:0px; font-size:0px; border:0px;" src="{{ asset('assets/img/welcome-mail-modules.png') }}" width="600" height="170" alt="icon">
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="15"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="5"></td>
        </tr>
        <!-- END BODY -->

        <!-- FOOTER -->
        <tr>
            <td align="center">
                <table class="col-600" width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-left:20px; margin-right:20px;">
                    <tbody>
                        <tr>
                            <td height="35"></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <a href="{{ $url }}" style="background-color: #7DBE38; border: none; color: white; padding: 15px 32px; text-align: center; text-decoration: none; display: inline-block; font-size: 16px; margin: 4px 2px; cursor: pointer; border-radius: 20px;">QUIERO VER MÁS</a>
                            </td>
                        </tr>
                        <tr>
                            <td height="10"></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END FOOTER -->
    </tbody>
</table>
