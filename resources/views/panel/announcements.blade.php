@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body ">
            {{-- TITLE --}}
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Anuncios</h6>
                </div>
            </div>

            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#create-announcement-modal" class="greenBtn btn w-50 my-4 mb-2 text-white" onclick="">Crear anuncio</a>

            <div class="table-responsive">
                <table class="table table-light table-border table-striped table-bordered">
                    <thead class="thead-light">
                        <tr>
                            <th>Imagen</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody class="text-center">
                        @foreach ($files as $name => $url)
                            <tr>
                                <td>
                                    <img class="imgTableAd" src="{{ $url }}">
                                </td>
                                <td>
                                    <div class="col d-flex justify-content-center">
                                        <button type="button" class="btn btn-danger delete-button" data-name="{{ $name }}">
                                            <i class="fas fa-trash-alt fa-2x"></i>
                                        </button>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- MODAL FORM CREATE --}}
    <div class="modal fade" id="create-announcement-modal" tabindex="-1" role="dialog" aria-labelledby="create-announcement-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="create-announcement-modal-label">Crear Anuncio</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-announcement" action="#" method="POST">
                        @csrf
                        <div class="w-50 mx-auto p-1 pb-5">
                            <div class="col">
                                <label class="form-label font-weight-bold">Imagen</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="file" class="form-control-file" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear Anuncio</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        <script src="{{ mix('js/panel/announcements.js') }}"></script>
    @endpush
@endsection
