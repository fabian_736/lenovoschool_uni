      <!-- Head -->
      @include('layouts.auth.components.head')
      <!-- End Head -->

      <div class="container position-sticky z-index-sticky top-0">
          <div class="row">
              <div class="col-12">
                  <!-- Navbar  // NO SE ESTA UTILIZANDO
       @include('layouts.auth.components.navbar')
       End Navbar -->
              </div>
          </div>
      </div>
      <main class="main-content  mt-0">
          <div class="page-header align-items-start min-vh-100" style="background-image: url('img/background_t.png');">
              <span class="mask bg-gradient-dark opacity-6"></span>


              @yield('content')

              <!-- Navbar -->
              @include('layouts.auth.components.footer')
              <!-- End Navbar -->

          </div>
      </main>


      <!-- End -->
      @include('layouts.auth.components.end')
      <!-- End HTML -->
