<footer class="footer position-absolute bottom-2 py-2 w-100">
    <div class="container">
        <div class="row align-items-center justify-content-lg-between">
            <div class="col-12 col-md-6 my-auto">
                <div class="copyright text-center text-sm text-white text-lg-start">
                    ©
                    <script>
                        document.write(new Date().getFullYear())
                    </script>
                    <a href="https://www.peoplemarketing.com" class="font-weight-bold text-white" target="_blank">all rights reserved. People by Overall</a>
                </div>
            </div>
            <div class="col-12 col-md-6">
                <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                    <li class="nav-item">
                        <a href="https://www.peoplemarketing.com" class="nav-link text-white" target="_blank">People by Overall</a>
                    </li>
                    <li class="nav-item">
                        <a href="https://www.overallpharma.com" class="nav-link text-white" target="_blank">Sobre nosotros</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</footer>
