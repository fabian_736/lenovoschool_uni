<!-- Head -->
@include('panel.layouts.content.components.head')
<!-- End Head -->

<!-- Sidebar -->
@include('panel.layouts.content.components.sidebar')
<!-- End Sidebar -->

<main class="main-content position-relative max-height-vh-100 h-100 border-radius-lg ">

    <!-- Navbar -->
    @include('panel.layouts.content.components.navbar')
    <!-- End Navbar -->

    <div class="container-fluid py-4">

        <div class="row p-3 mx-auto">

            <div class="col">

                @yield('content')

            </div>

        </div>

        <!-- Footer -->
        @include('panel.layouts.content.components.footer')
        <!-- End Footer -->

    </div>
</main>

<!--  End HTML   -->
@include('panel.layouts.content.components.end')

<script>
    $(document).ready(function() {
        setTimeout(function() {
            $(".message").slideUp(500).empty();
            $(".alert").slideUp(500).empty();
            $(".success").slideUp(500).empty();
        }, 2000);

    });
</script>
