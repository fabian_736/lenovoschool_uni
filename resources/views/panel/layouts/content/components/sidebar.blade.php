<aside class="sidenav navbar navbar-vertical navbar-expand-xs border-0 border-radius-xl my-3 fixed-start ms-3" id="sidenav-main" style="background: #25353f !important;">
    <div class="sidenav-header">
        <i class="fas fa-times p-3 cursor-pointer text-white opacity-5 position-absolute end-0 top-0 d-none d-xl-none" aria-hidden="true" id="iconSidenav"></i>
        <a class="navbar-brand m-0 d-flex justify-content-center" href="{{ route('panel.index') }}" target="_blank">
            <img src="{{ asset('/other/panel/img/logo_blanco.png') }}" class="navbar-brand-img h-100 mx-auto" alt="main_logo">
        </a>
    </div>
    <hr class="horizontal light mt-0 mb-2">
    <div class="collapse navbar-collapse  w-auto  max-height-vh-100" style="min-height:67%" id="sidenav-collapse-main">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link text-white  {{ request()->is('home') ? 'active' : '' }}" href="{{ route('panel.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">dashboard</i>
                    </div>
                    <span class="nav-link-text ms-1">Inicio</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('panel/users') ? 'active' : '' }}" href="{{ route('panel.users.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">group</i>
                    </div>
                    <span class="nav-link-text ms-1">Usuarios</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('panel/challenges') ? 'active' : '' }}" href="{{ route('panel.challenges.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Desafíos</span>
                </a>
            </li>
            <li class="nav-item">
                @php
                    $routeIsRewards = request()->is('panel/rewards', 'panel/rewards/*');
                @endphp
                <a class="nav-link text-white {{ $routeIsRewards ? 'active' : 'collapsed' }}" data-bs-toggle="collapse" aria-expanded="{{ $routeIsRewards ? 'true' : 'false' }}" href="#profileExample3">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Premios</span>
                </a>
                <div class="collapse {{ $routeIsRewards ? 'show' : '' }}" id="profileExample3">
                    <ul class="nav nav-sm flex-column">
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('panel/rewards/create') ? 'active' : '' }}" href="{{ route('panel.rewards.create') }}">
                                <span class="sidenav-mini-icon"> P </span>
                                <span class="sidenav-normal  ms-2  ps-1">Crear premios</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('panel/rewards/redemption-status') ? 'active' : '' }}" href="{{ route('panel.rewards.redemption-status.index') }}">
                                <span class="sidenav-mini-icon"> P </span>
                                <span class="sidenav-normal  ms-2  ps-1">Seguimiento de premios</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('panel/rewards') ? 'active' : '' }}" href="{{ route('panel.rewards.all') }}">
                                <span class="sidenav-mini-icon"> A </span>
                                <span class="sidenav-normal  ms-2  ps-1">Ver todos los premios</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('panel/live-lessons') ? 'active' : '' }}" href="{{ route('panel.live-lessons.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Clases en vivo</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('panel/gallery') ? 'active' : '' }}" href="{{ route('panel.gallery.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">collections_bookmark</i>
                    </div>
                    <span class="nav-link-text ms-1">Galería</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('panel/penalizations') ? 'active' : '' }}" href="{{ route('panel.penalizations.index') }} ">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Penalizaciones</span>
                </a>
            </li>
            <li class="nav-item">
                @php
                    $routeIsContents = request()->is('panel/announcements', 'panel/badges', 'other/tutorial');
                @endphp
                <a class="nav-link text-white {{ $routeIsContents ? 'active' : 'collapsed' }}" data-bs-toggle="collapse" aria-expanded="{{ $routeIsContents ? 'true' : 'false' }}" href="#profileExample5">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">receipt_long</i>
                    </div>
                    <span class="nav-link-text ms-1">Contenidos</span>
                </a>
                <div class="collapse {{ $routeIsContents ? 'show' : '' }}" id="profileExample5">
                    <ul class="nav nav-sm flex-column">
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('panel/announcements') ? 'active' : '' }}" href="{{ route('panel.announcements.index') }}">
                                <span class="sidenav-mini-icon"> P </span>
                                <span class="sidenav-normal  ms-2  ps-1">Anuncios</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('panel/badges') ? 'active' : '' }}" href="{{ route('panel.badges.index') }}">
                                <span class="sidenav-mini-icon"> P </span>
                                <span class="sidenav-normal  ms-2  ps-1">Insignias</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white {{ request()->is('other/tutorial') ? 'active' : '' }}" href="{{ route('panel.other.tutorial') }}">
                                <span class="sidenav-mini-icon"> P </span>
                                <span class="sidenav-normal  ms-2  ps-1">Tutoriales</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </li>
            <li class="nav-item mt-3">
                <h6 class="ps-4 ms-2 text-uppercase text-xs text-white font-weight-bolder opacity-8">Otras configuraciones</h6>
            </li>
            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('profile') ? 'active' : '' }}" href="{{ route('panel.profile.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">person</i>
                    </div>
                    <span class="nav-link-text ms-1">Mi perfil</span>
                </a>
            </li>

            <li class="nav-item">
                <a class="nav-link text-white {{ request()->is('config') ? 'active' : '' }}" href="{{ route('panel.config.index') }}">
                    <div class="text-white text-center me-2 d-flex align-items-center justify-content-center">
                        <i class="material-icons opacity-10">assignment</i>
                    </div>
                    <span class="nav-link-text ms-1">Configuracion de cuenta</span>
                </a>
            </li>
        </ul>
    </div>
    <div class="sidenav-footer position-absolute w-100 bottom-0">
        <div class="mx-3">
            <form action="{{ route('logout') }}" method="post">
                @csrf
                <button type="submit" class="btn bg-gradient-danger mt-4 w-100">Cerrar Sesión</button>
            </form>
        </div>
    </div>
</aside>
