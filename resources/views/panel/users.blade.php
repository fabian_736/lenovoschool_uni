@extends('panel.layouts.content.app')
@push('scripts')
    <script src="{{ mix('js/panel/users.js') }}"></script>
@endpush
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Usuarios</h6>
                </div>
            </div>
            <button type="button" id="create-user-button" class="btn greenBtn btn w-50 my-4 mb-2 text-white">Crear usuario</button>
            <div class="table-responsive">
                <table id="users-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo electrónico</th>
                            <th>Cargo</th>
                            <th>Compañía</th>
                            <th>Tipo de usuario</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="user-modal" tabindex="-1" role="dialog" aria-labelledby="user-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="user-modal-label"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="user-form" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="name" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">Apellido</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="lastname" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="document_type_id">Tipo de documento</label>
                                    <select class="custom-select form-select my-3" name="document_type_id" id="document_type_id" autocomplete="off" required>
                                        <option value="" disabled selected>Seleccione una opción</option>
                                        @foreach ($documentTypes as $documentType)
                                            <option value="{{ $documentType->id }}">{{ $documentType->name }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold">Número de documento</label>
                                    <div class="input-group input-group-outline my-3">
                                        <input type="number" name="document_number" class="form-control" required />
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Correo electrónico</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="email" name="email" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Cargo</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="position" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">Empresa</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="company" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="status">Estado</label>
                                    <select class="custom-select form-select" id="status" name="active">
                                        <option selected disabled>Seleccione una opción</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white"></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <template id="table-template">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>
@endsection
