@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Clases en vivo</h6>
                </div>
            </div>
            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#create-class-modal"
                class="greenBtn btn w-50 my-4 mb-2 text-white">Crear clase en vivo</a>
            <div class="table-responsive">
                <table id="classes-table" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Profesor</th>
                            <th>Fecha programada</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="create-class-modal" tabindex="-1" role="dialog" aria-labelledby="create-class-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="create-class-modal-label">Crear clase en vivo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-class" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col my-5">
                                <p class="lead">
                                    Recuerda que debes iniciar sesión con la cuenta verificada de Lenovo School en
                                    Vimeo.<br><br>
                                    Por favor, da click en <a href="https://vimeo.com" target="_blank"
                                        class="text-dark font-weight-bold">VIMEO</a> para crear tu sesión en vivo <br>
                                    Una vez que hayas obtenido el link de tu transmicion, vuelve al panel y llena el
                                    siguiente formulario:
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Título</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="title" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subtítulo</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="subtitle" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Descripción</label>
                                <div class="input-group input-group-outline my-3">
                                    <textarea name="description" class="form-control"></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Profesor</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="teacher" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Fecha programada</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="date" name="scheduled_date" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">Hora programada</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="time" name="scheduled_time" step="1" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">ID de la sesión en vivo</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="vimeo_live_event_id" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">ID de la grabación</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="vimeo_video_id" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Imagen de portada:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="cover" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear clase</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-class-modal" tabindex="-1" role="dialog" aria-labelledby="update-class-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="update-class-modal-label">Crear clase en vivo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="update-class" action="#" method="POST">
                        @csrf
                        @method('put')
                        <div class="row">
                            <div class="col my-5">
                                <p class="lead">
                                    Recuerda que debes iniciar sesión con la cuenta verificada de Lenovo School en
                                    Vimeo.<br><br>
                                    Por favor, da click en <a href="https://vimeo.com" target="_blank"
                                        class="text-dark font-weight-bold">VIMEO</a> para crear tu sesión en vivo <br>
                                    Una vez que hayas obtenido el link de tu transmicion, vuelve al panel y llena el
                                    siguiente formulario:
                                </p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Título</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="title" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subtítulo</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="subtitle" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Descripción</label>
                                <div class="input-group input-group-outline my-3">
                                    <textarea name="description" class="form-control"></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Profesor</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="teacher" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Fecha programada</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="date" name="scheduled_date" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">Hora programada</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="time" name="scheduled_time" step="1" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">ID de la sesión en vivo</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="vimeo_live_event_id" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">ID de la grabación</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="vimeo_video_id" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Imagen de portada:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="cover" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear clase</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <template id="table-template">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
                <div class="col d-flex justify-content-center">
                    <button class="delete-button" data-id="">
                        <i class="fas fa-trash-alt fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

        window.addEventListener("DOMContentLoaded", async () => {
            const getClasses = async () => {
                const classes = await axios.get("{{ route('panel.live-lessons.list') }}")
                    .then(response => response.data.data);

                const tbody = document.createElement("tbody");
                const template = document.getElementById("table-template");

                classes.forEach(question => {
                    const tr = template.content.cloneNode(true);

                    tr.querySelector("td:first-child").textContent = question.title;
                    tr.querySelector("td:nth-child(2)").textContent = question.teacher;
                    tr.querySelector("td:nth-child(3)").textContent = `${question.scheduled_date} ${question.scheduled_time}`;

                    const editButton = tr.querySelector("td:nth-child(4) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = question.id;
                        editButton.addEventListener("click", editClass);
                    }

                    const deleteButton = tr.querySelector("td:nth-child(4) .delete-button");

                    if (deleteButton) {
                        deleteButton.dataset.id = question.id;
                        deleteButton.addEventListener("click", deleteQuestion);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#classes-table tbody").replaceWith(tbody);
            }

            const createClassForm = document.getElementById("create-class");

            const createQuestion = e => {
                e.preventDefault();

                const createClassData = new FormData(createClassForm);

                axios.post("{{ route('panel.live-lessons.store') }}", createClassData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getClasses();
                    })
                    .then(() => {
                        $('#create-class-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(createClassForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = createClassForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            createClassForm.addEventListener("submit", createQuestion);

            const updateClassForm = document.getElementById("update-class");

            const editClass = e => {
                e.preventDefault();

                axios.get(`/panel/live-lessons/${e.target.closest(".edit-button").dataset.id}`)
                    .then((response) => {
                        const lesson = response.data.data;

                        updateClassForm.elements["title"].value = lesson.title;
                        updateClassForm.elements["subtitle"].value = lesson.subtitle;
                        updateClassForm.elements["description"].value = lesson.description;
                        updateClassForm.elements["teacher"].value = lesson.teacher;
                        updateClassForm.elements["scheduled_date"].value = lesson.scheduled_date;
                        updateClassForm.elements["scheduled_time"].value = lesson.scheduled_time;
                        updateClassForm.elements["vimeo_live_event_id"].value = lesson.vimeo_live_event_id;
                        updateClassForm.elements["vimeo_video_id"].value = lesson.vimeo_video_id;
                        updateClassForm.dataset.id = lesson.id;

                        $('#update-class-modal').modal('show');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: error.response.data.message,
                            text: "Consulte con el administrador del sistema.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });
                    });
            };

            const updateClass = e => {
                e.preventDefault();

                const updateClassData = new FormData(updateClassForm);

                axios.post(
                        `/panel/live-lessons/${updateClassForm.dataset.id}`,
                        updateClassData
                    )
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getClasses();
                    }).then(() => {
                        $('#update-class-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(updateClassForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = updateClassForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            updateClassForm.addEventListener("submit", updateClass);

            const deleteQuestion = e => {
                e.preventDefault();

                Swal.fire({
                    title: '¿Estás seguro que quiere eliminar esta pregunta?',
                    text: "La pregunta y todsas sus opciones de respuesta asociadas serán eliminadas",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete(
                                `/panel/live-lessons/${e.target.closest('.delete-button').dataset.id}`)
                            .then((response) => {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.data.message,
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                });
                                getClasses();
                            })
                            .catch((error) => {
                                Swal.fire({
                                    title: error.response.data.message,
                                    text: "Consulte con el administrador del sistema.",
                                    icon: 'error',
                                    confirmButtonColor: '#3085d6',
                                });
                            });
                    }
                });
            }

            getClasses();
        });
    </script>
@endsection
