@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Galería</h6>
                </div>
            </div>
            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#create-video-modal"
                class="greenBtn btn w-50 my-4 mb-2 text-white">Crear video</a>
            <div class="table-responsive">
                <table id="gallery-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Título</th>
                            <th>Previsualización</th>
                            <th>Archivo</th>
                            <th>Puntos por descargar</th>
                            <th>Puntos por comentar</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="create-video-modal" tabindex="-1" role="dialog" aria-labelledby="create-video-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="create-video-modal-label">Crear video</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-video" action="#" method="POST">
                        @csrf
                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Título</label>
                                <div class="input-group input-group-outline">
                                    <input type="text" name="title" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Descripción</label>
                                <div class="input-group input-group-outline">
                                    <textarea class="form-control" name="description" cols="30" rows="10"></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos por descargar</label>
                                <div class="input-group input-group-outline">
                                    <input type="number" name="points_for_downloading" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos por comentar</label>
                                <div class="input-group input-group-outline">
                                    <input type="number" name="points_per_comment" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="status">Estado</label>
                                    <select class="form-select" id="status" name="status">
                                        <option selected disabled>Seleccione una opción</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="custom-file-label font-weight-bold" for="preview">Imagen de
                                    previsualización</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="preview" id="preview">
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="custom-file-label font-weight-bold" for="file">Video</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="file" id="file">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-video-modal" tabindex="-1" role="dialog" aria-labelledby="update-video-modal-label"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="update-video-modal-label">Crear clase en vivo</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="update-video" action="#" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row mb-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Título</label>
                                <div class="input-group input-group-outline">
                                    <input type="text" name="title" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Descripción</label>
                                <div class="input-group input-group-outline">
                                    <textarea class="form-control" name="description" cols="30" rows="10"></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos por descargar</label>
                                <div class="input-group input-group-outline">
                                    <input type="number" name="points_for_downloading" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos por comentar</label>
                                <div class="input-group input-group-outline">
                                    <input type="number" name="points_per_comment" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="status">Estado</label>
                                    <select class="form-select" id="status" name="status">
                                        <option selected disabled>Seleccione una opción</option>
                                        <option value="1">Activo</option>
                                        <option value="0">Inactivo</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="custom-file-label font-weight-bold" for="preview">Imagen de
                                    previsualización</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="preview" id="preview">
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col">
                                <label class="custom-file-label font-weight-bold" for="file">Video</label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" name="file" id="file">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <template id="table-template">
        <tr>
            <td></td>
            <td>
                <a href="" target="_blank" rel="noopener noreferrer" download>Descargar</a>
            </td>
            <td>
                <a href="" target="_blank" rel="noopener noreferrer" download>Descargar</a>
            </td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>

    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

        window.addEventListener("DOMContentLoaded", async () => {
            const getGallery = async () => {
                const gallery = await axios.get("{{ route('panel.gallery.list') }}")
                    .then(response => response.data.data);

                const tbody = document.createElement("tbody");
                const template = document.getElementById("table-template");

                gallery.forEach(video => {
                    const tr = template.content.cloneNode(true);

                    tr.querySelector("td:first-child").textContent = video.title;
                    tr.querySelector("td:nth-child(2) a").href = video.preview_file_url;
                    tr.querySelector("td:nth-child(3) a").href = video.file_url;
                    tr.querySelector("td:nth-child(4)").textContent = video.points_for_downloading;
                    tr.querySelector("td:nth-child(5)").textContent = video.points_per_comment;

                    const editButton = tr.querySelector("td:nth-child(6) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = video.id;
                        editButton.addEventListener("click", editVideo);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#gallery-table tbody").replaceWith(tbody);
            }

            const createVideoForm = document.getElementById("create-video");

            const createVideo = e => {
                e.preventDefault();

                const createVideoData = new FormData(createVideoForm);

                axios.post("{{ route('panel.gallery.store') }}", createVideoData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getGallery();
                    })
                    .then(() => {
                        $('#create-video-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(createVideoForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = createVideoForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            createVideoForm.addEventListener("submit", createVideo);

            const updateVideoForm = document.getElementById("update-video");

            const editVideo = e => {
                e.preventDefault();

                axios.get(`/panel/gallery/${e.target.closest(".edit-button").dataset.id}`)
                    .then((response) => {
                        const video = response.data.data;

                        updateVideoForm.elements["title"].value = video.title;
                        updateVideoForm.elements["description"].value = video.description;
                        updateVideoForm.elements["points_for_downloading"].value = video
                            .points_for_downloading;
                        updateVideoForm.elements["points_per_comment"].value = video.points_per_comment;
                        updateVideoForm.elements["status"].value = video.status;
                        updateVideoForm.dataset.id = video.id;

                        $('#update-video-modal').modal('show');
                    })
                    .catch((error) => {
                        console.log(error);
                        Swal.fire({
                            title: error.response.data.message,
                            text: "Consulte con el administrador del sistema.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });
                    });
            };

            const updateVideo = e => {
                e.preventDefault();

                const updateUserData = new FormData(updateVideoForm);

                axios.post(`/panel/gallery/${updateVideoForm.dataset.id}`, updateUserData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getGallery();
                    })
                    .then(() => {
                        $('#update-video-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(updateVideoForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = updateVideoForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            updateVideoForm.addEventListener("submit", updateVideo);

            getGallery();
        });
    </script>
    <script>
        (function() {
            'use strict';
            document.querySelectorAll(".input-file").forEach(input => {
                input.addEventListener("change", e => {
                    let filename;

                    if (e.target.value) {
                        fileName = e.target.value.split('\\').pop();

                        e.target.labels.forEach(label => {
                            if (fileName) {
                                label.classList.add('has-file')
                                label.querySelector('span').textContent = fileName;
                            } else {
                                label.classList.remove('has-file');
                                label.querySelector('span').textContent = label.dataset.defaultText;
                            }
                        })
                    }
                });
            });
        })();
    </script>
@endsection
