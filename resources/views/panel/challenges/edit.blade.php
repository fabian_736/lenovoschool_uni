@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Actualizar desafio</h6>
                </div>
            </div>
            <form id="update-challenge" action="#" method="POST">
                @csrf
                @method('put')
                <div class="row">
                    <div class="col">
                        <div class="form-group">
                            <label class="form-label font-weight-bold" for="live-lesson">Clase en vivo asociada</label>
                            <select class="form-select" id="live-lesson" name="live_lesson_id">
                                @foreach ($liveLessons as $liveLesson)
                                    <option value="{{ $liveLesson->id }}" @if ($liveLesson->id == $challenge->live_lesson_id) selected @endif>{{ $liveLesson->title }}</option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Nombre del desafio</label>
                        <div class="input-group input-group-outline my-3">
                            <input id="challenge-name" type="text" class="form-control" name="name"
                                value="{{ $challenge->name }}" />
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Nombre del profresor</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="text" name="teacher_name" class="form-control"
                                value="{{ $challenge->teacher_name }}">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Imagen de portada:</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="file" name="cover" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <label class="form-label font-weight-bold">Firma del profesor:</label>
                        <div class="input-group input-group-outline my-3">
                            <input type="file" name="teacher_signature" class="form-control">
                            <div class="invalid-feedback"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col d-flex justify-content-center">
                        <button type="button" id="update-challenge-button"
                            class="greenBtn btn w-50 my-4 mb-2 text-white">Actualizar</button>
                    </div>
                    @if ($challenge->is_draft)
                        <div class="col d-flex justify-content-center">
                            <button type="button" id="publish-challenge-button"
                                class="greenBtn btn w-50 my-4 mb-2 text-white">Publicar</button>
                        </div>
                    @endif
                    @if ($challenge->is_published)
                        <div class="col d-flex justify-content-center">
                            <button type="button" id="move-to-draft-button" class="greenBtn btn w-50 my-4 mb-2 text-white">Pasar
                                a borrador</button>
                        </div>
                    @endif
                </div>
            </form>
        </div>
        <div class="col card mt-5">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Administración de preguntas</h6>
                </div>
            </div>
            <div class="card-body">
                @if (!$challenge->is_published)
                    <div class="row">
                        <div class="col">
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#create-question-modal"
                                class="greenBtn btn w-50 my-4 mb-2 text-white">Crear pregunta</a>
                        </div>
                    </div>
                @endif
                <table id="questions-table" class="table">
                    <thead>
                        <tr>
                            <th>Imagen</th>
                            <th>Texto de la pregunta</th>
                            <th>Puntos</th>
                            @if (!$challenge->is_published)
                                <th>Opciones</th>
                            @endif
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="modal fade" id="create-question-modal" tabindex="-1" role="dialog"
        aria-labelledby="create-question-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="create-question-modal-label">Crear pregunta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-question" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-label">Texto de la pregunta</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="label" id="question-label" min="3" max="2000" required class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="question-type" class="form-label font-weight-bold">Tipo de pregunta</label>
                                <div class="input-group input-group-outline my-3">
                                    <select id="question-type" class="form-control" required name="type">
                                        <option value="" disabled selected>Seleccione una opción</option>
                                        <option value="unique">Selección única</option>
                                        <option value="multiple">Selección múltiple</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-points">Puntos</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="number" min="1" max="5000" name="points" id="question-points" required
                                        class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subir imagen:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="image" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear pregunta</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-question-modal" tabindex="-1" role="dialog"
        aria-labelledby="update-question-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="update-question-modal-label">Actualizar pregunta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="update-question" action="#" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-label">Texto de la pregunta</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="label" id="question-label" min="3" max="2000" required class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label for="question-type" class="form-label font-weight-bold">Tipo de pregunta</label>
                                <div class="input-group input-group-outline my-3">
                                    <select id="question-type" class="form-control" required name="type">
                                        <option value="" disabled selected>Seleccione una opción</option>
                                        <option value="unique">Selección única</option>
                                        <option value="multiple">Selección múltiple</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-points">Puntos</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="number" min="1" max="5000" name="points" id="question-points" required
                                        class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subir imagen:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="image" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Actualizar</button>
                            </div>
                        </div>
                    </form>
                    <div class="col">
                        <div class="col">
                            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#create-question-option-modal" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear opción de respuesta</a>
                        </div>
                    </div>
                    <table id="questions-options-table" class="table table-sm">
                        <thead>
                            <tr>
                                <th>Imagen</th>
                                <th>Texto de la opción de respuesta</th>
                                <th>Es la respuesta correcta</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="create-question-option-modal" tabindex="-1" role="dialog"
        aria-labelledby="create-question-option-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="create-question-option-modal-label">Crear opción de
                        respuesta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-question-option" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-label">Label</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="label" min="3" max="2000" id="question-label" required class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group" role="radiogroup" aria-labelledby="is_correct-label">
                                    <span class="form-label font-weight-bold" id="is_correct-label">¿Es la respuesta
                                        correcta?</span>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="is_correct_1" name="is_correct"
                                            value="1" required>
                                        <label class="custom-control-label" for="is_correct_1" value="1">Sí</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="is_correct_0" name="is_correct"
                                            value="0" required>
                                        <label class="custom-control-label" for="is_correct_0" value="0">No</label>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subir imagen:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="image" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear opción de
                                    respuesta</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="update-question-option-modal" tabindex="-1" role="dialog"
        aria-labelledby="update-question-option-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="update-question-option-modal-label">Actualizar opción de
                        respuesta</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="update-question-option" action="#" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold" for="question-label">Texto de la opción de respuesta</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="label" min="3" max="2000" id="question-label" required class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group" role="radiogroup" aria-labelledby="is_correct-label">
                                    <span class="form-label font-weight-bold" id="is_correct-label">¿Es la respuesta
                                        correcta?</span>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" class="custom-control-input" id="is_correct_1" name="is_correct"
                                            value="1" required>
                                        <label class="custom-control-label" for="is_correct_1" value="1">Sí</label>
                                    </div>
                                    <div class="custom-control custom-radio mb-3">
                                        <input type="radio" class="custom-control-input" id="is_correct_0" name="is_correct"
                                            value="0" required>
                                        <label class="custom-control-label" for="is_correct_0" value="0">No</label>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Subir imagen:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="image" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <template id="questions-template">
        <tr>
            <td>
                <img src="" width="60" />
            </td>
            <td></td>
            <td></td>
            @if (!$challenge->is_published)
                <td class="row mx-auto">
                    <div class="col d-flex justify-content-center">
                        <button class="btn edit-button">
                            <i class="fas fa-edit fa-2x"></i>
                        </button>
                    </div>
                    <div class="col d-flex justify-content-center">
                        <button class="btn delete-button">
                            <i class="fas fa-trash-alt fa-2x"></i>
                        </button>
                    </div>
                </td>
            @endif
        </tr>
    </template>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";
    </script>
    <script>
        const challengeForm = document.getElementById("update-challenge");

        const updateFormButton = document.getElementById("update-challenge-button");
        const publishFormButton = document.getElementById("publish-challenge-button");
        const moveToDraftButton = document.getElementById("move-to-draft-button");

        const getChallengeFormData = () => new FormData(challengeForm);

        const publishChallenge = () => {
            Swal.fire({
                    title: '¿Estás seguro que quieres publicar este desafio?',
                    text: "El desafío será visible para todos los usuarios",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, publicar'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        putChallenge("{{ route('panel.challenges.publish', request()->challenge) }}", true);
                    }
                });
        }

        const moveChallengeToDraft = () => {
            Swal.fire({
                    title: '¿Estás seguro que quieres cambiar el estado del desafío a borrador?',
                    text: "El desafío quedará inaccesible para todos los usuarios",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, mover a borradores'
                })
                .then((result) => {
                    if (result.isConfirmed) {
                        putChallenge("{{ route('panel.challenges.move_to_draft', request()->challenge) }}", true);
                    }
                });
        }

        const updateChallenge = () => {
            putChallenge("{{ route('panel.challenges.update', request()->challenge) }}");
        }

        const putChallenge = (url, reloadAfterResponse = false) => {
            axios.post(url, getChallengeFormData())
                .then((response) => {
                    Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        })
                        .then(() => {
                            if (reloadAfterResponse) {
                                location.reload();
                            }
                        });
                })
                .catch((error) => {
                    Swal.fire({
                        title: "Ocurrió un error al validar sus datos",
                        text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response
                            .data.message,
                        icon: 'error',
                        confirmButtonColor: '#3085d6',
                    });

                    Array.from(challengeForm.elements).forEach(input => {
                        if (input.classList.contains("form-control")) {
                            input.classList.remove("is-invalid");
                            input.nextElementSibling.textContent = "";
                        }
                    });

                    if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                        for (const key in error.response.data.errors) {
                            if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                const input = challengeForm.elements[key];
                                input.classList.add("is-invalid");
                                input.nextElementSibling.textContent = error.response.data.errors[key][0];
                            }
                        }
                    }
                });
        }

        updateFormButton.addEventListener("click", updateChallenge);

        if (publishFormButton) {
            publishFormButton.addEventListener("click", publishChallenge);
        }

        if (moveToDraftButton) {
            moveToDraftButton.addEventListener("click", moveChallengeToDraft);
        }
    </script>

    <script>
        window.addEventListener("DOMContentLoaded", async () => {
            const getQuestions = async () => {
                const questions = await axios.get(
                        "{{ route('panel.challenges.questions.index', ['challenge' => $challenge]) }}")
                    .then(response => response.data.data);

                const tbody = document.createElement("tbody");
                const template = document.getElementById("questions-template");

                questions.forEach(question => {
                    const tr = template.content.cloneNode(true);

                    if (question.image == null) {
                        tr.querySelector("td:first-child img").remove();
                    } else {
                        tr.querySelector("td:first-child img").src = question.image;
                    }

                    tr.querySelector("td:nth-child(2)").textContent = question.label;
                    tr.querySelector("td:nth-child(3)").textContent = question.points;

                    const editButton = tr.querySelector("td:nth-child(4) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = question.id;
                        editButton.addEventListener("click", editQuestion);
                    }

                    const deleteButton = tr.querySelector("td:nth-child(4) .delete-button");

                    if (deleteButton) {
                        deleteButton.dataset.id = question.id;
                        deleteButton.addEventListener("click", deleteQuestion);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#questions-table tbody").replaceWith(tbody);
            }

            const createQuestionForm = document.getElementById("create-question");

            const createQuestion = e => {
                e.preventDefault();

                const createQuestionData = new FormData(createQuestionForm);

                axios.post("{{ route('panel.challenges.questions.store', ['challenge' => $challenge]) }}",
                        createQuestionData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getQuestions();
                    })
                    .then(() => {
                        $('#create-question-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(createQuestionForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = createQuestionForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            createQuestionForm.addEventListener("submit", createQuestion);

            const deleteQuestion = e => {
                e.preventDefault();

                Swal.fire({
                    title: '¿Estás seguro que quiere eliminar esta pregunta?',
                    text: "La pregunta y todsas sus opciones de respuesta asociadas serán eliminadas",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete(
                                `/panel/challenges/{{ request()->challenge->id }}/questions/${e.target.closest(".delete-button").dataset.id}`
                            )
                            .then((response) => {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.data.message,
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                });
                                getQuestions();
                            })
                            .catch((error) => {
                                Swal.fire({
                                    title: error.response.data.message,
                                    text: "Consulte con el administrador del sistema.",
                                    icon: 'error',
                                    confirmButtonColor: '#3085d6',
                                });
                            });
                    }
                });
            }

            const updateQuestionForm = document.getElementById("update-question");

            const editQuestion = e => {
                e.preventDefault();

                axios.get(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${e.target.closest(".edit-button").dataset.id}`
                    )
                    .then((response) => {
                        const question = response.data.data;

                        updateQuestionForm.elements["label"].value = question.label;
                        updateQuestionForm.elements["type"].value = question.type;
                        updateQuestionForm.elements["points"].value = question.points;
                        updateQuestionForm.dataset.id = question.id;

                        renderQuestionOptionsTable(question.options);

                        $('#update-question-modal').modal('show');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: error.response.data.message,
                            text: "Consulte con el administrador del sistema.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });
                    });
            };

            const updateQuestion = e => {
                e.preventDefault();

                const updateQuestionData = new FormData(updateQuestionForm);

                axios.post(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${updateQuestionForm.dataset.id}`,
                        updateQuestionData
                    )
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getQuestions();
                    }).then(() => {
                        $('#update-question-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(updateQuestionForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = updateQuestionForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][
                                        0
                                    ];
                                }
                            }
                        }
                    });
            }
            updateQuestionForm.addEventListener("submit", updateQuestion);

            const getQuestionsOptions = async question => {
                const options = await axios.get(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${question}/options`)
                    .then(response => response.data.data);

                renderQuestionOptionsTable(options);
            }

            renderQuestionOptionsTable = data => {
                const tbody = document.createElement("tbody");
                const template = document.getElementById("questions-template");

                data.forEach(option => {
                    const tr = template.content.cloneNode(true);

                    if (option.image == null) {
                        tr.querySelector("td:first-child img").remove();
                    } else {
                        tr.querySelector("td:first-child img").src = option.image;
                    }

                    tr.querySelector("td:nth-child(2)").textContent = option.label;
                    tr.querySelector("td:nth-child(3)").textContent = option.is_correct ? "Sí" : "No";

                    const editButton = tr.querySelector("td:nth-child(4) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = option.id;
                        editButton.addEventListener("click", editQuestionOption);
                    }

                    const deleteButton = tr.querySelector("td:nth-child(4) .delete-button");

                    if (deleteButton) {
                        deleteButton.dataset.id = option.id;
                        deleteButton.addEventListener("click", deleteQuestionOption);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#questions-options-table tbody").replaceWith(tbody);
            }

            const createQuestionOptionForm = document.getElementById("create-question-option");

            const createQuestionOption = e => {
                e.preventDefault();

                const createQuestionOptionData = new FormData(createQuestionOptionForm);

                axios.post(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${updateQuestionForm.dataset.id}/options`,
                        createQuestionOptionData)
                    .then((response) => {
                        $('#create-question-option-modal').modal('hide');
                        Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                            })
                            .then(() => {
                                $('#update-question-modal').modal('show');
                            });
                        getQuestionsOptions(updateQuestionForm.dataset.id);
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(createQuestionOptionForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                if (input.nextElementSibling.classList.contains(
                                        "invalid-feedback")) {
                                    input.nextElementSibling.textContent = "";
                                }
                            }

                            if (Array.isArray(input)) {
                                input.forEach(input => {
                                    input.classList.remove("is-invalid");
                                    if (input.nextElementSibling.classList.contains(
                                            "invalid-feedback")) {
                                        input.nextElementSibling.textContent = "";
                                    }
                                });
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    let inputs = createQuestionOptionForm.elements[key];
                                    if (!(inputs.forEach)) {
                                        inputs = [inputs];
                                    }

                                    inputs.forEach(input => {
                                        input.classList.add("is-invalid");
                                        const feedBack = input.parentNode.querySelector(
                                            ".invalid-feedback");
                                        if (feedBack) {
                                            feedBack.textContent = error.response.data.errors[key][
                                                0
                                            ];
                                        }
                                    });
                                }
                            }
                        }
                    });
            }
            createQuestionOptionForm.addEventListener("submit", createQuestionOption);

            const deleteQuestionOption = e => {
                e.preventDefault();

                Swal.fire({
                    title: '¿Está seguro que quiere eliminar esta opción de respuesta?',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Si, eliminar'
                }).then((result) => {
                    if (result.isConfirmed) {
                        axios.delete(
                                `/panel/challenges/{{ request()->challenge->id }}/questions/${updateQuestionForm.dataset.id}/options/${e.target.closest(".delete-button").dataset.id}`
                            )
                            .then((response) => {
                                Swal.fire({
                                    position: 'center',
                                    icon: 'success',
                                    title: response.data.message,
                                    showConfirmButton: true,
                                    confirmButtonText: "Cerrar"
                                });
                                getQuestionsOptions(updateQuestionForm.dataset.id);
                            })
                            .catch((error) => {
                                Swal.fire({
                                    title: error.response.data.message,
                                    text: "Consulte con el administrador del sistema.",
                                    icon: 'error',
                                    confirmButtonColor: '#3085d6',
                                });
                            });
                    }
                });
            }

            const updateQuestionOptionForm = document.getElementById("update-question-option");

            const editQuestionOption = e => {
                e.preventDefault();

                $('#update-question-modal').modal('hide');
                axios.get(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${updateQuestionForm.dataset.id}/options/${e.target.closest(".edit-button").dataset.id}`
                    )
                    .then((response) => {
                        const question = response.data.data;

                        updateQuestionOptionForm.elements["label"].value = question.label;

                        updateQuestionOptionForm.elements["is_correct"].forEach(input => {
                            input.checked = (input.value == 1 && question.is_correct) || (input
                                .value == "0" && !question.is_correct);
                        });

                        updateQuestionOptionForm.elements["is_correct"].value = question.is_correct;
                        updateQuestionOptionForm.dataset.id = question.id;

                        $('#update-question-option-modal').modal('show');
                    })
                    .catch((error) => {
                        Swal.fire({
                                title: error.response.data.message,
                                text: "Consulte con el administrador del sistema.",
                                icon: 'error',
                                confirmButtonColor: '#3085d6',
                            })
                            .then(() => {
                                $('#update-question-modal').modal('show');
                            });
                    });
            };

            const updateQuestionOption = e => {
                e.preventDefault();

                const updateQuestionOptionData = new FormData(updateQuestionOptionForm);

                axios.post(
                        `/panel/challenges/{{ request()->challenge->id }}/questions/${updateQuestionForm.dataset.id}/options/${updateQuestionOptionForm.dataset.id}`,
                        updateQuestionOptionData
                    )
                    .then((response) => {
                        $('#update-question-option-modal').modal('hide');
                        Swal.fire({
                                position: 'center',
                                icon: 'success',
                                title: response.data.message,
                                showConfirmButton: true,
                                confirmButtonText: "Cerrar"
                            })
                            .then(() => {
                                $('#update-question-modal').modal('show');
                            });
                        getQuestionsOptions(updateQuestionForm.dataset.id);
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(updateQuestionOptionForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                if (input.nextElementSibling.classList.contains(
                                        "invalid-feedback")) {
                                    input.nextElementSibling.textContent = "";
                                }
                            }

                            if (Array.isArray(input)) {
                                input.forEach(input => {
                                    input.classList.remove("is-invalid");
                                    if (input.nextElementSibling.classList.contains(
                                            "invalid-feedback")) {
                                        input.nextElementSibling.textContent = "";
                                    }
                                });
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    let inputs = updateQuestionOptionForm.elements[key];
                                    if (!(inputs.forEach)) {
                                        inputs = [inputs];
                                    }

                                    inputs.forEach(input => {
                                        input.classList.add("is-invalid");
                                        const feedBack = input.parentNode.querySelector(
                                            ".invalid-feedback");
                                        if (feedBack) {
                                            feedBack.textContent = error.response.data.errors[key][
                                                0
                                            ];
                                        }
                                    });
                                }
                            }
                        }
                    });
            }
            updateQuestionOptionForm.addEventListener("submit", updateQuestionOption);

            getQuestions();
        });
    </script>
@endsection
