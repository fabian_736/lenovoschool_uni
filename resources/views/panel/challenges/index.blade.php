@extends('panel.layouts.content.app')
@section('content')
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script type="module" src="{{ url('js/scripts/panel_chall_index.js') }}"></script>


    <link rel="stylesheet" href="style.css">
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Listado de todos los desafíos</h6>
                </div>
            </div>
            <a href="javascript:;" data-bs-toggle="modal" data-bs-target="#exampleModal"
                class="greenBtn btn w-50 my-4 mb-2 text-white">Crear desafio</a>
            <div class="table-responsive">
                <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Estado</th>
                            <th>Fecha de publicación</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($challenges as $challenge)
                            <tr>
                                <td>{{ $challenge->name }}</td>
                                <td>{{ $challenge->translated_status }}</td>
                                <td>{{ $challenge->published_at?->format('Y-m-d H:i:s') }}</td>
                                <td class="row mx-auto">
                                    <div class="col d-flex justify-content-center">
                                        <a href="{{ route('panel.challenges.show', $challenge) }}">
                                            <i class="fas fa-edit fa-2x"></i>
                                        </a>
                                    </div>
                                    <div class="col d-flex justify-content-center">
                                        <a href="javascript:;" class="delete-button" data-id="{{ $challenge->id }}">
                                            <i class="fas fa-trash-alt fa-2x"></i>
                                        </a>
                                    </div>
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="6">No hay desafíos para mostrar</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="exampleModalLabel">Crear desafio</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="create-challenge" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="live-lesson">Clase en vivo
                                        asociada</label>
                                    <select class="form-select" id="live-lesson" name="live_lesson_id">
                                        <option selected disabled>Seleccione una opción</option>
                                        @foreach ($liveLessons as $liveLesson)
                                            <option value="{{ $liveLesson->id }}">{{ $liveLesson->title }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre del desafio</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="name" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre del profresor</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="teacher_name" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Imagen de portada:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="cover" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Firma del profesor:</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="teacher_signature" class="form-control">
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear desafio</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
