@extends('panel.layouts.content.app')
@section('content')
    <div class="page-header min-height-300 border-radius-xl mt-4" style="background-image: url('img/tutorial.png');">
        <span class="mask  bg-gradient-primary  opacity-6"></span>
    </div>
    <div class="card card-body mx-3 mx-md-4 mt-n6">
        <div class="row gx-4 mb-2">
            <div class="col-auto">
                <div class="avatar avatar-xl position-relative">
                    <img src="{{ auth()->user()->getFirstMediaUrl('avatar') }}" alt="profile_image"
                        class="w-100 border-radius-lg shadow-sm">
                </div>
            </div>
            <div class="col-auto my-auto">
                <div class="h-100">
                    <h5 class="mb-1">
                        Usuario Prueba
                    </h5>
                    <p class="mb-0 font-weight-normal text-sm">
                        Super Admin
                    </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="row">
                <div class="col-12 col-xl-12">
                    <div class="card card-plain h-100">
                        <div class="card-header pb-0 p-3">
                            <h6 class="mb-0">Solicitud de cambios</h6>
                        </div>
                        <div class="card-body p-3">
                            <div class="row">
                                <form action="">
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group input-group-outline my-3">
                                                <label class="form-label">Nombre</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="input-group input-group-outline my-3">
                                                <label class="form-label">Apellido</label>
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="input-group input-group-outline my-3">
                                                <label class="form-label">Empresa</label>
                                                <textarea name="" id="" cols="30" rows="10" class="form-control"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <a href="javascript:;" onclick="confirm()"
                                                class="greenBtn btn w-25 my-4 mb-2 text-white">Enviar solicitud</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
