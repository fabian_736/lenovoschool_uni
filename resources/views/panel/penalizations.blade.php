@extends('panel.layouts.content.app')
@section('content')
    {{-- HEADER TABLA --}}
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Penalizacion de puntos a los usuarios</h6>
                </div>
            </div>
            <div class="table-responsive">
                <table id="users-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Correo electrónico</th>
                            <th>Puntos disponibles</th>
                            <th>Puntos Totales</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- MODAL --}}
    <div class="modal fade" id="update-user-modal" tabindex="-1" role="dialog" aria-labelledby="update-user-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="update-user-modal-label">Modificar Puntos</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="update-user" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="name" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos Disponibles</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="number" name="available_redemption_points" class="form-control" disabled required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                            <div class="col">
                                <label class="form-label font-weight-bold">Puntos Totales</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="number" name="total_redemption_points" class="form-control" disabled required />
                                </div>
                                <hr>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label class="form-label font-weight-bold">Razón de la penalización</label>
                                    <div class="input-group input-group-outline my-3">
                                        <textarea name="reason" class="form-control" required cols="30" rows="10"></textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <label class="form-label font-weight-bold">Puntos a reducir</label>
                                    <div class="input-group input-group-outline my-3">
                                        <input type="number" name="deducted_points" class="form-control" required />
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col d-flex justify-content-center">
                                    <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Penalizar</button>
                                </div>
                            </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- TABLE CONTENT --}}
    <template id="table-template">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>

    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        axios.defaults.headers.common['Content-Type'] = "Application/json";
        axios.defaults.headers.common['Accepts'] = "Application/json";
        axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

        window.addEventListener("DOMContentLoaded", async () => {
            const getUSers = async () => {

                const users = await axios.get("{{ route('panel.users.list') }}")
                    .then(response => response.data.data);

                const tbody = document.createElement("tbody");
                const template = document.getElementById("table-template");

                const students = users.filter(user => user.user_type === 'student')

                students.forEach(user => {
                    const tr = template.content.cloneNode(true);

                    tr.querySelector("td:first-child").textContent = user.fullname;
                    tr.querySelector("td:nth-child(2)").textContent = user.email;
                    tr.querySelector("td:nth-child(3)").textContent = user.available_redemption_points;
                    tr.querySelector("td:nth-child(4)").textContent = user.total_redemption_points;

                    const editButton = tr.querySelector("td:nth-child(5) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = user.id;
                        editButton.addEventListener("click", openForm);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#users-table tbody").replaceWith(tbody);
            }


            const updateUserForm = document.getElementById("update-user");

            const openForm = e => {
                e.preventDefault();
                axios.get(`/panel/users/${e.target.closest(".edit-button").dataset.id}`)
                    .then((response) => {
                        const user = response.data.data;

                        updateUserForm.elements["name"].value = user.fullname;
                        updateUserForm.elements["total_redemption_points"].value = user.total_redemption_points;
                        updateUserForm.elements["available_redemption_points"].value = user.available_redemption_points;
                        updateUserForm.dataset.id = user.id;

                        $('#update-user-modal').modal('show');
                    })
                    .catch((error) => {
                        console.log(error);
                        Swal.fire({
                            title: error.response.data.message,
                            text: "Consulte con el administrador del sistema.",
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });
                    });
            };

            const updateUser = e => {
                e.preventDefault();

                const updateUserData = {
                    "reason": updateUserForm.elements.reason.value,
                    "deducted_points": updateUserForm.elements.deducted_points.value,
                };

                axios.post(`/panel/penalizations/${updateUserForm.dataset.id}`, updateUserData)
                    .then((response) => {
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: response.data.message,
                            showConfirmButton: true,
                            confirmButtonText: "Cerrar"
                        });
                        getUSers();
                    })
                    .then(() => {
                        $('#update-user-modal').modal('hide');
                    })
                    .catch((error) => {
                        Swal.fire({
                            title: "Ocurrió un error al validar sus datos",
                            text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                            icon: 'error',
                            confirmButtonColor: '#3085d6',
                        });

                        Array.from(updateUserForm.elements).forEach(input => {
                            if (input.classList.contains("form-control")) {
                                input.classList.remove("is-invalid");
                                input.nextElementSibling.textContent = "";
                            }
                        });

                        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                            for (const key in error.response.data.errors) {
                                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                                    const input = updateUserForm.elements[key];
                                    input.classList.add("is-invalid");
                                    input.nextElementSibling.textContent = error.response.data.errors[key][0];
                                }
                            }
                        }
                    });
            }
            updateUserForm.addEventListener("submit", updateUser);

            getUSers();
        });
    </script>
@endsection
