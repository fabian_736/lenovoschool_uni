@extends('panel.layouts.content.app')
@push('scripts')
    <script src="{{ mix('js/panel/badges.js') }}"></script>
@endpush
@section('content')
    <div class="row-reverse">
        <div class="col card card-body ">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Insignias</h6>
                </div>
            </div>
            <button type="button" id="create-badge" class="greenBtn btn w-50 my-4 mb-2 text-white">Crear insignia</button>
            <div class="table-responsive">
                <table id="badges-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nombre</th>
                            <th>Descripción</th>
                            <th>Tipo</th>
                            <th>Condiciones de obtención</th>
                            <th>Estado</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="badge-modal" tabindex="-1" role="dialog" aria-labelledby="badge-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="badge-modal-label"></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="badge-form" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="name" class="form-control" required />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Descripción</label>
                                <div class="input-group input-group-outline my-3">
                                    <textarea name="description" id="description" cols="30" rows="10" class="form-control" required></textarea>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="type">Tipo</label>
                                    <select class="form-select my-3" id="type" name="type" required>
                                        <option value="" selected disabled>Seleccione una opción</option>
                                        <option value="TOTAL_VIEWED_LIVE_LESSONS">Clases en vivo vistas</option>
                                        <option value="TOTAL_GALLERY_COMMENTS">Comentarios en la galería</option>
                                        <option value="TOTAL_FINISHED_CHALLENGES">Desafíos finalizados</option>
                                        <option value="TOTAL_GALLERY_DOWNLOADS">Descargas en la galería</option>
                                        <option value="TOTAL_LOGINS">Días iniciando sesión</option>
                                        <option value="TOTAL_REWARD_REDEMPTIONS">Premios redimidos</option>
                                        <option value="TOTAL_REDEEMED_POINTS">Puntos redimidos</option>
                                        <option value="TOTAL_ACCUMULATED_POINTS">Puntos totales acumulados</option>
                                        <option value="TOTAL_COMMENTED_GALLERY_VIDEOS">Videos de la galería comentados</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold">Cantidad requerida</label>
                                    <div class="input-group input-group-outline my-3">
                                        <input type="number" name="total" min="1" step="1" class="form-control" required />
                                        <div class="invalid-feedback"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="status">Estado</label>
                                    <select class="form-select my-3" id="status" name="status" required>
                                        <option value="" selected disabled>Seleccione una opción</option>
                                        <option value="1">Activa</option>
                                        <option value="0">Inactiva</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold" for="image">Imagen</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="file" name="image" class="form-control-file" />
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white"></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <template id="table-template">
        <tr>
            <td>
                <img src="" alt="" style="height: 80px;" />
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="btn greenBtn edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>
@endsection
