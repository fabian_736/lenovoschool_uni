<form action="{{ route('panel.rewards.store') }}" method="post" enctype="multipart/form-data">
    @csrf
    <div class="row">
        <div class="col">
            <label class="form-label">Nombre del premio</label>
            <div class="input-group input-group-outline my-3">
                <input type="text" class="form-control" name="name">
            </div>
        </div>
        <div class="col">
            <label class="form-label">Descripción</label>
            <div class="input-group input-group-outline my-3">
                <input type="text" class="form-control" name="description">
            </div>
        </div>
    </div>
    <div class="row">

        <div class="col">
            <label class="form-label font-weight-bold">Puntos necesarios para redimir</label>
            <div class="input-group input-group-outline my-3">
                <input type="number" class="form-control" name="redeem_points" required>
            </div>
        </div>

        <div class="col">
            <label class="form-label font-weight-bold">Cantidad disponible</label>
            <div class="input-group input-group-outline my-3">
                <input type="number" name="quantity_available" class="form-control" required>
            </div>
        </div>
    </div>
    <div class="col">
        <label class="form-label font-weight-bold">Subir imagen:</label>
        <div class="input-group input-group-outline my-3">
            <input type="file" name="image" class="form-control">
        </div>
    </div>
    </div>
    <div class="row">


        <div class="col d-flex justify-content-center">
            <button type="submit" href="javascript:;" onclick="confirm()"
                class="greenBtn btn w-50 my-4 mb-2 text-white">Crear premio</button>
        </div>
    </div>
</form>