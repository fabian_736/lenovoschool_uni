@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Crear un premio</h6>
                </div>
            </div>
            @include('panel.rewards.partials.form')
        </div>
    </div>


    <script>
        const confirm = () => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Premio registrado',
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                window.location = "{{ route('panel.rewards.all') }}";
            });
        }
    </script>
@endsection
