@extends('panel.layouts.content.app')
@section('content')
    <div class="row-reverse">
        <div class="col card card-body">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Listado de todos los premios</h6>
                </div>
                @if (session('success'))
                    <div class="alert alert-success">{{ session('success') }}</div>
                @endif
                @if (session('message'))
                    <div class="alert alert-info">{{ session('message') }}</div>
                @endif
                @if (session('error'))
                    <div class="alert alert-danger">{{ session('error') }}</div>
                @endif
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
            </div>
            <div class=" table-responsive">
            <table id="example" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Puntos necesarios</th>
                        <th>Cantidad disponible</th>
                        <th>Fotografia</th>
                        <th>Fecha</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($rewards as $reward)
                        <tr data-id="{{ $reward->id }}">
                            <td>{{ $reward->name }}</td>
                            <td>{{ $reward->redeem_points }}</td>
                            <td>{{ $reward->quantity_available }}</td>
                            <td>
                                @if (count($reward->media) > 0)
                                    <a href="{{ $reward->getFirstMediaUrl('reward') }}" target="_blank">Ver </a>
                                @else
                                    N/A
                                @endif
                            </td>
                            <td>{{ $reward->created_at }}</td>
                            <td class="row mx-auto">
                                <div class="col d-flex justify-content-center">
                                    <a href="{{ route('panel.rewards.edit', [$reward]) }}" class="btn btn-primary">
                                        <i class="fa fa-edit"></i></a>
                                </div>
                                <div class="col d-flex justify-content-center">
                                    <form action="{{ route('panel.rewards.destroy', [$reward]) }}" method="post">
                                        @csrf
                                        {{ method_field('delete') }}
                                        <Button type="submit">
                                            <i class="fas fa-trash-alt fa-2x"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>

        </div>
        @endforeach
        </tbody>
        </table>
    </div>
        {{ $rewards->render() }}
    </div>
    </div>


    <!-- Modal -->


    <script>
        const update = () => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: 'Premio actualizado',
                showConfirmButton: false,
                timer: 1500
            }).then(function() {
                window.location = "{{ route('panel.rewards.all') }}";
            });
        }

        const undelete = () => {
            Swal.fire({
                title: '¿Estás seguro que quieres eliminar este premio?',
                text: "Eliminacion permanente",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Si, Eliminar'
            }).then((result) => {
                if (result.isConfirmed) {
                    Swal.fire(
                        'Eliminando!',
                        'Este premio ha sido eliminado.',
                        'success'
                    )
                }
            }).then(function() {
                window.location = "{{ route('panel.rewards.all') }}";
            });
        }
    </script>
@endsection
