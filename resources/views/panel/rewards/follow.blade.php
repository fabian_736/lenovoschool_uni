@extends('panel.layouts.content.app')
@push('scripts')
    <script src="{{ mix('js/panel/rewards/tracking.js') }}"></script>
@endpush
@section('content')
    <div class="row-reverse">
        <div class="col card card-body ">
            <div class="card-header p-0 position-relative mt-n4 mx-3 z-index-2 mb-5">
                <div class="bg-gradient-primary shadow-primary border-radius-lg pt-4 pb-3">
                    <h6 class="text-white text-capitalize ps-3">Seguimiento de redenciones</h6>
                </div>
            </div>
            <div class="table-responsive">
                <table id="redemptions-table" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>Premio redimido</th>
                            <th>Usuario</th>
                            <th>Destinatario</th>
                            <th>Documento del destinatario</th>
                            <th>email del destinatario</th>
                            <th>Departamento de entrega</th>
                            <th>Dirección de entrega</th>
                            <th>Teléfono del destinatario</th>
                            <th>Estado de la entrega</th>
                            <th>Fecha de redención</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="redemption-modal" tabindex="-1" role="dialog" aria-labelledby="redemption-modal-label" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content ">
                <div class="modal-header">
                    <h5 class="modal-title font-weight-normal" id="redemption-modal-label">Actualizar estado del beneficio</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i class="far fa-times-circle text-dark"></i>
                    </button>
                </div>
                <div class="modal-body p-5">
                    <form id="redemption-form" action="#" method="POST">
                        @csrf
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Premio</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="reward_name" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Usuario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="user_name" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Nombre del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_name" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Tipo de documento del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_document_type" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Documento del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_document_number" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Email del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_email" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Departamento de entrega del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_departament" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Dirección del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_address" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <label class="form-label font-weight-bold">Teléfono del destinatario</label>
                                <div class="input-group input-group-outline my-3">
                                    <input type="text" name="receiver_phone" class="form-control" disabled />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold" for="status">Estado</label>
                                    <select class="form-select my-3" id="status" name="status" required>
                                        <option value="" selected disabled>Seleccione una opción</option>
                                        <option value="pending">Pendiente de envío</option>
                                        <option value="sent">Enviado</option>
                                        <option value="delivered">Entregado</option>
                                    </select>
                                    <div class="invalid-feedback"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="form-label font-weight-bold">Fecha de redención</label>
                                    <div class="input-group input-group-outline my-3">
                                        <input type="text" name="created_at" class="form-control" disabled />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col d-flex justify-content-center">
                                <button type="submit" class="greenBtn btn w-50 my-4 mb-2 text-white">Actualizar</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <template id="table-template">
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td class="row mx-auto">
                <div class="col d-flex justify-content-center">
                    <button class="edit-button" data-id="">
                        <i class="fas fa-edit fa-2x"></i>
                    </button>
                </div>
            </td>
        </tr>
    </template>
@endsection
