import axios from "@axios";
import $ from "@jquery";
import swal from "sweetalert";

class Badge {
    /**
     * @type {HTMLTableElement} The badges table
     */
    #badgesTable = document.getElementById("badges-table");

    /**
     * @type {HTMLFormElement} The badge form
     */
    #badgeForm = document.getElementById("badge-form");

    /**
     * @type {HTMLButtonElement} The badge form
     */
    #createButton = document.getElementById("create-badge");

    /**
     * @type {HTMLTemplateElement} The badge table row template
     */
    #template = document.getElementById("table-template");

    /**
     * @type {JQuery<HTMLElement>} The badge modal
     */
    #badgeModal = $("#badge-modal");

    /**
     * @type {HTMLHeadingElement}
     */
    #badgeModalTitle = document.querySelector("#badge-modal .modal-title");

    /**
     * @type {HTMLButtonElement}
     */
    #badgeModalButton = document.querySelector("#badge-form button");

    /**
     * @type {string} The current route for the badge form
     */
    #url;

    /**
     * @type {bool} indicates that is necessary the method spoofing for put request
     */
    #spoofingPutRequest;

    /**
     * @type {string} The current ID for the request
     */
    #currentId;

    /**
     * Creates a new instance of the class
     */
    constructor() {
        this.#badgeForm.addEventListener("submit", this.submitForm);
        this.#createButton.addEventListener("click", this.openModalForCreate);

        this.getBadges();
    }

    /**
     * Get and render the badges
     */
    getBadges = async () => {
        await axios.get("/panel/badges/list")
            .then(({ data }) => {
                while (this.#badgesTable.tBodies.length > 0) {
                    this.#badgesTable.tBodies[0].remove();
                }

                const tbody = this.#badgesTable.createTBody();

                data.data.forEach(badge => {
                    const tr = this.#template.content.cloneNode(true);

                    tr.querySelector("td:first-child img").src = badge.image;
                    tr.querySelector("td:nth-child(2)").textContent = badge.name;
                    tr.querySelector("td:nth-child(3)").textContent = badge.description;
                    tr.querySelector("td:nth-child(4)").textContent = badge.type;
                    tr.querySelector("td:nth-child(5)").textContent = badge.total;
                    tr.querySelector("td:nth-child(6)").textContent = badge.status ? "Activa" : "Inactiva";

                    const editButton = tr.querySelector("td:nth-child(7) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = badge.id;
                        editButton.addEventListener("click", this.editBadge);
                    }

                    tbody.appendChild(tr);
                });
            });
    };

    /**
     * Opens the modal for create a new badge
     */
    openModalForCreate = () => {
        this.#badgeModalTitle.textContent = "Crear insignia";
        this.#badgeModalButton.textContent = "Crear";
        this.#badgeForm.reset();
        this.#url = "/panel/badges";
        this.#spoofingPutRequest = false;
        this.#badgeModal.modal("show");
    };

    /**
     * Fetch the given badge and opens the dialog for update
     * @param {Event} e The click event
     */
    editBadge = e => {
        e.preventDefault();

        this.#currentId = e.target.closest(".edit-button").dataset.id;

        axios.get(`/panel/badges/${this.#currentId}`)
            .then((response) => {
                this.#badgeForm.reset();

                const badge = response.data.data;

                this.#badgeForm.elements["name"].value = badge.name;
                this.#badgeForm.elements["description"].value = badge.description;
                this.#badgeForm.elements["type"].value = badge.type_value;
                this.#badgeForm.elements["total"].value = badge.total;
                this.#badgeForm.elements["status"].value = badge.status ? "1" : "0";

                this.#badgeModalTitle.textContent = "Actualizar insignia";
                this.#badgeModalButton.textContent = "Actualizar";

                this.#url = `/panel/badges/${this.#currentId}`;
                this.#spoofingPutRequest = true;
                this.#badgeModal.modal("show");
            })
            .catch((error) => {
                console.log(error);
                swal({
                    title: error.response.data.message,
                    text: "Consulte con el administrador del sistema.",
                    icon: "error",
                });
            });
    };

    /**
     * Make a request to create/update a badge
     * @param {Event} e The submit event
     */
    submitForm = e => {
        e.preventDefault();

        const data = new FormData(this.#badgeForm);

        if (this.#spoofingPutRequest) {
            data.append("_method", "PUT");
        }

        axios.post(this.#url, data)
            .then((response) => {
                swal({
                    title: response.data.message,
                    icon: "success",
                });

                this.getBadges();
            })
            .then(() => {
                this.#badgeModal.modal("hide");
            })
            .catch((error) => {
                console.log(error);

                swal({
                    title: "Ocurrió un error al validar sus datos",
                    text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                    icon: "error",
                });

                Array.from(this.#badgeForm.elements).forEach(input => {
                    if (input.classList.contains("form-control")) {
                        input.classList.remove("is-invalid");
                        input.nextElementSibling.textContent = "";
                    }
                });

                if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                    for (const key in error.response.data.errors) {
                        if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                            const input = this.#badgeForm.elements[key];
                            input.classList.add("is-invalid");
                            input.nextElementSibling.textContent = error.response.data.errors[key][0];
                        }
                    }
                }
            });
    };
}

new Badge();
