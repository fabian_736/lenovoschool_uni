import axios from "@axios";
import $ from "@jquery";
import swal from "sweetalert";

class RewardTracking {
    /**
     * @type {HTMLTableElement} The redemptions table
     */
    #redemptionsTable = document.getElementById("redemptions-table");

    /**
     * @type {HTMLFormElement} The redemption form
     */
    #redemptionForm = document.getElementById("redemption-form");

    /**
     * @type {HTMLTemplateElement} The redemption table row template
     */
    #template = document.getElementById("table-template");

    /**
     * @type {JQuery<HTMLElement>} The redemption modal
     */
    #redemptionModal = $("#redemption-modal");

    /**
     * @type {string} The current ID for the request
     */
    #currentId;

    /**
     * Creates a new instance of the class
     */
    constructor() {
        this.#redemptionForm.addEventListener("submit", this.submitForm);

        this.getRedemptions();
    }

    /**
     * Get and render the redemptions
     */
    getRedemptions = async () => {
        await axios.get("/panel/rewards/redemption-status/list")
            .then(({ data }) => {
                while (this.#redemptionsTable.tBodies.length > 0) {
                    this.#redemptionsTable.tBodies[0].remove();
                }

                const tbody = this.#redemptionsTable.createTBody();

                data.data.forEach(redemption => {
                    const tr = this.#template.content.cloneNode(true);

                    tr.querySelector("td:nth-child(1)").textContent = redemption.reward.name;
                    tr.querySelector("td:nth-child(2)").textContent = redemption.user.fullname;
                    tr.querySelector("td:nth-child(3)").textContent = redemption.receiver_name;
                    tr.querySelector("td:nth-child(4)").textContent = `${redemption.receiver_document_type.name} ${redemption.receiver_document_number}`;
                    tr.querySelector("td:nth-child(5)").textContent = redemption.receiver_email;
                    tr.querySelector("td:nth-child(6)").textContent = redemption.receiver_departament.name;
                    tr.querySelector("td:nth-child(7)").textContent = redemption.receiver_address;
                    tr.querySelector("td:nth-child(8)").textContent = redemption.receiver_phone;
                    tr.querySelector("td:nth-child(9)").textContent = redemption.status_label;
                    tr.querySelector("td:nth-child(10)").textContent = this.#formatDate(redemption.created_at);

                    const editButton = tr.querySelector("td:nth-child(11) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = redemption.id;
                        editButton.addEventListener("click", this.editRedemption);
                    }

                    tbody.appendChild(tr);
                });
            });
    };

    /**
     * Fetch the given redemption and opens the dialog for update
     * @param {Event} e The click event
     */
    editRedemption = e => {
        e.preventDefault();

        this.#currentId = e.target.closest(".edit-button").dataset.id;

        axios.get(`/panel/rewards/redemption-status/${this.#currentId}`)
            .then((response) => {
                this.#redemptionForm.reset();

                const redemption = response.data.data;

                this.#redemptionForm.elements["reward_name"].value = redemption.reward.name;
                this.#redemptionForm.elements["user_name"].value = redemption.user.fullname;
                this.#redemptionForm.elements["receiver_name"].value = redemption.receiver_name;
                this.#redemptionForm.elements["receiver_document_type"].value = redemption.receiver_document_type.name;
                this.#redemptionForm.elements["receiver_document_number"].value = redemption.receiver_document_number;
                this.#redemptionForm.elements["receiver_email"].value = redemption.receiver_email;
                this.#redemptionForm.elements["receiver_departament"].value = redemption.receiver_departament.name;
                this.#redemptionForm.elements["receiver_address"].value = redemption.receiver_address;
                this.#redemptionForm.elements["receiver_phone"].value = redemption.receiver_phone;
                this.#redemptionForm.elements["status"].value = redemption.status;
                this.#redemptionForm.elements["created_at"].value = this.#formatDate(redemption.created_at);

                this.#redemptionModal.modal("show");
            })
            .catch((error) => {
                swal({
                    title: error.response.data.message,
                    text: "Consulte con el administrador del sistema.",
                    icon: "error",
                });
            });
    };

    /**
     * Make a request to create/update a redemption
     * @param {Event} e The submit event
     */
    submitForm = e => {
        e.preventDefault();

        const data = {
            "status": this.#redemptionForm.elements["status"].value
        };

        axios.patch(`/panel/rewards/redemption-status/${this.#currentId}`, data)
            .then((response) => {
                swal({
                    title: response.data.message,
                    icon: "success",
                });

                this.getRedemptions();
            })
            .then(() => {
                this.#redemptionModal.modal("hide");
            })
            .catch((error) => {
                console.log(error);

                swal({
                    title: "Ocurrió un error al validar sus datos",
                    text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                    icon: "error",
                });

                Array.from(this.#redemptionForm.elements).forEach(input => {
                    if (input.classList.contains("form-control")) {
                        input.classList.remove("is-invalid");
                        input.nextElementSibling.textContent = "";
                    }
                });

                if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                    for (const key in error.response.data.errors) {
                        if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                            const input = this.#redemptionForm.elements[key];
                            input.classList.add("is-invalid");
                            input.nextElementSibling.textContent = error.response.data.errors[key][0];
                        }
                    }
                }
            });
    };

    /**
     * Fomats the given date string to locale string
     * @param {string} dateString
     * @returns string
     */
    #formatDate(dateString) {

        const date = new Date(dateString);
        const dateOptions = {
            year: "numeric",
            month: "long",
            day: "numeric",
            hour: "numeric",
            minute: "numeric",
            hour12: true
        };

        return new Intl.DateTimeFormat("es-pe", dateOptions).format(date);
    }
}

new RewardTracking();
