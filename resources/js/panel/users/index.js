import axios from "@axios";
import $ from "@jquery";
import swal from "sweetalert";

class User {
    /**
     * @type {HTMLTableElement} The users table
     */
    #usersTable = document.getElementById("users-table");

    /**
     * @type {HTMLFormElement} The user form
     */
    #userForm = document.getElementById("user-form");

    /**
     * @type {HTMLButtonElement} The create user button
     */
    #createButton = document.getElementById("create-user-button");

    /**
     * @type {HTMLTemplateElement} The user table row template
     */
    #template = document.getElementById("table-template");

    /**
     * @type {JQuery<HTMLElement>} The user modal
     */
    #userModal = $("#user-modal");

    /**
     * @type {HTMLHeadingElement}
     */
    #userModalTitle = document.querySelector("#user-modal .modal-title");

    /**
     * @type {HTMLButtonElement}
     */
    #userModalButton = document.querySelector("#user-form button");

    /**
     * @type {string} The current route for the user form
     */
    #url;

    /**
     * @type {bool} indicates that is necessary the method spoofing for put request
     */
    #spoofingPutRequest;

    /**
     * @type {string} The current ID for the request
     */
    #currentId;

    /**
     * Creates a new instance of the class
     */
    constructor() {
        this.#userForm.addEventListener("submit", this.submitForm);
        this.#createButton.addEventListener("click", this.openModalForCreate);

        this.getUsers();
    }

    /**
     * Get and render the users
     *
     * @returns {Promise<void>}
     */
    getUsers = async () => {
        await axios.get("/panel/users/list")
            .then(({ data }) => {
                const users = data.data;

                /** @type {HTMLTableSectionElement} */
                const tbody = document.createElement("tbody");

                /** @type {HTMLTemplateElement} */
                const template = document.getElementById("table-template");

                users.forEach(user => {
                    const tr = template.content.cloneNode(true);

                    tr.querySelector("td:first-child").textContent = user.fullname;
                    tr.querySelector("td:nth-child(2)").textContent = user.email;
                    tr.querySelector("td:nth-child(3)").textContent = user.position;
                    tr.querySelector("td:nth-child(4)").textContent = user.company;
                    tr.querySelector("td:nth-child(5)").textContent = user.user_type;

                    const editButton = tr.querySelector("td:nth-child(6) .edit-button");

                    if (editButton) {
                        editButton.dataset.id = user.id;
                        editButton.addEventListener("click", this.editUser);
                    }

                    tbody.appendChild(tr);
                });

                document.querySelector("#users-table tbody").replaceWith(tbody);
            });
    };

    /**
     * Opens the modal for create a new user
     */
    openModalForCreate = () => {
        this.#userModalTitle.textContent = "Crear usuario";
        this.#userModalButton.textContent = "Crear";
        this.#userForm.reset();
        this.#url = "/panel/users";
        this.#spoofingPutRequest = false;
        this.#userModal.modal("show");
    };

    /**
     * Fetch the given user and opens the dialog for update
     * @param {Event} e The click event
     */
    editUser = e => {
        e.preventDefault();

        this.#currentId = e.target.closest(".edit-button").dataset.id;

        axios.get(`/panel/users/${this.#currentId}`)
            .then(({ data }) => {
                this.#userForm.reset();

                const user = data.data;

                this.#userForm.elements["name"].value = user.name;
                this.#userForm.elements["lastname"].value = user.lastname;
                this.#userForm.elements["document_type_id"].value = user.document_type_id;
                this.#userForm.elements["document_number"].value = user.document_number;
                this.#userForm.elements["email"].value = user.email;
                this.#userForm.elements["position"].value = user.position;
                this.#userForm.elements["company"].value = user.company;
                this.#userForm.elements["active"].value = user.active ? 1 : 0;
                this.#userForm.dataset.id = user.id;

                this.#userModalTitle.textContent = `Actualizar datos de ${user.fullname}`;
                this.#userModalButton.textContent = "Actualizar";

                this.#spoofingPutRequest = true;
                this.#url = `/panel/users/${this.#currentId}`;
                this.#userModal.modal("show");
            })
            .catch((error) => {
                console.log(error);
                swal({
                    title: error.response.data.message,
                    text: "Consulte con el administrador del sistema.",
                    icon: "error",
                });
            });
    };

    /**
     * Make a request to create/update a user
     * @param {Event} e The submit event
     */
    submitForm = e => {
        e.preventDefault();

        const data = {
            "name": this.#userForm.elements.name.value,
            "lastname": this.#userForm.elements.lastname.value,
            "document_type_id": this.#userForm.elements.document_type_id.value,
            "document_number": this.#userForm.elements.document_number.value,
            "email": this.#userForm.elements.email.value,
            "position": this.#userForm.elements.position.value,
            "company": this.#userForm.elements.company.value,
            "active": this.#userForm.elements.active.value,
        };

        if (this.#spoofingPutRequest) {
            data._method = "PUT";
        }

        axios.post(this.#url, data)
            .then((response) => {
                swal({
                    title: response.data.message,
                    icon: "success",
                });

                this.getUsers();
            })
            .then(() => {
                this.#userModal.modal("hide");
            })
            .catch((error) => {
                swal({
                    title: "Ocurrió un error al validar sus datos",
                    text: Object.hasOwnProperty.call(error.response.data, "errors") ? "" : error.response.data.message,
                    icon: "error",
                });

                Array.from(this.#userForm.elements).forEach(input => {
                    if (input.classList.contains("form-control")) {
                        input.classList.remove("is-invalid");
                        input.nextElementSibling.textContent = "";
                    }
                });

                if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                    for (const key in error.response.data.errors) {
                        if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                            const input = this.#userForm.elements[key];
                            input.classList.add("is-invalid");
                            input.nextElementSibling.textContent = error.response.data.errors[key][0];
                        }
                    }
                }
            });
    };
}

new User();
