import axios from "@axios";
import swal from "sweetalert";

const createAnnouncementForm = document.getElementById("create-announcement");

createAnnouncementForm.addEventListener("submit", createAnnouncement);

function createAnnouncement(e) {
    e.preventDefault();

    const data = new FormData(createAnnouncementForm);

    axios.post("/panel/announcements", data)
        .then(response => {
            console.log(response.data);
            swal({
                title: response.data.message,
                icon: "success",
            })
                .then(() => {
                    location.reload();
                });
        });
}

[].forEach.call(document.querySelectorAll(".delete-button"), button => {
    button.addEventListener("click", deleteAnnouncement);
});

function deleteAnnouncement(e) {
    swal({
        title: "¿Estás seguro que quieres eliminar este anuncio?",
        text: "Eliminacion permanente",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    })
        .then(async (willDelete) => {
            if (willDelete) {
                const button = e.target.closest(".delete-button");
                const filename = button.dataset.name;

                await axios.delete(location.pathname, { data: { filename: filename } })
                    .then(({ data }) => {
                        swal({
                            text: data.message,
                            icon: "success",
                            button: {
                                text: "Cerrar",
                                visible: true,
                                closeModal: true,
                            }
                        });

                        button.closest("tr").remove();
                    })
                    .catch(error => {
                        swal({
                            title: "Ocurrió un problema al intentar eliminar el anuncio",
                            text: error.response.data.message,
                            icon: "error",
                            button: {
                                text: "Cerrar",
                                visible: true,
                                closeModal: true,
                            }
                        });
                    });
            }
        });
}
