import axios from "@axios";
import $ from "@jquery";
import Player from "@vimeo/player";
import { auditTime, distinctUntilChanged, merge, Subject } from "rxjs";

export default class LiveLessonTracker {
    /**
     * @type {HTMLIFrameElement|HTMLElement|string} The video container
     */
    #containerEl;

    /**
     * @type {Player} The vimeo player instance
     */
    #player;

    /**
     * @type {string} The user live lesson url
     */
    #lessonUrl;

    /**
     * @type {Subject}
     */
    #endedSubject$;

    /**
     * @type {Subject}
     */
    #pauseSubject$;

    /**
     * @type {Subject}
     */
    #playSubject$;

    /**
     * @type {Subject}
     */
    #seekedSubject$;

    /**
     * @type {Subject}
     */
    #timeupdateSubject$;

    /**
     * @type {Subject}
     */
    #requestSubject$;

    /**
     * The watch time viewed by user
     * @type {number}
     */
    #timeWatched = 0;

    /**
     * Creates a new instance of the Live lesson tracker
     * @param {HTMLIFrameElement|HTMLElement|string} containerId The Id of the element that will contains the video player
     */
    constructor(containerEl) {
        this.#containerEl = containerEl;
    }

    /**
     * Fetch the user live lessson for the given live lesson
     *
     * @async
     * @param {number} classId The live lesson id
     * @returns {Promise<object>} The user live lesson data
     */
    fetchClass = async classId => {
        const userLiveLesson = await axios.get(`/live-lessons/load-lesson/${classId}`)
            .then(async ({ data }) => {
                data = data.data;

                await this.#loadVideo(data.vimeo_video_id);

                this.#timeWatched = data.watched == 1 ? this.videoDuration() : data.watched_time ?? 0;

                return data;
            });

        this.#lessonUrl = `/live-lessons/${userLiveLesson.metadata.id}/tracking`;

        return userLiveLesson;
    };

    /**
     * Instantiate the vimeo player
     *
     * @async
     * @param {number} videoId The vimeo video ID
     */
    #initPlayer = async videoId => {
        this.#player = new Player(this.#containerEl, { id: videoId });
        await this.#player.ready();
        await this.#onReady();
    };

    /**
     * Replaces the current video in the player
     *
     * @async
     * @param {number} videoId The vimeo video ID
     */
    #loadVideo = async videoId => {
        if (this.#player instanceof Player) {
            await this.#player.loadVideo(videoId);
        } else {
            await this.#initPlayer(videoId);
            await this.#player.setColor("#6ac346");
        }
    };

    #onReady = async () => {

        this.#player.on("loaded", this.onLoaded);

        this.#player.on("play", this.onPlay);
        this.#player.on("pause", this.onPause);
        this.#player.on("ended", this.onEnded);
        this.#player.on("seeked", this.onSeeked);
        this.#player.on("timeupdate", this.onTimeupdate);
    };

    /**
     * Creates the player observers and subscriptions to track the video
     */
    onLoaded = () => {
        this.#endedSubject$ = new Subject().pipe(distinctUntilChanged());
        this.#pauseSubject$ = new Subject().pipe(distinctUntilChanged());
        this.#playSubject$ = new Subject().pipe(distinctUntilChanged());
        this.#seekedSubject$ = new Subject().pipe(distinctUntilChanged());
        this.#timeupdateSubject$ = new Subject().pipe(
            auditTime(5000),
            distinctUntilChanged()
        );
        this.#requestSubject$ = merge(
            this.#endedSubject$,
            this.#pauseSubject$,
            this.#playSubject$,
            this.#seekedSubject$,
            this.#timeupdateSubject$
        )
            .subscribe(this.trackVideo);
    };

    /**
     * Sends a new user's "play" action to the tracking data suscription
     * @param {object} data The returned data from de vimeo player
     */
    onPlay = data => {
        this.#playSubject$.next({
            interaction_type: "play",
            watched_time: data.seconds,
        });
    };

    /**
     * Sends a new user's "pause" action to the tracking data suscription
     * @param {object} data The returned data from de vimeo player
     */
    onPause = data => {
        this.#pauseSubject$.next({
            interaction_type: "pause",
            watched_time: data.seconds,
        });
    };

    /**
     * Sends a new user's "ended" action to the tracking data suscription
     * @param {object} data The returned data from de vimeo player
     */
    onEnded = data => {
        this.#endedSubject$.next({
            interaction_type: "ended",
            watched_time: data.seconds,
        });
        this.#endedSubject$.unsubscribe();
        this.#pauseSubject$.unsubscribe();
        this.#playSubject$.unsubscribe();
        this.#seekedSubject$.unsubscribe();
        this.#timeupdateSubject$.unsubscribe();
        this.#requestSubject$.unsubscribe();
    };

    /**
     * Sends a new user's "seeked" action to the tracking data suscription
     * @param {object} data The returned data from de vimeo player
     */
    onSeeked = data => {
        if (this.#timeWatched < data.seconds) {
            this.#player.setCurrentTime(this.#timeWatched < 0 ? 0 : this.#timeWatched);
        }
        this.#seekedSubject$.next({
            interaction_type: "seeked",
            watched_time: this.#timeWatched,
        });
    };

    /**
     * Sends a new video's "time update" event to the watched time suscription
     * @param {object} data The returned data from de vimeo player
     */
    onTimeupdate = data => {
        if (data.seconds - 1 < this.#timeWatched && data.seconds > this.#timeWatched) {
            this.#timeWatched = data.seconds;
            // This prevents seeking. The reason this is needed
            // is because when the user tries to seek, a time update is called which results in the
            // watchedTime becoming the same as the seeked time before it goes into the function 'seeked' (below) resulting
            // in both values being the same. We need to get the most recent time update before the seek.
            // (data.seconds - 1 < currentTime) basically if you seek, this will return false and the current time wont get updated
            // (data.seconds > currentTime) if they seek backwards then dont update current time so they can seek back to where they were before
        }
        this.#timeupdateSubject$.next({
            interaction_type: "timeupdate",
            watched_time: data.seconds,
        });
    };

    /**
     * Creates a new tracking record for the given data
     * @param {object} data the event to track
     */
    trackVideo = async data => {
        await axios.patch(this.#lessonUrl, data)
            .then(() => {
                if (data.interaction_type == "ended") {
                    $("#modal_two").modal("show");
                }
            });
    };

    /**
     * Gets the video duration in seconds
     *
     * @async
     * @returns {Promise<number>}
     */
    videoDuration = async () => {
        if (this.#player instanceof Player) {
            return await this.#player.getDuration();
        }

        return 0;
    };
}
