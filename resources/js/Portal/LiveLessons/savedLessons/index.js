import $ from "@jquery";
import LiveLessonTracker from "../LiveLessonTracker";

const searchParams = new URLSearchParams(location.search);

const liveLessonTracker = new LiveLessonTracker("player");

const videoTitleElement = document.getElementById("video-title");
const videoSubtitleElement = document.getElementById("video-subtitle");
const videoDurationElement = document.getElementById("video-duration");
const videoTeacherElement = document.getElementById("video-teacher");
const videoDescriptionElement = document.getElementById("video-description");

document.addEventListener("DOMContentLoaded", async () => {
    Array.prototype.forEach.call(document.querySelectorAll("#class-selector li"), li => {
        li.addEventListener("click", changeVideo);
    });

    if (searchParams.has("view")) {
        await fetchVideo(searchParams.get("view"));
    }
});

/**
 * Replaces the video in the player
 * @param {object} e The click event
 */
const changeVideo = async e => {
    e.preventDefault();

    const target = e.target.closest("li");
    const classId = target.dataset.id;

    await fetchVideo(classId);
};

/**
 * Replaces the video in the player and show the video information
 * @param {number} classId The vimeo video ID
 */
const fetchVideo = async classId => {
    const userLiveLesson = await liveLessonTracker.fetchClass(classId);

    videoTitleElement.textContent = userLiveLesson.title;
    videoSubtitleElement.textContent = userLiveLesson.subtitle;
    videoTeacherElement.textContent = userLiveLesson.teacher;
    videoDescriptionElement.textContent = userLiveLesson.description;

    const videoDuration = await liveLessonTracker.videoDuration();
    const videoDurationAsDate = new Date(videoDuration * 1000);

    videoDurationElement.textContent = `${videoDurationAsDate.getUTCHours()}h ${videoDurationAsDate.getUTCMinutes()}m`;

    $("#classSave").hide();
    $("#content_video").show();
};
