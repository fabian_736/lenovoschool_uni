import LiveLessonTracker from "../LiveLessonTracker";


window.addEventListener("DOMContentLoaded", async () => {
    const playerElement = document.getElementById("player");
    const classId = playerElement.dataset.class;
    playerElement.removeAttribute("data-class");

    const liveLessonTracker = new LiveLessonTracker(playerElement);
    await liveLessonTracker.fetchClass(classId);
});
