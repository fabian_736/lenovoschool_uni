import axios from "@axios";
import swal from "sweetalert";

class Redemption {
    /**
     * @type {HTMLFormElement}
     */
    #redemptionForm = document.getElementById("redemption-form");

    /**
     * @type {HTMLSelectElement}
     */
    #receiverDocumentTypeId = document.getElementById("receiver_document_type_id");

    /**
     * @type {HTMLInputElement}
     */
    #receiverDocumentNumber = document.getElementById("receiver_document_number");

    /**
     * @type {HTMLInputElement}
     */
    #receiverName = document.getElementById("receiver_name");

    /**
     * @type {HTMLSelectElement}
     */
    #receiverDepartamentId = document.getElementById("receiver_departament_id");

    /**
     * @type {HTMLInputElement}
     */
    #receiverAddress = document.getElementById("receiver_address");

    /**
     * @type {HTMLInputElement}
     */
    #receiverEmail = document.getElementById("receiver_email");

    /**
     * @type {HTMLInputElement}
     */
    #receiverPhone = document.getElementById("receiver_phone");

    /**
     * @type {HTMLButtonElement}
     */
    #redemptionButton = document.getElementById("button_submit");

    /**
     * @type {boolean}
     */
    #processingRedemption = false;

    /**
     * Inits the redemption form
     */
    constructor() {
        this.#redemptionForm.addEventListener("submit", this.redeem);
    }

    /**
     * Submit the redemption form
     * @param {MouseEvent} e
     * @returns {void}
     */
    redeem = e => {
        e.preventDefault();

        if (this.#processingRedemption) {
            return;
        }

        this.#processingRedemption = true;
        this.#redemptionButton.disabled = true;

        const data = {
            receiver_document_type_id: this.#receiverDocumentTypeId.value,
            receiver_document_number: this.#receiverDocumentNumber.value,
            receiver_name: this.#receiverName.value,
            receiver_departament_id: this.#receiverDepartamentId.value,
            receiver_address: this.#receiverAddress.value,
            receiver_email: this.#receiverEmail.value,
            receiver_phone: this.#receiverPhone.value,
        };

        Array.from(this.#redemptionForm.elements).forEach(input => {
            if (input.classList.contains("form-control")) {
                input.classList.remove("is-invalid");
                input.nextElementSibling.textContent = "";
            }
        });

        axios.post(location.pathname, data)
            .then(({ data }) => {
                swal({
                    title: data.message
                })
                    .then(() => {
                        location.replace("/rewards");
                    });
            })
            .catch(error => {
                if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                    for (const key in error.response.data.errors) {
                        if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                            /** @type {HTMLFormControlsCollection} */
                            const input = this.#redemptionForm.elements[key];
                            input.classList.add("is-invalid");
                            input.nextElementSibling.textContent = error.response.data.errors[key][0];
                        }
                    }
                }
            })
            .finally(() => {
                this.#processingRedemption = false;
                this.#redemptionButton.disabled = false;
            });
    };
}

new Redemption();
