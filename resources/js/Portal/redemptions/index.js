import axios from "@axios";
import $ from "@jquery";

class Reward {
    #rewardsContainer = document.getElementById("rewards-container");
    #template = document.getElementById("reward-template");
    #redeemModal = $("#redeem-modal");
    #rewardImage = document.getElementById("reward-image");
    #rewardName = document.getElementById("reward-name");
    #requiredRedemablePoints = document.getElementById("required-redemable-points");
    #redemptionLink = document.getElementById("redemption-link");
    #redemptionErrorModal = $("#redemption-error-modal");
    #redemptionErrorMessageElement = document.getElementById("redemption-error-message");

    constructor() {
        this.list();
    }

    list = async () => {
        return await axios.get("/rewards/list")
            .then(({ data }) => {
                const chunkedRewards = this.#chunk(data.data, 4);

                while (this.#rewardsContainer.firstElementChild) {
                    this.#rewardsContainer.firstElementChild.remove();
                }

                chunkedRewards.forEach((chunk) => {
                    const row = document.createElement("div");
                    row.classList.add("row");

                    chunk.forEach((reward) => {
                        const ticket = this.#template.content.cloneNode(true);

                        ticket.querySelector(".reward-image").src = reward.image;
                        ticket.querySelector(".reward-name").textContent = reward.name;
                        ticket.querySelector(".reward-points").textContent = `${reward.redeem_points} puntos`;

                        const button = ticket.querySelector(".redeem-button");
                        button.dataset.id = `${reward.id}`;
                        button.addEventListener("click", this.checkRedemptionConditions);

                        row.appendChild(ticket);
                    });

                    this.#rewardsContainer.appendChild(row);
                });
            });
    };

    checkRedemptionConditions = async (e) => {
        const currentId = e.target.closest(".redeem-button").dataset.id;

        await axios.get(`/rewards/${currentId}/check-redemption-conditions`)
            .then(({ data }) => {
                const reward = data.data;

                this.#rewardImage.src = reward.image;
                this.#rewardName.textContent = reward.name;
                this.#requiredRedemablePoints.textContent = `Puntos requeridos: ${reward.redeem_points}`;
                this.#redemptionLink.href = `/rewards/${reward.id}/redeem`;
                this.#redeemModal.modal("show");
            })
            .catch(error => {
                this.#redemptionErrorMessageElement.textContent = error.response.data.message;
                this.#redemptionErrorModal.modal("show");
            });
    };

    /**
     * Chunks the given array in the given size
     * @param {array} array The array to chunk
     * @param {number} size The size of each chunk
     * @returns {array}
     */
    #chunk = (array, size) => {
        const chunked_arr = [];
        let index = 0;
        while (index < array.length) {
            chunked_arr.push(array.slice(index, size + index));
            index += size;
        }

        return chunked_arr;
    };
}

new Reward();
