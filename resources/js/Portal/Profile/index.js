import axios from "@axios";
import $ from "@jquery";
import swal from "sweetalert";

class Profile {
    /**
     * @type {HTMLInputElement}
     */
    #profileImageInput = document.getElementById("file");

    /**
     * @type {HTMLImageElement}
     */
    #profileImagePreviewElement = document.getElementById("profile-image-preview");

    /**
     * @type {HTMLFormElement}
     */
    #profileImageForm = document.getElementById("profile-image-form");

    /**
     * @type {HTMLImageElement}
     */
    #currentProfileImage = document.getElementById("current-profile-image");

    /**
     * @type {HTMLImageElement}
     */
    #avatarElement = document.getElementById("avatar");

    /**
     * @type {Jquery<HTMLElement>}
     */
    #modalProfilePhoto = $("#modalProfilePhoto");

    /**
     * @type {HTMLButtonElement}
     */
    #showProfileImageModalButton = document.getElementById("show-profile-image-modal");

    /**
     * @type {HTMLFormElement}
     */
    #userProfileForm = document.getElementById("user-profile-form");

    /**
     * @type {HTMLButtonElement}
     */
    #showPasswordUpdateModalButton = document.getElementById("show-update-password-modal");

    /**
     * @type {Jquery<HTMLElement>}
     */
    #passwordUpdateModal = $("#password-modal");

    /**
     * @type {HTMLFormElement}
     */
    #passwordUpdateForm = document.getElementById("password-form");

    /**
     * @type {HTMLInputElement}
     */
    #currentPasswordInput = document.getElementById("current-password");

    /**
     * @type {HTMLInputElement}
     */
    #newPasswordInput = document.getElementById("password");

    /**
     * @type {HTMLInputElement}
     */
    #newPassworConfirmationdInput = document.getElementById("password_confirmation");

    /**
     * Creates a new class instance
     */
    constructor() {
        this.#initProfileImageForm();
        this.#initProfileUpdateForm();
        this.#initPasswordUpdateForm();
    }

    /**
     * Init the profile image form
     */
    #initProfileImageForm = () => {
        this.#showProfileImageModalButton.addEventListener("click", this.showProfileImageModalButton);
        this.#profileImageInput.addEventListener("change", this.readURL);
        this.#profileImageForm.addEventListener("submit", this.updateProfileImage);
    };

    /**
     * Shows the image profile modal
     */
    showProfileImageModalButton = () => {
        this.#modalProfilePhoto.modal("show");
    };

    /**
     *  Reads the selected file and displays it in the preview element
     * @param {MouseEvent} event
     */
    readURL = event => {
        if (event.target.files.length > 0) {
            const reader = new FileReader();

            reader.onload = e => {
                this.#profileImagePreviewElement.src = e.target.result;
            };

            reader.readAsDataURL(event.target.files[0]);
        }
    };

    /**
     *  Updates the user profile image
     * @param {SubmitEvent} event
     */
    updateProfileImage = event => {
        event.preventDefault();

        const data = new FormData(this.#profileImageForm);

        axios.post("/profile/avatar", data)
            .then(({ data }) => {
                this.#currentProfileImage.src = data.url;
                this.#avatarElement.src = data.url;

                this.#modalProfilePhoto.modal("hide");
                swal({
                    title: data.message,
                    icon: "success",
                    closeOnClickOutside: true,
                    closeOnEsc: true,
                });
            })
            .catch(function () {
                swal({
                    title: "Ocurrió un error al actualizar la imagen de perfil",
                    icon: "error",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });
            });
    };

    /**
     * Init the user profile information form
     */
    #initProfileUpdateForm = () => {
        this.#userProfileForm.addEventListener("submit", this.updateProfileInformation);
    };

    /**
     * Handles the update event
     * @param {MouseEvent} event
     */
    updateProfileInformation = event => {
        event.preventDefault();

        const formData = new FormData(this.#userProfileForm);
        const data = {};

        formData.forEach((value, key) => (data[key] = value));

        Array.from(this.#userProfileForm.elements).forEach(input => {
            if (input.classList.contains("form-control")) {
                input.classList.remove("is-invalid");
                input.nextElementSibling.textContent = "";
            }
        });

        axios.put("/user/profile-information", data)
            .then(function () {
                swal({
                    title: "Información actualizada",
                    icon: "success",
                    closeOnClickOutside: true,
                    closeOnEsc: true,
                });
            })
            .catch(error => {
                swal({
                    title: "Ocurrió un error al actualizar sus datos",
                    icon: "error",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });

                this.#handleAxiosError(error, this.#userProfileForm);
            });
    };

    /**
     * Inits the update password form
     */
    #initPasswordUpdateForm = () => {
        this.#showPasswordUpdateModalButton.addEventListener("click", this.showPasswordUpdateModal);
        this.#passwordUpdateForm.addEventListener("submit", this.updateUserPassword);
    };

    /**
     * Show the update password form
     */
    showPasswordUpdateModal = () => {
        this.#passwordUpdateModal.modal("show");
    };

    /**
     * Updates the user password
     * @param {SubmitEvent} event
     */
    updateUserPassword = event => {
        event.preventDefault();

        const data = {
            "current_password": this.#currentPasswordInput.value,
            "password": this.#newPasswordInput.value,
            "password_confirmation": this.#newPassworConfirmationdInput.value
        };

        Array.from(this.#passwordUpdateForm.elements).forEach(input => {
            if (input.classList.contains("form-control")) {
                input.classList.remove("is-invalid");
                input.nextElementSibling.textContent = "";
            }
        });

        axios.put("/user/password", data)
            .then(function () {
                this.#passwordUpdateModal.modal("hide");

                swal({
                    title: "Contraseña actualizada",
                    icon: "success",
                    closeOnClickOutside: true,
                    closeOnEsc: true,
                });
            })
            .catch(error => {
                swal({
                    title: "Ocurrió un error al actualizar su contraseña",
                    icon: "error",
                    closeOnClickOutside: false,
                    closeOnEsc: false,
                });

                this.#handleAxiosError(error, this.#passwordUpdateForm);
            });
    };

    /**
     * Display de errors in the given form
     * @param {object} error The error object
     * @param {HTMLFormElement} form The form to display the errors
     */
    #handleAxiosError = (error, form) => {
        if (Object.hasOwnProperty.call(error.response.data, "errors")) {
            for (const key in error.response.data.errors) {
                if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                    /** @type {HTMLFormControlsCollection} */
                    const input = form.elements[key];
                    input.classList.add("is-invalid");
                    input.nextElementSibling.textContent = error.response.data.errors[key][0];
                }
            }
        }
    };
}

new Profile();
