import jquery from "jquery";

import "bootstrap";

window.$ = window.jQuery = require("jquery");

export default jquery;
