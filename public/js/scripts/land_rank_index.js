function open_one() {
    $('.filtro_one').show();
    $('.filtro_two').hide();
    $('.filtro_three').hide();
}

function open_two() {
    $('.filtro_one').hide();
    $('.filtro_two').show();
    $('.filtro_three').hide();
}

function open_three() {
    $('.filtro_one').hide();
    $('.filtro_two').hide();
    $('.filtro_three').show();
}

function query() {
    const mediaQuery = window.matchMedia('(max-width: 1366px)')

    if (mediaQuery.matches) {
        $('.col_second').removeClass("offset-md-1");
    }
}
window.onload = query;
