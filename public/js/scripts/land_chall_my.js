axios.defaults.headers.common['Content-Type'] = "Application/json";
axios.defaults.headers.common['Accepts'] = "Application/json";
axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

const downloadDiploma = e => {
    const challenge = e.target.closest(".challenge-card").dataset.id;

    axios.post(`/challenges/my-challenges/${challenge}/download`, {}, {
        responseType: "blob"
    })
        .then(response => {
            const filenameHeader = response.headers["content-disposition"].split(';')[1];
            const filename = filenameHeader.substring(filenameHeader.indexOf("=") + 1);
            const url = URL.createObjectURL(response.data);
            const link = document.createElement('a');
            link.href = url;
            link.setAttribute('download', filename);
            document.body.append(link);
            link.click();
            document.body.removeChild(link);
            URL.revokeObjectURL(url);

            $("#modal_two").modal("show");
        })
        .catch((error) => {
            Swal.fire({
                title: "Ocurrió un error al descargar el diploma",
                text: "Consulte con el administrador del sistema",
                icon: 'error',
                confirmButtonColor: '#3085d6',
            });
        });
};

Array.from(document.querySelectorAll(".challenge-card")).forEach(card => {
    card.addEventListener("click", downloadDiploma);
});
