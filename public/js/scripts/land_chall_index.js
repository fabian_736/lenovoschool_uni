axios.defaults.headers.common['Content-Type'] = "Application/json";
axios.defaults.headers.common['Accepts'] = "Application/json";
axios.defaults.headers.common['X-Requested-With'] = "XMLHttpRequest";

const redeemAttemptForm = document.getElementById("redeem-attempt-form");
let currentRedeemAttemptId;

Array.prototype.forEach.call(document.querySelectorAll(".card_challenge:not(.card_challenge_inactive)"), element => {
    element.addEventListener("click", e => {
        e.preventDefault();

        const userChallengeId = e.target.closest(".card_challenge").dataset.id;

        axios.get(`/challenges/${userChallengeId}/status`)
            .then(response => {
                const data = response.data.data;

                if (data.can_have_more_attempts) {
                    showRedemptionDialog(data);
                } else {
                    showChallengeAttemptDialog(data);
                }
            })
            .catch(() => { });
    });
});

redeemAttemptForm.addEventListener("submit", e => {
    e.preventDefault();

    axios.post(`/challenges/${currentRedeemAttemptId}/redeem-attempt`, {})
        .then(response => {
            location.replace(`/challenges/${response.data.data.user_challenge_id}`);
        })
        .catch(error => {
            document.querySelector("#error-modal .modal-body p").textContent = error.response.data.message;
            $("#redeem-attempt").modal("hide");
            $("#error-modal").modal("show");
            $('#error-modal').on('hidden.bs.modal', function (event) {
                location.reload();
            });
        });
});

function showRedemptionDialog(data) {
    const redeemAttemptRetryPoints = document.getElementById("redeem-attempt-retry-points");
    const availablePoints = data.challenge.questions_sum_points - data.max_earned_points;
    currentRedeemAttemptId = data.id;

    redeemAttemptRetryPoints.textContent = availablePoints;
    $("#redeem-attempt").modal("show");
}

function showChallengeAttemptDialog(data) {
    const challengeModalWarningChallengeTitle = document.getElementById("challenge-modal-warning-challenge-title");
    const challengeModalWarningChallengeMessage = document.getElementById("challenge-modal-warning-challenge-message");
    const challengeModalWarningReadyMessage = document.getElementById("challenge-modal-warning-ready-message");
    const challengeModalWarningChallengeLink = document.getElementById("challenge-modal-warning-challenge-link");
    const challengeModalWarningChallengeContainer = document.getElementById("challenge-modal-warning-challenge-link-container");

    let message;
    let challengeAvailable = false;

    challengeModalWarningChallengeTitle.textContent = data.challenge.name;

    switch (data.status) {
        case "Finished":
            message = 'Ya has finalizado este desafío.';
            break;
        case "Max attempts reached":
            message = 'Ya no te quedan más intentos para superar este desafío.';
            break;
        case "Unavailable":
            message = 'Este desafío aun no se encuentra disponible, vuelve más tarde.';
            break;
        default:
            message = `Inicia este desafio y supéralo respondiendo correctamente cada pregunta, tendrás 30 minutos para lograrlo. Puntos a ganar: ${data.challenge.questions_sum_points || 0}`;
            challengeAvailable = true;
            break;
    }

    challengeModalWarningChallengeMessage.textContent = message;

    if (challengeAvailable) {
        challengeModalWarningReadyMessage.style.removeProperty("display");
        challengeModalWarningChallengeLink.href = `/challenges/${data.id}`;
        challengeModalWarningChallengeContainer.style.removeProperty("display");
    } else {
        challengeModalWarningReadyMessage.style.setProperty("display", "none", "important");
        challengeModalWarningChallengeLink.href = "";
        challengeModalWarningChallengeContainer.style.setProperty("display", "none", "important");
    }

    $("#challenge-modal-warning").modal("show");
}

$(document).ready(function () {
    $('#myModal').modal({
        backdrop: 'static',
        keyboard: false
    });
});
