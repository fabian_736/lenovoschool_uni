const form = document.getElementById("create-challenge");

const create = e => {
    e.preventDefault();
    const data = new FormData(form);

    axios.post("/panel/challenges", data, {
        headers: {
            'Content-Type': 'Application/json',
            'Accepts': 'Application/json',
            'X-Requested-With': 'XMLHttpRequest'
        }
    })
        .then((response) => {
            Swal.fire({
                position: 'center',
                icon: 'success',
                title: response.data.message,
                showConfirmButton: true,
                confirmButtonText: "Cerrar"
            })
                .then((data) => {
                    window.location = response.data.url;
                });
        })
        .catch((error) => {
            Swal.fire({
                title: "Ocurrió un error al validar sus datos",
                icon: 'error',
                confirmButtonColor: '#3085d6',
            });

            Array.from(form.elements).forEach(input => {
                if (input.classList.contains("form-control")) {
                    input.classList.remove("is-invalid");
                    input.nextElementSibling.textContent = "";
                }
            });

            if (Object.hasOwnProperty.call(error.response.data, "errors")) {
                for (const key in error.response.data.errors) {
                    if (Object.hasOwnProperty.call(error.response.data.errors, key)) {
                        const input = form.elements[key];
                        input.classList.add("is-invalid");
                        input.nextElementSibling.textContent = error.response.data.errors[key][0];
                    }
                }
            }
        });
};

form.addEventListener("submit", create);

const deleteChallenge = e => {
    e.preventDefault();
    Swal.fire({
        title: '¿Estás seguro que quieres eliminar este desafio?',
        text: "Eliminacion permanente",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Si, Eliminar'
    }).then((result) => {
        if (result.isConfirmed) {
            axios.delete(`/panel/challenges/${e.target.closest("a").dataset.id}`, {
                headers: {
                    'Content-Type': 'Application/json',
                    'Accepts': 'Application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
                .then((response) => {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: response.data.message,
                        showConfirmButton: true,
                        confirmButtonText: "Cerrar"
                    })
                        .then((data) => {
                            location.reload();
                        });
                });
        }
    });
};

Array.prototype.forEach.call(document.querySelectorAll(".delete-button"), link => {
    link.addEventListener("click", deleteChallenge);
});
