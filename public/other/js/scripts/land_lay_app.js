function deshabilitaRetroceso() {
    window.location.hash = "no-back-button";
    window.location.hash = "Again-No-back-button" //chrome
    window.onhashchange = function () { window.location.hash = ""; }
}

$("#1").click(function () {
    $("#point_2").css({
        'background': 'white'
    });
    $("#point_3").css({
        'background': 'white'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/1.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'block'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#2").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': 'white'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/2.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'block'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#3").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/3.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'block'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#4").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': '#7DBE38'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/4.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'block'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#5").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': '#7DBE38'
    });
    $("#point_5").css({
        'background': '#7DBE38'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/5.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'block'
    });
});

$("#point_1").click(function () {
    $("#point_2").css({
        'background': 'white'
    });
    $("#point_3").css({
        'background': 'white'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/1.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'block'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#point_2").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': 'white'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/2.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'block'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#point_3").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': 'white'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/3.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'block'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#point_4").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': '#7DBE38'
    });
    $("#point_5").css({
        'background': 'white'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/4.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'block'
    });

    $("#6").css({
        'display': 'none'
    });
});

$("#point_5").click(function () {
    $("#point_2").css({
        'background': '#7DBE38'
    });
    $("#point_3").css({
        'background': '#7DBE38'
    });
    $("#point_4").css({
        'background': '#7DBE38'
    });
    $("#point_5").css({
        'background': '#7DBE38'
    });
    $(".body").css({
        'background-image': 'url("landing/img/tutorial/5.png")',
        'background-size': '100% 100%',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed'
    });

    $("#1").css({
        'display': 'none'
    });

    $("#2").css({
        'display': 'none'
    });

    $("#3").css({
        'display': 'none'
    });

    $("#4").css({
        'display': 'none'
    });

    $("#5").css({
        'display': 'none'
    });

    $("#6").css({
        'display': 'block'
    });
});
