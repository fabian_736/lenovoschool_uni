axios.defaults.headers.common["Content-Type"] = "Application/json";
axios.defaults.headers.common["Accepts"] = "Application/json";
axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";

const requiredCheckboxes = [...new Set(Array.from(document.querySelectorAll("input[type=checkbox][data-required]")).map(checkbox => checkbox.name))];
const form = document.getElementById("challenge-form");
const redeemAttemptForm = document.getElementById("redeem-attempt-form");

form.addEventListener("submit", e => {
    e.preventDefault();

    const requireCheckboxes = requiredCheckboxes.filter(name => {
        return document.querySelectorAll(`input[type=checkbox][name='${name}']:checked`).length == 0;
    })
        .forEach(name => {
            const input = document.querySelector(`input[type=checkbox][name='${name}']`);
            input.setAttribute("required", "");
            input.setCustomValidity("Debes marcar al menos una opción");
        });

    if (!form.checkValidity()) {
        form.reportValidity();

        requiredCheckboxes.forEach(name => {
            Array.from(document.querySelectorAll(`input[type=checkbox][name='${name}']`))
                .forEach(input => {
                    input.removeAttribute("required");
                    input.setCustomValidity("");
                });
        });
    } else {
        axios.post(form.action, new FormData(form))
            .then(response => {
                if (response.data.data.earned_points == response.data.data.challenge_points) {
                    $("#modal_1").modal({
                        backdrop: "static",
                        "show": true,
                        "keyboard": false
                    });
                    $("#modal_1").on("hidden.bs.modal", function (e) {
                        location.replace("/challenges");
                    });
                } else {
                    document.getElementById("retry-points").textContent = response.data.data.challenge_points - response.data.data.max_earned_points;

                    $("#redeem-attempt").modal({
                        backdrop: "static",
                        "show": true,
                        "keyboard": false
                    });
                }
            });
    }

    redeemAttemptForm.addEventListener("submit", e => {
        e.preventDefault();

        axios.post(`${location.pathname}/redeem-attempt`, {})
            .then(response => {
                location.reload();
            })
            .catch(error => {
                document.querySelector("#error-modal .modal-body p").textContent = error.response.data.message;
                $("#redeem-attempt").modal("hide");
                $("#error-modal").modal("show");
                $("#error-modal").on("hidden.bs.modal", function (event) {
                    location.replace("/challenges");
                });
            });
    });
});
