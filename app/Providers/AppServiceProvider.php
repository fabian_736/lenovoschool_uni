<?php

namespace App\Providers;

use App\Models\Badge;
use App\Models\Challenge;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\LiveLesson;
use App\Models\Penalization;
use App\Models\PointTransaction;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\Reward;
use App\Models\RewardRedemption;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Illuminate\Validation\Rules\Password;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Relation::enforceMorphMap([
            'badge' => Badge::class,
            'challenge' => Challenge::class,
            'comment' => Comment::class,
            'gallery' => Gallery::class,
            'live_lesson' => LiveLesson::class,
            'media' => Media::class,
            'question' => Question::class,
            'question_option' => QuestionOption::class,
            'reward' => Reward::class,
            'reward_redemption' => RewardRedemption::class,
            'penalization' => Penalization::class,
            'point_transaction' => PointTransaction::class,
            'user' => User::class,
            'user_challenge' => UserChallenge::class,
            'user_challenge_attempt' => UserChallengeAttempt::class,
        ]);

        // @codeCoverageIgnoreStart
        Password::defaults(function () {
            $rule = Password::min(8)
                ->letters()
                ->mixedCase()
                ->numbers()
                ->symbols();

            return $this->app->isProduction()
                ? $rule->uncompromised()
                : $rule;
        });
        // @codeCoverageIgnoreEnd
    }
}
