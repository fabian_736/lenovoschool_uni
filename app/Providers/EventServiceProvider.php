<?php

namespace App\Providers;

use App\Events\AwardedBadge;
use App\Events\ProfileInformation;
use App\Events\SyncBadges;
use App\Events\UpdatedProgress;
use App\Listeners\AwardedBadgeListener;
use App\Listeners\LogSuccessfulLogin;
use App\Listeners\ProfileInformationListener;
use App\Listeners\RegisteredListener;
use App\Listeners\SyncBadgesListener;
use App\Listeners\UpdatedProgressListener;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Login::class => [
            LogSuccessfulLogin::class,
        ],
        Registered::class => [
            RegisteredListener::class,
        ],
        UpdatedProgress::class => [
            UpdatedProgressListener::class,
        ],
        SyncBadges::class => [
            SyncBadgesListener::class,
        ],
        AwardedBadge::class => [
            AwardedBadgeListener::class,
        ],
        ProfileInformation::class => [
            ProfileInformationListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }
}
