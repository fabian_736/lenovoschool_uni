<?php

namespace App\Providers;

use App\Enums\UserTypes;
use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('is-administrator', function (User $user) {
            if ($user->user_type == UserTypes::administrator->value) {
                return true;
            }
        });

        Gate::define('is-student', function (User $user) {
            if ($user->user_type == UserTypes::student->value) {
                return true;
            }
        });

        Gate::after(function ($user, $ability) {
            return $user->user_type == UserTypes::superadmin->value;
        });
    }
}
