<?php

namespace App\Rules\Panel\Announcements;

use Illuminate\Contracts\Validation\Rule;
use League\Flysystem\PathTraversalDetected;
use League\Flysystem\WhitespacePathNormalizer;

class IsValidFilename implements Rule
{
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $pathNormalizer = new WhitespacePathNormalizer();
            $pathNormalizer->normalizePath($value);

            return true;
        } catch (PathTraversalDetected $th) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'El :attribute indicado no es válido';
    }
}
