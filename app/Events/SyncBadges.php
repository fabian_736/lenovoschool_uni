<?php

namespace App\Events;

use App\Enums\BadgeTypes;
use App\Models\Badge;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class SyncBadges
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The badge instances to sync.
     *
     * @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge>
     */
    public $badges;

    /**
     * The user instances to check.
     *
     * @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User>
     */
    public $users;

    /**
     * Create a new event instance.
     *
     * @param  Badge|null  $badge
     * @param  array<int, \App\Enums\BadgeTypes>  $badgeTypes
     * @param  User|null  $user
     * @return void
     */
    public function __construct(?Badge $badge = null, array $badgeTypes = [], ?User $user = null)
    {
        $this->validateBadgeTypes($badgeTypes);

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> */
        $users = new Collection();

        if ($user !== null) {
            $users->push($user);
        } else {
            $users = User::active()->get();
        }

        $this->users = $users;

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
        $badges = new Collection();

        if (! empty($badgeTypes)) {
            /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
            $badgesPerType = Badge::whereIn('type', $badgeTypes)->active()->get();

            $badges = $badges->merge($badgesPerType);
        }

        if ($badge !== null) {
            $badges = $badges->push($badge);
        }

        $this->badges = $badges->unique();
    }

    /**
     * Validate if the given array of badge types is valid
     *
     * @param  array<int, \App\Enums\BadgeTypes>  $badgeTypes
     * @return void
     *
     * @throws \InvalidArgumentException
     *
     * @codeCoverageIgnore
     */
    private function validateBadgeTypes(array $badgeTypes)
    {
        foreach ($badgeTypes as $badgeType) {
            if (! ($badgeType instanceof BadgeTypes)) {
                throw new \InvalidArgumentException(sprintf(
                    'All $badgeTypes values should be an instance of [%s]',
                    BadgeTypes::class
                ));
            }
        }
    }
}
