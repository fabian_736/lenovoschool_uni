<?php

namespace App\Events;

use App\Models\Badge;
use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AwardedBadge
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The badge instances.
     *
     * @var Collection<int, \App\Models\Badge>
     */
    public $badges;

    /**
     * The user instance.
     *
     * @var User
     */
    public $user;

    /**
     * Create a new event instance.
     *
     * @param  User  $user
     * @param  Collection<int, \App\Models\Badge>  $badges
     */
    public function __construct(User $user, Collection $badges)
    {
        $this->user = $user;
        $this->badges = $badges;
    }
}
