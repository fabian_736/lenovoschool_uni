<?php

namespace App\Services;

use App\Models\LiveLesson;
use Illuminate\Support\Facades\DB;

class LiveLessonService
{
    /**
     * Return all records.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\LiveLesson>
     */
    public function list()
    {
        return LiveLesson::all();
    }

    /**
     * Creates a new Live lesson.
     *
     * @param  array<string, mixed>  $input
     * @return LiveLesson
     */
    public function store(array $input): LiveLesson
    {
        /** @var LiveLesson|null */
        $liveLesson = null;

        DB::transaction(function () use ($input, &$liveLesson) {
            $liveLesson = LiveLesson::create($input);

            $liveLesson->addMedia($input['cover'])
                ->usingName('thumbnail')
                ->toMediaCollection('cover');
        });

        /** @var LiveLesson $liveLesson */
        $liveLesson->refresh();

        return $liveLesson;
    }

    /**
     * Update the given live lesson.
     *
     * @param  LiveLesson  $liveLesson
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(LiveLesson $liveLesson, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($liveLesson, $input, &$updated) {
            $updated = $liveLesson->update($input);

            if ($updated && ! empty($input['cover'])) {
                $liveLesson->addMedia($input['cover'])
                    ->usingName('thumbnail')
                    ->toMediaCollection('cover');
            }
        });

        return $updated;
    }

    /**
     * Destroy the given live lesson.
     *
     * @param  LiveLesson  $liveLesson
     * @return bool|null
     */
    public function destroy(LiveLesson $liveLesson): ?bool
    {
        return $liveLesson->delete();
    }
}
