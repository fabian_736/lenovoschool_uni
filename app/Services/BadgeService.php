<?php

namespace App\Services;

use App\Events\SyncBadges;
use App\Models\Badge;
use Illuminate\Support\Facades\DB;

class BadgeService
{
    /**
     * Creates a new badge.
     *
     * @param  array<string, mixed>  $input
     * @return Badge
     */
    public function store(array $input): Badge
    {
        /** @var Badge|null */
        $badge = null;

        DB::transaction(function () use ($input, &$badge) {
            $badge = Badge::create($input);

            $badge->addMedia($input['image'])
                ->usingName('image')
                ->toMediaCollection('image');

            if ($badge->status) {
                SyncBadges::dispatch($badge);
            }
        });

        /** @var Badge $badge */
        $badge->refresh();

        return $badge;
    }

    /**
     * Update the given badge.
     *
     * @param  Badge  $badge
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Badge $badge, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($badge, $input, &$updated) {
            $updated = $badge->update($input);

            if ($updated) {
                if (! empty($input['image'])) {
                    $badge->addMedia($input['image'])
                        ->usingName('image')
                        ->toMediaCollection('image');
                }

                if ($badge->status) {
                    SyncBadges::dispatch($badge);
                }
            }
        });

        return $updated;
    }
}
