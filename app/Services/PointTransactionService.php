<?php

namespace App\Services;

use App\Contracts\HasPointTransactions;
use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Events\SyncBadges;
use App\Models\PointTransaction;
use App\Models\User;
use App\Notifications\Points\EarnedPointsNotification;
use Illuminate\Support\Facades\DB;

class PointTransactionService
{
    /**
     * Creates a new transaction for the given transactionable model.
     *
     * @param  \App\Contracts\HasPointTransactions  $transactionable
     * @param  \App\Models\User  $user
     * @param  \App\Enums\PointTransactionTypes  $transactionType
     * @param  int  $points
     * @return \App\Models\PointTransaction|null
     */
    public function storeTransaction(HasPointTransactions $transactionable, User $user, PointTransactionTypes $transactionType, int $points): ?PointTransaction
    {
        // Obteniendo el valor absoluto de los puntos a asignar/deducir
        $points = abs($points);

        // Si no hay puntos que asignar, no se creará la transacción
        if ($points == 0) {
            return null;
        }

        /** @var PointTransaction|null */
        $transaction = null;

        DB::transaction(function () use (&$transaction, $transactionable, $user, $points, $transactionType) {
            // Determinando cuantos puntos se van a sumar o restar a los puntos disponibles
            $pointsToAddInTheAvailablePoints = match ($transactionType) {
                PointTransactionTypes::redemption, PointTransactionTypes::penalization => $points * -1,
                default => $points,
            };

            // Deteminando cuandos puntos sumar o restar a los puntos totales del usuario
            $pointsToAddInTheTotalPoints = match ($transactionType) {
                PointTransactionTypes::redemption, PointTransactionTypes::penalization => 0,
                default => $points,
            };

            $transaction = $transactionable->pointTransaction()->create([
                'user_id' => $user->id,
                'type' => $transactionType,
                'points' => $pointsToAddInTheAvailablePoints,
            ]);

            $user->total_redemption_points += $pointsToAddInTheTotalPoints;
            $user->available_redemption_points += $pointsToAddInTheAvailablePoints;
            $user->save();

            $badgeEventsToDispatch = [];

            if ($transactionType == PointTransactionTypes::earnedPoints) {
                $badgeEventsToDispatch[] = BadgeTypes::TOTAL_ACCUMULATED_POINTS;

                $user->notify(new EarnedPointsNotification($transaction));
            }

            if ($transactionType == PointTransactionTypes::redemption) {
                $badgeEventsToDispatch[] = BadgeTypes::TOTAL_REDEEMED_POINTS;
            }

            if (! empty($badgeEventsToDispatch)) {
                SyncBadges::dispatch(null, $badgeEventsToDispatch, $user);
            }
        });

        return $transaction;
    }
}
