<?php

namespace App\Services;

use App\Models\Gallery;
use Illuminate\Support\Facades\DB;

class GalleryService
{
    /**
     * Return all videos.
     *
     * @param  bool  $onlyActive
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Gallery>
     */
    public static function list($onlyActive = false)
    {
        $query = Gallery::with('media');

        if ($onlyActive) {
            $query->active();
        }

        return $query->get();
    }

    /**
     * Creates a new gallery.
     *
     * @param  array<string, mixed>  $input
     * @return Gallery
     */
    public function store(array $input): Gallery
    {
        /** @var Gallery|null */
        $gallery = null;

        DB::transaction(function () use ($input, &$gallery) {
            $gallery = Gallery::create($input);

            $gallery->addMedia($input['preview'])
                ->usingName('preview')
                ->toMediaCollection('preview');

            $gallery->addMedia($input['file'])
                ->usingName('file')
                ->toMediaCollection('file');
        });

        /** @var Gallery $gallery */
        $gallery->refresh();

        return $gallery;
    }

    /**
     * Update the given gallery.
     *
     * @param  Gallery  $gallery
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Gallery $gallery, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($gallery, $input, &$updated) {
            $updated = $gallery->update($input);

            if ($updated) {
                if (! empty($input['preview'])) {
                    $gallery->addMedia($input['preview'])
                        ->usingName('preview')
                        ->toMediaCollection('preview');
                }

                if (! empty($input['file'])) {
                    $gallery->addMedia($input['file'])
                        ->usingName('file')
                        ->toMediaCollection('file');
                }
            }
        });

        return $updated;
    }
}
