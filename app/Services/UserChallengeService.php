<?php

namespace App\Services;

use App\Enums\UserChallengeStatus;
use App\Exceptions\Portal\FailedRewardRedemptionAttemptException;
use App\Exceptions\Portal\UserChallengeAttemptCannotBeCreatedException;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use Barryvdh\DomPDF\Facade\Pdf;

class UserChallengeService
{
    /**
     * Return all published challenges.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\UserChallenge>
     */
    public static function listWhereUserIsRegistered(User $user)
    {
        return UserChallenge::with([
            'challenge' => fn ($query) => $query->withSum('questions', 'points'),
        ])
            ->withCount('attempts')
            ->whereUserId($user->id)
            ->get();
    }

    /**
     * Creates a new user challenge.
     *
     * @param  array<string, mixed>  $input
     * @return UserChallenge
     */
    public function store(array $input): UserChallenge
    {
        $userChallenge = UserChallenge::firstOrCreate($input);

        $userChallenge->refresh();

        return $userChallenge;
    }

    /**
     * Update the given user challenge.
     *
     * @param  UserChallenge  $challenge
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(UserChallenge $challenge, array $input): bool
    {
        return $challenge->update($input);
    }

    /**
     * Returns the last UserChallengeAttempt for the given UserChallenge.
     *
     * @param  UserChallenge  $userChallenge
     * @return UserChallengeAttempt|null
     */
    public function getLastAttempt(UserChallenge $userChallenge): ?UserChallengeAttempt
    {
        if ($userChallenge->lastAttempt()->exists()) {
            return $userChallenge->lastAttempt;
        }

        return null;
    }

    /**
     * Checks if the user can redeem a new attempt for the given user challenge.
     *
     * @param  UserChallenge  $userChallenge
     * @return void
     *
     * @throws UserChallengeAttemptCannotBeCreatedException
     * @throws FailedRewardRedemptionAttemptException
     */
    public function canRedeemNewAttempt(UserChallenge $userChallenge): void
    {
        /** @var \App\Models\User */
        $user = $userChallenge->user;

        if ($user->available_redemption_points < config('points-model.redemptions.challenge_attempt')) {
            throw new FailedRewardRedemptionAttemptException('No tienes puntos suficientes para redimir este premio.');
        }

        $userChallenge->loadCount('attempts');

        if (
            $userChallenge->attempts_count >= 3 &&
            $userChallenge->status != UserChallengeStatus::maxAttemptsReached->value
        ) {
            $this->update($userChallenge, ['status' => UserChallengeStatus::maxAttemptsReached->value]);
            $userChallenge->refresh();
        }

        switch ($userChallenge->status) {
            case UserChallengeStatus::finished->value:
                throw new UserChallengeAttemptCannotBeCreatedException('No se pudo redimir el desafío porque ya está finalizado.', 422);
            case UserChallengeStatus::unavailable->value:
                throw new UserChallengeAttemptCannotBeCreatedException('No se pudo redimir el desafío porque no está disponible.', 422);
            case UserChallengeStatus::maxAttemptsReached->value:
                throw new UserChallengeAttemptCannotBeCreatedException('Ya ha alcanzado el número máximo de intentos posible para este desafío.', 422);
        }

        /** @var \App\Models\UserChallengeAttempt */
        $lastAttempt = $this->getLastAttempt($userChallenge);

        if ($lastAttempt->finished_at === null) {
            throw new UserChallengeAttemptCannotBeCreatedException('No se pudo redimir el nuevo intento porque ya tiene un intento sin terminar.', 422);
        }
    }

    /**
     * Generates the user challenge diploma and stores it in storage.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media
     */
    public function generateDiploma(UserChallenge $userChallenge)
    {
        $userChallenge->load(['challenge', 'user']);
        $currentDay = now('America/Lima');
        $currentDay = $currentDay->translatedFormat('d \d\e F \d\e Y');

        /** @var string */
        $pdf = Pdf::loadView('modules.landing.challenge.pdf.diploma', compact('userChallenge', 'currentDay'))
            ->setPaper('a4', 'landscape')
            ->output();

        $diploma = $userChallenge->addMediaFromString($pdf)
            ->usingName('diploma')
            ->usingFileName('diploma.pdf')
            ->toMediaCollection('diploma');

        return $diploma;
    }

    /**
     * Returns the diploma file.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media|null
     */
    public function getDiploma(UserChallenge $userChallenge)
    {
        $diploma = $userChallenge->getFirstMedia('diploma');

        return $diploma;
    }
}
