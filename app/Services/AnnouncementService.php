<?php

namespace App\Services;

use App\Exceptions\Panel\AnnounceCannotBeDeletedException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use League\Flysystem\WhitespacePathNormalizer;

class AnnouncementService
{
    /**
     * Returns all announcements images.
     *
     * @return Collection<string, string>
     */
    public function list(): Collection
    {
        return collect(Storage::disk('public')->files('announcements'))
            ->mapWithKeys(function ($file) {
                return [
                    File::basename($file) => Storage::disk('public')->url($file),
                ];
            });
    }

    /**
     * Stores the given file in storage.
     *
     * @param  UploadedFile  $file
     * @return void
     */
    public function store(UploadedFile $file)
    {
        $newfilename = Str::orderedUuid();
        $fileExtension = $file->extension();
        $filename = "$newfilename.$fileExtension";

        Storage::disk('public')->putFileAs('announcements', $file, $filename);
    }

    /**
     * Deletes the given file from storage.
     *
     * @param  string  $filename
     * @return bool
     *
     * @throws AnnounceCannotBeDeletedException
     */
    public function destroy(string $filename): bool
    {
        $pathNormalizer = new WhitespacePathNormalizer();
        $filename = $pathNormalizer->normalizePath($filename);

        $filePath = "announcements/$filename";

        if (! Storage::disk('public')->exists($filePath)) {
            throw new AnnounceCannotBeDeletedException('El archvivo que intenta eliminar no existe.');
        }

        return Storage::disk('public')->delete($filePath);
    }
}
