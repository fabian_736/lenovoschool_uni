<?php

namespace App\Services;

use App\Models\Challenge;
use Illuminate\Support\Facades\DB;

class ChallengeService
{
    /**
     * Return all challenges.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Challenge>
     */
    public static function list()
    {
        return Challenge::all();
    }

    /**
     * Creates a new challenge.
     *
     * @param  array<string, mixed>  $input
     * @return Challenge
     */
    public function store(array $input): Challenge
    {
        /** @var Challenge|null */
        $challenge = null;

        DB::transaction(function () use ($input, &$challenge) {
            $challenge = Challenge::create($input);

            $challenge->addMedia($input['cover'])
                ->usingName('challenge_cover')
                ->toMediaCollection('cover');

            $challenge->addMedia($input['teacher_signature'])
                ->usingName('teacher_signature')
                ->toMediaCollection('teacher_signature');
        });

        /** @var Challenge $challenge */
        $challenge->refresh();

        return $challenge;
    }

    /**
     * Update the given challenge.
     *
     * @param  Challenge  $challenge
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Challenge $challenge, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($challenge, $input, &$updated) {
            $updated = $challenge->update($input);

            if ($updated) {
                if (! empty($input['cover'])) {
                    $challenge->addMedia($input['cover'])
                        ->usingName('challenge_cover')
                        ->toMediaCollection('cover');
                }

                if (! empty($input['teacher_signature'])) {
                    $challenge->addMedia($input['teacher_signature'])
                        ->usingName('teacher_signature')
                        ->toMediaCollection('teacher_signature');
                }
            }
        });

        return $updated;
    }

    /**
     * Destroy the given challenge.
     *
     * @param  Challenge  $challenge
     * @return bool|null
     */
    public function destroy(Challenge $challenge): ?bool
    {
        return $challenge->delete();
    }
}
