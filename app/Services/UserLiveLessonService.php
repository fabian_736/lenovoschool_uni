<?php

namespace App\Services;

use App\Models\LiveLesson;
use App\Models\UserLiveLesson;
use App\Models\UserLiveLessonTrackingHistory;

class UserLiveLessonService
{
    /**
     * Finds or creates a new user live lesson.
     *
     * @param  LiveLesson  $liveLesson
     * @return UserLiveLesson
     */
    public function findOrCreateUserLiveLesson(LiveLesson $liveLesson): UserLiveLesson
    {
        if ($liveLesson->metadata()->exists()) {
            /** @var UserLiveLesson */
            $userLiveLesson = $liveLesson->metadata;
        } else {
            /** @var UserLiveLesson */
            $userLiveLesson = $this->store([
                'user_id' => auth()->id(),
                'live_lesson_id' => $liveLesson->id,
            ]);
        }

        return $userLiveLesson;
    }

    /**
     * Creates a new user live lesson.
     *
     * @param  array<string, mixed>  $input
     * @return UserLiveLesson
     */
    public function store(array $input): UserLiveLesson
    {
        $userLiveLesson = UserLiveLesson::create($input);

        $userLiveLesson->refresh();

        return $userLiveLesson;
    }

    /**
     * Update the given yser live lesson.
     *
     * @param  UserLiveLesson  $userLiveLesson
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(UserLiveLesson $userLiveLesson, array $input): bool
    {
        return $userLiveLesson->update($input);
    }

    /**
     * Creates a tracking record for the given user live lesson.
     *
     * @param  UserLiveLesson  $userLiveLesson
     * @param  array<string, mixed>  $input
     * @return UserLiveLessonTrackingHistory
     */
    public function addTrackingToHistory(UserLiveLesson $userLiveLesson, array $input): UserLiveLessonTrackingHistory
    {
        return $userLiveLesson->tracking()->create($input);
    }
}
