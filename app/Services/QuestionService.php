<?php

namespace App\Services;

use App\Models\Challenge;
use App\Models\Question;
use Illuminate\Support\Facades\DB;

class QuestionService
{
    /**
     * Return all questions for the given challenge.
     *
     * @param  Challenge  $challenge
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Question>
     */
    public function list(Challenge $challenge)
    {
        return Question::where('challenge_id', $challenge->id)->get();
    }

    /**
     * Creates a new question.
     *
     * @param  array<string, mixed>  $input
     * @return Question
     */
    public function store(array $input): Question
    {
        /** @var Question|null */
        $question = null;

        DB::transaction(function () use ($input, &$question) {
            $question = Question::create($input);

            if (! empty($input['image'])) {
                $question->addMedia($input['image'])
                    ->usingName('image')
                    ->toMediaCollection('image');
            }
        });

        /** @var Question $question */
        $question->refresh();

        return $question;
    }

    /**
     * Update the given question.
     *
     * @param  Question  $question
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Question $question, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($question, $input, &$updated) {
            $updated = $question->update($input);

            if ($updated && ! empty($input['image'])) {
                $question->addMedia($input['image'])
                    ->usingName('image')
                    ->toMediaCollection('image');
            }
        });

        return $updated;
    }

    /**
     * Destroy the given question.
     *
     * @param  Question  $question
     * @return bool|null
     */
    public function destroy(Question $question): ?bool
    {
        return $question->delete();
    }
}
