<?php

namespace App\Services;

use App\Models\QuestionOption;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use Illuminate\Database\Eloquent\Collection as EloquentCollection;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class UserChallengeAttemptService
{
    /**
     * Creates a new user challenge attempt.
     *
     * @param  array<string, mixed>  $input
     * @return UserChallengeAttempt
     */
    public function store(array $input): UserChallengeAttempt
    {
        /** @var UserChallengeAttempt $attempt */
        $attempt = UserChallengeAttempt::create($input);

        $attempt->refresh();

        return $attempt;
    }

    /**
     * Update the given user challenge.
     *
     * @param  UserChallengeAttempt  $challenge
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(UserChallengeAttempt $challenge, array $input): bool
    {
        return $challenge->update($input);
    }

    /**
     * Sync the responses given for the given question.
     *
     * @param  UserChallenge  $userChallenge
     * @param  UserChallengeAttempt  $userChallengeAttempt
     * @param  \Illuminate\Support\Collection<string, mixed>  $request
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption>
     */
    public function syncResponses(UserChallenge $userChallenge, UserChallengeAttempt $userChallengeAttempt, Collection $request): EloquentCollection
    {
        $answers = $request->filter(fn ($value, $key) => Str::startsWith($key, 'question-'))
            ->flatten();

        /** @var \Illuminate\Support\Collection<int, string> */
        $challengeQuestionIds = $userChallenge->challenge->questions->pluck('id');

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
        $challengeQuestionOptions = QuestionOption::whereIn('question_id', $challengeQuestionIds)->get();

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
        $selectedOptions = QuestionOption::whereIn('id', $answers)
            ->get()
            ->intersect($challengeQuestionOptions);

        $userChallengeAttempt->attemptChoices()->sync($selectedOptions);

        return $selectedOptions;
    }
}
