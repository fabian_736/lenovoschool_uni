<?php

namespace App\Services;

use App\Exceptions\Portal\FailedRewardRedemptionAttemptException;
use App\Models\Reward;
use App\Models\RewardRedemption;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class RewardService
{
    /**
     * Returns a paginated listing of rewards.
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function list()
    {
        return Reward::with('media')->paginate();
    }

    /**
     * Store a new reward in storage.
     *
     * @param  array<string, mixed>  $input
     * @return Reward
     */
    public function store(array $input): Reward
    {
        /** @var Reward|null */
        $reward = null;

        DB::transaction(function () use ($input, &$reward) {
            $reward = Reward::create($input);

            $reward->addMedia($input['image'])
                ->usingName('reward')
                ->toMediaCollection('image');
        });

        /** @var Reward $reward */
        $reward->refresh();

        return $reward;
    }

    /**
     * Updates the given reward.
     *
     * @param  Reward  $reward
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(Reward $reward, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($reward, $input, &$updated) {
            $updated = $reward->update($input);

            if ($updated && ! empty($input['image'])) {
                $reward->addMedia($input['image'])
                    ->usingName('reward')
                    ->toMediaCollection('image');
            }
        });

        return $updated;
    }

    /**
     * Deletes the given reward.
     *
     * @param  Reward  $reward
     * @return bool|null
     */
    public function destroy(Reward $reward): ?bool
    {
        return $reward->delete();
    }

    /**
     * Redeem the given reward.
     *
     * @param  Reward  $reward
     * @param  array<string, mixed>  $input
     * @return RewardRedemption
     */
    public function reedeem(Reward $reward, array $input): RewardRedemption
    {
        /** @var RewardRedemption|null */
        $rewardRedemption = null;

        DB::transaction(function () use ($reward, $input, &$rewardRedemption) {
            // Creando el registro de redención
            $rewardRedemption = $reward->redemptions()->create($input);

            // Actualizando la cantidad disponible del premio
            $reward->quantity_available--;
            $reward->save();
        });

        /** @var RewardRedemption $rewardRedemption */
        return $rewardRedemption;
    }

    /**
     * Update the status of the given redemption.
     *
     * @param  RewardRedemption  $rewardRedemption
     * @param  string  $status
     * @return bool
     */
    public function updateRedemptionStatus(RewardRedemption $rewardRedemption, string $status): bool
    {
        return $rewardRedemption->update([
            'status' => $status,
        ]);
    }

    /**
     * Validates if the given reward can be redeemed by the given user.
     *
     * @param  Reward  $reward
     * @param  User  $user
     * @return void
     *
     * @throws FailedRewardRedemptionAttemptException
     */
    public function validateIfRewardCanBeRedeemed(Reward $reward, User $user)
    {
        // Validando si el premio aun tiene existencias
        if ($reward->quantity_available < 1) {
            throw new FailedRewardRedemptionAttemptException('Este premio ya no está disponible');
        }

        // Validando si el usuario tiene los puntos necesarios para redimir el premio
        if ($user->available_redemption_points < $reward->redeem_points) {
            $requiredPoints = $reward->redeem_points - $user->available_redemption_points;

            throw new FailedRewardRedemptionAttemptException("No podemos redimir tu premio, te faltan $requiredPoints puntos para reclamarlo");
        }
    }
}
