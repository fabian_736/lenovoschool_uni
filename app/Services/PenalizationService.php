<?php

namespace App\Services;

use App\Models\Penalization;

class PenalizationService
{
    /**
     * Creates a new Penalization in the database.
     *
     * @param  array<string, mixed>  $input
     * @return Penalization
     */
    public function store(array $input): Penalization
    {
        return Penalization::create($input);
    }
}
