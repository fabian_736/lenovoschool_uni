<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\UploadedFile;

class UserService
{
    /**
     * Creates a new user.
     *
     * @param  array<string, mixed>  $input
     * @return User
     */
    public function store(array $input): User
    {
        /** @var User */
        $user = User::create($input);
        event(new Registered($user));

        return $user;
    }

    /**
     * Update the given user.
     *
     * @param  User  $user
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(User $user, array $input): bool
    {
        return $user->update($input);
    }

    /**
     * Update the profile image for the given user.
     *
     * @param  User  $user
     * @param  UploadedFile  $file
     * @return \Spatie\MediaLibrary\MediaCollections\Models\Media
     */
    public function updateProfileImage(User $user, UploadedFile $file)
    {
        return $user
            ->addMedia($file)
            ->usingName('avatar')
            ->toMediaCollection('avatar');
    }
}
