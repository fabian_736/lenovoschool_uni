<?php

namespace App\Services;

use App\Models\Comment;
use App\Models\Gallery;

class CommentService
{
    /**
     * Return all comments.
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\Comment>
     */
    public static function list(?Gallery $gallery = null)
    {
        $comments = Comment::with('author.media')
            ->orderByDesc('created_at');

        if (! is_null($gallery)) {
            $comments->where('gallery_id', $gallery->id);
        }

        return $comments->get();
    }

    /**
     * Creates a new comment.
     *
     * @param  array<string, mixed>  $input
     * @return Comment
     */
    public function store(array $input): Comment
    {
        return Comment::create($input);
    }
}
