<?php

namespace App\Services;

use App\Models\Question;
use App\Models\QuestionOption;
use Illuminate\Support\Facades\DB;

class QuestionOptionService
{
    /**
     * Return all questions options for the given question.
     *
     * @param  Question  $question
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption>
     */
    public static function list(Question $question)
    {
        return QuestionOption::where('question_id', $question->id)->get();
    }

    /**
     * Creates a new question option.
     *
     * @param  array<string, mixed>  $input
     * @return QuestionOption
     */
    public function store(array $input): QuestionOption
    {
        /** @var QuestionOption|null */
        $option = null;

        DB::transaction(function () use ($input, &$option) {
            $option = QuestionOption::create($input);

            if (! empty($input['image'])) {
                $option->addMedia($input['image'])
                    ->usingName('image')
                    ->toMediaCollection('image');
            }
        });

        /** @var QuestionOption $option */
        $option->refresh();

        return $option;
    }

    /**
     * Update the given question option.
     *
     * @param  QuestionOption  $questionOption
     * @param  array<string, mixed>  $input
     * @return bool
     */
    public function update(QuestionOption $questionOption, array $input): bool
    {
        $updated = false;

        DB::transaction(function () use ($questionOption, $input, &$updated) {
            $updated = $questionOption->update($input);

            if ($updated && ! empty($input['image'])) {
                $questionOption->addMedia($input['image'])
                    ->usingName('image')
                    ->toMediaCollection('image');
            }
        });

        return $updated;
    }

    /**
     * Destroy the given question.
     *
     * @param  QuestionOption  $questionOption
     * @return bool|null
     */
    public function destroy(QuestionOption $questionOption): ?bool
    {
        return $questionOption->delete();
    }
}
