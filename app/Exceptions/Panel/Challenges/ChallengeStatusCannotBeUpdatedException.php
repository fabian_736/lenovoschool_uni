<?php

namespace App\Exceptions\Panel\Challenges;

use App\Traits\JsonResponses;
use RuntimeException;

class ChallengeStatusCannotBeUpdatedException extends RuntimeException
{
    use JsonResponses;

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return $this->jsonErrorResponse($this->getMessage(), 422);
    }
}
