<?php

namespace App\Exceptions\Portal;

use App\Traits\JsonResponses;
use Exception;

class UserChallengeAttemptCannotBeCreatedException extends Exception
{
    use JsonResponses;

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function render($request)
    {
        return $this->jsonErrorResponse($this->getMessage(), 422);
    }
}
