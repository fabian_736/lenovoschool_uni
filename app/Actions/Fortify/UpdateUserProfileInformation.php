<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\UpdatesUserProfileInformation;
use Carbon\Carbon;

class UpdateUserProfileInformation implements UpdatesUserProfileInformation
{
    /**
     * The user service instance
     *
     * @var UserService
     */
    private UserService $userService;

    /**
     * Creates a new controller instance
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Validate and update the given user's profile information.
     *
     * @param  mixed  $user
     * @param  array<string, mixed>  $input
     * @return void
     */
    public function update($user, array $input)
    {
        $dt = new Carbon;
        $before = $dt->subYears(18)->format('Y-m-d');

        $validatedInput = Validator::make($input, [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'document_type_id' => 'required|exists:document_types,id',
            'document_number' => [
                'required',
                'numeric',
                'integer',
                'digits_between:8,16',
                Rule::unique('users', 'document_number')
                    ->where(fn ($query) => $query->where('document_type_id', $input['document_type_id']))
                    ->ignore($user->id),
            ],
            'cellphone_number' => 'required|numeric|integer|digits_between:7,9',
            'email' => [
                'required',
                'string',
                'email:rfc,dns',
                'max:255',
                Rule::unique(User::class)->ignore($user->id),
            ],
            'position' => 'required|string|max:255',
            'company' => 'required|string|max:255',
            'date_of_birth' => 'required|date|before:' . $before,
        ])
            ->validateWithBag('updateProfileInformation');

        $this->userService->update($user, $validatedInput);
    }
}
