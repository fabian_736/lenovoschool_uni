<?php

namespace App\Actions\Fortify;

use App\Enums\UserTypes;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Laravel\Fortify\Contracts\CreatesNewUsers;
use Carbon\Carbon;

class CreateNewUser implements CreatesNewUsers
{
    /**
     * The user service instance
     *
     * @var UserService
     */
    private UserService $userService;

    /**
     * Creates a new controller instance
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Validate and create a newly registered user.
     *
     * @param  array<string, mixed>  $input
     * @return \App\Models\User
     */
    public function create(array $input)
    {
        $dt = new Carbon;
        $before = $dt->subYears(18)->format('Y-m-d');

        $rules = [
            'name' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'document_type_id' => 'required|exists:document_types,id',
            'document_number' => [
                'required',
                'string',
                'integer',
                'digits_between:8,16',
                Rule::unique('users', 'document_number')
                    ->where(fn ($query) => $query->where('document_type_id', $input['document_type_id'])),
            ],
            'cellphone_number' => 'required|numeric|integer|digits_between:7,10',
            'email' => [
                'required',
                'string',
                'email:rfc,dns',
                'max:255',
                Rule::unique(User::class),
            ],
            'position' => 'required', Rule::in(['Gerente','Accionista','Ejecutivo de cuenta']),
            'company' => 'required|string|max:255',
            'date_of_birth' => 'required|date|before:' . $before,
            'file' => 'nullable|file|max:5000|mimes:jpg,png',
        ];
        $attributes = [
            'name' => 'nombre',
            'lastname' => 'apellido',
            'document_type_id' => 'Tipo de documento',
            'document_number' => 'Número de documento',
            'cellphone_number' => 'Número de teléfono',
            'email' => '',
            'position' => 'Cargo',
            'date_of_birth' => 'Fecha de nacimiento',
            'company' => 'compañía',
            'file' => 'imagen de perfil',
        ];

        $validatedInput = Validator::make(data: $input, rules: $rules, customAttributes: $attributes)
            ->safe()
            ->merge([
                'user_type' => UserTypes::student,
                'active' => 1,
            ]);

        DB::transaction(function () use ($validatedInput) {
            $userInut = $validatedInput->only([
                'name',
                'lastname',
                'document_type_id',
                'document_number',
                'cellphone_number',
                'email',
                'position',
                'chanel',
                'company',
                'user_type',
                'active',
                'date_of_birth',
            ]);

            $user = $this->userService->store($userInut);

            if(isset($input['file'])){
                $this->userService->updateProfileImage($user, $validatedInput->file);
            }

        });

        session()->flash('registerStatus', '¡Registro exitoso! Se le ha enviado un correo electrónico para establecer su contraseña.');

        throw new AuthenticationException();
    }
}
