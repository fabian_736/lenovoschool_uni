<?php

namespace App\Actions\Fortify;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Laravel\Fortify\Fortify;

class AuthenticateLoginAttempt
{
    public function __invoke(Request $request)
    {
        $user = User::where('email', $request->email)->first();

        if (
            $user &&
            Hash::check($request->password, $user->password)
        ) {
            $this->checkIfUserIsActive($user);

            return $user;
        }
    }

    private function checkIfUserIsActive(User $user)
    {
        if (! $user->active) {
            throw ValidationException::withMessages([
                Fortify::username() => ['Su usuario se encuentra inactivo'],
            ]);
        }
    }
}
