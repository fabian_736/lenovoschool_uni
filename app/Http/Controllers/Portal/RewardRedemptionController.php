<?php

namespace App\Http\Controllers\Portal;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Events\SyncBadges;
use App\Http\Controllers\Controller;
use App\Http\Requests\Portal\Reward\RedeemRequest;
use App\Http\Resources\RewardResource;
use App\Models\Departament;
use App\Models\DocumentType;
use App\Models\Reward;
use App\Notifications\Rewards\RedeemedRewardNotification;
use App\Services\PointTransactionService;
use App\Services\RewardService;
use App\Traits\JsonResponses;
use Illuminate\Support\Facades\DB;

class RewardRedemptionController extends Controller
{
    use JsonResponses;

    /** @var RewardService */
    private $rewardService;

    /** @var PointTransactionService */
    private $pointTransactionService;

    /**
     * Create a new controller instance.
     *
     * @param  RewardService  $rewardService
     * @param  PointTransactionService  $pointTransactionService
     * @return void
     */
    public function __construct(RewardService $rewardService, PointTransactionService $pointTransactionService)
    {
        $this->rewardService = $rewardService;
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Display a listing of the rewards.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('modules.landing.reward.index');
    }

    /**
     * Returns all available rewards
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $rewards = Reward::with('media')
            ->orderByDesc('redeem_points')
            ->where('quantity_available', '>', 0)
            ->where('redeem_points', '>', 0)
            ->get();

        return $this->resourceResponse('Premios obtenidos correctamente', RewardResource::collection($rewards));
    }

    /**
     * Return the given reward.
     *
     * @param  Reward  $reward
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkRedemptionConditions(Reward $reward)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $this->rewardService->validateIfRewardCanBeRedeemed($reward, $user);

        $reward->load('media');

        return $this->resourceResponse('', new RewardResource($reward));
    }

    /**
     * Display the redemption form for the given reward.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function viewReedemptionForm(Reward $reward)
    {
        $reward->loadMedia('image');
        $documentTypes = DocumentType::all();
        $departaments = Departament::all();

        return view('modules.landing.reward.formulario', compact('departaments', 'documentTypes', 'reward'));
    }

    /**
     * Redeem the given reward.
     *
     * @param  RedeemRequest  $request
     * @param  Reward  $reward
     * @return \Illuminate\Http\JsonResponse
     */
    public function redeem(RedeemRequest $request, Reward $reward)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $this->rewardService->validateIfRewardCanBeRedeemed($reward, $user);

        $input = $request->safe()->merge(['user_id' => $user->id])->toArray();

        DB::transaction(function () use ($reward, $input, $user) {
            $rewardRedemption = $this->rewardService->reedeem($reward, $input);
            $this->pointTransactionService->storeTransaction(
                transactionable: $rewardRedemption,
                user: $user,
                transactionType: PointTransactionTypes::redemption,
                points: $reward->redeem_points
            );

            $user->notify(new RedeemedRewardNotification($reward));

            SyncBadges::dispatch(null, [BadgeTypes::TOTAL_REWARD_REDEMPTIONS], $user);
        });

        return $this->jsonResponse('Redención completada con éxito');
    }
}
