<?php

namespace App\Http\Controllers\Portal;

use App\Enums\BadgeTypes;
use App\Events\SyncBadges;
use App\Events\UpdatedProgress;
use App\Http\Controllers\Controller;
use App\Http\Resources\LiveLessonResource;
use App\Models\LiveLesson;
use App\Models\UserLiveLesson;
use App\Notifications\Challenges\NewChallengeNotification;
use App\Services\UserChallengeService;
use App\Services\UserLiveLessonService;
use App\Traits\JsonResponses;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserLiveLessonController extends Controller
{
    use JsonResponses;

    /** @var UserLiveLessonService */
    private $userLiveLessonService;

    /** @var UserChallengeService */
    private $userChallengeService;

    /**
     * Create a new controller instance.
     *
     * @param  UserLiveLessonService  $userLiveLessonService
     * @return void
     */
    public function __construct(UserLiveLessonService $userLiveLessonService, UserChallengeService $userChallengeService)
    {
        $this->userLiveLessonService = $userLiveLessonService;
        $this->userChallengeService = $userChallengeService;
    }

    /**
     * Display the live lessons general view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('modules.landing.class.index');
    }

    /**
     * Returns the events for the given time lapse.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function calendar(Request $request)
    {
        $now = now('America/Lima');
        $startDate = $now->startOfMonth()->toDateString();
        $endDate = $now->endOfMonth()->toDateString();
        $liveLessons = LiveLesson::whereBetween('scheduled_date', [$request->input('start', $startDate), $request->input('end', $endDate)])->get();

        return $this->resourceResponse(
            message: 'Clases en vivo consultadas correctamente',
            data: LiveLessonResource::collection($liveLessons)
        );
    }

    /**
     * Display the current live lesson.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Contracts\View\View
     */
    public function liveLesson()
    {
        $now = now()->format('Y-m-d');

        $liveLesson = LiveLesson::where('scheduled_date', $now)->first();

        if (is_null($liveLesson)) {
            return redirect()->route('live-lessons.index');
        }

        return view('modules.landing.class.class', compact('liveLesson'));
    }

    /**
     * Display the past live lessons.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function savedLessons()
    {
        $now = now()->format('Y-m-d');

        $liveLessons = LiveLesson::where('scheduled_date', '<', $now)->orderByDesc('scheduled_date')->get();

        return view('modules.landing.class.save', compact('liveLessons'));
    }

    /**
     * Loads the given live lesson.
     *
     * @param  LiveLesson  $liveLesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function loadLesson(LiveLesson $liveLesson)
    {
        $this->userLiveLessonService->findOrCreateUserLiveLesson($liveLesson);

        $liveLesson->load('metadata');

        return $this->resourceResponse(
            message: 'Clase en vivo consultada correctamente',
            data: new LiveLessonResource($liveLesson)
        );
    }

    /**
     * Creates a new tracking record for the given user live lesson.
     *
     * @param  Request  $request
     * @param  UserLiveLesson  $userLiveLesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function trackLiveLesson(Request $request, UserLiveLesson $userLiveLesson)
    {
        if (! $userLiveLesson->watched) {
            $interactionType = $request->input('interaction_type');
            $watchedTime = $request->input('watched_time', 0);

            $trackingInput = [
                'interaction_type' => $interactionType,
                'watched_time' => $watchedTime,
                'created_at' => now(),
            ];

            $userLiveLessonInput = [];

            if ($userLiveLesson->watched_time < $watchedTime) {
                $userLiveLessonInput['watched_time'] = $watchedTime;
            }

            if ($interactionType == 'ended') {
                $userLiveLessonInput['watched'] = true;
            }

            DB::transaction(function () use ($interactionType, $userLiveLessonInput, $userLiveLesson, $trackingInput) {
                if (! empty($userLiveLessonInput)) {
                    $this->userLiveLessonService->update($userLiveLesson, $userLiveLessonInput);
                }

                $this->userLiveLessonService->addTrackingToHistory($userLiveLesson, $trackingInput);

                if ($interactionType == 'ended') {
                    /** @var \App\Models\User */
                    $user = auth()->user();

                    // Register the user to the live lesson's challenge
                    if ($userLiveLesson->liveLesson->challenge()->exists()) {
                        /** @var \App\Models\Challenge */
                        $challenge = $userLiveLesson->liveLesson->challenge;

                        $input = [
                            'user_id' => $user->id,
                            'challenge_id' => $challenge->id,
                        ];

                        $userChallenge = $this->userChallengeService->store($input);

                        if ($userChallenge->wasRecentlyCreated) {
                            $user->notify(new NewChallengeNotification($userChallenge));
                        }
                    }

                    UpdatedProgress::dispatch($user);
                    SyncBadges::dispatch(null, [BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS], $user);
                }
            });
        }

        return response()->json(status: Response::HTTP_CREATED);
    }
}
