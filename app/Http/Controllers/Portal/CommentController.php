<?php

namespace App\Http\Controllers\Portal;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Events\SyncBadges;
use App\Http\Controllers\Controller;
use App\Http\Requests\Portal\CommentRequest;
use App\Http\Resources\CommentResource;
use App\Models\Comment;
use App\Models\Gallery;
use App\Services\CommentService;
use App\Services\PointTransactionService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class CommentController extends Controller
{
    use JsonResponses;

    /** @var CommentService */
    private $commentService;

    /** @var PointTransactionService */
    private $pointTransactionService;

    /**
     * Create a new controller instance.
     *
     * @param  CommentService  $commentService
     * @param  PointTransactionService  $pointTransactionService
     * @return void
     */
    public function __construct(CommentService $commentService, PointTransactionService $pointTransactionService)
    {
        $this->commentService = $commentService;
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @param  Gallery  $video
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Gallery $video)
    {
        $comments = $this->commentService->list($video);

        return $this->resourceResponse(
            message: 'Comentarios consultados correctamente.',
            data: CommentResource::collection($comments)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Portal\CommentRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CommentRequest $request, Gallery $video)
    {
        /** @var Comment|null */
        $comment = null;

        DB::transaction(function () use ($request, $video, &$comment) {
            /** @var \App\Models\User */
            $user = auth()->user();
            $input = $request->safe()->merge([
                'author_id' => $user->id,
                'gallery_id' => $video->id,
            ])->toArray();

            $comment = $this->commentService->store($input);

            SyncBadges::dispatch(null, [BadgeTypes::TOTAL_GALLERY_COMMENTS, BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS], $user);

            $comment->refresh()->load('author.media');

            $totalUserCommentsForGallery = Comment::where('author_id', auth()->id())
                ->where('gallery_id', $video->id)
                ->count();

            if ($totalUserCommentsForGallery <= 10) {
                $this->pointTransactionService->storeTransaction(
                    transactionable: $comment,
                    user: $user,
                    transactionType: PointTransactionTypes::earnedPoints,
                    points: $video->points_per_comment
                );
            }
        });

        return $this->resourceResponse(
            message: 'Comentario creado correctamente.',
            data: new CommentResource($comment),
            code: Response::HTTP_CREATED
        );
    }
}
