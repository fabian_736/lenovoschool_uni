<?php

namespace App\Http\Controllers\Portal;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Events\SyncBadges;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use App\Models\PointTransaction;
use App\Services\GalleryService;
use App\Services\PointTransactionService;

class GalleryController extends Controller
{
    /** @var GalleryService */
    private $galleryService;

    /** @var PointTransactionService */
    private $pointTransactionService;

    /**
     * Create a new controller instance.
     *
     * @param  GalleryService  $galleryService
     * @param  PointTransactionService  $pointTransactionService
     * @return void
     */
    public function __construct(GalleryService $galleryService, PointTransactionService $pointTransactionService)
    {
        $this->galleryService = $galleryService;
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $gallery = $this->galleryService->list(true);

        return view('modules.landing.gallery.index', compact('gallery'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function viewVideo(Gallery $video)
    {
        $video->load('media');

        return view('modules.landing.gallery.video', compact('video'));
    }

    /**
     * Downloads the given video.
     *
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadVideo(Gallery $video)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        /** @var \Spatie\MediaLibrary\MediaCollections\Models\Media */
        $file = $video->getFirstMedia('file');

        $hasPointsAssigned = PointTransaction::where('user_id', $user->id)
            ->where('transactionable_type', $video->getMorphClass())
            ->where('transactionable_id', $video->id)
            ->exists();

        if (! $hasPointsAssigned) {
            $this->pointTransactionService->storeTransaction(
                transactionable: $video,
                user: $user,
                transactionType: PointTransactionTypes::earnedPoints,
                points: $video->points_for_downloading
            );

            SyncBadges::dispatch(null, [BadgeTypes::TOTAL_GALLERY_DOWNLOADS], $user);
        }

        return response()->download($file->getPath(), $file->file_name);
    }
}
