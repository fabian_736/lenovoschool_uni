<?php

namespace App\Http\Controllers\Portal;

use App\Events\ProfileInformation;
use App\Http\Controllers\Controller;
use App\Http\Requests\Portal\Profile\UpdateProfileImageRequest;
use App\Models\DocumentType;
use App\Models\User;
use App\Services\UserService;
use App\Traits\JsonResponses;

class ProfileInformationController extends Controller
{
    use JsonResponses;

    /**
     * The user service instance
     *
     * @var UserService
     */
    private UserService $userService;

    /**
     * Creates a new controller instance
     *
     * @param  UserService  $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Displays the user profile view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $user = auth()->user();
        $documentTypes = DocumentType::all();

        $rankedUsers = User::with('media')->selectRaw('`id`, RANK() OVER(ORDER BY `general_ranking_points` DESC) AS `rank`');

        $user = User::select(['users.*', 'ranked_users.rank'])
            ->joinSub($rankedUsers, 'ranked_users', function ($join) {
                $join->on('users.id', '=', 'ranked_users.id');
            })
            ->where('users.id', auth()->id())
            ->first();
        return view('modules.landing.profile.index', compact('user', 'documentTypes','user'));
    }

    /**
     * Handle the incoming request.
     *
     * @param  UpdateProfileImageRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateProfileImage(UpdateProfileImageRequest $request)
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $image = $this->userService->updateProfileImage($user, $request->safe()->file);

        ProfileInformation::dispatch($user);

        return $this->jsonResponse(
            message: 'Imagen de perfil actualizada correctamente.',
            metadata: ['url' => $image->getUrl()]
        );
    }
}
