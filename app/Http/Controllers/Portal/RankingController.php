<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class RankingController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke()
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> */
        $generalRanking = $this->getGeneralRanking();
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> */
        $challengesRanking = $this->getChallengesRanking();
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> */
        $liveLessonsRanking = $this->getLiveLessonsRanking();

        return view('modules.landing.ranking.index', compact('generalRanking', 'challengesRanking', 'liveLessonsRanking'));
    }

    /**
     * Gets the top 10 users by general ranking points
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\User>
     */
    private function getGeneralRanking()
    {
        $rankedUsers = User::with('media')->selectRaw('`id`, RANK() OVER(ORDER BY `general_ranking_points` DESC) AS `rank`')->where('general_ranking_points', '>', 0);

        $users = User::select(['users.*', 'ranked_users.rank'])
            ->joinSub($rankedUsers, 'ranked_users', function ($join) {
                $join->on('users.id', '=', 'ranked_users.id');
            })
            ->where('rank', '<=', 10)
            ->orWhere('users.id', auth()->id())
            ->orderBy('rank')
            ->orderBy('general_ranking_points')
            ->orderBy('name')
            ->get();

        return $users;
    }

    /**
     * Gets the top 10 users by challenge ranking points
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\User>
     */
    private function getChallengesRanking()
    {
        $rankedUsers = User::with('media')->selectRaw('`id`, RANK() OVER(ORDER BY `challenge_ranking_points` DESC) AS `rank`')->where('challenge_ranking_points', '>', 0);

        $users = User::select(['users.*', 'ranked_users.rank'])
            ->joinSub($rankedUsers, 'ranked_users', function ($join) {
                $join->on('users.id', '=', 'ranked_users.id');
            })
            ->where('rank', '<=', 10)
            ->orWhere('users.id', auth()->id())
            ->orderBy('rank')
            ->orderBy('general_ranking_points')
            ->orderBy('name')
            ->get();

        return $users;
    }

    /**
     * Gets the top 10 users by live lesson ranking points
     *
     * @return \Illuminate\Database\Eloquent\Collection<int, \App\Models\User>
     */
    private function getLiveLessonsRanking()
    {
        $rankedUsers = User::with('media')->selectRaw('`id`, RANK() OVER(ORDER BY `live_lesson_ranking_points` DESC) AS `rank`')->where('live_lesson_ranking_points', '>', 0);

        $users = User::select(['users.*', 'ranked_users.rank'])
            ->joinSub($rankedUsers, 'ranked_users', function ($join) {
                $join->on('users.id', '=', 'ranked_users.id');
            })
            ->where('rank', '<=', 10)
            ->orWhere('users.id', auth()->id())
            ->orderBy('rank')
            ->orderBy('general_ranking_points')
            ->orderBy('name')
            ->get();

        return $users;
    }
}
