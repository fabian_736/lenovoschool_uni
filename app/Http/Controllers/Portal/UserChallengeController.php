<?php

namespace App\Http\Controllers\Portal;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Enums\UserChallengeStatus;
use App\Events\SyncBadges;
use App\Events\UpdatedProgress;
use App\Http\Controllers\Controller;
use App\Http\Resources\UserChallengeAttemptResource;
use App\Http\Resources\UserChallengeResource;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use App\Services\PointTransactionService;
use App\Services\UserChallengeAttemptService;
use App\Services\UserChallengeService;
use App\Traits\JsonResponses;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UserChallengeController extends Controller
{
    use JsonResponses;

    /** @var UserChallengeService */
    private $userChallengeService;

    /** @var UserChallengeAttemptService */
    private $userChallengeAttemptService;

    /** @var PointTransactionService */
    private $pointTransactionService;

    /**
     * Create a new controller instance.
     *
     * @param  UserChallengeService  $userChallengeService
     * @param  UserChallengeAttemptService  $userChallengeAttemptService
     * @param  PointTransactionService  $pointTransactionService
     * @return void
     */
    public function __construct(UserChallengeService $userChallengeService, UserChallengeAttemptService $userChallengeAttemptService, PointTransactionService $pointTransactionService)
    {
        $this->userChallengeService = $userChallengeService;
        $this->userChallengeAttemptService = $userChallengeAttemptService;
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Display a listing of the challenges.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        $userChallenges = $this->userChallengeService->listWhereUserIsRegistered($user);

        return view('modules.landing.challenge.index', compact('userChallenges'));
    }

    /**
     * Returns the user challenge data.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function getUserChallengeData(UserChallenge $userChallenge)
    {
        $userChallenge->load([
            'challenge' => function ($query) {
                $query->withSum('questions', 'points');
            },
        ]);

        return $this->resourceResponse('', new UserChallengeResource($userChallenge));
    }

    /**
     * Display the given user challenge.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Illuminate\Contracts\View\View
     */
    public function challengeIndex(UserChallenge $userChallenge)
    {
        $userChallenge->load([
            'challenge',
            'challenge.questions' => fn ($query) => $query->inRandomOrder(),
            'challenge.questions.media',
            'challenge.questions.options' => fn ($query) => $query->inRandomOrder(),
            'challenge.questions.options.media',
        ]);

        /** @var UserChallengeAttempt|null */
        $attempt = $this->userChallengeService->getLastAttempt($userChallenge);

        $this->authorize('view', [$userChallenge, $attempt]);

        if ($attempt == null) {
            $userChallenge->loadMax('attempts', 'attempt');
            $nextAttempt = ($userChallenge->attempts_max_attempt ?? 0) + 1;

            $input = [
                'user_challenge_id' => $userChallenge->id,
                'started_at' => now(),
                'attempt' => $nextAttempt,
            ];

            /** @var UserChallengeAttempt */
            $attempt = $this->userChallengeAttemptService->store($input);
        }

        $hoy_minute= date('Y-m-d H:i:s');
        $fin_minute = strtotime ( '+10 minute' , strtotime($attempt->started_at));

            $minutos_fin = date("Y-m-d H:i:s", $fin_minute);



            $minutos_hoy = date("i", strtotime($hoy_minute));

            $Segundos_hoy = date("s",strtotime($hoy_minute));

            $minutos_res = strtotime ( '-'.$minutos_hoy.' minute -'.$Segundos_hoy.' second', strtotime($minutos_fin) ) ;

            if(date('i', $minutos_res) > 10){
             $minutos_res = date('Y-m-d H:i:s','Y-m-d H:0:0');
            }



        return view('modules.landing.challenge.test', compact('userChallenge', 'attempt','minutos_res'));
    }

    /**
     * Score and finish the given user challenge.
     *
     * @param  Request  $request
     * @param  UserChallenge  $userChallenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function finishChallenge(Request $request, UserChallenge $userChallenge, UserChallengeAttempt $userChallengeAttempt)
    {
        $hoy= date('Y-m-d H:i:s');
        $fin_minute = strtotime ( '+10 minute' , strtotime($userChallengeAttempt->started_at) ) ;



        $this->authorize('finishChallenge', [$userChallenge, $userChallengeAttempt]);

        $previousEarnedPoints = $userChallenge->max_earned_points ?? 0;

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
        $answers = $this->userChallengeAttemptService->syncResponses($userChallenge, $userChallengeAttempt, $request->collect());

        $results = $this->scoreAnswers($userChallenge, $answers);

        if(date('Y-m-d H:i:s',$fin_minute) < $hoy){

            $userChallengeAttempt->finished_at = now();
            $userChallengeAttempt->save();

            return $this->jsonResponse(
                message: 'Exedio el tiempo para responder.',
                data: $results,
            );
        }

        DB::transaction(function () use ($userChallenge, $userChallengeAttempt, $results, $previousEarnedPoints) {
            $earnedPoints = $results['earned_points'];
            $challengePoints = $results['challenge_points'];

            $userChallengeInput = [];

            if ($userChallenge->max_earned_points < $earnedPoints) {
                $userChallengeInput['max_earned_points'] = $earnedPoints;
            }

            $userChallenge->loadCount('attempts');

            if ($challengePoints == $earnedPoints) {
                $userChallengeInput['status'] = UserChallengeStatus::finished->value;
            } elseif ($userChallenge->attempts_count >= 3) {
                $userChallengeInput['status'] = UserChallengeStatus::maxAttemptsReached->value;
            }

            $this->userChallengeService->update($userChallenge, $userChallengeInput);

            if ($challengePoints == $earnedPoints) {
                $this->userChallengeService->generateDiploma($userChallenge);
            }

            $this->userChallengeAttemptService->update($userChallengeAttempt, [
                'finished_at' => now(),
                'earned_points' => $earnedPoints,
            ]);

            $realEarnedPoints = $earnedPoints > $previousEarnedPoints ? $earnedPoints - $previousEarnedPoints : 0;

            $user = $userChallenge->user;

            $this->pointTransactionService->storeTransaction(
                transactionable: $userChallenge,
                user: $user,
                transactionType: PointTransactionTypes::earnedPoints,
                points: $realEarnedPoints
            );

            UpdatedProgress::dispatch($user);
            SyncBadges::dispatch(null, [BadgeTypes::TOTAL_FINISHED_CHALLENGES], $user);
        });

        return $this->jsonResponse(
            message: 'Desafío finalizado.',
            data: $results
        );
    }

    /**
     * Redeems a new attempt for the given user challenge.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Illuminate\Http\JsonResponse
     *
     * @throws \App\Exceptions\Portal\FailedRewardRedemptionAttemptException
     * @throws \App\Exceptions\Portal\UserChallengeAttemptCannotBeCreatedException
     */
    public function redeemAttempt(UserChallenge $userChallenge)
    {
        $this->userChallengeService->canRedeemNewAttempt($userChallenge);

        /** @var UserChallengeAttempt|null */
        $attempt = null;

        DB::transaction(function () use (&$attempt, $userChallenge) {
            $userChallenge->loadMax('attempts', 'attempt');
            $nextAttempt = ($userChallenge->attempts_max_attempt ?? 0) + 1;

            $input = [
                'user_challenge_id' => $userChallenge->id,
                'started_at' => now(),
                'attempt' => $nextAttempt,
            ];

            /** @var UserChallengeAttempt */
            $attempt = $this->userChallengeAttemptService->store($input);

            $this->pointTransactionService->storeTransaction(
                transactionable: $attempt,
                user: $userChallenge->user,
                transactionType: PointTransactionTypes::redemption,
                points: config('points-model.redemptions.challenge_attempt')
            );
        });

        return $this->resourceResponse(
            'Puntos redimidos correctamente.',
            new UserChallengeAttemptResource($attempt),
            Response::HTTP_CREATED
        );
    }

    /**
     * Show the listing of finished user challenges for the user.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function viewChallengesWithDiploma()
    {
        $userChallenges = UserChallenge::with([
            'challenge' => fn ($query) => $query->withSum('questions', 'points'),
        ])
            ->where('user_id', auth()->id())
            ->get()
            ->filter(function (UserChallenge $userChallenge) {
                return $userChallenge->status == UserChallengeStatus::finished->value;
            });

        return view('modules.landing.challenge.certificate', compact('userChallenges'));
    }

    /**
     * Downloads the diploma for the given challenge.
     *
     * @param  UserChallenge  $userChallenge
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     *
     * @throws \Exception
     */
    public function downloadDiploma(UserChallenge $userChallenge)
    {
        /** @var \Spatie\MediaLibrary\MediaCollections\Models\Media */
        $diploma = $this->userChallengeService->getDiploma($userChallenge);

        return response()->download($diploma->getPath(), $diploma->file_name);
    }

    /**
     * Score the user challenge answers.
     *
     * @param  Collection<int, \App\Models\QuestionOption>  $userResponses
     * @param  UserChallenge  $userChallenge
     * @return array<string, int>
     */
    public function scoreAnswers(UserChallenge $userChallenge, Collection $userResponses): array
    {
        /** @var Collection<int, \App\Models\Question> */
        $challengeQuestions = $userChallenge->challenge->questions;

        $earnedPoints = $challengeQuestions->filter(function (Question $question) use ($userResponses) {
            [$correctOptions, $incorrectOptions] = $question->options->partition(fn ($value) => $value->is_correct);

            // Validates if all correct options are contained in the user responses
            $hasAllCorrectResponses = $correctOptions->every(function (QuestionOption $option) use ($userResponses) {
                return $userResponses->contains($option);
            });

            // Validates if none of incorrect options are contained in the user responses
            $doesNotHasAnyIncorrectResponses = $incorrectOptions->every(function (QuestionOption $option) use ($userResponses) {
                return ! $userResponses->contains($option);
            });

            // The question is considered to have been answered correctly if and
            // only if the user selected all the correct answers and none of the
            // incorrect answers.
            return $doesNotHasAnyIncorrectResponses && $hasAllCorrectResponses;
        })
            ->reduce(function (int $total, Question $question) {
                return $total + $question->points;
            }, 0);

        /** @var int */
        $challengePoints = intval($userChallenge->challenge->loadSum('questions', 'points')->questions_sum_points, 10);

        return [
            'earned_points' => $earnedPoints,
            'challenge_points' => $challengePoints,
            'max_earned_points' => $earnedPoints > $userChallenge->max_earned_points ? $earnedPoints : $userChallenge->max_earned_points,
        ];
    }
}
