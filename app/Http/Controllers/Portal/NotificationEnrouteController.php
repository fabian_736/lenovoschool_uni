<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;


class NotificationEnrouteController extends Controller
{
    public function read_notification($id, ){
        $read_notifi = Notification::find($id);
       $read_notifi->read_at = date('Y-m-d H:i:s');
       $url = json_decode($read_notifi->data);
        $read_notifi->save();
        return redirect($url->url) ;
    }
}
