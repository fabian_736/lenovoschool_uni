<?php

namespace App\Http\Controllers\Portal;

use App\Http\Controllers\Controller;
use App\Models\Badge;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserBadgesController extends Controller
{
    use RefreshDatabase;

    /**
     * Displays the user badges view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke()
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
        $badges = Badge::with([
            'users' => function ($query) {
                $query->wherePivot('user_id', auth()->id());
            },
            'media',
        ])
            ->get();

        /** @var \App\Models\User */
        $user = auth()->user();

        return view('modules.landing.medal.index', compact('badges', 'user'));
    }

    public function my_medal()
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
        $badges = Badge::with([
            'users' => function ($query) {
                $query->wherePivot('user_id', auth()->id());
            },
            'media',
        ])
            ->get();

        /** @var \App\Models\User */
        $user = auth()->user();

        return view('modules.landing.medal.my_medal', compact('badges', 'user'));
    }

}
