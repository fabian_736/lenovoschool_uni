<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;
use App\Services\AnnouncementService;

class StudentHomeController extends Controller
{
    /** @var AnnouncementService */
    private $announcementService;

    /**
     * Create a new controller instance.
     *
     * @param  AnnouncementService  $announcementService
     * @return void
     */
    public function __construct(AnnouncementService $announcementService)
    {
        $this->announcementService = $announcementService;
    }

    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke()
    {
        $files = $this->announcementService->list();

        return view('modules.landing.home.index', compact('files'));
    }
}
