<?php

namespace App\Http\Controllers\Home;

use App\Enums\UserTypes;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function __invoke()
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        $redirect = $user->user_type == UserTypes::student->value ? 'portal.index' : 'panel.index';

        return redirect()->route($redirect);
    }
}
