<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class PanelHomeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function __invoke()
    {
        return view('panel.home.index');
    }
}
