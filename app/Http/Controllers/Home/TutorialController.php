<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

class TutorialController extends Controller
{
    /**
     * Muestra la vista del tutorial.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('modules.landing.tutorial.index');
    }

    /**
     * Actualiza la fecha de finalización del tutorial.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function finishTutorial()
    {
        /** @var \App\Models\User */
        $user = auth()->user();
        if ($user->tutorial_finished_at === null) {
            $user->tutorial_finished_at = now();
            $user->save();
        }

        return redirect()->route('portal.index');
    }
}
