<?php

namespace App\Http\Controllers\Panel;

use App\Enums\ChallengeQuestionTypes;
use App\Enums\ChallengeStatus;
use App\Exceptions\Panel\Challenges\ChallengeStatusCannotBeUpdatedException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\ChallengeRequest;
use App\Http\Resources\ChallengeResource;
use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\UserChallengeAttempt;
use App\Services\ChallengeService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class ChallengeController extends Controller
{
    use JsonResponses;

    /** @var ChallengeService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  ChallengeService  $service
     * @return void
     */
    public function __construct(ChallengeService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $challenges = $this->service->list();
        $liveLessons = LiveLesson::doesntHave('challenge')->get();

        return view('panel.challenges.index', compact('challenges', 'liveLessons'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  ChallengeRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ChallengeRequest $request)
    {
        $challenge = $this->service->store($request->validated());

        $challenge->refresh();

        return $this->resourceResponse(
            message: 'Desafío creado correctamente.',
            data: new ChallengeResource($challenge),
            code: Response::HTTP_CREATED,
            metadata: ['url' => route('panel.challenges.show', ['challenge' => $challenge])]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Challenge  $challenge
     * @return \Illuminate\Contracts\View\View
     */
    public function show(Challenge $challenge)
    {
        $challenge->load('questions');
        $liveLessons = LiveLesson::where(function ($query) use ($challenge) {
            return $query->doesntHave('challenge')
                ->orWhere('id', $challenge->live_lesson_id);
        })->get();

        return view('panel.challenges.edit', compact('challenge', 'liveLessons'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  ChallengeRequest  $request
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ChallengeRequest $request, Challenge $challenge)
    {
        $this->service->update($challenge, $request->validated());

        return $this->jsonResponse(
            message: 'Desafío actualizado correctamente.'
        );
    }

    /**
     * Publish the specified resource.
     *
     * @param  ChallengeRequest  $request
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function publish(ChallengeRequest $request, Challenge $challenge)
    {
        $this->authorize('publish', $challenge);

        $this->checkIfChallengeCanBePublished($challenge);

        $input = $request->safe()->merge([
            'status' => ChallengeStatus::published->value,
            'published_at' => now(),
        ])
            ->toArray();

        $this->service->update($challenge, $input);

        return $this->jsonResponse(
            message: 'Desafío publicado correctamente.'
        );
    }

    /**
     * Change the status to draft to the specified resource.
     *
     * @param  ChallengeRequest  $request
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function moveToDraft(ChallengeRequest $request, Challenge $challenge)
    {
        $this->authorize('moveToDraft', $challenge);

        $userChallenges = $challenge->userChallenges->pluck('id');
        $attemptsCount = UserChallengeAttempt::whereIn('user_challenge_id', $userChallenges)->count();

        if ($attemptsCount >= 1) {
            throw new ChallengeStatusCannotBeUpdatedException('El desafío no se pudo mover a borrador porque ya hay estudiantes inscritos en el.');
        }

        $input = $request->safe()->merge([
            'status' => ChallengeStatus::draft->value,
            'published_at' => null,
        ])
            ->toArray();

        $this->service->update($challenge, $input);

        return $this->jsonResponse(
            message: 'Desafío movido a borradores correctamente.'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Challenge $challenge)
    {
        $this->service->destroy($challenge);

        return $this->jsonResponse(
            message: 'Desafío eliminado correctamente.'
        );
    }

    /**
     * Verify if the challenge can be published.
     *
     * @param  Challenge  $challenge
     * @return void
     *
     * @throws ChallengeStatusCannotBeUpdatedException
     */
    private function checkIfChallengeCanBePublished(Challenge $challenge)
    {
        if (! $challenge->questions()->exists()) {
            throw new ChallengeStatusCannotBeUpdatedException('El desafío no se pudo publicar porque no tiene preguntas.');
        }

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Question> */
        $questions = $challenge->questions;

        foreach ($questions as $question) {
            if (! $question->options()->exists()) {
                throw new ChallengeStatusCannotBeUpdatedException(
                    sprintf(
                        'El desafío no se pudo publicar porque la pregunta "%s" no tiene opciones de respuesta.',
                        $question->label
                    )
                );
            }

            /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\QuestionOption> */
            $options = $question->options;

            if ($options->count() <= 1) {
                throw new ChallengeStatusCannotBeUpdatedException(
                    sprintf(
                        'El desafío no se pudo publicar porque la pregunta "%s" necesita al menos dos (2) opciones de respuesta.',
                        $question->label
                    )
                );
            }

            $correctOptions = $options->reduce(function ($total, \App\Models\QuestionOption $question) {
                return $total + ($question->is_correct ? 1 : 0);
            }, 0);

            if ($question->type == ChallengeQuestionTypes::unique->value && $correctOptions != 1) {
                throw new ChallengeStatusCannotBeUpdatedException(
                    sprintf(
                        'El desafío no se pudo publicar porque la pregunta "%s" requiere una (1) opción de respuesta correcta. Se encontraron %s opciones de respuesta correctas.',
                        $question->label,
                        $correctOptions
                    )
                );
            }

            if ($question->type == ChallengeQuestionTypes::multiple->value && $correctOptions <= 1) {
                throw new ChallengeStatusCannotBeUpdatedException(
                    sprintf(
                        'El desafío no se pudo publicar porque la pregunta "%s" requiere dos o más opciones de respuesta correctas.',
                        $question->label
                    )
                );
            }
        }
    }
}
