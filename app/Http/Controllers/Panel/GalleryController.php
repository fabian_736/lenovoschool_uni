<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\GalleryRequest;
use App\Http\Resources\GalleryResource;
use App\Models\Gallery;
use App\Services\GalleryService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class GalleryController extends Controller
{
    use JsonResponses;

    /** @var GalleryService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  GalleryService  $service
     * @return void
     */
    public function __construct(GalleryService $service)
    {
        $this->service = $service;
    }

    /**
     * Display the view of the gallery administration.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('panel.gallery');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $gallery = $this->service->list();

        return $this->resourceResponse(
            message: 'Galería consultada correctamente.',
            data: GalleryResource::collection($gallery)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Panel\GalleryRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(GalleryRequest $request)
    {
        $gallery = $this->service->store($request->validated());

        return $this->resourceResponse(
            message: 'Video creado correctamente.',
            data: new GalleryResource($gallery),
            code: Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Gallery $gallery)
    {
        return $this->resourceResponse(
            message: '',
            data: new GalleryResource($gallery)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Panel\GalleryRequest  $request
     * @param  \App\Models\Gallery  $gallery
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(GalleryRequest $request, Gallery $gallery)
    {
        $this->service->update($gallery, $request->validated());

        return $this->jsonResponse(
            message: 'Video actualizado correctamente.'
        );
    }
}
