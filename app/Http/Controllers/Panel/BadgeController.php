<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\BadgeRequest;
use App\Http\Resources\BadgeResource;
use App\Models\Badge;
use App\Services\BadgeService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class BadgeController extends Controller
{
    use JsonResponses;

    /** @var BadgeService */
    private $badgeService;

    /**
     * Create a new controller instance.
     *
     * @param  BadgeService  $badgeService
     * @return void
     */
    public function __construct(BadgeService $badgeService)
    {
        $this->badgeService = $badgeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('panel.badges');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $badges = Badge::with('media')->get();

        return $this->resourceResponse(
            message: 'Insignias obtenidas correctamente.',
            data: BadgeResource::collection($badges)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Panel\BadgeRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BadgeRequest $request)
    {
        $badge = $this->badgeService->store($request->validated());

        return $this->resourceResponse('Insignia creada correctamente', new BadgeResource($badge), Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Badge  $badge
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Badge $badge)
    {
        return $this->resourceResponse('', new BadgeResource($badge));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Panel\BadgeRequest  $request
     * @param  \App\Models\Badge  $badge
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BadgeRequest $request, Badge $badge)
    {
        $this->badgeService->update($badge, $request->validated());

        return $this->jsonResponse(
            message: 'Insignia actualizada correctamente.'
        );
    }
}
