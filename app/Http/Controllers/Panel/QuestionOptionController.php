<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\QuestionOptionRequest;
use App\Http\Resources\QuestionOptionResource;
use App\Models\Challenge;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Services\QuestionOptionService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class QuestionOptionController extends Controller
{
    use JsonResponses;

    /** @var QuestionOptionService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  QuestionOptionService  $service
     * @return void
     */
    public function __construct(QuestionOptionService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the questions options for the given question.
     *
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Challenge $challenge, Question $question)
    {
        $questions = $this->service->list($question);

        return $this->resourceResponse(
            message: 'Opciones de respuesta obtenidas correctamente.',
            data: QuestionOptionResource::collection($questions)
        );
    }

    /**
     * Store a newly question option for the given question.
     *
     * @param  QuestionOptionRequest  $request
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionOptionRequest $request, Challenge $challenge, Question $question)
    {
        $this->authorize('create', [QuestionOption::class, $challenge, $question]);

        $input = $request->safe()->merge([
            'question_id' => $question->id,
        ])->toArray();

        $option = $this->service->store($input);
        $option->refresh();

        return $this->resourceResponse(
            message: 'Opción de respuesta creada correctamente.',
            data: new QuestionOptionResource($option),
            code: Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified question option.
     *
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @param  QuestionOption  $option
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Challenge $challenge, Question $question, QuestionOption $option)
    {
        return $this->resourceResponse(
            message: '',
            data: new QuestionOptionResource($option),
            code: Response::HTTP_OK
        );
    }

    /**
     * Update the specified question option.
     *
     * @param  QuestionOptionRequest  $request
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @param  QuestionOption  $option
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(QuestionOptionRequest $request, Challenge $challenge, Question $question, QuestionOption $option)
    {
        $this->authorize('update', [$option, $challenge, $question]);

        $this->service->update($option, $request->validated());

        return $this->jsonResponse(
            message: 'Opción de respuesta actualizada correctamente.'
        );
    }

    /**
     * Remove the specified question option from storage.
     *
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @param  QuestionOption  $option
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Challenge $challenge, Question $question, QuestionOption $option)
    {
        $this->authorize('delete', [$option, $challenge, $question]);

        $this->service->destroy($option);

        return $this->jsonResponse(
            message: 'Opción de respuesta eliminada correctamente.'
        );
    }
}
