<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\RewardRequest;
use App\Models\Reward;
use App\Services\RewardService;

class RewardController extends Controller
{
    /** @var RewardService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  RewardService  $service
     * @return void
     */
    public function __construct(RewardService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $rewards = $this->service->list();

        return view('panel.rewards.all', compact('rewards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  RewardRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(RewardRequest $request)
    {
        $this->service->store($request->validated());
        session()->flash('success', 'Premio creado correctamente');

        return redirect()->route('panel.rewards.all');
    }

    /**
     * Edit the form for editing the specified resource.
     *
     * @param  Reward  $reward
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Reward $reward)
    {
        return view('panel.rewards.edit', compact('reward'));
    }

    /**
     * Updates the reward.
     *
     * @param  RewardRequest  $request
     * @param  Reward  $reward
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(RewardRequest $request, Reward $reward)
    {
        $this->service->update($reward, $request->validated());
        session()->flash('success', 'Premio actualizado correctamente');

        return redirect()->route('panel.rewards.all');
    }

    /**
     * Deletes the given reward.
     *
     * @param  Reward  $reward
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Reward $reward)
    {
        $this->service->destroy($reward);
        session()->flash('success', 'Premio eliminado correctamente');

        return redirect()->back();
    }
}
