<?php

namespace App\Http\Controllers\Panel;

use App\Enums\UserTypes;
use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\UserRequest;
use App\Http\Resources\UserResource;
use App\Models\DocumentType;
use App\Models\User;
use App\Services\UserService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class UserController extends Controller
{
    use JsonResponses;

    /** @var UserService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  UserService  $service
     * @return void
     */
    public function __construct(UserService $service)
    {
        $this->service = $service;
    }

    /**
     * Display the view of the users.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $documentTypes = DocumentType::all();

        return view('panel.users', compact('documentTypes'));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $users = User::get();

        return $this->resourceResponse(
            message: 'Usuarios obtenidos correctamente.',
            data: UserResource::collection($users)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Panel\UserRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        $validatedInput = $request->safe()
            ->merge([
                'user_type' => UserTypes::administrator,
            ])
            ->toArray();
        $user = $this->service->store($validatedInput);

        return $this->resourceResponse(
            message: 'Usuario creado correctamente.',
            data: new UserResource($user),
            code: Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(User $user)
    {
        return $this->resourceResponse(
            message: '',
            data: new UserResource($user)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Panel\UserRequest  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UserRequest $request, User $user)
    {
        $this->service->update($user, $request->validated());

        return $this->jsonResponse(
            message: 'Usuario actualizado correctamente.'
        );
    }
}
