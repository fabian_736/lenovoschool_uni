<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\Announcements\CreateAnnouncementRequest;
use App\Http\Requests\Panel\Announcements\DeleteAnnouncementRequest;
use App\Services\AnnouncementService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class AnnouncementController extends Controller
{
    use JsonResponses;

    /** @var AnnouncementService */
    private $announcementService;

    /**
     * Create a new controller instance.
     *
     * @param  AnnouncementService  $announcementService
     * @return void
     */
    public function __construct(AnnouncementService $announcementService)
    {
        $this->announcementService = $announcementService;
    }

    /**
     * Display the announcements view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        $files = $this->announcementService->list();

        return view('panel.announcements', compact('files'));
    }

    /**
     * Uploads a new announcement.
     *
     * @param  \App\Http\Requests\Panel\Announcements\CreateAnnouncementRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CreateAnnouncementRequest $request)
    {
        $this->announcementService->store($request->safe()->file);

        return $this->jsonResponse(message: 'Anuncio creado correctamente', code: Response::HTTP_CREATED);
    }

    /**
     * Deletes the given file from storage.
     *
     * @param  \App\Http\Requests\Panel\Announcements\DeleteAnnouncementRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(DeleteAnnouncementRequest $request)
    {
        $this->announcementService->destroy($request->safe()->filename);

        return $this->jsonResponse('Anuncio eliminado correctamente');
    }
}
