<?php

namespace App\Http\Controllers\Panel;

use App\Enums\PointTransactionTypes;
use App\Exceptions\Panel\Penalizations\UserCannotBePenalizedException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\PenalizationRequest;
use App\Models\User;
use App\Services\PenalizationService;
use App\Services\PointTransactionService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PenalizationController extends Controller
{
    use JsonResponses;

    /** @var PenalizationService */
    private $penalizationService;

    /** @var PointTransactionService */
    private $pointTransactionService;

    /**
     * Create a new controller instance.
     *
     * @param  PenalizationService  $penalizationService
     * @param  PointTransactionService  $pointTransactionService
     * @return void
     */
    public function __construct(PenalizationService $penalizationService, PointTransactionService $pointTransactionService)
    {
        $this->penalizationService = $penalizationService;
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('panel.penalizations');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Panel\PenalizationRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PenalizationRequest $request, User $user)
    {
        if ($user->available_redemption_points < $request->safe()->deducted_points) {
            throw new UserCannotBePenalizedException("No se pudo penalizar al usuario porque no tiene suficientes puntos para deducir. Se pueden deducir un máximo de $user->available_redemption_points para este usuario");
        }

        $input = $request->safe()->merge([
            'penalizing_user_id' => auth()->id(),
            'penalized_user_id' => $user->id,
        ])->toArray();

        DB::transaction(function () use ($input, $user) {
            $penalization = $this->penalizationService->store($input);
            $this->pointTransactionService->storeTransaction(
                transactionable: $penalization,
                user: $user,
                transactionType: PointTransactionTypes::penalization,
                points: $penalization->deducted_points
            );
        });

        return $this->jsonResponse(message: 'Usuario penalizado correctamente.', code: Response::HTTP_CREATED);
    }
}
