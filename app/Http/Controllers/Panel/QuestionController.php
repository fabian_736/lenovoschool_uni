<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\QuestionRequest;
use App\Http\Resources\QuestionResource;
use App\Models\Challenge;
use App\Models\Question;
use App\Services\QuestionService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class QuestionController extends Controller
{
    use JsonResponses;

    /** @var QuestionService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  QuestionService  $service
     * @return void
     */
    public function __construct(QuestionService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the questions for the given challenge.
     *
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Challenge $challenge)
    {
        $questions = $this->service->list($challenge);

        return $this->resourceResponse(
            message: 'Preguntas obtenidas correctamente.',
            data: QuestionResource::collection($questions)
        );
    }

    /**
     * Store a newly question for the given challenge.
     *
     * @param  QuestionRequest  $request
     * @param  Challenge  $challenge
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(QuestionRequest $request, Challenge $challenge)
    {
        $this->authorize('create', [Question::class, $challenge]);

        $input = $request->safe()->merge([
            'challenge_id' => $challenge->id,
        ])
            ->toArray();

        $question = $this->service->store($input);

        $question->refresh();

        return $this->resourceResponse(
            message: 'Pregunta creada correctamente.',
            data: new QuestionResource($question),
            code: Response::HTTP_CREATED
        );
    }

    /**
     * Display the specified question.
     *
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Challenge $challenge, Question $question)
    {
        $question->load('options');

        return $this->resourceResponse(
            message: '',
            data: new QuestionResource($question),
            code: Response::HTTP_OK
        );
    }

    /**
     * Update the specified question.
     *
     * @param  QuestionRequest  $request
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(QuestionRequest $request, Challenge $challenge, Question $question)
    {
        $this->authorize('update', [$question, $challenge]);

        $this->service->update($question, $request->validated());

        return $this->jsonResponse(
            message: 'Pregunta actualizada correctamente.'
        );
    }

    /**
     * Remove the specified question from storage.
     *
     * @param  Challenge  $challenge
     * @param  Question  $question
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Challenge $challenge, Question $question)
    {
        $this->authorize('delete', [$question, $challenge]);

        $this->service->destroy($question);

        return $this->jsonResponse(
            message: 'Pregunta eliminada correctamente.'
        );
    }
}
