<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\LiveLessonRequest;
use App\Http\Resources\LiveLessonResource;
use App\Models\LiveLesson;
use App\Services\LiveLessonService;
use App\Traits\JsonResponses;
use Illuminate\Http\Response;

class LiveLessonController extends Controller
{
    use JsonResponses;

    /** @var LiveLessonService */
    private $service;

    /**
     * Create a new controller instance.
     *
     * @param  LiveLessonService  $service
     * @return void
     */
    public function __construct(LiveLessonService $service)
    {
        $this->service = $service;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('panel.live-lessons.index');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $questions = $this->service->list();

        return $this->resourceResponse(
            message: 'Clases en vivo obtenidas correctamente.',
            data: LiveLessonResource::collection($questions)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  LiveLessonRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(LiveLessonRequest $request)
    {
        $challenge = $this->service->store($request->validated());

        $challenge->refresh();

        return $this->resourceResponse(
            message: 'Clase en vivo creada correctamente.',
            data: new LiveLessonResource($challenge),
            code: Response::HTTP_CREATED,
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\LiveLesson  $liveLesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(LiveLesson $liveLesson)
    {
        return $this->resourceResponse(
            message: '',
            data: new LiveLessonResource($liveLesson)
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Panel\LiveLessonRequest  $request
     * @param  \App\Models\LiveLesson  $liveLesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(LiveLessonRequest $request, LiveLesson $liveLesson)
    {
        $this->service->update($liveLesson, $request->validated());

        return $this->jsonResponse(
            message: 'Clase en vivo actualizada correctamente.'
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\LiveLesson  $liveLesson
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(LiveLesson $liveLesson)
    {
        $this->service->destroy($liveLesson);

        return $this->jsonResponse(
            message: 'Clase en vivo eliminada correctamente.'
        );
    }
}
