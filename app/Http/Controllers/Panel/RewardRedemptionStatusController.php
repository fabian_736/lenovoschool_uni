<?php

namespace App\Http\Controllers\Panel;

use App\Http\Controllers\Controller;
use App\Http\Requests\Panel\RewardRedemptionStatusUpdateRequest;
use App\Http\Resources\RewardRedemptionResource;
use App\Models\RewardRedemption;
use App\Services\RewardService;
use App\Traits\JsonResponses;

class RewardRedemptionStatusController extends Controller
{
    use JsonResponses;

    /**
     * The reward service instance
     *
     * @var RewardService
     */
    private RewardService $rewardService;

    /**
     * Create a new controller instance.
     *
     * @param  RewardService  $rewardService
     * @return void
     */
    public function __construct(RewardService $rewardService)
    {
        $this->rewardService = $rewardService;
    }

    /**
     * Display the reward redemptions view.
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return view('panel.rewards.follow');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function list()
    {
        $redemptions = RewardRedemption::with(['receiverDepartament', 'receiverDocumentType', 'reward', 'user'])->orderByRaw("FIELD(`status`, 'pending', 'sent', 'delivered')")->latest()->paginate();

        return $this->resourceResponse(
            message: 'Redenciones obtenidas correctamente.',
            data: RewardRedemptionResource::collection($redemptions)
        );
    }

    /**
     * Returns the given reward redemption.
     *
     * @param  \App\Models\RewardRedemption  $rewardRedemption
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(RewardRedemption $rewardRedemption)
    {
        $rewardRedemption->load(['receiverDepartament', 'receiverDocumentType', 'reward', 'user']);

        return $this->resourceResponse('', new RewardRedemptionResource($rewardRedemption));
    }

    /**
     * Update the status of the given redemption.
     *
     * @param  RewardRedemptionStatusUpdateRequest  $request
     * @param  RewardRedemption  $rewardRedemption
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(RewardRedemptionStatusUpdateRequest $request, RewardRedemption $rewardRedemption)
    {
        $this->rewardService->updateRedemptionStatus($rewardRedemption, $request->safe()->status);

        return $this->jsonResponse(
            message: 'Redención actualizada correctamente.'
        );
    }
}
