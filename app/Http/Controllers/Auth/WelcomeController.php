<?php

namespace App\Http\Controllers\Auth;

use App\Actions\Fortify\PasswordValidationRules;
use App\Events\ProfileInformation;
use App\Models\User as User;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Spatie\WelcomeNotification\WelcomeController as BaseWelcomeController;

class WelcomeController extends BaseWelcomeController
{
    use PasswordValidationRules;

    /**
     * Almacena la contraseña del usuario.
     *
     * @param  Request  $request
     * @param  User  $user
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function savePassword(Request $request, Authenticatable $user)
    {
        $request->validate($this->rules());

        /** @var string */
        $password = $request->input('password');

        $user->password = Hash::make($password);
        $user->welcome_valid_until = null;
        $user->email_verified_at = now();
        $user->save();

        $user = User::whereId($user->id)->first();

        ProfileInformation::dispatch($user);

        return $this->sendPasswordSavedResponse();
    }

    /**
     * Reglas de validación de la contraseña.
     *
     * @return array<string, array>
     */
    protected function rules(): array
    {
        return [
            'password' => $this->passwordRules(),
        ];
    }

    /**
     * Retorna la url a la que debe ser redireccionado el usuario después de establecer su contraseña.
     *
     * @return string
     */
    public function redirectTo(): string
    {
        return route('index');
    }
}
