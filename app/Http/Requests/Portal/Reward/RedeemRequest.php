<?php

namespace App\Http\Requests\Portal\Reward;

use Illuminate\Foundation\Http\FormRequest;

class RedeemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'receiver_name' => 'required|string|between:3,128',
            'receiver_document_type_id' => 'required|exists:document_types,id',
            'receiver_document_number' => 'required|string|between:8,16',
            'receiver_departament_id' => 'required|exists:departaments,id',
            'receiver_address' => 'required|string|between:10,200',
            'receiver_email' => 'required|email:rfc,dns',
            'receiver_phone' => 'required|numeric|digits_between:7,9',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'receiver_name' => 'nombre completo',
            'receiver_document_type_id' => 'tipo de documento',
            'receiver_document_number' => 'número de documento',
            'receiver_departament_id' => 'departamento de entrega',
            'receiver_address' => 'dirección',
            'receiver_email' => 'correo electrónico',
            'receiver_phone' => 'teléfono de contacto',
        ];
    }
}
