<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class LiveLessonRequest extends FormRequest
{
    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    protected function prepareForValidation()
    {
        $this->merge([
            'scheduled_datetime' => "{$this->input('scheduled_date')} {$this->input('scheduled_time')}",
        ]);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|between:5,100',
            'subtitle' => 'required|between:5,100',
            'description' => 'required|between:5,1000',
            'teacher' => 'required|string|between:5,128',
            'scheduled_date' => [
                'required',
                Rule::unique('live_lessons', 'scheduled_date')->ignore($this->liveLesson?->id),
            ],
            'scheduled_time' => 'required',
            'scheduled_datetime' => ['required', 'date', 'after_or_equal:today', 'date_format:Y-m-d H:i:s'],
            'vimeo_live_event_id' => 'required|numeric|integer',
            'vimeo_video_id' => 'required|numeric|integer',
            'cover' => [
                Rule::when(is_null($this->liveLesson), 'required', 'nullable'),
                'file',
                'mimes:png,jpg',
                'max:10000',
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'title' => 'título',
            'subtitle' => 'subtítulo',
            'description' => 'descripción',
            'teacher' => 'profesor',
            'scheduled_date' => 'fecha programada',
            'scheduled_time' => 'hora programada',
            'vimeo_live_event_id' => 'ID de la sesión en vivo',
            'vimeo_video_id' => 'ID de la grabación',
            'cover' => 'Imagen de portada',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array<string, string>
     */
    public function messages()
    {
        return [
            'scheduled_date.unique' => 'Ya existe una clase en vivo programada para el :input',
        ];
    }
}
