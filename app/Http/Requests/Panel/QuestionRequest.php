<?php

namespace App\Http\Requests\Panel;

use App\Enums\ChallengeQuestionTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rules\Enum;

class QuestionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'label' => 'required|string|between:3,2000',
            'points' => 'required|numeric|integer|between:1,5000',
            'type' => ['required', new Enum(ChallengeQuestionTypes::class)],
            'image' => 'nullable|file|mimes:png,jpg|max:10000',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'label' => 'texto de la pregunta',
            'points' => 'puntos',
            'type' => 'tipo de pregunta',
            'image' => 'imagen',
        ];
    }
}
