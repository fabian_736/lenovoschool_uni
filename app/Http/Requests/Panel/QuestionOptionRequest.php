<?php

namespace App\Http\Requests\Panel;

use App\Enums\ChallengeQuestionTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class QuestionOptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'label' => 'required|string|between:3,2000',
            'is_correct' => [
                'required',
                'boolean',
                Rule::when(
                    condition: $this->question->type == ChallengeQuestionTypes::unique->value,
                    rules: [
                        Rule::unique('question_options')
                            ->where(function ($query) {
                                return $query
                                    ->where('is_correct', 1)
                                    ->where('question_id', $this->question->id);
                            })
                            ->ignore($this->option),
                    ]
                ),
            ],
            'image' => 'nullable|file|mimes:png,jpg|max:10000',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'label' => 'texto de la opción de respuesta',
            'is_correct' => '¿es la respuesta correcta?',
            'image' => 'imagen',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array<string, string>
     */
    public function messages()
    {
        return [
            'is_correct.unique' => 'Ya existe una opción de respuesta definida como la opción correcta.',
        ];
    }
}
