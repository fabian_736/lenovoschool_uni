<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChallengeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'live_lesson_id' => [
                'required',
                'exists:live_lessons,id',
                Rule::unique('challenges')
                    ->ignore($this->challenge),
            ],
            'name' => 'required|string|between:5,100',
            'teacher_name' => 'required|string|between:3,128',
            'cover' => [
                Rule::when(is_null($this->challenge), 'required', 'nullable'),
                'image',
                'mimes:png,jpg,jpeg',
                'max:10000',
            ],
            'teacher_signature' => [
                Rule::when(is_null($this->challenge), 'required', 'nullable'),
                'image',
                'mimes:png,jpg,jpeg',
                'max:10000',
            ],
            'status' => 'exclude',
            'published_at' => 'exclude',
        ];
    }

    /**
     * Get custom messages for validator errors.
     *
     * @return array<string, string>
     */
    public function messages()
    {
        return [
            'live_lesson_id.unique' => 'La :attribute seleccionada ya posee un desafío asociado.',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'live_lesson_id' => 'Clase en vivo asociada',
            'name' => 'nombre del desafio',
            'teacher_name' => 'nombre del profesor',
            'cover' => 'cover',
            'teacher_signature' => 'firma del profesor',
        ];
    }
}
