<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class GalleryRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title' => 'required|string|between:3,150',
            'description' => 'required|string|min:30',
            'points_for_downloading' => 'required|numeric|integer|min:1',
            'points_per_comment' => 'required|numeric|integer|min:1',
            'status' => 'required|boolean',
            'preview' => [
                Rule::when(is_null($this->gallery), 'required', 'sometimes'),
                'image',
                'mimes:png,jpg,jpeg',
                'max:5000',
            ],
            'file' => [
                Rule::when(is_null($this->gallery), 'required', 'sometimes'),
                'file',
                'mimes:mp4',
                'max:150000',
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'title' => 'título',
            'description' => 'descripción',
            'points_for_downloading' => 'puntos por descargar',
            'points_per_comment' => 'puntos por comentar',
            'status' => 'stado',
            'preview' => 'imagen de previsualización',
            'file' => 'video',
        ];
    }
}
