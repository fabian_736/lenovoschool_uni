<?php

namespace App\Http\Requests\Panel;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RewardRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:3,100',
            'description' => 'required|string|between:3,200',
            'redeem_points' => 'required|numeric|integer|min:1',
            'quantity_available' => 'required|numeric|integer|min:1',
            'image' => [
                Rule::when(is_null($this->reward), 'required', 'nullable'),
                'file',
                'image',
                'mimes:png,jpg,jpeg',
                'max:5000',
            ],
        ];
    }
}
