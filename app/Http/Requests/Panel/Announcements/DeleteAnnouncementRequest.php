<?php

namespace App\Http\Requests\Panel\Announcements;

use App\Rules\Panel\Announcements\IsValidFilename;
use Illuminate\Foundation\Http\FormRequest;

class DeleteAnnouncementRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'filename' => ['required', 'string', 'max:100', new IsValidFilename()],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'filename' => 'nombre del archivo',
        ];
    }
}
