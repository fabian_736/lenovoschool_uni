<?php

namespace App\Http\Requests\Panel;

use App\Enums\BadgeTypes;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Illuminate\Validation\Rules\Enum;

class BadgeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => [
                'required',
                'string',
                'between:3,100',
                Rule::unique('badges', 'name')->ignore($this->badge?->id),
            ],
            'description' => 'required|string|between:10,200',
            'image' => [
                Rule::when(is_null($this->badge), 'required', 'sometimes'),
                'image',
                'mimes:png,svg',
                'max:5000',
            ],
            'type' => ['required', new Enum(BadgeTypes::class)],
            'total' => 'required|numeric|integer|between:1,16777215',
            'status' => 'required|boolean',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'name' => 'nombre',
            'description' => 'descripción',
            'type' => 'tipo',
            'total' => 'cantidad requerida',
            'status' => 'estado',
            'image' => 'imagen',
        ];
    }
}
