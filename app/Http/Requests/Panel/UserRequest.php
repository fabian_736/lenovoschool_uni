<?php

namespace App\Http\Requests\Panel;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => 'required|string|between:3,128',
            'lastname' => 'required|string|between:3,128',
            'document_type_id' => 'required|exists:document_types,id',
            'document_number' => [
                'required',
                'numeric',
                'integer',
                'digits_between:8,16',
                Rule::unique('users', 'document_number')
                    ->where(fn ($query) => $query->where('document_type_id', $this->document_type_id))
                    ->ignore($this->user?->id),
            ],
            'email' => [
                'required',
                'string',
                'email',
                'max:255',
                Rule::unique(User::class)->ignore($this->user?->id),
            ],
            'position' => 'required|string|between:3,50',
            'company' => 'required|string|between:3,50',
            'active' => 'required|boolean',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array<string, string>
     */
    public function attributes()
    {
        return [
            'name' => 'nombre',
            'lastname' => 'apellido',
            'document_type_id' => 'tipo de documento',
            'document_number' => 'número de documento',
            'cellphone_number' => 'número de teléfono',
            'email' => 'correo electrónico',
            'position' => 'cargo',
            'company' => 'compañía',
            'active' => 'estado',
        ];
    }
}
