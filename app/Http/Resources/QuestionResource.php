<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Question
 */
class QuestionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'type' => $this->type,
            'points' => $this->points,
            'challenge_id' => $this->challenge_id,
            'image' => $this->hasMedia('image') ? $this->getFirstMediaUrl('image') : null,
            'options' => QuestionOptionResource::collection($this->whenLoaded('options')),
        ];
    }
}
