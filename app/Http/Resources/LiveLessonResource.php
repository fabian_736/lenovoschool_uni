<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\LiveLesson
 */
class LiveLessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'subtitle' => $this->subtitle,
            'description' => $this->description,
            'teacher' => $this->teacher,
            'scheduled_date' => $this->scheduled_date,
            'scheduled_time' => $this->scheduled_time,
            'vimeo_live_event_id' => $this->vimeo_live_event_id,
            'vimeo_video_id' => $this->vimeo_video_id,
            'vimeo_player_url' => $this->vimeo_player_url,
            'vimeo_event_url' => $this->vimeo_event_url,
            'vimeo_event_chat_url' => $this->vimeo_event_chat_url,
            'metadata' => new UserLiveLessonResource($this->whenLoaded('metadata')),
            'cover' => $this->getFirstMediaUrl('cover'),
        ];
    }
}
