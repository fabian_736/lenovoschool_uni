<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Gallery
 */
class GalleryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'description' => $this->description,
            'points_for_downloading' => $this->points_for_downloading,
            'points_per_comment' => $this->points_per_comment,
            'status' => $this->status,
            'preview_file_url' => $this->preview_file_url,
            'file_url' => $this->file_url,
        ];
    }
}
