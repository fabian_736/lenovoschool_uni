<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\RewardRedemption
 */
class RewardRedemptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => $this->whenLoaded('user'),
            'reward_id' => $this->reward_id,
            'reward' => $this->whenLoaded('reward'),
            'receiver_name' => $this->receiver_name,
            'receiver_document_type_id' => $this->receiver_document_type_id,
            'receiver_document_type' => new DocumentTypeResource($this->whenLoaded('receiverDocumentType')),
            'receiver_document_number' => $this->receiver_document_number,
            'receiver_email' => $this->receiver_email,
            'receiver_departament_id' => $this->receiver_departament_id,
            'receiver_departament' => $this->whenLoaded('receiverDepartament'),
            'receiver_address' => $this->receiver_address,
            'receiver_phone' => $this->receiver_phone,
            'status' => $this->status->value,
            'status_label' => $this->status->label(),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
