<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\Comment
 */
class CommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'author_id' => $this->author_id,
            'author' => new UserResource($this->whenLoaded('author')),
            'gallery_id' => $this->gallery_id,
            'gallery' => new GalleryResource($this->whenLoaded('gallery')),
            'comment' => $this->comment,
        ];
    }
}
