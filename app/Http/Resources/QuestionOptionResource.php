<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\QuestionOption
 */
class QuestionOptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'label' => $this->label,
            'is_correct' => $this->is_correct,
            'question_id' => $this->question_id,
            'image' => $this->hasMedia('image') ? $this->getFirstMediaUrl('image') : null,
        ];
    }
}
