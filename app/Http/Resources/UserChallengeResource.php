<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\UserChallenge
 */
class UserChallengeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'user' => new UserResource($this->whenLoaded('user')),
            'challenge_id' => $this->challenge_id,
            'challenge' => new ChallengeResource($this->whenLoaded('challenge')),
            'max_earned_points' => $this->max_earned_points,
            'status' => $this->status,
            'attempts' => UserChallengeAttemptResource::collection($this->whenLoaded('attempts')),
            'last_attempt' => new UserChallengeAttemptResource($this->whenLoaded('lastAttempt')),
            'can_have_more_attempts' => $this->canHaveMoreAttempts(),
        ];
    }
}
