<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\UserChallengeAttempt
 */
class UserChallengeAttemptResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_challenge_id' => $this->user_challenge_id,
            'user_challenge' => new UserChallengeResource($this->whenLoaded('userChallenge')),
            'started_at' => $this->started_at,
            'finished_at' => $this->finished_at,
            'earned_points' => $this->earned_points,
            'attempt' => $this->attempt,
            'attempt_choices' => $this->whenLoaded('attemptChoices'),
        ];
    }
}
