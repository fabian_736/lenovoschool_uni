<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\User
 */
class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'lastname' => $this->lastname,
            'fullname' => $this->fullname,
            'document_type_id' => $this->document_type_id,
            'document_number' => $this->document_number,
            'cellphone_number' => $this->cellphone_number,
            'email' => $this->email,
            'position' => $this->position,
            'company' => $this->company,
            'email_verified_at' => $this->email_verified_at,
            'user_type' => $this->user_type,
            'active' => $this->active,
            'total_redemption_points' => $this->total_redemption_points,
            'available_redemption_points' => $this->available_redemption_points,
            'user_challenges' => UserChallengeResource::collection($this->whenLoaded('userChallenges')),
            'user_live_lessons' => UserLiveLessonResource::collection($this->whenLoaded('userLiveLessons')),
            'avatar_url' => $this->getFirstMediaUrl('avatar'),
        ];
    }
}
