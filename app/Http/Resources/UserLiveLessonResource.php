<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @mixin \App\Models\UserLiveLesson
 */
class UserLiveLessonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array<string, mixed>
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'live_lesson_id' => $this->live_lesson_id,
            'watched' => $this->watched,
            'watched_time' => $this->watched_time,
        ];
    }
}
