<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class TutorialMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var \App\Models\User */
        $user = auth()->user();

        if ($user->tutorial_finished_at === null) {
            return redirect()->route('tutorial.index');
        }

        return $next($request);
    }
}
