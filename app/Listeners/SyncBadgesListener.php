<?php

namespace App\Listeners;

use App\Enums\BadgeTypes;
use App\Enums\PointTransactionTypes;
use App\Enums\UserChallengeStatus;
use App\Events\AwardedBadge;
use App\Events\SyncBadges;
use App\Models\Badge;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\User;
use Illuminate\Support\Facades\DB;

class SyncBadgesListener
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\SyncBadges  $event
     * @return void
     */
    public function handle(SyncBadges $event)
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
        $badges = $event->badges;

        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\User> */
        $users = $event->users;

        $users->each(function (User $user) use ($badges) {
            $badgesToAttach = $badges->filter(function (Badge $badge) use ($user) {
                return $this->userQualifiesForBadge($user, $badge);
            });

            if (! $badgesToAttach->isEmpty()) {
                $user->badges()->attach($badgesToAttach);

                AwardedBadge::dispatch($user, $badgesToAttach);
            }
        });
    }

    /**
     * Determines if the given user can receive the given badge.
     *
     * @param  User  $user
     * @param  Badge  $badge
     * @return bool
     */
    public function userQualifiesForBadge(User $user, Badge $badge): bool
    {
        if ($user->badges->contains($badge)) {
            return false;
        }

        $userTotal = 0;
        $requiredTotal = $badge->total;

        switch ($badge->type) {
            case BadgeTypes::TOTAL_ACCUMULATED_POINTS:
                $userTotal = $user->total_redemption_points;
                break;
            case BadgeTypes::TOTAL_COMMENTED_GALLERY_VIDEOS:
                $userTotal = Comment::distinct('gallery_id')->where('author_id', $user->id)->count();
                break;
            case BadgeTypes::TOTAL_FINISHED_CHALLENGES:
                $userTotal = $user->userChallenges()->where('status', UserChallengeStatus::finished)->count();
                break;
            case BadgeTypes::TOTAL_GALLERY_COMMENTS:
                $userTotal = $user->galleryComments()->count();
                break;
            case BadgeTypes::TOTAL_GALLERY_DOWNLOADS:
                $userTotal = $user->pointTransactions()->whereHasMorph('transactionable', Gallery::class)->count();
                break;
            case BadgeTypes::TOTAL_VIEWED_LIVE_LESSONS:
                $userTotal = $user->userLiveLessons()->where('watched', true)->count();
                break;
            case BadgeTypes::TOTAL_REDEEMED_POINTS:
                $userTotal = $user->pointTransactions()->where('type', PointTransactionTypes::redemption)->count();
                break;
            case BadgeTypes::TOTAL_REWARD_REDEMPTIONS:
                $userTotal = $user->rewardRedemptions()->count();
                break;
            case BadgeTypes::TOTAL_LOGINS:
                $userTotal = $user->authenticationLogs()
                    ->groupBy(DB::raw('DATE(created_at)'))
                    ->count();
                break;
        }

        return $userTotal >= $requiredTotal;
    }
}
