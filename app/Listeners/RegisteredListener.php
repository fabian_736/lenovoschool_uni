<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Registered;

class RegisteredListener
{
    /**
     * Handle the event.
     *
     * @param  Registered  $event
     * @return void
     */
    public function handle(Registered $event)
    {
        /** @var \App\Models\User */
        $user = $event->user;

        $expiresAt = now()->addDay();

        $user->sendWelcomeNotification($expiresAt);
    }
}
