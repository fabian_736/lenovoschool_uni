<?php

namespace App\Listeners;

use App\Enums\BadgeTypes;
use App\Events\SyncBadges;
use Illuminate\Auth\Events\Login;

class LogSuccessfulLogin
{
    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        /** @var \App\Models\User */
        $user = $event->user;

        $user->authenticationLogs()->create([
            'ip' => request()->ip(),
        ]);

        SyncBadges::dispatch(null, [BadgeTypes::TOTAL_LOGINS], $user);
    }
}
