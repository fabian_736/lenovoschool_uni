<?php

namespace App\Listeners;

use App\Events\UpdatedProgress;
use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\UserChallenge;
use App\Models\UserLiveLesson;
use App\Services\UserService;

class UpdatedProgressListener
{
    /**
     * The user service.
     *
     * @var UserService
     */
    private $userService;

    /**
     * The user instance.
     *
     * @var \App\Models\User
     */
    private $user;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\UpdatedProgress  $event
     * @return void
     */
    public function handle(UpdatedProgress $event)
    {
        $this->user = $event->user;

        $totalLiveLessons = LiveLesson::count();
        $totalChallenges = Challenge::count();
        $totalUserLiveLessons = $this->getWatchedLiveLessonsCount();
        $totalUserChallenges = $this->getFinishedChallengesCount();

        $totalEarnedPointsInChallenges = $this->getTotalEarnedPointsInChallenges();
        $totalEarnedPointsInLiveLessons = $this->getTotalEarnedPointsInLiveLessons();

        $totalProgress = (100 * ($totalUserLiveLessons + $totalUserChallenges)) / ($totalLiveLessons + $totalChallenges);

        $input = [
            'total_progress' => $totalProgress,
            'challenge_ranking_points' => $totalEarnedPointsInChallenges,
            'live_lesson_ranking_points' => $totalEarnedPointsInLiveLessons,
            'general_ranking_points' => $totalEarnedPointsInChallenges + $totalEarnedPointsInLiveLessons,
        ];

        $this->userService->update($this->user, $input);
    }

    /**
     * Returns the total watched live lessons
     *
     * @return int
     */
    private function getWatchedLiveLessonsCount(): int
    {
        return UserLiveLesson::where('user_id', $this->user->id)->where('watched', true)->count();
    }

    /**
     * Returns the total watched live lessons
     *
     * @return int
     */
    private function getFinishedChallengesCount(): int
    {
        return UserChallenge::where('user_id', $this->user->id)
            ->whereHas('lastAttempt', fn ($query) => $query->whereNotNull('finished_at'))
            ->count();
    }

    /**
     * Returns the total watched live lessons
     *
     * @return int
     */
    private function getTotalEarnedPointsInChallenges(): int
    {
        return UserChallenge::where('user_id', $this->user->id)->sum('max_earned_points');
    }

    /**
     * Returns the total watched live lessons
     *
     * @return int
     */
    private function getTotalEarnedPointsInLiveLessons(): int
    {
        $totalViewedLiveLessons = UserLiveLesson::whereUserId($this->user->id)
            ->join('live_lessons', 'user_live_lessons.live_lesson_id', '=', 'live_lessons.id')
            ->whereHas('tracking', function ($query) {
                return $query
                    ->where('watched_time', '>=', config('points-model.live_lessons.required_viewed_time'))
                    ->whereRaw('`live_lessons`.`scheduled_date` = DATE(`user_live_lesson_tracking_histories`.`created_at`)');
            })
            ->count();

        return $totalViewedLiveLessons * config('points-model.award.attend_a_live_lesson_on_time');
    }
}
