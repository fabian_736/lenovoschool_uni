<?php

namespace App\Listeners;

use App\Enums\PointTransactionTypes;
use App\Events\ProfileInformation;
use App\Services\PointTransactionService;
use Illuminate\Support\Facades\DB;

class ProfileInformationListener
{
    /** @var PointTransactionService */
    private PointTransactionService $pointTransactionService;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(PointTransactionService $pointTransactionService)
    {
        $this->pointTransactionService = $pointTransactionService;
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\ProfileInformation  $event
     * @return void
     */
    public function handle(ProfileInformation $event)
    {
        $user = $event->user;

        if (
            $user->password !== null &&
            $user->hasMedia('avatar') &&
            ! $user->pointTransaction()->exists()
        ) {
            DB::transaction(function () use ($user) {
                $this->pointTransactionService->storeTransaction(
                    transactionable: $user,
                    user: $user,
                    transactionType: PointTransactionTypes::earnedPoints,
                    points: config('points-model.award.completing_registration')
                );
            });
        }
    }
}
