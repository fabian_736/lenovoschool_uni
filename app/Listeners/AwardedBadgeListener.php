<?php

namespace App\Listeners;

use App\Events\AwardedBadge;
use App\Models\Badge;
use App\Notifications\Badges\AwardedBadgeNotification;

class AwardedBadgeListener
{
    /**
     * Handle the event.
     *
     * @param  \App\Events\AwardedBadge  $event
     * @return void
     */
    public function handle(AwardedBadge $event)
    {
        /** @var \Illuminate\Database\Eloquent\Collection<int, \App\Models\Badge> */
        $badges = $event->badges;

        /** @var \App\Models\User */
        $user = $event->user;

        $badges->each(function (Badge $badge) use ($user) {
            $user->notify(new AwardedBadgeNotification($badge));
        });
    }
}
