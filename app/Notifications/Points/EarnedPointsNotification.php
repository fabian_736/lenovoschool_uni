<?php

namespace App\Notifications\Points;

use App\Contracts\HasPointTransactions;
use App\Models\Comment;
use App\Models\Gallery;
use App\Models\PointTransaction;
use App\Models\User;
use App\Models\UserChallenge;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class EarnedPointsNotification extends Notification
{
    use Queueable;

    /**
     * The point transaction instance
     *
     * @var PointTransaction
     */
    public PointTransaction $pointTransaction;

    /**
     * The points source
     *
     * @var string
     */
    public string $pointsSource;

    /**
     * The points source name
     *
     * @var string|null
     */
    public $pointsSourceName;

    /**
     * Create a new notification instance.
     *
     * @param  PointTransaction  $pointTransaction
     * @return void
     *
     * @throws \UnexpectedValueException
     */
    public function __construct(PointTransaction $pointTransaction)
    {
        $this->pointTransaction = $pointTransaction;

        $transactionable = $this->pointTransaction->transactionable;

        $this->pointsSource = match (true) {
            $transactionable instanceof Comment => 'realizar un comentario',
            $transactionable instanceof Gallery => 'descargar un video',
            $transactionable instanceof UserChallenge => 'responder correctamente las preguntas de tu desafio',
            $transactionable instanceof User => 'completar tu registro',
            // @codeCoverageIgnoreStart
            default => throw new \UnexpectedValueException(sprintf(
                '[%s::$pointTransaction] must implement the [%s] interface.',
                $transactionable::class,
                HasPointTransactions::class
            ))
            // @codeCoverageIgnoreEnd
        };

        $this->pointsSourceName = match (true) {
            $transactionable instanceof Comment => $transactionable->gallery->title,
            $transactionable instanceof Gallery => $transactionable->title,
            $transactionable instanceof UserChallenge => $transactionable->challenge->name,
            default => null
        };

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject("¡Felicidades! Has ganado {$this->pointTransaction->points} puntos")
            ->view('mail.points.earned-points', [
                'notifiable' => $notifiable,
                'pointTransaction' => $this->pointTransaction,
                'pointsSource' => $this->pointsSource,
                'pointsSourceName' => $this->pointsSourceName,
            ]);
    }

    /**
     * Get the array representation of the notification for the database channel.
     *
     * @param  mixed  $notifiable
     * @return array<string, mixed>
     */
    public function toDatabase($notifiable)
    {
        return [
            'main_text' => sprintf(
                'Felicidades! Ganaste %s puntos por %s.',
                $this->pointTransaction->points,
                $this->pointsSource
            ),
            'image' => '',
            'url' => route('rewards.index'),
        ];
    }
}
