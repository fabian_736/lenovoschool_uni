<?php

namespace App\Notifications\Badges;

use App\Models\Badge;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class AwardedBadgeNotification extends Notification
{
    use Queueable;

    /**
     * The badge instance.
     *
     * @var \App\Models\Badge
     */
    public $badge;

    /**
     * Create a new notification instance.
     *
     * @param  Badge  $badge
     * @return void
     */
    public function __construct(Badge $badge)
    {
        $this->badge = $badge;

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('¡Felicidades! Has ganado una insignia')
            ->view(
                'mail.badges.awarded-badge',
                [
                    'badge' => $this->badge,
                    'notifiable' => $notifiable,
                ]
            );
    }

    /**
     * Get the array representation of the notification for the database channel.
     *
     * @param  mixed  $notifiable
     * @return string[]
     */
    public function toDatabase($notifiable)
    {
        return [
            'main_text' => sprintf('¡Felicidades! Has ganado la insignia %s', $this->badge->name),
            'image' => $this->badge->getFirstMediaUrl('image'),
            'url' => route('badges'),
        ];
    }
}
