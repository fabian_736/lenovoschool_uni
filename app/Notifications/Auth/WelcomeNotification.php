<?php

namespace App\Notifications\Auth;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Spatie\WelcomeNotification\WelcomeNotification as BaseWelcomeNotification;

class WelcomeNotification extends BaseWelcomeNotification
{
    use Queueable;

    public function buildWelcomeNotificationMessage(): MailMessage
    {
        return (new MailMessage)
            ->subject('¡Bienvenido a Lenovo School!')
            ->view(
                'mail.auth.welcome',
                [
                    'user' => $this->user,
                    'url' => $this->showWelcomeFormUrl,
                ]
            );
    }
}
