<?php

namespace App\Notifications\Rewards;

use App\Models\Reward;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class RedeemedRewardNotification extends Notification
{
    use Queueable;

    /**
     * The reward instance.
     *
     * @var \App\Models\Reward
     */
    public $reward;

    /**
     * Create a new notification instance.
     *
     * @param  Reward  $reward
     * @return void
     */
    public function __construct(Reward $reward)
    {
        $this->reward = $reward;

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('¡Felicidades! has redimido un premio')
            ->view('mail.rewards.redeemed-reward', [
                'notifiable' => $notifiable,
                'reward' => $this->reward,
            ]);
    }

    /**
     * Get the array representation of the notification for the database channel.
     *
     * @param  mixed  $notifiable
     * @return string[]
     */
    public function toDatabase($notifiable)
    {
        return [
            'main_text' => sprintf(
                'Redimiste un %s por %s puntos',
                $this->reward->name,
                $this->reward->redeem_points
            ),
            'image' => $this->reward->getFirstMediaUrl('image'),
            'url' => route('rewards.index'),
        ];
    }
}
