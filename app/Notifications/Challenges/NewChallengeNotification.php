<?php

namespace App\Notifications\Challenges;

use App\Models\UserChallenge;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewChallengeNotification extends Notification
{
    use Queueable;

    /**
     * The user challenge instance
     *
     * @var UserChallenge
     */
    public $userChallenge;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(UserChallenge $userChallenge)
    {
        $userChallenge->load([
            'challenge' => fn ($query) => $query->withSum('questions', 'points'),
        ]);

        $this->userChallenge = $userChallenge;

        $this->afterCommit();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array<string>
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Nuevo desafío disponible en Lenovo School')
            ->view('mail.challenges.new-challenge', [
                'userChallenge' => $this->userChallenge,
            ]);
    }

    /**
     * Get the array representation of the notification for the database channel.
     *
     * @param  mixed  $notifiable
     * @return string[]
     */
    public function toDatabase($notifiable)
    {
        return [
            'main_text' => 'Hay un nuevo desafío disponible, descúbrelo.',
            'image' => $this->userChallenge->challenge->getFirstMediaUrl('cover'),
            'url' => route('challenges.index'),
        ];
    }
}
