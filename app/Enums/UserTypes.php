<?php

namespace App\Enums;

enum UserTypes: string
{
    case superadmin = 'superadmin';
    case administrator = 'administrator';
    case student = 'student';
}
