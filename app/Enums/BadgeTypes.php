<?php

namespace App\Enums;

enum BadgeTypes: string
{
    case TOTAL_ACCUMULATED_POINTS = 'TOTAL_ACCUMULATED_POINTS';
    case TOTAL_COMMENTED_GALLERY_VIDEOS = 'TOTAL_COMMENTED_GALLERY_VIDEOS';
    case TOTAL_FINISHED_CHALLENGES = 'TOTAL_FINISHED_CHALLENGES';
    case TOTAL_GALLERY_COMMENTS = 'TOTAL_GALLERY_COMMENTS';
    case TOTAL_GALLERY_DOWNLOADS = 'TOTAL_GALLERY_DOWNLOADS';
    case TOTAL_LOGINS = 'TOTAL_LOGINS';
    case TOTAL_REDEEMED_POINTS = 'TOTAL_REDEEMED_POINTS';
    case TOTAL_REWARD_REDEMPTIONS = 'TOTAL_REWARD_REDEMPTIONS';
    case TOTAL_VIEWED_LIVE_LESSONS = 'TOTAL_VIEWED_LIVE_LESSONS';

    /**
     * Returns the human-readable label for the cases
     *
     * @return string
     */
    public function label(): string
    {
        return match ($this) {
            static::TOTAL_ACCUMULATED_POINTS => 'Puntos totales acumulados',
            static::TOTAL_COMMENTED_GALLERY_VIDEOS => 'Videos de la galería comentados',
            static::TOTAL_FINISHED_CHALLENGES => 'Desafíos finalizados',
            static::TOTAL_GALLERY_COMMENTS => 'Comentarios en la galería',
            static::TOTAL_GALLERY_DOWNLOADS => 'Descargas en la galería',
            static::TOTAL_LOGINS => 'Días iniciando sesión',
            static::TOTAL_REDEEMED_POINTS => 'Puntos redimidos',
            static::TOTAL_REWARD_REDEMPTIONS => 'Premios redimidos',
            static::TOTAL_VIEWED_LIVE_LESSONS => 'Clases en vivo vistas',
        };
    }
}
