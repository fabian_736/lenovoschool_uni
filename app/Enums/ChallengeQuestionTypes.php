<?php

namespace App\Enums;

enum ChallengeQuestionTypes: string
{
    case unique = 'unique';
    case multiple = 'multiple';
}
