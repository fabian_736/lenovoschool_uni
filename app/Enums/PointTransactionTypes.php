<?php

namespace App\Enums;

enum PointTransactionTypes: string
{
    case earnedPoints = 'Earned points';
    case redemption = 'Redemption';
    case penalization = 'Penalization';
}
