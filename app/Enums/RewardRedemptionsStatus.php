<?php

namespace App\Enums;

enum RewardRedemptionsStatus: string
{
    case pending = 'pending';
    case sent = 'sent';
    case delivered = 'delivered';

    public function label(): string
    {
        return match ($this) {
            static::pending => 'Pendiente de envío',
            static::sent => 'Enviado',
            static::delivered => 'Entregado',
        };
    }
}
