<?php

namespace App\Enums;

enum ChallengeStatus: string
{
    case draft = 'Draft';
    case published = 'Published';
}
