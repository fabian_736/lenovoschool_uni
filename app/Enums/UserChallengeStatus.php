<?php

namespace App\Enums;

enum UserChallengeStatus: string
{
    case unavailable = 'Unavailable';
    case inProgress = 'In progress';
    case finished = 'Finished';
    case maxAttemptsReached = 'Max attempts reached';
}
