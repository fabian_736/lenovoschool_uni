<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

/**
 * @mixin IdeHelperUserLiveLesson
 */
class UserLiveLesson extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'live_lesson_id',
        'watched',
        'watched_time',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'watched' => 'boolean',
        'watched_time' => 'double',
    ];

    /**
     * Retrieves the model's user.
     *
     * @return BelongsTo<\App\Models\User, \App\Models\UserLiveLesson>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieves the model's livel lesson.
     *
     * @return BelongsTo<\App\Models\LiveLesson, \App\Models\UserLiveLesson>
     */
    public function liveLesson(): BelongsTo
    {
        return $this->belongsTo(LiveLesson::class);
    }

    /**
     * Retrieves the model's tracking.
     *
     * @return HasMany<\App\Models\UserLiveLessonTrackingHistory>
     */
    public function tracking(): HasMany
    {
        return $this->hasMany(UserLiveLessonTrackingHistory::class);
    }
}
