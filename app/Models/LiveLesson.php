<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @property-read string $vimeo_player_url
 * @property-read string $vimeo_event_url
 * @property-read string $vimeo_event_chat_url
 * @mixin IdeHelperLiveLesson
 */
class LiveLesson extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'title',
        'subtitle',
        'description',
        'teacher',
        'scheduled_date',
        'scheduled_time',
        'vimeo_live_event_id',
        'vimeo_video_id',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $cast = [
        'scheduled_date' => 'date:Y-m-d',
        'scheduled_time' => 'date:H:i:s',
    ];

    /**
     * Gets the url for the vimeo video.
     *
     * @return Attribute<string, never>
     */
    public function vimeoPlayerUrl(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return 'https://player.vimeo.com/video/'.$attributes['vimeo_video_id'];
            },
        );
    }

    /**
     * Gets the url for the vimeo live.
     *
     * @return Attribute<string, never>
     */
    public function vimeoEventUrl(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return 'https://vimeo.com/event/'.$attributes['vimeo_live_event_id'].'/embed';
            },
        );
    }

    /**
     * Gets the url for the vimeo live comments.
     *
     * @return Attribute<string, never>
     */
    public function vimeoEventChatUrl(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return 'https://vimeo.com/event/'.$attributes['vimeo_live_event_id'].'/chat';
            },
        );
    }

    /**
     * Retrieves the model's associated challenge.
     *
     * @return HasOne<\App\Models\Challenge>
     */
    public function challenge(): HasOne
    {
        return $this->hasOne(Challenge::class);
    }

    /**
     * Retrieves the model's metadata.
     *
     * @return HasOne<\App\Models\UserLiveLesson>
     */
    public function metadata(): HasOne
    {
        return $this->hasOne(UserLiveLesson::class)
            ->ofMany([
                'id' => 'max',
            ], function ($query) {
                $query->where('user_id', auth()->id());
            });
    }

    /**
     * Registra las colecciones de media del modelo.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('cover')
            ->useFallbackUrl('/img/no_cover.png')
            ->singleFile();
    }
}
