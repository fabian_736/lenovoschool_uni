<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use App\Enums\RewardRedemptionsStatus;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @mixin IdeHelperRewardRedemption
 */
class RewardRedemption extends Model implements HasPointTransactions
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'reward_id',
        'receiver_name',
        'receiver_document_type_id',
        'receiver_document_number',
        'receiver_email',
        'receiver_departament_id',
        'receiver_address',
        'receiver_phone',
        'status',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'status' => RewardRedemptionsStatus::class,
        'receiver_document_number' => 'string',
        'receiver_phone' => 'string',
    ];

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphOne<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphOne
    {
        return $this->morphOne(PointTransaction::class, 'transactionable');
    }

    /**
     * Retrieves the departament to send the reward.
     *
     * @return BelongsTo<\App\Models\Departament, \App\Models\RewardRedemption>
     */
    public function receiverDepartament(): BelongsTo
    {
        return $this->belongsTo(Departament::class, 'receiver_departament_id');
    }

    /**
     * Retrieves the reward receiver document type.
     *
     * @return BelongsTo<\App\Models\DocumentType, \App\Models\RewardRedemption>
     */
    public function receiverDocumentType(): BelongsTo
    {
        return $this->belongsTo(DocumentType::class, 'receiver_document_type_id');
    }

    /**
     * Retrieves the model's reward.
     *
     * @return BelongsTo<\App\Models\Reward, \App\Models\RewardRedemption>
     */
    public function reward(): BelongsTo
    {
        return $this->belongsTo(Reward::class);
    }

    /**
     * Retrieves the model's user.
     *
     * @return BelongsTo<\App\Models\User, \App\Models\RewardRedemption>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
