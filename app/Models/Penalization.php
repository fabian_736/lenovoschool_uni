<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @mixin IdeHelperPenalization
 */
class Penalization extends Model implements HasPointTransactions
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'penalizing_user_id',
        'penalized_user_id',
        'reason',
        'deducted_points',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $cast = [
        'deducted_points' => 'integer',
    ];

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphOne<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphOne
    {
        return $this->morphOne(PointTransaction::class, 'transactionable');
    }
}
