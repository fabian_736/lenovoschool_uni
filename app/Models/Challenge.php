<?php

namespace App\Models;

use App\Enums\ChallengeStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @property-read bool $is_draft
 * @property-read bool $is_published
 * @property-read int|null $questions_sum_points
 * @mixin IdeHelperChallenge
 */
class Challenge extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'live_lesson_id',
        'name',
        'teacher_name',
        'status',
        'published_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'published_at' => 'date:Y-m-d',
    ];

    /**
     * Retrieves the status column translated.
     *
     * @return Attribute<string, never>
     */
    public function translatedStatus(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return match ($attributes['status']) {
                    ChallengeStatus::draft->value => 'Borrador',
                    ChallengeStatus::published->value => 'Publicado',
                    default => 'Desconocido'
                };
            },
        );
    }

    /**
     * Returns true if the model is a draft.
     *
     * @return Attribute<bool, never>
     */
    public function isDraft(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return $attributes['status'] == ChallengeStatus::draft->value;
            }
        );
    }

    /**
     * Returns true if the model is published.
     *
     * @return Attribute<bool, never>
     */
    public function isPublished(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return $attributes['status'] == ChallengeStatus::published->value;
            }
        );
    }

    /**
     * Retrieves the model's associated live lesson.
     *
     * @return BelongsTo<\App\Models\LiveLesson, \App\Models\Challenge>
     */
    public function liveLesson(): BelongsTo
    {
        return $this->belongsTo(LiveLesson::class);
    }

    /**
     * Retrieves the questions associated to the model.
     *
     * @return HasMany<Question>
     */
    public function questions(): HasMany
    {
        return $this->hasMany(Question::class);
    }

    /**
     * Retrieves all user challenges associated to the model.
     *
     * @return HasMany<UserChallenge>
     */
    public function userChallenges(): HasMany
    {
        return $this->hasMany(UserChallenge::class);
    }

    /**
     * Register the model's media collections.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('cover')
            ->useFallbackUrl('/img/challenge_cover.png')
            ->singleFile();

        $this->addMediaCollection('teacher_signature')
            ->singleFile();
    }
}
