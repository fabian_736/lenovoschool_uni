<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperGallery
 */
class Gallery extends Model implements HasMedia, HasPointTransactions
{
    use HasFactory, InteractsWithMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'gallery';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'title',
        'description',
        'points_for_downloading',
        'points_per_comment',
        'status',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $cast = [
        'points_for_downloading' => 'double',
        'points_per_comment' => 'double',
        'status' => 'bool',
    ];

    /**
     * Scope a query to only include active videos.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('status', true);
    }

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphMany<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphMany
    {
        return $this->morphMany(PointTransaction::class, 'transactionable');
    }

    /**
     * Gets the url for the preview file.
     *
     * @return string
     */
    public function getPreviewFileUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('preview');
    }

    /**
     * Gets the url for the model's file.
     *
     * @return string
     */
    public function getFileUrlAttribute(): string
    {
        return $this->getFirstMediaUrl('file');
    }

    /**
     * Register the model's media collection.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('preview')
            ->useFallbackUrl('/img/no_cover.png')
            ->singleFile();

        $this->addMediaCollection('file')
            ->useFallbackUrl('/img/no_cover.png')
            ->singleFile();
    }
}
