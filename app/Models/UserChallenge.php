<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use App\Enums\UserChallengeStatus;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

/**
 * @mixin IdeHelperUserChallenge
 */
class UserChallenge extends Model implements HasMedia, HasPointTransactions
{
    use HasFactory, InteractsWithMedia;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'challenge_id',
        'max_earned_points',
        'status',
    ];

    /**
     * Gets the model status.
     *
     * @return Attribute<string, never>
     */
    public function status(): Attribute
    {
        return Attribute::make(
            get: function ($value) {
                if ($this->challenge->is_draft) {
                    return UserChallengeStatus::unavailable->value;
                }

                return $value;
            },
        );
    }

    /**
     * Determines if the model can have more attempts.
     *
     * @return bool
     */
    public function canHaveMoreAttempts(): bool
    {
        $isInProgress = $this->status == UserChallengeStatus::inProgress->value;
        $hasAnActiveAttempt = $this->lastAttempt->finished_at === null;

        return $isInProgress && ! $hasAnActiveAttempt;
    }

    /**
     * Retrieves the user associated to the model.
     *
     * @return BelongsTo<\App\Models\User, \App\Models\UserChallenge>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Retrieves the challenge associated to the model.
     *
     * @return BelongsTo<\App\Models\Challenge, \App\Models\UserChallenge>
     */
    public function challenge(): BelongsTo
    {
        return $this->belongsTo(Challenge::class);
    }

    /**
     * Retrieves the model's attempts.
     *
     * @return HasMany<\App\Models\UserChallengeAttempt>
     */
    public function attempts(): HasMany
    {
        return $this->hasMany(UserChallengeAttempt::class);
    }

    /**
     * Retrieves the model's attempts.
     *
     * @return HasOne<\App\Models\UserChallengeAttempt>
     */
    public function lastAttempt(): HasOne
    {
        return $this->hasOne(UserChallengeAttempt::class)
            ->ofMany('attempt')
            ->withDefault([
                'earned_points' => 0,
                'attempt' => 0,
            ]);
    }

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphMany<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphMany
    {
        return $this->morphMany(PointTransaction::class, 'transactionable');
    }

    /**
     * Register the model's media collection.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('diploma')
            ->singleFile();
    }
}
