<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use App\Notifications\Auth\ResetPasswordNotification;
use App\Notifications\Auth\WelcomeNotification;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\WelcomeNotification\ReceivesWelcomeNotification;

/**
 * @property-read string $fullname
 * @mixin IdeHelperUser
 */
class User extends Authenticatable implements HasMedia, HasPointTransactions
{
    use HasApiTokens, HasFactory, InteractsWithMedia, Notifiable, ReceivesWelcomeNotification;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'name',
        'lastname',
        'document_type_id',
        'document_number',
        'cellphone_number',
        'email',
        'position',
        'company',
        'date_of_birth',
        'user_type',
        'total_redemption_points',
        'available_redemption_points',
        'active',
        'total_progress',
        'challenge_ranking_points',
        'live_lesson_ranking_points',
        'general_ranking_points',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'total_redemption_points' => 'int',
        'available_redemption_points' => 'int',
        'active' => 'bool',
        'total_progress' => 'int',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array<int, string>
     */
    protected $appends = ['fullname'];

    /**
     * Gets the user fullname.
     *
     * @return Attribute<string, never>
     */
    public function fullname(): Attribute
    {
        return Attribute::make(
            get: function ($value, $attributes) {
                return "{$attributes['name']} {$attributes['lastname']}";
            },
        );
    }

    /**
     * Scope a query to only include active users.
     *
     * @param  \Illuminate\Database\Eloquent\Builder<static>  $query
     * @return \Illuminate\Database\Eloquent\Builder<static>
     */
    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    /**
     * Route notifications for the mail channel.
     *
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return array<string>|string
     *
     * @codeCoverageIgnore
     */
    public function routeNotificationForMail($notification)
    {
        return [$this->email => $this->fullname];
    }

    /**
     * Sends a custom welcome notification instead of the default welcome notification
     *
     * @param  Carbon  $validUntil
     * @return void
     */
    public function sendWelcomeNotification(Carbon $validUntil)
    {
        $this->notify(new WelcomeNotification($validUntil));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * Retrieves the model's authentication logs.
     *
     * @return HasMany<\App\Models\AuthenticationLog>
     */
    public function authenticationLogs(): HasMany
    {
        return $this->hasMany(AuthenticationLog::class);
    }

    /**
     * Retrieves the badges awarded to the user.
     *
     * @return BelongsToMany<\App\Models\Badge>
     */
    public function badges(): BelongsToMany
    {
        return $this->belongsToMany(Badge::class, 'user_badges')->withTimestamps();
    }

    /**
     * Retrieves the user's comments in gallery.
     *
     * @return HasMany<\App\Models\Comment>
     */
    public function galleryComments(): HasMany
    {
        return $this->hasMany(Comment::class, 'author_id');
    }

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphOne<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphOne
    {
        return $this->morphOne(PointTransaction::class, 'transactionable');
    }

    /**
     * Retrieves the user's point transactions.
     *
     * @return HasMany<\App\Models\PointTransaction>
     */
    public function pointTransactions(): HasMany
    {
        return $this->hasMany(PointTransaction::class);
    }

    /**
     * Retrieves the user's reward redemptions.
     *
     * @return HasMany<\App\Models\RewardRedemption>
     */
    public function rewardRedemptions(): HasMany
    {
        return $this->hasMany(RewardRedemption::class);
    }

    /**
     * Retrieves the model's user challenges.
     *
     * @return HasMany<\App\Models\UserChallenge>
     */
    public function userChallenges(): HasMany
    {
        return $this->hasMany(UserChallenge::class);
    }

    /**
     * Retrieves the model's user live lessons.
     *
     * @return HasMany<\App\Models\UserLiveLesson>
     */
    public function userLiveLessons(): HasMany
    {
        return $this->hasMany(UserLiveLesson::class);
    }

    /**
     * Register the model's media collection.
     *
     * @return void
     */
    public function registerMediaCollections(): void
    {
        $this->addMediaCollection('avatar')
            ->useFallbackUrl('/img/user.png')
            ->singleFile();
    }
}
