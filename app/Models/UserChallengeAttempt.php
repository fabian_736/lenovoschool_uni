<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;

/**
 * @mixin IdeHelperUserChallengeAttempt
 */
class UserChallengeAttempt extends Model implements HasPointTransactions
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_challenge_id',
        'started_at',
        'finished_at',
        'earned_points',
        'attempt',
    ];

    /**
     * Retrieves the model's user challenge.
     *
     * @return BelongsTo<\App\Models\UserChallenge, \App\Models\UserChallengeAttempt>
     */
    public function userChallenge(): BelongsTo
    {
        return $this->belongsTo(UserChallenge::class);
    }

    /**
     * Retrieves the given responses for the model.
     *
     * @return BelongsToMany<\App\Models\QuestionOption>
     */
    public function attemptChoices(): BelongsToMany
    {
        return $this->belongsToMany(QuestionOption::class, 'user_challenge_attempt_choices');
    }

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphMany<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphMany
    {
        return $this->morphMany(PointTransaction::class, 'transactionable');
    }
}
