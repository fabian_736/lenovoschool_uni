<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperUserLiveLessonTrackingHistory
 */
class UserLiveLessonTrackingHistory extends Model
{
    use HasFactory;

    /**
     * The name of the "updated at" column.
     *
     * @var string|null
     */
    const UPDATED_AT = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_live_lesson_id',
        'interaction_type',
        'watched_time',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'watched_time' => 'double',
    ];

    /**
     * Retrieves the related user live lesson
     *
     * @return BelongsTo<\App\Models\UserLiveLesson, \App\Models\UserLiveLessonTrackingHistory>
     */
    public function userLiveLesson(): BelongsTo
    {
        return $this->belongsTo(UserLiveLesson::class);
    }
}
