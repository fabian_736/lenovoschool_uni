<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

/**
 * @mixin IdeHelperPointTransaction
 */
class PointTransaction extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'type',
        'user_id',
        'points',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $cast = [
        'points' => 'integer',
    ];

    /**
     * Retrieve the transactionable model associated to the transaction.
     *
     * @return MorphTo<\Illuminate\Database\Eloquent\Model, \App\Models\PointTransaction>
     */
    public function transactionable(): MorphTo
    {
        return $this->morphTo();
    }
}
