<?php

namespace App\Models;

use App\Contracts\HasPointTransactions;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphOne;

/**
 * @mixin IdeHelperComment
 */
class Comment extends Model implements HasPointTransactions
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'author_id',
        'gallery_id',
        'comment',
    ];

    /**
     * Retrieve the author associated to the model.
     *
     * @return BelongsTo<\App\Models\User, \App\Models\Comment>
     */
    public function author(): BelongsTo
    {
        return $this->belongsTo(User::class, 'author_id');
    }

    /**
     * Retrieve the gallery associated to the model.
     *
     * @return BelongsTo<\App\Models\Gallery, \App\Models\Comment>
     */
    public function gallery(): BelongsTo
    {
        return $this->belongsTo(Gallery::class);
    }

    /**
     * Retrieves the point transactions for the model.
     *
     * @return MorphOne<\App\Models\PointTransaction>
     */
    public function pointTransaction(): MorphOne
    {
        return $this->morphOne(PointTransaction::class, 'transactionable');
    }
}
