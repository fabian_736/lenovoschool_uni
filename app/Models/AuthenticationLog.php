<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @mixin IdeHelperAuthenticationLog
 */
class AuthenticationLog extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<string>
     */
    protected $fillable = [
        'user_id',
        'ip',
    ];

    /**
     * Retrieves the user associated to the model.
     *
     * @return BelongsTo<\App\Models\User, \App\Models\AuthenticationLog>
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
