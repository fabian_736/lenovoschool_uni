<?php

namespace App\Policies;

use App\Models\Challenge;
use App\Models\Question;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class QuestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user, Challenge $challenge)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden añadir preguntas a los desafíos publicados.');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, Question $question, Challenge $challenge)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden actualizar las preguntas de los desafíos publicados.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Question  $question
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, Question $question, Challenge $challenge)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden eliminar preguntas a los desafíos publicados.');
    }
}
