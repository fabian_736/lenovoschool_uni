<?php

namespace App\Policies;

use App\Models\Challenge;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class ChallengePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can publish the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function publish(User $user, Challenge $challenge)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('Los desafíos publicados no pueden ser republicados.');
    }

    /**
     * Determine whether the user can move to draft the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Challenge  $challenge
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function moveToDraft(User $user, Challenge $challenge)
    {
        return $challenge->is_published ? Response::allow() :
            Response::deny('El desafío ya está en estado de borrador.');
    }
}
