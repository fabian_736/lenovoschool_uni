<?php

namespace App\Policies;

use App\Enums\UserChallengeStatus;
use App\Models\User;
use App\Models\UserChallenge;
use App\Models\UserChallengeAttempt;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserChallengePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\UserChallenge  $userChallenge
     * @return bool
     */
    public function view(User $user, UserChallenge $userChallenge, ?UserChallengeAttempt $userChallengeAttempt = null): bool
    {
        return $userChallenge->user_id == $user->id &&
            $userChallenge->status == UserChallengeStatus::inProgress->value &&
            $userChallengeAttempt?->finished_at === null;
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\UserChallenge  $userChallenge
     * @return bool
     */
    public function finishChallenge(User $user, UserChallenge $userChallenge, UserChallengeAttempt $userChallengeAttempt): bool
    {
        return $userChallenge->user_id == $user->id &&
            $userChallenge->status == UserChallengeStatus::inProgress->value &&
            $userChallengeAttempt->finished_at === null;
    }
}
