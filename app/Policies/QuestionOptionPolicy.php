<?php

namespace App\Policies;

use App\Models\Challenge;
use App\Models\Question;
use App\Models\QuestionOption;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class QuestionOptionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Challenge  $challenge
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function create(User $user, Challenge $challenge, Question $question)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden añadir opciones de respuesta a las preguntas de los desafíos publicados.');
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\QuestionOption  $questionOption
     * @param  \App\Models\Challenge  $challenge
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function update(User $user, QuestionOption $questionOption, Challenge $challenge, Question $question)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden actualizar las opciones de respuesta a las preguntas de los desafíos publicados.');
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\QuestionOption  $questionOption
     * @param  \App\Models\Challenge  $challenge
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function delete(User $user, QuestionOption $questionOption, Challenge $challenge, Question $question)
    {
        return $challenge->is_draft ? Response::allow() :
            Response::deny('No se pueden eliminar opciones de respuesta a las preguntas de los desafíos publicados.');
    }
}
