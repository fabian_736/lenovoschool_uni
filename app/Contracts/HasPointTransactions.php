<?php

namespace App\Contracts;

interface HasPointTransactions
{
    /**
     * Retrieves the point transactions for the model.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne<\App\Models\PointTransaction>|\Illuminate\Database\Eloquent\Relations\MorphMany<\App\Models\PointTransaction>
     */
    public function pointTransaction();
}
