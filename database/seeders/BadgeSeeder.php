<?php

namespace Database\Seeders;

use App\Enums\BadgeTypes;
use App\Models\Badge;
use Illuminate\Database\Seeder;

class BadgeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment('local')) {
            $badgeTypes = BadgeTypes::cases();

            foreach ($badgeTypes as $badgeType) {
                Badge::factory()->set('type', $badgeType)->set('total', 1)->create();
            }
        }
    }
}
