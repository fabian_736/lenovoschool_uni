<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            DocumentTypeSeeder::class,
            DepartamentSeeder::class,
            UserSeeder::class,
            ChallengeSeeder::class,
            RewardSeeder::class,
            BadgeSeeder::class,
        ]);
    }
}
