<?php

namespace Database\Seeders;

use App\Models\Challenge;
use App\Models\LiveLesson;
use App\Models\Question;
use App\Models\QuestionOption;
use Illuminate\Database\Seeder;

class ChallengeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment(['local'])) {
            $liveLesson = LiveLesson::factory()->set('scheduled_date', now()->toDateString())->create();
            $challenge = Challenge::factory()
                ->for($liveLesson)
                ->published()
                ->create();

            for ($index = 0; $index < 3; $index++) {
                $question = Question::factory()->for($challenge)->create();

                for ($i = 0; $i < 3; $i++) {
                    QuestionOption::factory()->for($question)->set('is_correct', $i == 0)->create();
                }
            }
        }
    }
}
