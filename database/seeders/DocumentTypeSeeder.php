<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $now = now();

            DocumentType::insert([
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'DNI'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'CE'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Pasaporte'],
            ]);

    }
}
