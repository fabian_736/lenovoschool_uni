<?php

namespace Database\Seeders;

use App\Models\Departament;
use Illuminate\Database\Seeder;

class DepartamentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

            $now = now();

            Departament::insert([
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Amazonas'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Áncash'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Apurímac'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Arequipa'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Ayacucho'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Cajamarca'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Callao'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Cusco'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Huancavelica'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Huánuco'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Ica'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Junín'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'La Libertad'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Lambayeque'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Lima'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Loreto'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Madre de Dios'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Moquegua'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Pasco'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Piura'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Puno'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'San Martín'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Tacna'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Tumbes'],
                ['created_at' => $now, 'updated_at' => $now, 'name' => 'Ucayali'],
            ]);

    }
}
