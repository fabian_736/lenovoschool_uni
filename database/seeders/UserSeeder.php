<?php

namespace Database\Seeders;

use App\Enums\UserTypes;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (app()->environment(['local'])) {
            $password = Hash::make('1234');
            $points = 20;

            User::factory()->create([
                'email' => 'root@test.com',
                'password' => $password,
                'user_type' => UserTypes::superadmin->value,
            ]);

            User::factory()->create([
                'email' => 'admin@test.com',
                'password' => $password,
                'user_type' => UserTypes::administrator->value,
            ]);

            User::factory()->create([
                'email' => 'student@test.com',
                'password' => $password,
                'user_type' => UserTypes::student->value,
                'total_redemption_points' => $points,
                'available_redemption_points' => $points,
            ]);
        }

        if (app()->environment(['production'])) {
            User::create([
                'name' => 'People',
                'lastname' => 'Marketing',
                'position' => 'Súper administrador',
                'company' => 'People Marketing',
                'email' => 'root@peoplemarketing.com',
                'password' => Hash::make('Peopleroot1234'),
                'user_type' => UserTypes::superadmin->value,
                'active' => 1,
            ]);
        }
    }
}
