<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('challenges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('live_lesson_id')
                ->unique()
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->string('name');
            $table->string('teacher_name');
            $table->enum('status', ['Draft', 'Published'])->default('Draft');
            $table->timestamp('published_at')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('challenge_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->enum('type', ['unique', 'multiple']);
            $table->string('label');
            $table->integer('points');
            $table->timestamps();
        });

        Schema::create('question_options', function (Blueprint $table) {
            $table->id();
            $table->foreignId('question_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->string('label');
            $table->boolean('is_correct');
            $table->timestamps();
        });

        Schema::create('user_challenges', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->foreignId('challenge_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->unsignedBigInteger('max_earned_points')->default(0);
            $table->enum('status', [
                'In progress',
                'Finished',
                'Max attempts reached',
            ])
                ->default('In progress');
            $table->timestamps();
        });

        Schema::create('user_challenge_attempts', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_challenge_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->timestamp('started_at');
            $table->timestamp('finished_at')->nullable();
            $table->bigInteger('earned_points')->nullable();
            $table->tinyInteger('attempt');
            $table->timestamps();
        });

        Schema::create('user_challenge_attempt_choices', function (Blueprint $table) {
            $table->foreignId('user_challenge_attempt_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
            $table->foreignId('question_option_id')
                ->constrained()
                ->restrictOnDelete()
                ->restrictOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('challenges');
        Schema::dropIfExists('questions');
        Schema::dropIfExists('question_options');
        Schema::dropIfExists('user_challenges');
        Schema::dropIfExists('user_challenge_responses');
    }
};
