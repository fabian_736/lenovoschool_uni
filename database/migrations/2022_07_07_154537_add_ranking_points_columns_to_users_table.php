<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->after('total_progress', function () use ($table) {
                $table->unsignedBigInteger('challenge_ranking_points')->default(0);
                $table->unsignedBigInteger('live_lesson_ranking_points')->default(0);
                $table->unsignedBigInteger('general_ranking_points')->default(0);
            });
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'challenge_ranking_points',
                'live_lesson_ranking_points',
                'general_ranking_points',
            ]);
        });
    }
};
