<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('departaments', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->foreignId('receiver_departament_id')->after('receiver_email')->constrained('departaments')->restrictOnDelete()->restrictOnUpdate();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->dropForeign(['receiver_departament_id']);
        });

        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->dropColumn('receiver_departament_id');
        });

        Schema::dropIfExists('departaments');
    }
};
