<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_types', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->timestamps();
        });

        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->foreignId('receiver_document_type_id')->nullable()->after('receiver_name')->constrained('document_types')->restrictOnDelete()->restrictOnUpdate();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->after('lastname', function () use ($table) {
                $table->foreignId('document_type_id')->nullable()->constrained('document_types')->restrictOnDelete()->restrictOnUpdate();
                $table->string('document_number')->nullable();
            });

            $table->unique(['document_type_id', 'document_number']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['document_type_id']);
            $table->dropUnique(['document_type_id', 'document_number']);
        });

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(['document_type_id', 'document_number']);
        });

        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->dropForeign(['receiver_document_type_id']);
        });

        Schema::table('reward_redemptions', function (Blueprint $table) {
            $table->dropColumn('receiver_document_type_id');
        });

        Schema::dropIfExists('document_types');
    }
};
