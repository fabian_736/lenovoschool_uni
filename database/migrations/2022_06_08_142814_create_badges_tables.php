<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('badges', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->text('description');
            $table->enum('type', [
                'TOTAL_ACCUMULATED_POINTS',
                'TOTAL_COMMENTED_GALLERY_VIDEOS',
                'TOTAL_FINISHED_CHALLENGES',
                'TOTAL_GALLERY_COMMENTS',
                'TOTAL_GALLERY_DOWNLOADS',
                'TOTAL_LOGINS',
                'TOTAL_REDEEMED_POINTS',
                'TOTAL_REWARD_REDEMPTIONS',
                'TOTAL_VIEWED_LIVE_LESSONS',
            ]);
            $table->unsignedMediumInteger('total');
            $table->boolean('status');
            $table->timestamps();
        });

        Schema::create('user_badges', function (Blueprint $table) {
            $table->foreignId('user_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('badge_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('badges');
        Schema::dropIfExists('user_badges');
    }
};
