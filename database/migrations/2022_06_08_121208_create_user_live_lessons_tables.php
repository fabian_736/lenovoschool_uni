<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_live_lessons', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('live_lesson_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->boolean('watched')->default(false);
            $table->string('watched_time')->nullable();
            $table->timestamps();

            $table->unique(['user_id', 'live_lesson_id']);
        });

        Schema::create('user_live_lesson_tracking_histories', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_live_lesson_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->string('interaction_type');
            $table->string('watched_time');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_live_lessons');
        Schema::dropIfExists('user_live_lesson_tracking_histories');
    }
};
