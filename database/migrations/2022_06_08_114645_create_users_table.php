<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Laravel\Fortify\Fortify;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('lastname');
            $table->string('cellphone_number')->nullable();
            $table->string('email')->unique();
            $table->string('position')->nullable();
            $table->string('company')->nullable();

            $table->bigInteger('total_points')->default(0);
            $table->bigInteger('available_points')->default(0);
            $table->tinyInteger(column: 'total_progress', unsigned: true)->default(0)->comment('Progreso total de clases en vivo y desafíos');

            $table->enum('user_type', ['superadmin', 'administrator', 'student']);
            $table->boolean('active')->default(0)->comment('Activo: 1; Inactivo: 0');
            $table->timestamp('email_verified_at')->nullable();
            $table->dateTime('tutorial_finished_at')->nullable();
            $table->timestamp('welcome_valid_until')->nullable();

            $table->string('password')->nullable();
            $table->rememberToken();
            $table->text('two_factor_secret')->nullable();
            $table->text('two_factor_recovery_codes')->nullable();

            if (Fortify::confirmsTwoFactorAuthentication()) {
                $table->timestamp('two_factor_confirmed_at')
                    ->nullable();
            }

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
