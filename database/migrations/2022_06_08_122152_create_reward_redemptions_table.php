<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_redemptions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->foreignId('reward_id')->constrained()->restrictOnDelete()->restrictOnUpdate();
            $table->string('receiver_name');
            $table->string('receiver_document_number');
            $table->string('receiver_email');
            $table->string('receiver_address');
            $table->string('receiver_phone');
            $table->enum('status', ['pending', 'sent', 'delivered'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reward_redemptions');
    }
};
