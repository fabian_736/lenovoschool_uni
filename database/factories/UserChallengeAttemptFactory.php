<?php

namespace Database\Factories;

use App\Models\UserChallenge;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserChallengeAttempt>
 */
class UserChallengeAttemptFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_challenge_id' => UserChallenge::factory(),
            'started_at' => now(),
            'finished_at' => null,
            'earned_points' => 0,
            'attempt' => $this->faker->numerify(),
        ];
    }

    /**
     * Indicate that the model's should be finished.
     *
     * @return static
     */
    public function finished()
    {
        return $this->state(function (array $attributes) {
            return [
                'finished_at' => now(),
            ];
        });
    }
}
