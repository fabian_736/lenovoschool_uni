<?php

namespace Database\Factories;

use App\Enums\ChallengeQuestionTypes;
use App\Models\Challenge;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Question>
 */
class QuestionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'challenge_id' => Challenge::factory(),
            'type' => ChallengeQuestionTypes::unique->value,
            'label' => $this->faker->text(100),
            'points' => $this->faker->randomNumber(3),
        ];
    }

    /**
     * Indicate that the model's type should be "multiple".
     *
     * @return static
     */
    public function typeMultiple()
    {
        return $this->state(function (array $attributes) {
            return [
                'type' => ChallengeQuestionTypes::multiple->value,
            ];
        });
    }
}
