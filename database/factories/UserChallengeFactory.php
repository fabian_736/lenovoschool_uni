<?php

namespace Database\Factories;

use App\Enums\UserChallengeStatus;
use App\Models\Challenge;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserChallenge>
 */
class UserChallengeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'challenge_id' => Challenge::factory()->published(),
            'max_earned_points' => 0,
            'status' => UserChallengeStatus::inProgress->value,
        ];
    }

    /**
     * Indicate that the model's should be finished.
     *
     * @return static
     */
    public function finished()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => UserChallengeStatus::finished->value,
            ];
        });
    }

    /**
     * Indicate that the model's should have max attempts reached.
     *
     * @return static
     */
    public function maxAttemptsReached()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => UserChallengeStatus::maxAttemptsReached->value,
            ];
        });
    }
}
