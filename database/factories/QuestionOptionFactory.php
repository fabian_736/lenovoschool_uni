<?php

namespace Database\Factories;

use App\Models\Question;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\QuestionOption>
 */
class QuestionOptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'question_id' => Question::factory(),
            'label' => function ($attributes) {
                if ($attributes['is_correct']) {
                    return 'Respuesta correcta';
                }

                return $this->faker->sentence();
            },
            'is_correct' => $this->faker->boolean(),
        ];
    }

    /**
     * Indicate that the model's "is_correct" column should be true.
     *
     * @return static
     */
    public function asCorrect()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_correct' => true,
            ];
        });
    }

    /**
     * Indicate that the model's "is_correct" column should be false.
     *
     * @return static
     */
    public function asIncorrect()
    {
        return $this->state(function (array $attributes) {
            return [
                'is_correct' => false,
            ];
        });
    }
}
