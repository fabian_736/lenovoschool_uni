<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\LiveLesson>
 */
class LiveLessonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->sentence(),
            'subtitle' => $this->faker->sentence(),
            'description' => $this->faker->text(),
            'teacher' => $this->faker->name(),
            'scheduled_date' => $this->faker->unique()->dateTimeBetween(endDate: '+30 years'),
            'scheduled_time' => now()->format('H:i:s'),
            'vimeo_live_event_id' => '2083897',
            'vimeo_video_id' => '706302451',
        ];
    }
}
