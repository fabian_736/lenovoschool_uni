<?php

namespace Database\Factories;

use App\Models\LiveLesson;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserLiveLesson>
 */
class UserLiveLessonFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'live_lesson_id' => LiveLesson::factory(),
            'watched' => $this->faker->boolean(),
            'watched_time' => $this->faker->randomFloat(2),
        ];
    }

    /**
     * Indicate that the model's should be watched.
     *
     * @return static
     */
    public function watched()
    {
        return $this->state(function (array $attributes) {
            return [
                'watched' => true,
            ];
        });
    }

    /**
     * Indicate that the model's should be unwatched.
     *
     * @return static
     */
    public function unwatched()
    {
        return $this->state(function (array $attributes) {
            return [
                'watched' => false,
            ];
        });
    }
}
