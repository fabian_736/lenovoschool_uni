<?php

namespace Database\Factories;

use App\Enums\BadgeTypes;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Badge>
 */
class BadgeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->realTextBetween(3, 100),
            'description' => $this->faker->realTextBetween(10, 200),
            'type' => $this->faker->randomElement(BadgeTypes::cases()),
            'total' => $this->faker->randomNumber(1),
            'status' => 1,
        ];
    }

    /**
     * Indicate that the model's tutorial should be incomplete.
     *
     * @return static
     */
    public function inactive()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => 0,
            ];
        });
    }
}
