<?php

namespace Database\Factories;

use App\Enums\ChallengeStatus;
use App\Models\LiveLesson;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Challenge>
 */
class ChallengeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'live_lesson_id' => LiveLesson::factory(),
            'name' => $this->faker->text(50),
            'teacher_name' => $this->faker->name(),
            'status' => ChallengeStatus::draft->value,
        ];
    }

    /**
     * Indicate that the model should be published.
     *
     * @return static
     */
    public function published()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => ChallengeStatus::published->value,
                'published_at' => now(),
            ];
        });
    }
}
