<?php

namespace Database\Factories;

use App\Enums\UserTypes;
use App\Models\DocumentType;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'lastname' => $this->faker->lastName(),
            'document_type_id' => DocumentType::factory(),
            'document_number' => $this->faker->regexify('[1-9]\d{7,15}'),
            'email' => $this->faker->unique()->safeEmail(),
            'email_verified_at' => now(),
            'company' => $this->faker->company(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'tutorial_finished_at' => now(),
            'user_type' => UserTypes::administrator->value,
            'position' => $this->faker->jobTitle(),
            'active' => 1,
            'total_redemption_points' => 1000,
            'available_redemption_points' => 1000,
            'challenge_ranking_points' => $this->faker->randomNumber(3),
            'live_lesson_ranking_points' => $this->faker->randomNumber(3),
            'general_ranking_points' => function ($attributes) {
                return $attributes['challenge_ranking_points'] + $attributes['live_lesson_ranking_points'];
            },
        ];
    }

    /**
     * Indicate that the model's tutorial should be incomplete.
     *
     * @return static
     */
    public function withoutTutorial()
    {
        return $this->state(function (array $attributes) {
            return [
                'tutorial_finished_at' => null,
            ];
        });
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }

    /**
     * Indicate that the model's should be inactive.
     *
     * @return static
     */
    public function inactive()
    {
        return $this->state(function (array $attributes) {
            return [
                'active' => 0,
            ];
        });
    }

    /**
     * Indicate that the model's should be super administrator.
     *
     * @return static
     */
    public function asSuperAdmin()
    {
        return $this->state(function (array $attributes) {
            return [
                'user_type' => UserTypes::administrator->value,
            ];
        });
    }

    /**
     * Indicate that the model's should be student.
     *
     * @return static
     */
    public function asStudent()
    {
        return $this->state(function (array $attributes) {
            return [
                'user_type' => UserTypes::student->value,
            ];
        });
    }

    /**
     * Indicate that the model should not have points.
     *
     * @return static
     */
    public function withoutPoints()
    {
        return $this->state(function (array $attributes) {
            return [
                'total_redemption_points' => 0,
                'available_redemption_points' => 0,
            ];
        });
    }
}
