<?php

namespace Database\Factories;

use App\Models\UserLiveLesson;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\UserLiveLessonTrackingHistory>
 */
class UserLiveLessonTrackingHistoryFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_live_lesson_id' => UserLiveLesson::factory(),
            'interaction_type' => $this->faker->randomElement(['play', 'pause', 'seeked', 'ended', 'timeupdate']),
            'watched_time' => $this->faker->randomFloat(2),
            'created_at' => now(),
        ];
    }
}
