<?php

namespace Database\Factories;

use App\Enums\RewardRedemptionsStatus;
use App\Models\Departament;
use App\Models\DocumentType;
use App\Models\Reward;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\RewardRedemption>
 */
class RewardRedemptionFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'reward_id' => Reward::factory(),
            'receiver_name' => $this->faker->name(),
            'receiver_document_type_id' => DocumentType::factory(),
            'receiver_document_number' => $this->faker->randomNumber(),
            'receiver_email' => $this->faker->safeEmail(),
            'receiver_departament_id' => Departament::factory(),
            'receiver_address' => $this->faker->address(),
            'receiver_phone' => $this->faker->phoneNumber(),
            'status' => RewardRedemptionsStatus::pending,
        ];
    }

    /**
     * Indicate that the model's should be sent.
     *
     * @return static
     */
    public function sent()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => RewardRedemptionsStatus::sent,
            ];
        });
    }

    /**
     * Indicate that the model's should be delivered.
     *
     * @return static
     */
    public function delivered()
    {
        return $this->state(function (array $attributes) {
            return [
                'status' => RewardRedemptionsStatus::delivered,
            ];
        });
    }
}
