const mix = require("laravel-mix");
const path = require("path");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean', fluent API for defining some Webpack build steps
 | for your Laravel applications. By default', we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.styles([
    "resources/assets/css/material-dashboard.css",
    "resources/assets/css/modify.css",
], "public/css/app.css");

mix.scripts([
    "resources/assets/js/core/jquery.min.js",
    "resources/assets/js/core/popper.min.js",
    "resources/assets/js/core/bootstrap-material-design.min.js",
    "resources/assets/js/plugins/perfect-scrollbar.jquery.min.js",
    "resources/assets/js/plugins/moment.min.js",
    "resources/assets/js/plugins/sweetalert2.js",
    "resources/assets/js/plugins/jquery.validate.min.js",
    "resources/assets/js/plugins/jquery.bootstrap-wizard.js",
    "resources/assets/js/plugins/bootstrap-selectpicker.js",
    "resources/assets/js/plugins/bootstrap-datetimepicker.min.js",
    "resources/assets/js/plugins/jquery.dataTables.min.js",
    "resources/assets/js/plugins/bootstrap-tagsinput.js",
    "resources/assets/js/plugins/jasny-bootstrap.min.js",
    "resources/assets/js/plugins/fullcalendar.min.js",
    "resources/assets/js/plugins/jquery-jvectormap.js",
    "resources/assets/js/plugins/nouislider.min.js",
    "resources/assets/js/plugins/arrive.min.js",
    "resources/assets/js/plugins/chartist.min.js",
    "resources/assets/js/plugins/bootstrap-notify.js",
    "resources/assets/js/material-dashboard.js",
    "resources/assets/demo/demo.js"
], "public/js/app.js")
    .js("resources/js/panel/Announcements", "js/panel/announcements.js")
    .js("resources/js/panel/Badges", "js/panel/badges.js")
    .js("resources/js/panel/rewards/tracking.js", "js/panel/rewards/tracking.js")
    .js("resources/js/panel/users", "js/panel/users.js")
    .js("resources/js/Portal/LiveLessons/live", "js/Portal/LiveLessons/live.js")
    .js("resources/js/Portal/LiveLessons/savedLessons", "js/Portal/LiveLessons/saved-lessons.js")
    .js("resources/js/Portal/Profile", "js/portal/profile.js")
    .js("resources/js/Portal/redemptions/index.js", "js/portal/redemptions/index.js")
    .js("resources/js/Portal/redemptions/redemption.js", "js/portal/redemptions/redemption.js");


// EL MIX QUE HABIA ANTES

mix.options({ runtimeChunkPath: "./js" });

mix.extract(["rxjs", "Player"], "js/Portal/LiveLessons/vendor.js")
    .extract("js/vendor.js");

mix.alias({
    "@axios": path.join(__dirname, "resources/js/axios.js"),
    "@jquery": path.join(__dirname, "resources/js/jquery.js")
});

if (mix.inProduction()) {
    mix.version();
} else {
    mix.sourceMaps();
}
